//
//  FB_api.h
//  iFriendBuuks
//
//  Created by Naruphon Sirimasrungsee on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KAL_NET_CONNECTION.h"
#import "KAL_RES_MANAGER.h"
typedef enum {
	FB_API_CMD_NONE = 0,
	FB_API_CMD_GRAPH = 1,
	
	FB_API_CMD_GRAPH_GETPROFILE = 11,
	FB_API_CMD_GRAPH_GETFRIENDS = 12,
	FB_API_CMD_GRAPH_GETALBUMS = 13,
	FB_API_CMD_GRAPH_GETPROFILE_PICTURE = 14,
	FB_API_CMD_GRAPH_GETPHOTOS = 15,
	FB_API_CMD_GRAPH_GETALBUM_PICTURE  = 16,
	FB_API_CMD_GRAPH_GETPICTURE = 17,

	FB_API_CMD_GRAPH_GETIMAGE = 101,
	
		FB_API_CMD_GRAPH_UPIMAGE = 201,
	
}FB_API_CMD;

@protocol FB_api_DELEGATE;
@interface FB_api : NSObject <KAL_NET_CONNECTION_DELEGATE>{
	KAL_NET_CONNECTION *__net_connection__;
		KAL_RES_MANAGER *__res_manager__;
		id<FB_api_DELEGATE> _delegate;
	FB_API_CMD _command;
	NSArray	*_cmdList;
	
	NSMutableArray *_requestQ;
	NSInteger _requestCount;
	NSString	*_userId;

	NSMutableArray *_addDBList;
}
@property (nonatomic,retain) id<FB_api_DELEGATE> _delegate;
@property  (assign) NSInteger _requestCount;

-(void)_initWithDelegate:(id<FB_api_DELEGATE>) delegate;
-(void)_setQue:(id)obj;

+(FB_api*)getInstanceWithDelegate:(id<FB_api_DELEGATE>) delegate;
-(NSMutableDictionary*)_getProfile:(NSString*)accToken ofUserID:(NSString*)user_id;
-(NSMutableDictionary*)_getFriends:(NSString*) accToken ofUserID:(NSString*)user_id;
-(NSMutableDictionary*)_getAlbums:(NSString*) accToken ofUserID:(NSString*)user_id;
-(NSMutableDictionary*)_getPhotos:(NSString*) accToken ofAlbumID:(NSString*)album_id;
-(UIImage*)_getProfilePicture:(NSString*) accToken ofUserID:(NSString*)user_id andRow:(NSInteger)row onView:(UIView*)oview;
-(UIImage*)_getAlbumPicture:(NSString*) accToken ofAlbumID:(NSString*)album_id andRow:(NSInteger)row onView:(UIView*)oview;
-(UIImage*)_getPictureFromUrl:(NSString*)url  andRow:(NSInteger)row onView:(UIView*)oview needRequest:(BOOL)request;

-(void)_upPicture:(UIImage*)img andMsg:(NSString*)msg toAlbumID:(NSString*)album_id withAccToken:(NSString*)accToken atIndex:(NSInteger)index;
@end

@protocol FB_api_DELEGATE
@required
-(void)fbApiNotAvailable;
@optional
-(void)fbProgressGetProfile:(CGFloat)prog;
-(void)fbProgressGetFriends:(CGFloat)prog;
-(void)fbProgressGetAlbums:(CGFloat)prog;
-(void)fbProgressGetPhotos:(CGFloat)prog;
-(void)fbProgressGetProfilePicture:(CGFloat)prog atRow:(NSInteger)row;
-(void)fbProgressGetAlbumPicture:(CGFloat)prog atRow:(NSInteger)row;
-(void)fbProgressGetPictureFromUrl:(CGFloat)prog atRow:(NSInteger)row;

-(void)fbResult:(NSMutableDictionary*)value ofGetProfile:(BOOL)status;
-(void)fbResult:(NSMutableDictionary*)value ofGetFriends:(BOOL)status;
-(void)fbResult:(NSMutableDictionary*)value ofGetAlbums:(BOOL)status;
-(void)fbResult:(NSMutableDictionary*)value ofGetPhotos:(BOOL)status;
-(void)fbResult:(UIImage*)value ofGetProfilePicture:(BOOL)status atRow:(NSInteger)row;
-(void)fbResult:(UIImage*)value ofGetAlbumPicture:(BOOL)status atRow:(NSInteger)row;
-(void)fbResult:(UIImage*)value ofGetPictureFromUrl:(BOOL)status atRow:(NSInteger)row;


-(void)fbProgressUploadPicture:(CGFloat)prog atIndex:(NSInteger)index;
-(void)fbResult:(NSMutableDictionary*)value ofUploadPicture:(BOOL)status;
@end

