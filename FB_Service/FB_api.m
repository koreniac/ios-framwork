//
//  FB_api.m
//  iFriendBuuks
//
//  Created by Naruphon Sirimasrungsee on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FB_api.h"

static FB_api *__self__ = nil;
@implementation FB_api
@synthesize _delegate;
@synthesize _requestCount;
-(void)_initWithDelegate:(id<FB_api_DELEGATE>) delegate{
	_delegate =delegate;
	_command = FB_API_CMD_NONE;
//	static NSString* kDialogBaseURL = @"https://m.facebook.com/dialog/";
//	static NSString* kGraphBaseURL = @"https://graph.facebook.com/";
//	static NSString* kRestserverBaseURL = @"https://api.facebook.com/method/";
	_cmdList = [[NSArray	 alloc] initWithObjects:
				@"",
				@"https://graph.facebook.com/",
				@"https://api.facebook.com/method/",
				nil
				];

	__res_manager__ = [KAL_RES_MANAGER getInstance];
	_requestQ = [[NSMutableArray alloc] init];
	_requestCount = 0;
}
-(void)_setQue:(id)obj{
	[_requestQ addObject:obj];
	++_requestCount;
}
+(FB_api*)getInstanceWithDelegate:(id<FB_api_DELEGATE>) delegate{
	if (!__self__) {
		if ((__self__ = [[FB_api alloc] init])) {
			[__self__ _initWithDelegate:delegate];
		}
	}[__self__ set_delegate:delegate];

	return __self__;
}

-(NSMutableDictionary*)_getProfile:(NSString*) accToken ofUserID:(NSString*)user_id{
	_command = FB_API_CMD_GRAPH_GETPROFILE;

	NSMutableString *_sumUrl = [[NSMutableString alloc] initWithString:@""];				
	[_sumUrl appendFormat:@"%@",[_cmdList objectAtIndex:FB_API_CMD_GRAPH]];
	[_sumUrl appendFormat:@"%@",user_id];

	
	loadresCOND *cond;
	NSMutableDictionary *result =[__res_manager__ loadResItemWithKey:_sumUrl andGetCondition:&cond];
	__net_connection__ = [KAL_NET_CONNECTION getInstance];													
	[__net_connection__ requestTo:_sumUrl withType:RESPTYPE_DIC byDelegate:__self__];//  andMode:XO_MODE_JSON];
	KAL_INDEXPATH *path = [KAL_NET_CONNECTION indexPathForRow:_command inSection:0 ofPiece:[__self__ _requestCount]];			
	TLog(@"_getProfile_accToken = %@",accToken);
	[__net_connection__ dependOnView:nil andPath:path cachingIs:NO andFBaccToken:accToken];	
	[__net_connection__ startRequest];
	[__self__ _setQue:_delegate];
	
	return result;
}
-(NSMutableDictionary*)_getFriends:(NSString*) accToken ofUserID:(NSString*)user_id{
	NSMutableDictionary *result = nil;
	_command = FB_API_CMD_GRAPH_GETFRIENDS;
	
	NSMutableString *_sumUrl = [[NSMutableString alloc] initWithString:@""];				
	[_sumUrl appendFormat:@"%@",[_cmdList objectAtIndex:FB_API_CMD_GRAPH]];
	[_sumUrl appendFormat:@"%@",user_id];
	[_sumUrl appendFormat:@"/%@",@"friends"];
	
	
	loadresCOND *cond;
	result = [__res_manager__ loadResItemWithKey:_sumUrl andGetCondition:&cond];
	__net_connection__ = [KAL_NET_CONNECTION getInstance];													
	[__net_connection__ requestTo:_sumUrl withType:RESPTYPE_DIC byDelegate:__self__];//  andMode:XO_MODE_JSON];
	KAL_INDEXPATH *path = [KAL_NET_CONNECTION indexPathForRow:_command inSection:0 ofPiece:[__self__ _requestCount]];			
	
	[__net_connection__ dependOnView:nil andPath:path cachingIs:NO andFBaccToken:accToken];	
	[__net_connection__ startRequest];
	[__self__ _setQue:_delegate];
	
	
	return result;
}
-(NSMutableDictionary*)_getAlbums:(NSString*) accToken ofUserID:(NSString*)user_id{
	NSMutableDictionary *result = nil;
	_command = FB_API_CMD_GRAPH_GETALBUMS;
	
	NSMutableString *_sumUrl = [[NSMutableString alloc] initWithString:@""];				
	[_sumUrl appendFormat:@"%@",[_cmdList objectAtIndex:FB_API_CMD_GRAPH]];
	[_sumUrl appendFormat:@"%@",user_id];
		[_sumUrl appendFormat:@"/%@",@"albums"];
	
	loadresCOND *cond;
	result = [__res_manager__ loadResItemWithKey:_sumUrl andGetCondition:&cond];
	__net_connection__ = [KAL_NET_CONNECTION getInstance];													
	[__net_connection__ requestTo:_sumUrl withType:RESPTYPE_DIC byDelegate:__self__];//  andMode:XO_MODE_JSON];
	KAL_INDEXPATH *path = [KAL_NET_CONNECTION indexPathForRow:_command inSection:0 ofPiece:[__self__ _requestCount]];			
	
	[__net_connection__ dependOnView:nil andPath:path cachingIs:NO andFBaccToken:accToken];	
	[__net_connection__ startRequest];
	[__self__ _setQue:_delegate];
	
	
	return result;
}
-(NSMutableDictionary*)_getPhotos:(NSString*) accToken ofAlbumID:(NSString*)album_id{
	NSMutableDictionary *result = nil;
	_command = FB_API_CMD_GRAPH_GETPHOTOS;
	
	NSMutableString *_sumUrl = [[NSMutableString alloc] initWithString:@""];				
	[_sumUrl appendFormat:@"%@",[_cmdList objectAtIndex:FB_API_CMD_GRAPH]];
	[_sumUrl appendFormat:@"%@",album_id];
	[_sumUrl appendFormat:@"/%@",@"photos"];
	
	loadresCOND *cond;
	result = [__res_manager__ loadResItemWithKey:_sumUrl andGetCondition:&cond];
	__net_connection__ = [KAL_NET_CONNECTION getInstance];													
	[__net_connection__ requestTo:_sumUrl withType:RESPTYPE_DIC byDelegate:__self__];//  andMode:XO_MODE_JSON];
	KAL_INDEXPATH *path = [KAL_NET_CONNECTION indexPathForRow:_command inSection:0 ofPiece:[__self__ _requestCount]];			
	
	[__net_connection__ dependOnView:nil andPath:path cachingIs:NO andFBaccToken:accToken];	
	[__net_connection__ startRequest];
	[__self__ _setQue:_delegate];
	
	
	return result;
}

-(UIImage*)_getProfilePicture:(NSString*) accToken ofUserID:(NSString*)user_id andRow:(NSInteger)row onView:(UIView*)oview{
	UIImage *result = nil;
	_command = FB_API_CMD_GRAPH_GETPROFILE_PICTURE;
	
	NSMutableString *_sumUrl = [[NSMutableString alloc] initWithString:@""];				
	[_sumUrl appendFormat:@"%@",[_cmdList objectAtIndex:FB_API_CMD_GRAPH]];
	[_sumUrl appendFormat:@"%@",user_id];
		[_sumUrl appendFormat:@"/%@",@"picture"];
	
	loadresCOND *cond;
	result = [__res_manager__ loadResItemWithKey:_sumUrl andGetCondition:&cond];
	__net_connection__ = [KAL_NET_CONNECTION getInstance];													
	[__net_connection__ requestTo:_sumUrl withType:RESPTYPE_IMG byDelegate:__self__];//  andMode:XO_MODE_JSON];
	KAL_INDEXPATH *path = [KAL_NET_CONNECTION indexPathForRow:_command inSection:row ofPiece:[__self__ _requestCount]];			
	
	[__net_connection__ dependOnView:nil andPath:path cachingIs:YES andFBaccToken:accToken];	
	[__net_connection__ startRequest];
	[__self__ _setQue:_delegate];
	
	
	return result;
}
-(UIImage*)_getAlbumPicture:(NSString*) accToken ofAlbumID:(NSString*)album_id andRow:(NSInteger)row onView:(UIView*)oview{
	UIImage *result = nil;
	_command = FB_API_CMD_GRAPH_GETALBUM_PICTURE;
	
	NSMutableString *_sumUrl = [[NSMutableString alloc] initWithString:@""];				
	[_sumUrl appendFormat:@"%@",[_cmdList objectAtIndex:FB_API_CMD_GRAPH]];
	[_sumUrl appendFormat:@"%@",album_id];
	[_sumUrl appendFormat:@"/%@",@"picture"];
	
	loadresCOND *cond;
	result = [__res_manager__ loadResItemWithKey:_sumUrl andGetCondition:&cond];
	__net_connection__ = [KAL_NET_CONNECTION getInstance];													
	[__net_connection__ requestTo:_sumUrl withType:RESPTYPE_IMG byDelegate:__self__];//  andMode:XO_MODE_JSON];
	KAL_INDEXPATH *path = [KAL_NET_CONNECTION indexPathForRow:_command inSection:row ofPiece:[__self__ _requestCount]];			
	
	[__net_connection__ dependOnView:nil andPath:path cachingIs:YES andFBaccToken:accToken];	
	[__net_connection__ startRequest];
	[__self__ _setQue:_delegate];
	
	
	return result;
}
-(UIImage*)_getPictureFromUrl:(NSString*)url  andRow:(NSInteger)row onView:(UIView*)oview needRequest:(BOOL)request{
    if (!url) {
        return nil;
    }
	UIImage *result = nil;
	_command = FB_API_CMD_GRAPH_GETPICTURE;
	
	NSMutableString *_sumUrl = [[NSMutableString alloc] initWithString:url];	

//	[_sumUrl appendFormat:@"%@",[_cmdList objectAtIndex:FB_API_CMD_GRAPH]];
//	[_sumUrl appendFormat:@"%@",user_id];
//	[_sumUrl appendFormat:@"/%@",@"picture"];
	
	loadresCOND *cond;
	result = [__res_manager__ loadResItemWithKey:_sumUrl andGetCondition:&cond];
	if (request) {
		__net_connection__ = [KAL_NET_CONNECTION getInstance];													
		[__net_connection__ requestTo:_sumUrl withType:RESPTYPE_IMG byDelegate:__self__];//  andMode:XO_MODE_JSON];
		KAL_INDEXPATH *path = [KAL_NET_CONNECTION indexPathForRow:_command inSection:row ofPiece:[__self__ _requestCount]];			
		
		[__net_connection__ dependOnView:nil andPath:path cachingIs:YES];
		[__net_connection__ startRequest];
		[__self__ _setQue:_delegate];
	}
	
	
	
	return result;
}
#pragma mark - Upload
-(void)_upPicture:(UIImage*)img andMsg:(NSString*)msg toAlbumID:(NSString*)album_id withAccToken:(NSString*)accToken atIndex:(NSInteger)index{
	_command = FB_API_CMD_GRAPH_UPIMAGE;
	

	NSMutableString *_sumUrl = [[NSMutableString alloc] initWithString:@""];				
	[_sumUrl appendFormat:@"%@",[_cmdList objectAtIndex:FB_API_CMD_GRAPH]];
	[_sumUrl appendFormat:@"%@",album_id];
	[_sumUrl appendFormat:@"/%@",@"photos"];
	


	__net_connection__ = [KAL_NET_CONNECTION getInstance];				
	[__net_connection__ postTo:_sumUrl  withType:PTYPE_MULTI byDelegate:__self__ andAccToken:accToken];
	NSString *message = msg;
	if (!msg||[msg isEqualToString:@"(null)"]) {
		message = @"";
	}
	[__net_connection__ postMultipath:@"message" andValue:message];
	[__net_connection__ postMultipath:@"source" andValue:img];
//	[__net_connection__ requestTo:_sumUrl withType:RESPTYPE_IMG byDelegate:__self__];//  andMode:XO_MODE_JSON];
	KAL_INDEXPATH *path = [KAL_NET_CONNECTION indexPathForRow:_command inSection:index ofPiece:[__self__ _requestCount]];			
	
	[__net_connection__ dependOnView:nil andPath:path cachingIs:NO andFBaccToken:accToken];	
	[__net_connection__ startRequest];
	[__self__ _setQue:_delegate];
	
}
#pragma mark -
#pragma mark KAL_NET_CONNECTION_DELEGATE
-(void)networkProgress:(CGFloat)progress withType:(respondTYPE) type andInfo:(KAL_GROUP_CONTAINER*) info{
//	FB_API_CMD_GRAPH_GETPROFILE = 11,
//	FB_API_CMD_GRAPH_GETFRIENDS = 12,
//	FB_API_CMD_GRAPH_GETALBUMS = 13,
//	FB_API_CMD_GRAPH_GETPROFILE_PICTURE = 14,
//	
//	FB_API_CMD_GRAPH_GETIMAGE = 101,
	KAL_INDEXPATH *KALPath = [info path];
	id __delegate = [_requestQ objectAtIndex:KALPath->piece];
	if (type==RESPTYPE_DIC) {TLog(@"__________ 1");

		if(KALPath->row==FB_API_CMD_GRAPH_GETPROFILE){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE");
			if ([__delegate respondsToSelector:@selector(fbProgressGetProfile:)]) {
				[__delegate fbProgressGetProfile:progress];
			}
			
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETFRIENDS){TLog(@"__________ FB_API_CMD_GRAPH_GETFRIENDS");
			if ([__delegate respondsToSelector:@selector(fbProgressGetFriends:)]) {
			[__delegate fbProgressGetFriends:progress];
			}
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETALBUMS){TLog(@"__________ FB_API_CMD_GRAPH_GETALBUMS");
			if ([__delegate respondsToSelector:@selector(fbProgressGetAlbums:)]) {
			[__delegate fbProgressGetAlbums:progress];
			}
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETPHOTOS){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE_PICTURE");
			//-(void)fbResult:(NSMutableDictionary*)value ofGetPhotos:(BOOL)status;
			
			if ([__delegate respondsToSelector:@selector(fbProgressGetPhotos:)]) {
			[__delegate fbProgressGetPhotos:progress];
			}
		}	
	}else if(type==RESPTYPE_DATA){
		
	}else if (type==RESPTYPE_IMG) {
		if(KALPath->row==FB_API_CMD_GRAPH_GETPROFILE_PICTURE){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE_PICTURE");
			[__delegate fbProgressGetProfilePicture:progress atRow:KALPath->section];
		}else if (KALPath->row==FB_API_CMD_GRAPH_GETALBUM_PICTURE) {
			[__delegate fbProgressGetAlbumPicture:progress atRow:KALPath->section];
		}
		
		else if (KALPath->row==FB_API_CMD_GRAPH_GETPICTURE) {
			[__delegate fbProgressGetPictureFromUrl:progress atRow:KALPath->section];
		}
	}
//	-(void)fbProgressGetProfile:(CGFloat)prog;
//	-(void)fbProgressGetFriends:(CGFloat)prog;
//	-(void)fbProgressGetAlbums:(CGFloat)prog;
//	-(void)fbProgressGetPhotos:(CGFloat)prog;
//	-(void)fbProgressGetProfilePicture:(CGFloat)prog atRow:(NSInteger)row;
//	-(void)fbProgressGetAlbumPicture:(CGFloat)prog atRow:(NSInteger)row;
//	-(void)fbProgressGetPictureFromUrl:(CGFloat)prog atRow:(NSInteger)row;
//	if(KALPath->row==FB_API_CMD_GRAPH_GETPROFILE){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE");
//		[__delegate fbResult:obj  ofGetProfile:YES];
//	}else if(KALPath->row==FB_API_CMD_GRAPH_GETFRIENDS){TLog(@"__________ FB_API_CMD_GRAPH_GETFRIENDS");
//		[__delegate fbResult:obj  ofGetFriends:YES];
//	}else if(KALPath->row==FB_API_CMD_GRAPH_GETALBUMS){TLog(@"__________ FB_API_CMD_GRAPH_GETALBUMS");
//		[__delegate fbResult:obj  ofGetAlbums:YES];
//	}else if(KALPath->row==FB_API_CMD_GRAPH_GETPROFILE_PICTURE){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE_PICTURE");
//		[__delegate fbResult:obj  ofGetProfilePicture:YES];
//	}
	
}
-(void)networkNotAvailable:(id) obj withType:(respondTYPE) type andInfo:(KAL_GROUP_CONTAINER*) info{
#ifdef POP_LOADING
	K_MyAlertWithActivityIndicatorStop();
#endif
	//	K_MyAlertWithMessage(@">>>Network Not Available<<<");
	_command = FB_API_CMD_NONE;
	[_delegate fbApiNotAvailable];
}
-(void)respondDidFinishObject:(id) obj withType:(respondTYPE) type  andInfo:(KAL_GROUP_CONTAINER*) info{
	_command = FB_API_CMD_NONE;
	TLog(@"<FB_API Link Succeed is %@ >",[info url]);
	//	TLog(@"obj = %@",obj);
	KAL_INDEXPATH *KALPath = [info path];
	id __delegate = [_requestQ objectAtIndex:KALPath->piece];
	postTYPE postType = (postTYPE)[info postType];
	if (postType==PTYPE_MULTI) {
		if ([[obj valueForKey:@"code"] intValue]>=400) {
			[__self__ respondDidFailedObject:@">400" withType:type andInfo:info];
			return;
		}
//#ifdef POP_LOADING
//		K_MyAlertWithActivityIndicatorStop();
//#endif
//		if(KALPath->row==TO_PF_CMD_UPDATE_AVATAR){TLog(@"__________ TO_PF_CMD_UPDATE_AVATAR");
//			[__delegate profileResult:obj ofUpdateAvatar:YES];
//		}else if(KALPath->row==TO_PF_CMD_UPDATEPROFILE_BYID){TLog(@"__________ TO_PF_CMD_UPDATEPROFILE_BYID");
//			[__delegate profileResult:obj ofUpdateProfileByID:YES];
//		}	
//		
//		
//		return;
		
		if (KALPath->row==FB_API_CMD_GRAPH_UPIMAGE) {
			[__delegate fbResult:obj ofUploadPicture:YES];
		}
		
	}
	
	
	if (type==RESPTYPE_DIC) {TLog(@"__________ 1");
		if ([[obj valueForKey:@"code"] intValue]>=400) {
			[__self__ respondDidFailedObject:@">400" withType:type andInfo:info];
			return;
		}TLog(@"__________ 2");
#ifdef POP_LOADING
		K_MyAlertWithActivityIndicatorStop();
#endif
		if(KALPath->row==FB_API_CMD_GRAPH_GETPROFILE){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE");
			[__delegate fbResult:obj  ofGetProfile:YES];
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETFRIENDS){TLog(@"__________ FB_API_CMD_GRAPH_GETFRIENDS");
			[__delegate fbResult:obj  ofGetFriends:YES];
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETALBUMS){TLog(@"__________ FB_API_CMD_GRAPH_GETALBUMS");
			[__delegate fbResult:obj  ofGetAlbums:YES];
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETPHOTOS){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE_PICTURE");
//-(void)fbResult:(NSMutableDictionary*)value ofGetPhotos:(BOOL)status;
			[__delegate fbResult:obj  ofGetPhotos:YES];
		}	
	}else if(type==RESPTYPE_DATA){
		
	}else if (type==RESPTYPE_IMG) {
		if(KALPath->row==FB_API_CMD_GRAPH_GETPROFILE_PICTURE){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE_PICTURE");
			[__delegate fbResult:obj  ofGetProfilePicture:YES atRow:KALPath->section];
		}else if (KALPath->row==FB_API_CMD_GRAPH_GETALBUM_PICTURE) {
			[__delegate fbResult:obj ofGetAlbumPicture:YES atRow:KALPath->section];
		}
		
		else if (KALPath->row==FB_API_CMD_GRAPH_GETPICTURE) {
			[__delegate fbResult:obj ofGetPictureFromUrl:YES atRow:KALPath->section];
		}
	}
	
}
-(void)respondDidFailedObject:(id) obj withType:(respondTYPE) type  andInfo:(KAL_GROUP_CONTAINER*) info{
	_command = FB_API_CMD_NONE;
	NSLog(@"<FB_API Link Failed is %@ >",[info url]);
	KAL_INDEXPATH *KALPath = [info path];
	id __delegate = [_requestQ objectAtIndex:KALPath->piece];
#ifdef POP_LOADING
	K_MyAlertWithActivityIndicatorStop();
#endif
	postTYPE postType = (postTYPE)[info postType];
	if (postType==PTYPE_MULTI) {
		
		if (KALPath->row==FB_API_CMD_GRAPH_UPIMAGE) {
			[__delegate fbResult:obj ofUploadPicture:NO];
		}
		
	}
	
	if (type==RESPTYPE_DIC) {
		if(KALPath->row==FB_API_CMD_GRAPH_GETPROFILE){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE");
			[__delegate fbResult:obj  ofGetProfile:NO];
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETFRIENDS){TLog(@"__________ FB_API_CMD_GRAPH_GETFRIENDS");
			[__delegate fbResult:obj  ofGetFriends:NO];
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETALBUMS){TLog(@"__________ FB_API_CMD_GRAPH_GETALBUMS");
			[__delegate fbResult:obj  ofGetAlbums:NO];
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETPHOTOS){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE_PICTURE");
			[__delegate fbResult:obj  ofGetPhotos:NO];
		}
	}else if(type==RESPTYPE_DATA){
		
	}else if (type==RESPTYPE_IMG) {
		if(KALPath->row==FB_API_CMD_GRAPH_GETPROFILE_PICTURE){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE_PICTURE");
			[__delegate fbResult:obj  ofGetProfilePicture:NO atRow:KALPath->section];
		}else if (KALPath->row==FB_API_CMD_GRAPH_GETALBUM_PICTURE) {
			[__delegate fbResult:obj ofGetAlbumPicture:NO atRow:KALPath->section];
		}else if (KALPath->row==FB_API_CMD_GRAPH_GETPICTURE) {
			[__delegate fbResult:obj ofGetPictureFromUrl:NO atRow:KALPath->section];
		}	
	}		
}
-(void)respondDidCancelObject:(id) obj withType:(respondTYPE) type andInfo:(KAL_GROUP_CONTAINER*) info{
	_command = FB_API_CMD_NONE;
	NSLog(@"<FB_API Link Cancel is %@ >",[info url]);
#ifdef POP_LOADING
	K_MyAlertWithActivityIndicatorStop();
#endif
    KAL_INDEXPATH *KALPath = [info path];
	id __delegate = [_requestQ objectAtIndex:KALPath->piece];
#ifdef POP_LOADING
	K_MyAlertWithActivityIndicatorStop();
#endif
	postTYPE postType = (postTYPE)[info postType];
	if (postType==PTYPE_MULTI) {
		
		if (KALPath->row==FB_API_CMD_GRAPH_UPIMAGE) {
			[__delegate fbResult:obj ofUploadPicture:NO];
		}
		
	}
	
	if (type==RESPTYPE_DIC) {
		if(KALPath->row==FB_API_CMD_GRAPH_GETPROFILE){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE");
			[__delegate fbResult:obj  ofGetProfile:NO];
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETFRIENDS){TLog(@"__________ FB_API_CMD_GRAPH_GETFRIENDS");
			[__delegate fbResult:obj  ofGetFriends:NO];
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETALBUMS){TLog(@"__________ FB_API_CMD_GRAPH_GETALBUMS");
			[__delegate fbResult:obj  ofGetAlbums:NO];
		}else if(KALPath->row==FB_API_CMD_GRAPH_GETPHOTOS){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE_PICTURE");
			[__delegate fbResult:obj  ofGetPhotos:NO];
		}
	}else if(type==RESPTYPE_DATA){
		
	}else if (type==RESPTYPE_IMG) {
		if(KALPath->row==FB_API_CMD_GRAPH_GETPROFILE_PICTURE){TLog(@"__________ FB_API_CMD_GRAPH_GETPROFILE_PICTURE");
			[__delegate fbResult:obj  ofGetProfilePicture:NO atRow:KALPath->section];
		}else if (KALPath->row==FB_API_CMD_GRAPH_GETALBUM_PICTURE) {
			[__delegate fbResult:obj ofGetAlbumPicture:NO atRow:KALPath->section];
		}else if (KALPath->row==FB_API_CMD_GRAPH_GETPICTURE) {
			[__delegate fbResult:obj ofGetPictureFromUrl:NO atRow:KALPath->section];
		}	
	}		
}

@end
