/*
 *  KAL_Configure.h
 *  NetConn_1021
 *
 *  Created by Naruphon Sirimasrungsee on 11/3/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#import "KAL_GROUP_CONTAINER.h"
#import "KAL_NET_CONNECTION.h"
#import "KAL_RES_MANAGER.h"


//#define K_CACHE_L2
//#define KORECONN_PRINT
#define KORECONN_GZIP
//#define NEW_RES_MAPPING
#define NEW_TIME_TO_HASH
//____________________________________KAL_RES_MANAGER
#define K_REQUEST_NEXT_FEED (60*1)			//second
#define K_REQUEST_NEXT_IMG (86400*0)+(3600*12)+(60*0)+(1)    //(60*2)//			//second
//--------------------------------    Month           Day          Hour     Minute  Second
#define K_CLEAN_DISK (2592000*1)+(86400*1)+(3600*0)+(60*1)+(2)

#define K_CONCURRENT_Q_FEED 10
#define K_CONCURRENT_Q_IMG 5
#define K_CONCURRENT_Q_DATA 3
#define K_CONCURRENT_Q_POST_MULTI 3


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

////EDIT DATA STRUCTURE
//#define K_RES_FEED_RESPOND @"response"
//#define K_RES_FEED_RESPOND_D @"d_response"
//#define K_RES_FEED_HEADER @"header"
//#define K_RES_FEED_HEADER_D @"d_header"
//#define K_RES_FEED_INFO @"info"
//#define K_RES_FEED_INFO_D @"d_info"
//#define K_RES_FEED_BODY @"body"
//#define K_RES_FEED_BODY_A @"a_body"
//#define K_RES_FEED_ITEM @"item"
//#define K_RES_FEED_ITEM_D @"d_item"
////EDIT DATA STRUCTURE
//
//
//
#define T_RES_PACKAGE @"KAL_RES_PACK"
#ifdef NEW_RES_MAPPING
#define T_RES_MANAGE_MAPFILE @"_T_MAPFILE.sqlite"    //@"_T_MAPFILE.plist"
#else
#define T_RES_MANAGE_MAPFILE @"_T_MAPFILE.plist"    //@"_T_MAPFILE.plist"
#endif

#define K_RES_MANAGE_PK @"_fromUrl"
#define K_RES_MANAGE_NAME @"_fileName"
#define K_RES_MANAGE_CONTENT @"_fileContent"
#define K_RES_MANAGE_TYPE @"_fileType"
#define K_RES_MANAGE_FILEDATE @"_fileDate"
#define K_RES_MANAGE_SAVEDATE @"_saveDate"	
#define K_RES_MANAGE_CLEANDATE @"_cleanDate"
#define K_RES_MANAGE_FAVOR @"_favor"
#define K_RES_MANAGE_RESIDENT @"_resident"
//#define K_RES_MANAGE_VALUE @"_fileBinary"			//Can be any object


//____________________________________KAL_NET_CONNECTION
#define TNC_URL @"_URL_"					//TNC == KAL_NET_CONNECTION
#define TNC_TYPE @"_TYPE_"
#define TNC_DELEGATE @"_DELEGATE_"
#define TNC_VIEW @"_VIEW_"
#define TNC_PATH @"_PATH_"
//____________________________________KAL_GROUP_CONTAINER


//#define DEG_TO_RAD 0.0174532925
#ifdef DEBUG
#    define TLog(...) NSLog(__VA_ARGS__)
//#define TLog(__FORMAT__, ...) NSLog((@"%s [Line %d] " __FORMAT__), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#    define TLog(...)  /* */
//#define TLog(__FORMAT__, ...) TFLog((@"%s [Line %d] " __FORMAT__), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#endif