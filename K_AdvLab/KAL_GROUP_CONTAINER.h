//
//  KAL_GROUP_CONTAINER.h
//  NetConn_1021
//
//  Created by Naruphon Sirimasrungsee on 10/27/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KAL_NET_CONNECTION.h"
#import "KoreXtoO.h"
@interface KAL_GROUP_CONTAINER : NSObject {

	NSString *url;
	respondTYPE type;
	KAL_INDEXPATH *path;
	id<KAL_NET_CONNECTION_DELEGATE> delegate;
	UIView *view;
	id postObj;
	XOMODE xoMode;
	BOOL caching;
	BOOL resident;
	
	NSTimeInterval timeOut;
	BOOL filePath;
	
	postTYPE postType;
	KoreConn *postConn;
	BOOL permanentQ;
	NSString	*fbAccToken;
}
@property (nonatomic,retain)	NSString	*fbAccToken;
@property (nonatomic,retain)	NSString *url;
@property (assign) respondTYPE type;
@property (assign)KAL_INDEXPATH *path;
@property (nonatomic,retain)id<KAL_NET_CONNECTION_DELEGATE> delegate;
@property (nonatomic,retain)UIView *view;
@property (nonatomic,retain) 	id postObj;
@property (assign) XOMODE xoMode;
@property (assign) 	BOOL caching;
@property (assign)	BOOL resident;
@property (assign) 	NSTimeInterval timeOut;
@property (assign)	BOOL filePath;
@property (assign)		postTYPE postType;
@property (nonatomic,retain)	KoreConn *postConn;
@property (assign)	BOOL permanentQ;
@end
