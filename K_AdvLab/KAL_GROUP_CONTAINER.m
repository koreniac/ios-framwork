//
//  KAL_GROUP_CONTAINER.m
//  NetConn_1021
//
//  Created by Naruphon Sirimasrungsee on 10/27/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "KAL_GROUP_CONTAINER.h"


@implementation KAL_GROUP_CONTAINER
@synthesize url,type,path,delegate,view,postObj,xoMode,caching,resident,permanentQ;
@synthesize timeOut,filePath;
@synthesize postType,postConn;
@synthesize fbAccToken;
-(id)init{
	if (self=[super init]) {
		caching = YES;
		resident = NO;
		filePath = NO;
		timeOut = DEFAULT_TIMEOUT;
		postType = PTYPE_NONE;
		postConn = nil;
		permanentQ = NO;
	}
	return self;
}
-(void)dealloc{
    if(fbAccToken){
        [fbAccToken release];
        fbAccToken = nil;
    }
    if (url) {
        [url release];
        url = nil;
    }
    if (view) {
        [view release];
        view = nil;
    }
    if(postObj){
        [postObj release];
        postObj = nil;
    }

    if (postConn) {
        [postConn release];
        postConn = nil;
    }
    [super dealloc];

}
@end
