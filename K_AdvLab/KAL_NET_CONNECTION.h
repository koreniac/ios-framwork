//
//  KAL_NET_CONNECTION.h
//  NetConn_1021
//
//  Created by Naruphon Sirimasrungsee on 10/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
// test post http://widget2.truelife.com/upload.php
//------------------------------------------------------------------------------------------------------------------
//	HowTo:
//
////////////																											## Get Xml file To Object
//
//			KoreFileManage *fm = [[KoreFileManage alloc] init];
//			NSString *filepath = [fm _getPathOfDirectory:FP_APP ofFile:@"test.xml"]; 
//
//			[__net_connection__ getFile:filepath withType:RESPTYPE_DIC byDelegate:self];
//			KAL_INDEXPATH *path = [KAL_NET_CONNECTION indexPathForRow:0 inSection:0 ofPiece:0];
//			[__net_connection__ dependOnView:nil andPath:path];	
//			[__net_connection__ startRequest];
//
//


#import <Foundation/Foundation.h>
#import "KoreFeedEngine.h"
#import "KoreConn.h"
#import "KoreXtoO.h"


typedef struct
{
	NSUInteger section;
	NSUInteger row;
	NSUInteger piece;
} KAL_INDEXPATH;
typedef enum{
	RESPTYPE_NONE = 0,
	RESPTYPE_DIC = 1,
	RESPTYPE_IMG = 2,
	RESPTYPE_DATA = 3,
	
} respondTYPE;

typedef enum{
	QTYPE_NONE = 0,
	QTYPE_DIC = 1,
	QTYPE_IMG = 2,
	QTYPE_DATA = 3,
	QTYPE_POST_MULTI = 4,
	
} queueTYPE;

typedef enum{
	PTYPE_NONE = 0,
	PTYPE_TXT = 1,
	PTYPE_IMG = 2,
	PTYPE_MULTI = 3,		//MULTI PATH
	
} postTYPE;

@class KAL_RES_MANAGER;
@class KAL_GROUP_CONTAINER;
@protocol KAL_NET_CONNECTION_DELEGATE;
@interface KAL_NET_CONNECTION : NSObject<KoreConnDelegate,KoreFeedEngineDelegate,KoreXtoODelegate> {

	NSMutableArray *_poolDelegate;
	NSMutableDictionary *_poolConnection;
	
	NSString *_poolUrl;
//	NSMutableDictionary *_poolDataDic;
	KAL_GROUP_CONTAINER *_poolDataDic;
	KAL_RES_MANAGER *__res_manager__;

//	BOOL _qStart;
	NSMutableArray *_qFeed;			NSInteger _feedConcurrent;	NSInteger	_feedMaxNow;
	NSMutableArray *_qImage;		NSInteger _imgConcurrent;	NSInteger	_imgMaxNow;
	NSMutableArray *_qData;			NSInteger _dataConcurrent;	NSInteger	_dataMaxNow;
	NSMutableArray *_qPostMulti;		NSInteger _postMultiConcurrent;	NSInteger	_postMultiMaxNow;
	NSTimeInterval _nextStep;
	
		KoreXtoO *_xoParser;
	BOOL _cancelingQ;
    NSInteger _reservFeed;
        NSInteger _reservImage;
        NSInteger _reservData;
    NSInteger _reservMulit;
}
//@property (assign)	BOOL _qStart;
-(void)_init;
-(void)_qProc;
-(BOOL)_canNewRequestWithType:(queueTYPE) type;
-(void)_addRequestType:(queueTYPE)type withObject:(id) obj;
-(BOOL)_canRemoveRequestType:(queueTYPE) type withObject:(id) obj;
-(void)_forceRemoveRequestType:(queueTYPE) type withObject:(id) obj;
+(KAL_INDEXPATH*)indexPathForRow:(NSUInteger)row inSection:(NSUInteger)section ofPiece:(NSUInteger) piece;
+(id)getInstance;
+(void)clearInstance;
+(NSInteger)eightEncryptRow:(NSInteger)row andSection:(NSInteger)section andPiece:(NSInteger)piece;
+(void)eightDecrypt:(NSInteger)tag toRow:(NSInteger**)row andSection:(NSInteger**)section andPiece:(NSInteger**)piece;
+(NSInteger)sixteenEncryptRow:(NSInteger)row andSection:(NSInteger)section;
+(void)sixteenDecrypt:(NSInteger)tag toRow:(NSInteger**)row andSection:(NSInteger**)section;
//-(void)setDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate;
//GET
-(void)requestTo:(NSString*) url withType:(respondTYPE)type byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate;
-(void)requestTo:(NSString*) url withType:(respondTYPE)type byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate andMode:(XOMODE) mode;
-(void)requestTo:(NSString*) url withType:(respondTYPE)type byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate atTimeout:(NSTimeInterval) tOut;
-(void)requestTo:(NSString*) url withType:(respondTYPE)type byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate andMode:(XOMODE) mode atTimeout:(NSTimeInterval) tOut;
//POST
-(void)postTo:(NSString*) url withImage:(id) obj byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate;

-(void)postTo:(NSString*) url withType:(postTYPE)postType byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate;
-(void)postTo:(NSString*) url withType:(postTYPE)postType byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate andMode:(XOMODE)mode;
-(void)postTo:(NSString*) url withType:(postTYPE)postType byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate andAccToken:(NSString*)accToken;
-(void)postMultipath:(NSString*)key andValue:(id)value;


-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path;
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path cachingIs:(BOOL) caching;
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path residentIs:(BOOL) resident;
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path permanentQIs:(BOOL) permanentQ;
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path cachingIs:(BOOL) caching permanentQIs:(BOOL) permanentQ;
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path residentIs:(BOOL) resident permanentQIs:(BOOL) permanentQ;
-(KoreConn*)startRequest;
-(void)cleanQ;
-(void)cleanQreservByType:(NSInteger)feed andImage:(NSInteger)image andData:(NSInteger)data andMulti:(NSInteger)multi;

//FromFile
-(void)getFile:(NSString*)filePath withType:(respondTYPE)type byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate;

//for FB
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path residentIs:(BOOL) resident andFBaccToken:(NSString*)accToken;
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path cachingIs:(BOOL) caching andFBaccToken:(NSString*)accToken;
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path andFBaccToken:(NSString*)accToken;

@end

@protocol KAL_NET_CONNECTION_DELEGATE<NSObject>
@optional
-(NSDictionary*)configKey;
-(NSDictionary*)configValue;
-(NSDictionary*)configExKey;
-(void)respondDidFinishObject:(id) obj withType:(respondTYPE) type andInfo:(KAL_GROUP_CONTAINER*) info;
-(void)respondDidFailedObject:(id) obj withType:(respondTYPE) type andInfo:(KAL_GROUP_CONTAINER*) info;
-(void)respondDidCancelObject:(id) obj withType:(respondTYPE) type andInfo:(KAL_GROUP_CONTAINER*) info;
-(void)networkNotAvailable:(id) obj withType:(respondTYPE) type andInfo:(KAL_GROUP_CONTAINER*) info;
-(void)networkProgress:(CGFloat)progress withType:(respondTYPE) type andInfo:(KAL_GROUP_CONTAINER*) info;


@end




