
//
//  KAL_NET_CONNECTION.m
//  NetConn_1021
//
//  Created by Naruphon Sirimasrungsee on 10/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "KAL_NET_CONNECTION.h"
#import "KAL_RES_MANAGER.h"
#import "KAL_GROUP_CONTAINER.h"
static KAL_NET_CONNECTION *__self__ = nil;
static 	BOOL _qStart = NO;
@implementation KAL_NET_CONNECTION

-(void)_init{
	_poolDelegate = nil;
	_poolConnection = [[NSMutableDictionary alloc] init];
	
	_poolUrl = nil;
	_poolDataDic = nil;
	__res_manager__ = [KAL_RES_MANAGER getInstance];
	_qFeed = nil;
	_qImage = nil;
	_qData = nil;
	
	_feedConcurrent = K_CONCURRENT_Q_FEED;			_feedMaxNow = 0;
	_imgConcurrent =  K_CONCURRENT_Q_IMG;			_imgMaxNow =  0;
	_dataConcurrent = K_CONCURRENT_Q_DATA;			_dataMaxNow = 0;
	_postMultiConcurrent = K_CONCURRENT_Q_POST_MULTI;			_postMultiMaxNow = 0;
	
	_qStart = NO;
	_nextStep = 0;
	_cancelingQ = NO;
}
//V1.0.1
//-(void)_qProc{TLog(@"_qProc start");
//	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
//		//[NSThread sleepForTimeInterval:10];	
//    //while (_qStart) 
//	{
//		_qStart = NO;
//        // do some work
//		//if ([NSDate timeIntervalSinceReferenceDate]>_nextStep)
//		{
//			
//
//		
//			if (_qFeed&&[_qFeed count]>0&&_feedMaxNow<[_qFeed count]&&_feedMaxNow<=_feedConcurrent&&[self _canNewRequestWithType:QTYPE_DIC]) {
//				KoreFeedEngine *kfe = [_qFeed objectAtIndex:_feedMaxNow-1];
//				[kfe _startFeed];
//			}
//			if (_qImage&&[_qImage count]>0&&_imgMaxNow<[_qImage count]&&_imgMaxNow<=_imgConcurrent&&[self _canNewRequestWithType:QTYPE_IMG]) {
//				KoreConn *kc = [_qImage objectAtIndex:_imgMaxNow-1];
//				[kc startConInQueue];
//			}
//			if (_qData&&[_qData count]>0&&_dataMaxNow<[_qData count]&& _dataMaxNow<=_dataConcurrent&&[self _canNewRequestWithType:QTYPE_DATA]) {
//				KoreConn *kc = [_qData objectAtIndex:_dataMaxNow-1];
//				[kc startConInQueue];
//			}
//			_nextStep = [NSDate timeIntervalSinceReferenceDate]+100.0;
//		}
//		[NSThread sleepForTimeInterval:1.0];	
//    }
//	
//    [pool release];	
//}

//V1.0.0
-(void)_qProc{//NSLog(@">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	if (_cancelingQ) {
		return;
	}
//	NSLog(@"_qFeed = %@",_qFeed);
//		NSLog(@"++++++++[_qImage count] = %d",[_qImage count]);
//		NSLog(@"++++++++_imgMaxNow = %d",_imgMaxNow);
//		NSLog(@"_feedConcurrent = %d",_feedConcurrent);

	if (_qFeed&&[_qFeed count]>0&&_feedMaxNow<[_qFeed count]&&[self _canNewRequestWithType:QTYPE_DIC]) {//_feedMaxNow<=_feedConcurrent&&
		KoreFeedEngine *kfe = [_qFeed objectAtIndex:_feedMaxNow-1];
		[kfe _startFeed];
	}
	if (_qImage&&[_qImage count]>0&&_imgMaxNow<[_qImage count]&&[self _canNewRequestWithType:QTYPE_IMG]) {//_imgMaxNow<=_imgConcurrent&&
		KoreConn *kc = [_qImage objectAtIndex:_imgMaxNow-1];
		[kc startConInQueue];
	}
	if (_qData&&[_qData count]>0&&_dataMaxNow<[_qData count]&&[self _canNewRequestWithType:QTYPE_DATA]) {//_dataMaxNow<=_dataConcurrent&&
		KoreConn *kc = [_qData objectAtIndex:_dataMaxNow-1];
		[kc startConInQueue];
	}//QTYPE_POST_MULTI
	if (_qPostMulti&&[_qPostMulti count]>0&&_postMultiMaxNow<[_qPostMulti count]&& [self _canNewRequestWithType:QTYPE_POST_MULTI]) {//_postMultiMaxNow<=_postMultiConcurrent&&
		KoreConn *kc = [_qPostMulti objectAtIndex:_postMultiMaxNow-1];
		[kc startConInQueue];
	}//QTYPE_POST_MULTI
}
-(BOOL)_canNewRequestWithType:(queueTYPE) type{
	BOOL result = NO;
	if(type==QTYPE_DIC){
		if (_feedMaxNow<_feedConcurrent) {
			++_feedMaxNow;
			result = YES;
			//NSLog(@"********************************_feedMaxNow + = %d",_feedMaxNow);
		}
		
	}else if(type==QTYPE_IMG){
		if (_imgMaxNow<_imgConcurrent) {
			++_imgMaxNow;
			result = YES;
		}
	}else if(type==QTYPE_DATA){
		if (_dataMaxNow<_dataConcurrent) {
			++_dataMaxNow;
			result = YES;
		}
	}else if(type==QTYPE_POST_MULTI){
		if (_postMultiMaxNow<_postMultiConcurrent) {
			++_postMultiMaxNow;
			result = YES;
		}
	}
	
	
	return result;
}
-(void)_addRequestType:(queueTYPE)type withObject:(id) obj{
	if(type==QTYPE_DIC){
		if (!_qFeed) {
			_qFeed = [[NSMutableArray alloc] init];
		}
		[_qFeed addObject:obj];
		
	}else if(type==QTYPE_IMG){
		if (!_qImage) {
			_qImage = [[NSMutableArray alloc] init];
		}
		[_qImage addObject:obj];
	}else if(type==QTYPE_DATA){
		if (!_qData) {
			_qData = [[NSMutableArray alloc] init];
		}
		[_qData addObject:obj];
	}else if(type==QTYPE_POST_MULTI){
		if (!_qPostMulti) {
			_qPostMulti = [[NSMutableArray alloc] init];
		}
		[_qPostMulti addObject:obj];
	}
}
-(BOOL)_canRemoveRequestType:(queueTYPE) type withObject:(id) obj{//NSLog(@"********************************_canRemoveRequestType = %@",obj);
	BOOL result = NO;
	if(type==QTYPE_DIC){
		if ([_qFeed indexOfObject:obj]!=NSNotFound) {
			[_qFeed removeObject:obj];
			if(_feedMaxNow>0){
				--_feedMaxNow;
			}
			//NSLog(@"********************************_feedMaxNow = %d",_feedMaxNow);
			result = YES;
		}
		
	}else if(type==QTYPE_IMG){
		if ([_qImage indexOfObject:obj]!=NSNotFound) {
			[_qImage removeObject:obj];
			if(_imgMaxNow>0){
				--_imgMaxNow;
			}
//			NSLog(@"********************* QTYPE_IMG = %d",_imgMaxNow);
			result = YES;
		}
	}else if(type==QTYPE_DATA){TLog(@"********************* QTYPE_DATA = %d",_dataMaxNow);
		if ([_qData indexOfObject:obj]!=NSNotFound) {TLog(@"********************* QTYPE_DATA FOUND");
			[_qData removeObject:obj];
			if(_dataMaxNow>0){
				--_dataMaxNow;
			}
			result = YES;
		}
	}else if(type==QTYPE_POST_MULTI){
		if ([_qPostMulti indexOfObject:obj]!=NSNotFound) {
			[_qPostMulti removeObject:obj];
			if(_postMultiMaxNow>0){
				--_postMultiMaxNow;
			}
			result = YES;
		}
	}
	
	return result;								   
}
-(void)_forceRemoveRequestType:(queueTYPE) type withObject:(id) obj{NSLog(@"********************************_canRemoveRequestType = %@",obj);

	
	if(type==QTYPE_DIC){
		if ([_qFeed indexOfObject:obj]!=NSNotFound) {
			KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:obj];
			if (container) {
				if (![container permanentQ]) {

					if ([_qFeed indexOfObject:obj]<K_CONCURRENT_Q_FEED) {
						
						[[(KoreFeedEngine*)obj _mainConn] cancleConn];
					}else {
						[_poolConnection removeObjectForKey:obj];	
						[_qFeed removeObject:obj];
					}

				}
				
			}

		}
		
	}else if(type==QTYPE_IMG){
		if ([_qImage indexOfObject:obj]!=NSNotFound) {
			KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:obj];
			if (container) {
				if (![container permanentQ]) {
					
					if ([_qImage indexOfObject:obj]<K_CONCURRENT_Q_IMG) {
						
						[(KoreConn*)obj cancleConn];
					}else {
						[_poolConnection removeObjectForKey:obj];	
						[_qImage removeObject:obj];
					}
					
				}
				
			}
		}
	}else if(type==QTYPE_DATA){
		if ([_qData indexOfObject:obj]!=NSNotFound) {
			KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:obj];
			if (container) {
				if (![container permanentQ]) {
					
					if ([_qData indexOfObject:obj]<K_CONCURRENT_Q_DATA) {
						
						[(KoreConn*)obj cancleConn];
					}else {
						[_poolConnection removeObjectForKey:obj];	
						[_qData removeObject:obj];
					}
					
				}
				
			}
		}
	}else if(type==QTYPE_POST_MULTI){
		if ([_qPostMulti indexOfObject:obj]!=NSNotFound) {
			KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:obj];
			if (container) {
				if (![container permanentQ]) {
					
					if ([_qPostMulti indexOfObject:obj]<K_CONCURRENT_Q_POST_MULTI) {
						
						[(KoreConn*)obj cancleConn];
					}else {
						[_poolConnection removeObjectForKey:obj];	
						[_qPostMulti removeObject:obj];
					}
					
				}
				
			}
		}
	}
	
							   
}
+(KAL_INDEXPATH*)indexPathForRow:(NSUInteger)row inSection:(NSUInteger)section ofPiece:(NSUInteger) piece{
	KAL_INDEXPATH *result = malloc(sizeof(KAL_INDEXPATH));
		result->row = row;
		result->section = section;
		result->piece = piece;
	return result;
}
+(id)getInstance{
	if (!__self__) {
		__self__ = [[KAL_NET_CONNECTION alloc] init];
		[__self__ _init];
	}
	return __self__;
}
+(void)clearInstance{
	if (__self__) {
		_qStart = NO;
		[__self__ release];
	}

}
//-(void)setDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate{
//	
//}
-(void)requestTo:(NSString*) url withType:(respondTYPE)type byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate andMode:(XOMODE) mode atTimeout:(NSTimeInterval) tOut{
	_poolDataDic  = nil;
	_poolUrl = nil;
	//	_poolDataDic = [[NSMutableDictionary	alloc] init];
	//	[_poolDataDic setValue:[url copy] forKey:TNC_URL];
	//	[_poolDataDic setValue:[NSString stringWithFormat:@"%d",type] forKey:TNC_TYPE];
	//	[_poolDataDic setValue:delegate forKey:TNC_DELEGATE];
	TLog(@"requestTo   url = %@",url);
	_poolDataDic = [[KAL_GROUP_CONTAINER alloc] init];
	[_poolDataDic setUrl:[url copy]];
	[_poolDataDic setType:type];
	[_poolDataDic setDelegate:delegate];
	[_poolDataDic setXoMode:mode];
	[_poolDataDic setTimeOut:tOut];
	
}
-(void)requestTo:(NSString*) url withType:(respondTYPE)type byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate atTimeout:(NSTimeInterval) tOut{
	[self requestTo:url withType:type byDelegate:delegate andMode:XO_MODE_XML atTimeout:tOut];
}
-(void)requestTo:(NSString*) url withType:(respondTYPE)type byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate andMode:(XOMODE) mode{
	
	[self requestTo:url withType:type byDelegate:delegate andMode:mode atTimeout:DEFAULT_TIMEOUT];//DEFAULT_TIMEOUT
//	_poolDataDic  = nil;
//	_poolUrl = nil;
//	//	_poolDataDic = [[NSMutableDictionary	alloc] init];
//	//	[_poolDataDic setValue:[url copy] forKey:TNC_URL];
//	//	[_poolDataDic setValue:[NSString stringWithFormat:@"%d",type] forKey:TNC_TYPE];
//	//	[_poolDataDic setValue:delegate forKey:TNC_DELEGATE];
//	
//	_poolDataDic = [[KAL_GROUP_CONTAINER alloc] init];
//	[_poolDataDic setUrl:[url copy]];
//	[_poolDataDic setType:type];
//	[_poolDataDic setDelegate:delegate];
//	[_poolDataDic setXoMode:mode];
////	TLog(@"_poolDataDic = %@",_poolDataDic);
//	NSLog(@"[_poolDataDic xoMode] *= %d",[_poolDataDic xoMode]);
}
-(void)requestTo:(NSString*) url withType:(respondTYPE)type byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate{
	
	[self requestTo:url withType:type byDelegate:delegate andMode:XO_MODE_XML];
//	_poolDataDic  = nil;
//	_poolUrl = nil;
////	_poolDataDic = [[NSMutableDictionary	alloc] init];
////	[_poolDataDic setValue:[url copy] forKey:TNC_URL];
////	[_poolDataDic setValue:[NSString stringWithFormat:@"%d",type] forKey:TNC_TYPE];
////	[_poolDataDic setValue:delegate forKey:TNC_DELEGATE];
//	
//	_poolDataDic = [[KAL_GROUP_CONTAINER alloc] init];
//	[_poolDataDic setUrl:[url copy]];
//	[_poolDataDic setType:type];
//	[_poolDataDic setDelegate:delegate];
//	[_poolDataDic setXoMode:XO_MODE_XML];
////	TLog(@"_poolDataDic = %@",_poolDataDic);
	
}
#pragma mark -------------------------------- Post +
-(void)postTo:(NSString*) url withImage:(id) obj byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate{
	_poolDataDic  = nil;
	_poolUrl = nil;
	//	_poolDataDic = [[NSMutableDictionary	alloc] init];
	//	[_poolDataDic setValue:[url copy] forKey:TNC_URL];
	//	[_poolDataDic setValue:[NSString stringWithFormat:@"%d",type] forKey:TNC_TYPE];
	//	[_poolDataDic setValue:delegate forKey:TNC_DELEGATE];
	
	_poolDataDic = [[KAL_GROUP_CONTAINER alloc] init];
	[_poolDataDic setUrl:[url copy]];
	[_poolDataDic setPostObj:obj];
	[_poolDataDic setPostType:PTYPE_IMG];
	[_poolDataDic setDelegate:delegate];
	TLog(@"postTo Image _poolDataDic = %@",_poolDataDic);
}
-(void)postTo:(NSString*) url withType:(postTYPE)postType byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate andMode:(XOMODE)mode{
	_poolDataDic  = nil;
	_poolUrl = nil;
	
	
	_poolDataDic = [[KAL_GROUP_CONTAINER alloc] init];
	[_poolDataDic setXoMode:mode];
	[_poolDataDic setUrl:[url copy]];
	[_poolDataDic setPostType:postType];
	[_poolDataDic setDelegate:delegate];
    [_poolDataDic setType:RESPTYPE_DIC];
	KoreConn *_mainConn = [[KoreConn alloc] initCon:self iden:@"KoreConn_PostMultipath"];
	[_mainConn setUrl:[[NSString alloc] initWithFormat:@"%@",url]];
	
	[_mainConn setMode:POST];
	
	[_mainConn setTimeOut:[_poolDataDic timeOut]];
	[_poolDataDic setPostConn:_mainConn];	
	

}
-(void)postTo:(NSString*) url withType:(postTYPE)postType byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate{
	_poolDataDic  = nil;
	_poolUrl = nil;

	
	_poolDataDic = [[KAL_GROUP_CONTAINER alloc] init];
	[_poolDataDic setUrl:[url copy]];
	[_poolDataDic setPostType:postType];
	[_poolDataDic setDelegate:delegate];
        [_poolDataDic setType:RESPTYPE_DIC];
	KoreConn *_mainConn = [[KoreConn alloc] initCon:self iden:@"KoreConn_PostMultipath"];
	[_mainConn setUrl:[[NSString alloc] initWithFormat:@"%@",url]];
	
	[_mainConn setMode:POST];
	
	[_mainConn setTimeOut:[_poolDataDic timeOut]];
	[_poolDataDic setPostConn:_mainConn];
	
}
-(void)postTo:(NSString*) url withType:(postTYPE)postType byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate andAccToken:(NSString*)accToken{
    _poolDataDic  = nil;
	_poolUrl = nil;
	
	
	_poolDataDic = [[KAL_GROUP_CONTAINER alloc] init];
    [_poolDataDic setXoMode:XO_MODE_JSON];
	[_poolDataDic setUrl:[url copy]];
	[_poolDataDic setPostType:postType];
	[_poolDataDic setDelegate:delegate];
	KoreConn *_mainConn = [[KoreConn alloc] initCon:self iden:@"KoreConn_PostMultipath"];
	[_mainConn setUrl:[[NSString alloc] initWithFormat:@"%@",url]];
	
	if (accToken&&![accToken isEqualToString:@"(null)"]) {
		[_mainConn set_fbAccToken:[accToken copy]];
		[_mainConn setMode:FB_POST];
	}else {
		[_mainConn setMode:POST];		
	}
    
    
	
	[_mainConn setTimeOut:[_poolDataDic timeOut]];
	[_poolDataDic setPostConn:_mainConn];
}
-(void)postMultipath:(NSString*)key andValue:(id)value{
	KoreConn *_mainConn = [_poolDataDic postConn];
	[_mainConn setValue:value forKey:key];
	
}
#pragma mark -------------------------------- Post -
#pragma mark -------------------------------- FB +
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path residentIs:(BOOL) resident andFBaccToken:(NSString*)accToken{
	//	[_poolDataDic setValue:view forKey:TNC_VIEW];
	//	[_poolDataDic setValue:path forKey:TNC_PATH];
	
	[_poolDataDic setView:view];
	[_poolDataDic setPath:path];
	[_poolDataDic setResident:resident];
	[_poolDataDic setFbAccToken:accToken];
}
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path cachingIs:(BOOL) caching andFBaccToken:(NSString*)accToken{
	[_poolDataDic setView:view];
	[_poolDataDic setPath:path];
	[_poolDataDic setCaching:caching];	//*_*?
    [_poolDataDic setFbAccToken:accToken];
}
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path andFBaccToken:(NSString*)accToken{
	//	[_poolDataDic setValue:view forKey:TNC_VIEW];
	//	[_poolDataDic setValue:path forKey:TNC_PATH];
	
	[_poolDataDic setView:view];
	[_poolDataDic setPath:path];
    [_poolDataDic setFbAccToken:accToken];
}
#pragma mark -------------------------------- FB -
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path residentIs:(BOOL) resident permanentQIs:(BOOL) permanentQ{
	[_poolDataDic setView:view];
	[_poolDataDic setPath:path];
	[_poolDataDic setResident:resident];
		[_poolDataDic setPermanentQ:permanentQ];		
}
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path cachingIs:(BOOL) caching permanentQIs:(BOOL) permanentQ{
	[_poolDataDic setView:view];
	[_poolDataDic setPath:path];
	[_poolDataDic setCaching:caching];	//*_*?
	[_poolDataDic setPermanentQ:permanentQ];		
}
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path permanentQIs:(BOOL) permanentQ{
	[_poolDataDic setView:view];
	[_poolDataDic setPath:path];
	[_poolDataDic setPermanentQ:permanentQ];	
}
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path residentIs:(BOOL) resident{
	//	[_poolDataDic setValue:view forKey:TNC_VIEW];
	//	[_poolDataDic setValue:path forKey:TNC_PATH];
	
	[_poolDataDic setView:view];
	[_poolDataDic setPath:path];
	[_poolDataDic setResident:resident];
}
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path cachingIs:(BOOL) caching{
	[_poolDataDic setView:view];
	[_poolDataDic setPath:path];
	[_poolDataDic setCaching:caching];	//*_*?
}
-(void)dependOnView:(UIView*) view andPath:(KAL_INDEXPATH*) path{
//	[_poolDataDic setValue:view forKey:TNC_VIEW];
//	[_poolDataDic setValue:path forKey:TNC_PATH];

	[_poolDataDic setView:view];
	[_poolDataDic setPath:path];
}

-(KoreConn*)startRequest{
	
	if (!_qStart) {
//		NSTimer *t = [NSTimer scheduledTimerWithTimeInterval:0.100 target:self selector:@selector(_qProc) userInfo:nil repeats:YES];
//		[[NSRunLoop currentRunLoop] addTimer:t forMode:NSRunLoopCommonModes];
//		[NSThread detachNewThreadSelector:@selector(_qProc) toTarget:self withObject:nil];  
		
		[NSTimer scheduledTimerWithTimeInterval:0.100 target:self selector:@selector(_qProc) userInfo:nil repeats:YES];

		_qStart = YES;
	}

	
	postTYPE postType = [_poolDataDic postType];
	NSInteger type = [_poolDataDic type];//[[_poolDataDic valueForKey:TNC_TYPE] intValue];
	NSString *url = [_poolDataDic url];//[_poolDataDic valueForKey:TNC_URL];
	if (!url || [url isEqualToString:@"(null)"]) {
		TLog(@"*******************************************>> NULL");
		return;
	}
	if (postType>PTYPE_NONE) {TLog(@"___________ 1");
		//http://widget2.truelife.com/iPhoneTest/post_test.php
		if (postType==PTYPE_IMG) {TLog(@"___________ 2");
			KoreConn *_mainCon = [[KoreConn alloc]initCon:self iden:[NSString stringWithFormat:@"KoreConn_PostMultipath"]];	TLog(@"___________ 3");
			[_mainCon setUrl:[[NSString alloc] initWithFormat:@"%@",url]];													TLog(@"___________ 4");
			
			//	UIImage *img = [UIImage imageNamed:@"july04_1024.png"];
			[_mainCon setValueImage:(UIImage*)[_poolDataDic postObj]];													TLog(@"___________ 5");
			[_mainCon setMode:POST];				
			[self _addRequestType:QTYPE_POST_MULTI withObject:_mainCon];
			[_poolConnection setObject:_poolDataDic forKey:_mainCon];
            [_poolDataDic release];
            _poolDataDic = nil;
			return _mainCon;
			
		}else if(postType==PTYPE_MULTI){
		
			
	//		KoreConn *_mainConn = [[KoreConn alloc] initCon:self iden:@"KoreConn_PostMultipath"];
	//		[_mainConn setUrl:[[NSString alloc] initWithFormat:@"%@",url]];
	//		
	//		[_mainConn setMode:POST];
	//
	//		[_mainConn setTimeOut:[_poolDataDic timeOut]];
			KoreConn *_mainConn = [_poolDataDic postConn];
			[self _addRequestType:QTYPE_POST_MULTI withObject:_mainConn];
			[_poolConnection setObject:_poolDataDic forKey:_mainConn];
            [_poolDataDic release];
            _poolDataDic = nil;
		return _mainConn;
		}
	}
	
//	TLog(@">>>>>>>>> type = %d",type);
//	TLog(@">>>>>>>>> url = %@",url);
//	TLog(@">>>>>>>>> dic = %@",[__res_manager__ _getItemProfile:url]);
	NSString *lastModDate = [[__res_manager__ _getItemProfile:url] valueForKey:K_RES_MANAGE_FILEDATE];
//	TLog(@">>>>>>>>> lastModDate = %@",lastModDate);
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	
	
	NSLocale *locale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_TH"] autorelease];
	
	[dateFormatter setLocale:locale];
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZ"];
	
	NSDate *dateFromString = [[[NSDate alloc] init] retain];
	dateFromString = [dateFormatter dateFromString:lastModDate];//modified
	TLog(@">>>>>>>>> dateFromString = %@",[dateFromString description]);
	if (type==RESPTYPE_DIC) {
		KoreFeedEngine *_KFE = nil;
		if ([_poolDataDic filePath]) {
			_KFE = [[KoreFeedEngine alloc] _initFeed:self withUrl:url andMode:XO_MODE_XML];	//TLog(@"____ 1");
			
			[_KFE _setMode:FEM_FILEPATH];		
			[self _addRequestType:QTYPE_DIC withObject:_KFE];																									//TLog(@"_KFE  = %@",_KFE);
			[_poolConnection setObject:_poolDataDic forKey:_KFE];//TLog(@"____ 3");
            [_poolDataDic release];
            _poolDataDic = nil;

		}else {
            if ([_poolDataDic fbAccToken]) {TLog(@"[_poolDataDic fbAccToken] = %@",[_poolDataDic fbAccToken]);
                
				KoreFeedEngine *_KFE = [[KoreFeedEngine alloc] _initFeedFB:self withUrl:url andAccessToken:[_poolDataDic fbAccToken]];	//TLog(@"____ 1");
				
				[_KFE _setMode:FEM_URL];																				//TLog(@"____ 3");
				[_KFE _setLastUpdateSinceRefDate:[lastModDate doubleValue]];
				[_KFE _setTimeout:[_poolDataDic timeOut]];
				[self _addRequestType:QTYPE_DIC withObject:_KFE];																									//TLog(@"_KFE  = %@",_KFE);
				[_poolConnection setObject:_poolDataDic forKey:_KFE];
                [_poolDataDic release];
                _poolDataDic = nil;
			}else{
			TLog(@"[_poolDataDic xoMode] = %d",[_poolDataDic xoMode]);
			_KFE = [[KoreFeedEngine alloc] _initFeed:self withUrl:url andMode:[_poolDataDic xoMode]];	//TLog(@"____ 1");
			
			[_KFE _setMode:FEM_URL];																				//TLog(@"____ 3");
			[_KFE _setLastUpdateSinceRefDate:[lastModDate doubleValue]];
			[_KFE _setTimeout:[_poolDataDic timeOut]];
			[self _addRequestType:QTYPE_DIC withObject:_KFE];																									//TLog(@"_KFE  = %@",_KFE);
			[_poolConnection setObject:_poolDataDic forKey:_KFE];
                [_poolDataDic release];
                _poolDataDic = nil;
//                TLog(@"__Add _poolConnection = %@",_poolConnection);
            }

		}

																												//TLog(@"_poolDataDic = %@",_poolDataDic);
		return [_KFE _mainConn];
		
	}
    else if(type == RESPTYPE_IMG){

		KoreConn *_mainConn = [[KoreConn alloc] initCon:self iden:@"KoreConn_Image"];
		[_mainConn setUrl:[[NSString alloc] initWithFormat:@"%@",url]];
        if ([_poolDataDic fbAccToken]) {
			[_mainConn set_fbAccToken:[_poolDataDic fbAccToken]];
			[_mainConn setMode:FB_GET];			
		}else {
			[_mainConn setMode:GET];	
		}
//		[_mainConn setMode:GET];
//		[_mainConn setLastUpadate:dateFromString];
		[_mainConn	setLastUpdateSinceRefDate:[lastModDate doubleValue]];
//		[_mainConn startConInQueue];
		[_mainConn setTimeOut:[_poolDataDic timeOut]];
		[self _addRequestType:QTYPE_IMG withObject:_mainConn];
		[_poolConnection setObject:_poolDataDic forKey:_mainConn];
        [_poolDataDic release];
        _poolDataDic = nil;
		return _mainConn;
	}
    else if(type == RESPTYPE_DATA){
		KoreConn *_mainConn = [[KoreConn alloc] initCon:self iden:@"KoreConn_Data"];
		[_mainConn setUrl:[[NSString alloc] initWithFormat:@"%@",url]];
		
		[_mainConn setMode:GET];
//		[_mainConn setLastUpadate:dateFromString];
		[_mainConn	setLastUpdateSinceRefDate:[lastModDate doubleValue]];
//		[_mainConn startCon];
		[_mainConn setTimeOut:[_poolDataDic timeOut]];
		[self _addRequestType:QTYPE_DATA withObject:_mainConn];
		[_poolConnection setObject:_poolDataDic forKey:_mainConn];
        [_poolDataDic release];
        _poolDataDic = nil;
//        KoreConn *kMainConn = [_mainConn retain];
//        [_mainConn release];
//        [_mainConn release];
		return _mainConn;
	}


	
}
-(void)cleanQreservByType:(NSInteger)feed andImage:(NSInteger)image andData:(NSInteger)data andMulti:(NSInteger)multi{
    _reservFeed = feed;
    _reservImage = image;
    _reservData = data;
    _reservMulit = multi;
    dispatch_queue_t		clearQ = dispatch_queue_create("KAL.Clear.queue", NULL);
	dispatch_sync(clearQ,		//dispatch_async		//dispatch_sync
				  ^{
					  _cancelingQ = YES;
					  if (_qFeed&&[_qFeed count]>0) {
//                          NSInteger lessThan = [_qFeed count]-_reservFeed;
						  for (NSInteger i=0;i<([_qFeed count]-_reservFeed);i++) {
//                              TLog(@"i = %d",i);
//                              TLog(@"[_qImage count] = %d",([_qFeed count]));
//                              TLog(@"_reservImage = %d",(_reservImage));
//                              TLog(@"i< = %d",([_qFeed count]-_reservFeed));
//                              if (i<[_qFeed count]) 
                              {
                                  KoreFeedEngine *kFeed = [_qFeed objectAtIndex:i];
                                  [self _forceRemoveRequestType:QTYPE_DIC withObject:kFeed];
                              }
						  }
					  }
					  if (_qImage&&[_qImage count]>0) {
//                          NSInteger lessThan = [_qImage count]-_reservImage;
//                          TLog(@"lessThan = %d",lessThan);
						  for (NSInteger i=0;i<([_qImage count]-_reservImage);i++) {
//                              TLog(@"i = %d",i);
//                              TLog(@"[_qImage count] = %d",([_qImage count]));
//                              TLog(@"_reservImage = %d",(_reservImage));
//                              TLog(@"i< = %d",([_qImage count]-_reservImage));
//                              if (i<[_qImage count])
                              {
                                  KoreConn *kCon = [_qImage objectAtIndex:i];
                                  [self _forceRemoveRequestType:QTYPE_IMG withObject:kCon];                                  
                              }

						  }
					  }
					  if (_qData&&[_qData count]>0) {
//                          NSInteger lessThan = [_qData count]-_reservData;
						  for (NSInteger i=0;i<([_qData count]-_reservData);i++) {
//                              if (i<[_qData count]) 
                              {
                                  KoreConn *kCon = [_qData objectAtIndex:i];
                                  [self _forceRemoveRequestType:QTYPE_DATA withObject:kCon];
                              }
						  }
					  }
					  //_qPostMulti
					  if (_qPostMulti&&[_qPostMulti count]>0) {
//                          NSInteger lessThan = [_qPostMulti count]-_reservMulit;
						  for (NSInteger i=0;i<([_qPostMulti count]-_reservMulit);i++) {
//                              if (i<[_qPostMulti count]) 
                              {
                                  KoreConn *kCon = [_qPostMulti objectAtIndex:i];
                                  [self _forceRemoveRequestType:QTYPE_POST_MULTI withObject:kCon];
                              }
						  }
					  }
                      
					  NSLog(@"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
					  
//					  NSLog(@"__________[_qImage count] = %d",[_qImage count]);
//					  NSLog(@"___________imgMaxNow = %d",_imgMaxNow);
					  _cancelingQ = NO;
				  }
				  );
	dispatch_release(clearQ);

}
-(void)cleanQ{
	dispatch_queue_t		clearQ = dispatch_queue_create("KAL.Clear.queue", NULL);
	dispatch_sync(clearQ,		//dispatch_async		//dispatch_sync
				  ^{
					  _cancelingQ = YES;
					  if (_qFeed&&[_qFeed count]>0) {
						  for (id kFeed in [_qFeed reverseObjectEnumerator]) {
							  [self _forceRemoveRequestType:QTYPE_DIC withObject:kFeed];
						  }
					  }
					  if (_qImage&&[_qImage count]>0) {
						  for (id	kCon in [_qImage reverseObjectEnumerator]) {
							  [self _forceRemoveRequestType:QTYPE_IMG withObject:kCon];
						  }
					  }
					  if (_qData&&[_qData count]>0) {
						  for (id	kCon in [_qData reverseObjectEnumerator]) {
							  [self _forceRemoveRequestType:QTYPE_DATA withObject:kCon];
						  }
					  }
                      //_qPostMulti
					  if (_qPostMulti&&[_qPostMulti count]>0) {
						  for (id	kCon in [_qPostMulti reverseObjectEnumerator]) {
							  [self _forceRemoveRequestType:QTYPE_POST_MULTI withObject:kCon];
						  }
					  }

					  NSLog(@"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
					  
					  NSLog(@"__________[_qImage count] = %d",[_qImage count]);
					  NSLog(@"___________imgMaxNow = %d",_imgMaxNow);
					  _cancelingQ = NO;
				  }
				  );
	dispatch_release(clearQ);

//*_* Multipart Upload Exceptional	
//	if (_qPostMulti&&[_qPostMulti count]>0) {
//		for (KoreConn	kCon in _qPostMulti) {
//			[self _forceRemoveRequestType:QTYPE_POST_MULTI withObject:kCon];
//		}
//	}
}
#pragma mark -	Additional Feature	+
-(void)getFile:(NSString*)filePath withType:(respondTYPE)type byDelegate:(id<KAL_NET_CONNECTION_DELEGATE>) delegate{
	_poolDataDic  = nil;
	_poolUrl = nil;
	//	_poolDataDic = [[NSMutableDictionary	alloc] init];
	//	[_poolDataDic setValue:[url copy] forKey:TNC_URL];
	//	[_poolDataDic setValue:[NSString stringWithFormat:@"%d",type] forKey:TNC_TYPE];
	//	[_poolDataDic setValue:delegate forKey:TNC_DELEGATE];
	
	_poolDataDic = [[KAL_GROUP_CONTAINER alloc] init];
	[_poolDataDic setUrl:[filePath copy]];
	[_poolDataDic setType:type];
	[_poolDataDic setDelegate:delegate];
	[_poolDataDic setXoMode:XO_MODE_XML];
	[_poolDataDic setFilePath:YES];


}
+(NSInteger)eightEncryptRow:(NSInteger)row andSection:(NSInteger)section andPiece:(NSInteger)piece{// Format 1 int is (0|section|row|piece)
    TLog(@"eightEncryptRow section[%d] row[%d] piece[%d]",section,row,piece);
	NSInteger result = 0;
	NSInteger enRow =  row<<8;
	NSInteger enSec = section<<16;
	NSInteger enPie = piece;
	result = (NSInteger)(enSec|enRow|enPie);
	
	return result;
}
+(void)eightDecrypt:(NSInteger)tag toRow:(NSInteger**)row andSection:(NSInteger**)section andPiece:(NSInteger**)piece{
	NSInteger maskSec = 0x00FF0000; 
	NSInteger maskRow = 0x0000FF00;
	NSInteger maskPie = 0x000000FF;
	*row = (NSInteger*)((tag&maskRow)>>8);
	*section = (NSInteger*)((tag&maskSec)>>16);
	*piece = (NSInteger*)(tag&maskPie);
	    TLog(@"eightDecrypt section[%d] row[%d] piece[%d]",section,row,piece);
}
+(NSInteger)sixteenEncryptRow:(NSInteger)row andSection:(NSInteger)section{// Format 1 int is (0|section|row|piece)
	NSInteger result = 0;

	NSInteger enSec = section<<16;
	NSInteger enRow =  row;
	result = (NSInteger)(enSec|enRow);
	
	return result;
}
+(void)sixteenDecrypt:(NSInteger)tag toRow:(NSInteger**)row andSection:(NSInteger**)section{
	NSInteger maskTop = 0xFFFF0000; 
	NSInteger maskLow = 0x0000FFFF;

	*row = (NSInteger*)(tag&maskLow);
	*section = (NSInteger*)((tag&maskTop)>>16);

	
}

#pragma mark Additional Feature	

#pragma mark - KoreXtoODelegate
-(void)parseDidFinish:(NSMutableDictionary*) dic withIden:(NSString*)iden{
	
}
-(void)parseDidFailed:(NSError *)parseError  withIden:(NSString*)iden{
	
}
# pragma mark -
#pragma mark KoreConn delegate methods
-(void)connectionProgress:(CGFloat) percentage from:(KoreConn*) con{
//	NSLog(@"KAL_conn = %f",percentage);
	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:con];
	NSInteger type = [container type];//[[container valueForKey:TNC_TYPE] intValue];
	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];
	if (delegate) {
		
        if ([delegate respondsToSelector:@selector(networkProgress:withType:andInfo:)]) {
            [delegate networkProgress:[con _progress] withType:type andInfo:container];	      
        }
	}

}
-(void)connectionNotAvailable:(KoreConn*) con{TLog(@"____connectionNotAvailable");
	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:con];
	NSInteger type = [container type];//[[container valueForKey:TNC_TYPE] intValue];
	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];//[container valueForKey:TNC_DELEGATE];
	
	queueTYPE qtype = QTYPE_NONE;
	if([[con _identifier] isEqualToString:@"KoreConn_Image"]){														//TLog(@"__________ 2");
		qtype = QTYPE_IMG;
				
	}
	else if([[con _identifier] isEqualToString:@"KoreConn_Data"]){
		qtype = QTYPE_DATA;
		
	}
	else if([[con _identifier] isEqualToString:@"KoreConn_PostMultipath"]){
		qtype = QTYPE_POST_MULTI;
		
	}
	[delegate networkNotAvailable:nil withType:type andInfo:container];
	
	
	
	[_poolConnection removeObjectForKey:con];
	[self _canRemoveRequestType:qtype	withObject:con];
//	[container release];
	container = nil;
//	[con release];

}
-(void)connectionDidFinish:(KoreConn *) con{TLog(@"KoreFeedEngine___connectionDidFinish ++");
//	NSMutableDictionary *container = [_poolConnection objectForKey:con];
	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:con];
	NSInteger type = [container type];//[[container valueForKey:TNC_TYPE] intValue];
	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];//[container valueForKey:TNC_DELEGATE];
	//	NSMutableDictionary *container = [_poolConnection valueForKey:[NSString stringWithFormat:@"%@",kFeed]];
																												//TLog(@"__________ 1");
	
	

	queueTYPE qtype = QTYPE_NONE;
	if([[con _identifier] isEqualToString:@"KoreConn_Image"]){														//TLog(@"__________ 2");
		qtype = QTYPE_IMG;
		if (container.type==RESPTYPE_IMG&&[[con _respContent] isEqualToString:@"image"]) {
			//UIImage *img = [UIImage imageWithData:[con _receivedData]];											//TLog(@"__________ 3");
			
																												//TLog(@"__________ 5");
			UIImage *img = [__res_manager__ saveResItemWithKey:con andType:type cachingIs:[container caching] residentIs:[container resident]];

            TLog(@"__img = %@",img);
			[delegate respondDidFinishObject:img withType:type andInfo:container];									//TLog(@"__________ 4");
            [img release];
            img = nil;
		}else {
			[delegate respondDidFailedObject:nil withType:type andInfo:container];
		}


	}
	else if([[con _identifier] isEqualToString:@"KoreConn_Data"]){
		qtype = QTYPE_DATA;
		[delegate respondDidFinishObject:[con _receivedData] withType:type andInfo:container];
		[__res_manager__ saveResItemWithKey:con  andType:type cachingIs:[container caching] residentIs:[container resident]];
	}
	else if([[con _identifier] isEqualToString:@"KoreConn_PostMultipath"]){
		qtype = QTYPE_POST_MULTI;
		
//		TLog(@"[con _receivedData] = %@",[[NSString alloc] initWithData:[con _receivedData] encoding:NSUTF8StringEncoding]);
//		dispatch_async(dispatch_get_global_queue(0, 0), ^{
//			// critical section
			_xoParser = [[KoreXtoO alloc] initParseEngine:self withMode:[container xoMode] andIden:@"KoreConn_PostMultipath"];
			[_xoParser parseData:[con _receivedData]];
			[delegate respondDidFinishObject:[_xoParser parseObj] withType:type andInfo:container];
//		});
		
//		[delegate respondDidFinishObject:[con _receivedData] withType:type andInfo:container];		
	
	}
	
	
	
	
	[_poolConnection removeObjectForKey:con];
	[self _canRemoveRequestType:qtype	withObject:con];
//	[container release];
    	container = nil;
//	[con release];
	
	
}
-(void)connectionDidFailed:(KoreConn *) con{TLog(@"KAL_________connectionDidFailed");
	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:con];
	NSInteger type = [container type];
	queueTYPE qtype = QTYPE_NONE;


	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];
	//MyAlertWithMessage(kNetworkNotFound);////TLog(@"connectionDidFailedconnectionDidFailed");
	if (!con) {
		K_MyAlertWithMessage(@"ERR::ConFailed Imp case");//TLog(@"connectionDidFailedconnectionDidFailed");
		return;
	}
	else if([[con _identifier] isEqualToString:@"KoreConn_Image"]){
		qtype = QTYPE_IMG;
		
	}
	else if([[con _identifier] isEqualToString:@"KoreConn_Data"]){
		qtype = QTYPE_DATA;
		
	}
	else if([[con _identifier] isEqualToString:@"KoreConn_PostMultipath"]){
		qtype = QTYPE_POST_MULTI;
//        TLog(@"KoreConn_PostMultipath [con _receivedData] = %@",[[NSString alloc] initWithData:[con _receivedData] encoding:NSUTF8StringEncoding]);
        //		dispatch_async(dispatch_get_global_queue(0, 0), ^{
        //			// critical section
        TLog(@"[container xoMode] = %d",[container xoMode]);
//        _xoParser = [[KoreXtoO alloc] initParseEngine:self withMode:[container xoMode] andIden:@"KoreConn_PostMultipath"];
//        [_xoParser parseData:[con _receivedData]];
//        //^_^ Failed
//        [delegate respondDidFailedObject:[_xoParser parseObj] withType:type andInfo:container];
        
        _xoParser = [[KoreXtoO alloc] initParseEngine:self withMode:[container xoMode] andIden:@"KoreConn_PostMultipath"];
        [_xoParser parseData:[con _receivedData]];
        [delegate respondDidFailedObject:[_xoParser parseObj] withType:type andInfo:container];
		[_xoParser release];
        _xoParser = nil;
	}


	[_poolConnection removeObjectForKey:con];
		[self _canRemoveRequestType:qtype	withObject:con];
//	[container release];
    	container = nil;
//	[con release];
}
-(void)connectionDidCancel:(KoreConn *) con{TLog(@"KAL_________connectionDidCancel");
	//if (!con)
	{TLog(@"KAL >>>>>>>>>> connectionDidCancel");

	}

	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:con];
	NSInteger type = [container type];//[[container valueForKey:TNC_TYPE] intValue];
	queueTYPE qtype = QTYPE_NONE;
	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];//[container valueForKey:TNC_DELEGATE];
	//	NSMutableDictionary *container = [_poolConnection valueForKey:[NSString stringWithFormat:@"%@",kFeed]];
	//TLog(@"__________ 1");
	
	
	
	
	if([[con _identifier] isEqualToString:@"KoreConn_Image"]){														//TLog(@"__________ 2");
		if (container.type==RESPTYPE_IMG&&[[con _respContent] isEqualToString:@"image"]) {

//			[delegate respondDidFinishObject:[__res_manager__ loadResItemUnconditionWithKey:[con _strRequest]] withType:type andInfo:container];
			[delegate respondDidCancelObject:[__res_manager__ loadResItemUnconditionWithKey:[con _strRequest]]withType:type andInfo:container];
		}else {
			[delegate respondDidFailedObject:nil withType:type andInfo:container];
		}
		
		qtype = QTYPE_IMG;
	}else if([[con _identifier] isEqualToString:@"KoreConn_Data"]){
		qtype = QTYPE_DATA;
		[delegate respondDidCancelObject:[__res_manager__ loadResItemUnconditionWithKey:[con _strRequest]]withType:type andInfo:container];
	}else if([[con _identifier] isEqualToString:@"KoreConn_PostMultipath"]){
		qtype = QTYPE_POST_MULTI;
		
	}
	
	//	TLog(@"[kFeed _strUrl] = %@",[kFeed _strUrl]);
	//	TLog(@"[__res_manager__ loadResItemWithKey:[kFeed _strUrl]] = %@",[__res_manager__ loadResItemUnconditionWithKey:[kFeed _strUrl]]);
	TLog(@".....Cancel.....Conn");
	[__res_manager__ saveResItemUnconditionWithKey:con];
	[_poolConnection removeObjectForKey:con];
		[self _canRemoveRequestType:qtype	withObject:con];
//	[container release];
    	container = nil;
//	[con release];
}
#pragma mark -
#pragma mark KoreFeedEngineDelegate
-(void)feedProgress:(CGFloat ) percentage from:(KoreFeedEngine*)kFeed{//TLog(@"KAL_feed = %f",percentage);
//	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:kFeed];
//	NSInteger type = [container type];//[[container valueForKey:TNC_TYPE] intValue];
//	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];
	
	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:kFeed];
	NSInteger type = [container type];//[[container valueForKey:TNC_TYPE] intValue];
	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];
	
	
    if ([delegate respondsToSelector:@selector(networkProgress:withType:andInfo:)]) {
        [delegate networkProgress:percentage withType:type andInfo:container];      
    }
}
-(void)feedNotAvailable:(KoreFeedEngine*)kFeed{TLog(@"____feedNotAvailable");
	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:kFeed];
	NSInteger type = [container type];//[[container valueForKey:TNC_TYPE] intValue];
	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];//[container valueForKey:TNC_DELEGATE];
	//	NSMutableDictionary *container = [_poolConnection valueForKey:[NSString stringWithFormat:@"%@",kFeed]];
	
	[delegate networkNotAvailable:nil withType:type andInfo:container];
	
//	[__res_manager__ saveResItemWithKey:kFeed  andType:type cachingIs:[container caching] residentIs:[container resident]]; //*_*?
	
	[_poolConnection removeObjectForKey:kFeed];
	[self _canRemoveRequestType:QTYPE_DIC withObject:kFeed];
//	[container release];
	container = nil;
	K_MyAlertWithActivityIndicatorStop();
	K_MyAlertWithTitleAndMessage(@"", K_MSG_NOTICE_NET_NOTAVILABLE);
}
-(void)feedDidFinish:(KoreFeedEngine *) kFeed{


//TLog(@"feedDidFinish _resultDic  = %@",[kFeed _resultDic]);

#ifdef NEW_TIMEOUT_LOGIC
    if([[kFeed _resultDic] count]==0){
        [__self__ feedDidFailed:kFeed];
        return;
    }
#endif
	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:kFeed];
	NSInteger type = [container type];//[[container valueForKey:TNC_TYPE] intValue];
	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];//[container valueForKey:TNC_DELEGATE];
//	NSMutableDictionary *container = [_poolConnection valueForKey:[NSString stringWithFormat:@"%@",kFeed]];
	NSMutableDictionary *mudic = [__res_manager__ saveResItemWithKey:kFeed  andType:type cachingIs:[container caching] residentIs:[container resident]];

	[delegate respondDidFinishObject:mudic withType:type andInfo:container];
//	[mudic release];
//    mudic = nil;


#ifdef NEW_TOKEN_EXPIRE_FORCE_LOGOUT
    if ([[mudic valueForKey:@"message_code"] isEqualToString:@"invalid_grant"]) {
        [[APP_DELEGATE _settingVc] logOut];  
    }
#endif
	
	
	[_poolConnection removeObjectForKey:kFeed];
	[self _canRemoveRequestType:QTYPE_DIC withObject:kFeed];
//	[container release];
	container = nil;

}
-(void)feedDidFailed:(KoreFeedEngine *) kFeed{//TLog(@"feedDidFailed <<< kFeed = %@",kFeed);
//____________________________________________________ Analyse Error
    
    
//____________________________________________________ Analyse Error
    
    
TLog(@"_poolConnection = %@",_poolConnection);
	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:kFeed];
	NSInteger type = [container type];
	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];
    if ([kFeed _resultDic]) {
        [delegate respondDidFailedObject:[kFeed _resultDic] withType:type andInfo:container];        
    }else{
        NSDictionary *dicError = nil;
        NSError *err = [[kFeed _mainConn] _err];
        TLog(@"err = %@",err);
        if (!err) {
            dicError = [[NSDictionary alloc] initWithObjectsAndKeys:
                        [NSString stringWithFormat:@"%d",[[kFeed _mainConn] _httpRespCode]],@"code",
                        @"Unknow Reason",@"domain",
                     
                        nil];           
        }else{
         dicError = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%d",[err code]],@"code",
                                  [err domain],@"domain",
                                  [err localizedDescription],@"localizedDescription",
                                  [err localizedFailureReason],@"localizedFailureReason",
                                  nil];
        }
        TLog(@"dicError = %@",dicError);
        [delegate respondDidFailedObject:dicError withType:type andInfo:container];
        [dicError release];
    }

	[_poolConnection removeObjectForKey:kFeed];
	[self _canRemoveRequestType:QTYPE_DIC withObject:kFeed];
//	[container release];
		container = nil;
}
-(void)feedDidCancel:(KoreFeedEngine *) kFeed{TLog(@"____feedDidCancel");
	KAL_GROUP_CONTAINER *container = [_poolConnection objectForKey:kFeed];
	NSInteger type = [container type];
	id<KAL_NET_CONNECTION_DELEGATE> delegate = [container delegate];
//	TLog(@"[kFeed _strUrl] = %@",[kFeed _strUrl]);
//	TLog(@"[__res_manager__ loadResItemWithKey:[kFeed _strUrl]] = %@",[__res_manager__ loadResItemUnconditionWithKey:[kFeed _strUrl]]);
//	[delegate respondDidFinishObject:[__res_manager__ loadResItemUnconditionWithKey:[kFeed _strUrl]] withType:type andInfo:container];
	[delegate respondDidCancelObject:[__res_manager__ loadResItemUnconditionWithKey:[kFeed _strUrl]] withType:type andInfo:container];
	TLog(@".....Cancel.....Feed");
	[__res_manager__ saveResItemUnconditionWithKey:kFeed];
	[_poolConnection removeObjectForKey:kFeed];
    [self _canRemoveRequestType:QTYPE_DIC withObject:kFeed];
//	[container release];
    	container = nil;
}

@end
