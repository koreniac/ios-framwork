//
//  KAL_RES_MANAGER.h
//  NetConn_1021
//
//  Created by Naruphon Sirimasrungsee on 10/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KAL_NET_CONNECTION.h"
#import "KoreFileManage.h"
#import "KoreConn.h"
#import "KoreFeedEngine.h"
#import "KoreDBMS.h"
typedef enum{
	RESCOND_NONE = 0,
	RESCOND_NEWREQ = 1,				// NEW REQUEST
	RESCOND_DISKTOMAP = 2,			// GET INFO FROM DISK TO MAPTABLE
	RESCOND_REPREQ = 3,				// REPEAT REQUEST REPEAT
}loadresCOND;


@interface KAL_RES_MANAGER : NSObject <KoreFeedEngineDelegate>{
	NSMutableDictionary *_mapTable;
	NSMutableDictionary *_resTable;
	KoreFileManage *_fm;
	NSMutableDictionary *_feedTmp;
	NSMutableArray *_garBagePool;
    KoreDBMS *__DBMS__;
}
-(void)_init;
-(void)_saveFileWithName:(NSString*)name andData:(NSData*) data;
-(NSDictionary*)_getItemProfile:(NSString*)key;
-(void)_cleanDisk;
-(void)_saveMap;

+(id)getInstance;
-(void)loadMappingTable;
-(id)loadResItemWithKey:(NSString*) key andGetCondition:(loadresCOND**) cond reSetResident:(BOOL) resident;
-(id)loadResItemUnconditionWithKey:(NSString*) key reSetResident:(BOOL) resident;
-(id)loadResItemWithKey:(NSString*) key andGetCondition:(loadresCOND**) cond;
-(id)loadResItemUnconditionWithKey:(NSString*) key;
-(id)_loadResItemWithKey:(NSString*) key;
-(id)saveResItemWithKey:(id) conOrFeed andType:(respondTYPE) type;			//Dic have to contain keyName and fileDate
-(id)saveResItemWithKey:(id) conOrFeed andType:(respondTYPE) type cachingIs:(BOOL)caching;
-(id)saveResItemWithKey:(id) conOrFeed andType:(respondTYPE) type cachingIs:(BOOL)caching residentIs:(BOOL)resident;
-(NSMutableDictionary*)saveResItemUnconditionWithKey:(id) conOrFeed;
-(void)saveMappingTable;

-(void)runDiskGarbage;
-(void)runHeapGarbage;

//V1.0.6 minor for theOne
-(NSString*)byPassKey:(NSString*)key;

//-(void)markCacheUpdateWithKey:(NSString*) key;

//V1.1.6 main update core resouce from plist to sqlite
-(void)_startUpdateFromInterval;
-(void)_insertOrUpdateWithList:(NSArray*)list;
-(NSMutableDictionary*)_queryFromUrl:(NSString*)url;




@end
