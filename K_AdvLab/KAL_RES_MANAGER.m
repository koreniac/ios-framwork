//
//  KAL_RES_MANAGER.m
//  NetConn_1021
//
//  Created by Naruphon Sirimasrungsee on 10/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "KAL_RES_MANAGER.h"
#import "KoreCryptonite.h"
static KAL_RES_MANAGER *_self_ = nil;
@implementation KAL_RES_MANAGER

-(void)_init{
	_mapTable = nil;
	_resTable = nil;
	_fm = [[KoreFileManage alloc] init];
	_garBagePool = [[NSMutableArray alloc] init];
	_resTable = [[NSMutableDictionary alloc] init];
        __DBMS__ = [[KoreDBMS alloc] init];
}
-(void)_saveFileWithName:(NSString*)name andData:(NSData*) data{
	NSString *path = [_fm _getPathOfDirectory:FP_DOC ofFile:[NSString stringWithFormat:@"%@",T_RES_PACKAGE]];
//	TLog(@"path = %@",path);
	BOOL checkFileExist = [_fm _checkFileAccessPath:path withAuthorize:AT_FILE_EXIST];
	if (!checkFileExist) {
		[_fm _createFileAt:[[_fm _getPathOfDirectory:FP_DOC ofFile:nil] stringByAppendingFormat:@"%@",T_RES_PACKAGE] withContent:nil byAttribute:nil];
	}
		
	
	
	[_fm _createFileAt:[[_fm _getPathOfDirectory:FP_DOC ofFile:nil] stringByAppendingFormat:@"%@/%@",T_RES_PACKAGE,name] withContent:data byAttribute:nil];
}
//1.1.6
#ifdef NEW_RES_MAPPING
-(NSDictionary*)_getItemProfile:(NSString*)key{
	NSURL *url = [NSURL URLWithString:key];
	NSString *relativeKey = key;//[url path];
	return [_self_ _queryFromUrl:relativeKey];//[_mapTable valueForKey:relativeKey];
}
#else
//<1.1.6
-(NSDictionary*)_getItemProfile:(NSString*)key;{
//	NSURL *url = [NSURL URLWithString:key];
	NSString *relativeKey = key;//[url path];
	return [_mapTable valueForKey:relativeKey];
}
#endif
-(void)_cleanDisk{NSLog(@"___________________________> _cleanDisk");
//	if (_mapTable) {
//		NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
////		NSArray *mapListValue= [_mapTable allValues];
//		NSArray *mapListKey = [_mapTable allKeys];
//		
//		for (NSInteger i=0; i<[mapListKey count]; i++) {
//			NSString *key = [mapListKey objectAtIndex:i];
//			NSMutableDictionary *dic = [_mapTable valueForKey:key];
//			NSString *saveDate = [dic valueForKey:K_RES_MANAGE_CLEANDATE];
//			NSString *resident = [dic valueForKey:K_RES_MANAGE_RESIDENT];
//			NSTimeInterval diff = abs(now-[saveDate doubleValue]);
////			TLog(@"......diff = %f",diff);
//			TLog(@"resident = %@",resident);
//			if(diff>K_CLEAN_DISK&&![resident isEqualToString:@"YES"]){
//				NSString *fileName = [dic valueForKey:K_RES_MANAGE_NAME];
//				NSString *fileType = [dic valueForKey:K_RES_MANAGE_TYPE];
//				
//				NSString *filePath = [[_fm _getPathOfDirectory:FP_DOC ofFile:nil] stringByAppendingFormat:@"%@/%@.%@",T_RES_PACKAGE,fileName,fileType];
//				TLog(@"Clean <%@>",filePath);
//				[_fm _deleteFileAt:filePath];
//				[_mapTable removeObjectForKey:key];
//			}
//		}
//		
//		
//	}
	
}
-(void)_saveMap{
#ifdef NEW_RES_MAPPING
    
#else
	NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[_mapTable retain]];
	[self _saveFileWithName:T_RES_MANAGE_MAPFILE andData:data];
	//	TLog(@"save _mapTable = %@",_mapTable);
#endif
}
+(id)getInstance{
	
	if (!_self_) {
		_self_ = [[KAL_RES_MANAGER alloc] init];
		[_self_ _init];
	}
	return _self_;
}
//1.1.6
#ifdef NEW_RES_MAPPING
-(void)loadMappingTable{
    	NSString *prePath = [_fm _getPathOfDirectory:FP_DOC ofFile:[NSString stringWithFormat:@"%@",T_RES_PACKAGE]];
	NSString *path = [_fm _getPathOfDirectory:FP_DOC ofFile:[NSString stringWithFormat:@"%@/%@",T_RES_PACKAGE,T_RES_MANAGE_MAPFILE]];
	TLog(@"path = %@",path);
    //	BOOL checkFileExist = [_fm _checkFileAccessPath:path withAuthorize:AT_FILE_EXIST];
    //	if (checkFileExist) {
    //
    //			
    //			NSData *data = [_fm readDataFrom:path];
    //			//		////TLog(@"data =  %@",data);
    //			if (data) {//////TLog(@"------- 1");
    //				NSDictionary *poolTmp = [NSKeyedUnarchiver unarchiveObjectWithData:data];//////TLog(@"------- 2");
    //				_mapTable = [[NSMutableDictionary alloc] initWithDictionary:poolTmp];
    ////				TLog(@"load _mapTable = %@",_mapTable);
    //			}
    //			
    //	}else {
    _mapTable = [[NSMutableDictionary alloc] init];
    //	}
    BOOL checkFileExist = [_fm _checkFileAccessPath:prePath withAuthorize:AT_FILE_EXIST];
    if (!checkFileExist){
            [_fm _createFileAt:prePath withContent:nil byAttribute:nil];
//        [_fm _createFileAt:[[_fm _getPathOfDirectory:FP_DOC ofFile:nil] stringByAppendingString:T_RES_PACKAGE] withContent:nil byAttribute:nil];
    }
    {
        //#define K_RES_MANAGE_NAME @"_fileName"
        //#define K_RES_MANAGE_CONTENT @"_fileContent"
        //#define K_RES_MANAGE_TYPE @"_fileType"
        //#define K_RES_MANAGE_FILEDATE @"_fileDate"
        //#define K_RES_MANAGE_SAVEDATE @"_saveDate"	
        //#define K_RES_MANAGE_CLEANDATE @"_cleanDate"
        //#define K_RES_MANAGE_FAVOR @"_favor"
        //#define K_RES_MANAGE_RESIDENT @"_resident"
        
        [__DBMS__ openDB:path forceOpen:YES];
        
        
        if([__DBMS__ openTBname:@"mapLinkWithSorce" forceOpenCmd:
            @"CREATE TABLE mapLinkWithSorce ( _fromUrl TEXT PRIMARY KEY, _fileName TEXT, _fileContent TEXT, _fileType TEXT, _fileDate TEXT, _saveDate TEXT, _cleanDate TEXT, _favor TEXT, _resident TEXT )"]){TLog(@"______________ addHistory");
            
            
        }
        
    }
	
}
#else
//<1.1.6
-(void)loadMappingTable{
	NSString *path = [_fm _getPathOfDirectory:FP_DOC ofFile:[NSString stringWithFormat:@"%@/%@",T_RES_PACKAGE,T_RES_MANAGE_MAPFILE]];
	TLog(@"path = %@",path);
	BOOL checkFileExist = [_fm _checkFileAccessPath:path withAuthorize:AT_FILE_EXIST];
	if (checkFileExist) {

			
			NSData *data = [_fm readDataFrom:path];
			//		////TLog(@"data =  %@",data);
			if (data) {//////TLog(@"------- 1");
				NSDictionary *poolTmp = [NSKeyedUnarchiver unarchiveObjectWithData:data];//////TLog(@"------- 2");
				_mapTable = [[NSMutableDictionary alloc] initWithDictionary:poolTmp];
//				TLog(@"load _mapTable = %@",_mapTable);
			}
			
	}else {
		_mapTable = [[NSMutableDictionary alloc] init];
	}

	
}
#endif
-(id)_loadResItemWithKey:(NSString*) key andGetCondition:(NSInteger**) cond{
	//dispatch_queue_t		qConn = dispatch_queue_create("KAL.RES.test", NULL);
//	dispatch_sync(qConn, 
//				  ^{
//					  [self _loadResItemWithKey:key];
//				  }
//				  );
//	dispatch_release(qConn);
    return nil;
}
//1.1.6
#ifdef NEW_RES_MAPPING
-(id)loadResItemWithKey:(NSString*) key andGetCondition:(loadresCOND**) cond reSetResident:(BOOL) resident{
	//	loadresCOND &condition = *cond;
	NSURL *url = [NSURL URLWithString:key];
	NSString *relativeKey = key;//[url path];
	id result = nil;
	result =  [self loadResItemUnconditionWithKey:key reSetResident:resident];
	
	NSDictionary *dataDic = [_self_ _queryFromUrl:relativeKey];//[_mapTable valueForKey:relativeKey];
	if (!dataDic) {
		TLog(@"********* 2");
		//		loadresCOND *condd = *cond;
		*cond = RESCOND_NEWREQ;
		return nil;
	}
	
	double saveDate = [[dataDic valueForKey:K_RES_MANAGE_SAVEDATE] doubleValue];
	double fileDate = [[dataDic valueForKey:K_RES_MANAGE_FILEDATE] doubleValue];
	if ([NSDate timeIntervalSinceReferenceDate]>saveDate) {
		if (fileDate<1.0) {
			TLog(@"********* 0 has Data But request");
			*cond = RESCOND_NEWREQ;
		}else {
			TLog(@"********* 1 This 's time to request");
			*cond = RESCOND_REPREQ;
		}
		
		
	}else {TLog(@"********* 2 This 's time from Map");
		*cond = RESCOND_DISKTOMAP;
	}
    NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
	NSTimeInterval howLongUpdate = 0;
    howLongUpdate =now+K_REQUEST_NEXT_IMG;
    [dataDic setValue:[NSString stringWithFormat:@"%f",howLongUpdate] forKey:K_RES_MANAGE_SAVEDATE];
    NSArray *infoList = [[NSArray  alloc] initWithObjects:dataDic, nil];
    
    [_self_ _insertOrUpdateWithList:infoList];
    [infoList release];
    
	[dataDic release];
	return result;
}
#else
//<1.1.6
-(id)loadResItemWithKey:(NSString*) key andGetCondition:(loadresCOND**) cond reSetResident:(BOOL) resident{
	//	loadresCOND &condition = *cond;
//	NSURL *url = [NSURL URLWithString:key];
	NSString *relativeKey = key;//[url path];
	id result = nil;
	result =  [self loadResItemUnconditionWithKey:key reSetResident:resident];
	
	NSDictionary *dataDic = [_mapTable valueForKey:relativeKey];
	if (!dataDic) {
		TLog(@"********* 2");
		//		loadresCOND *condd = *cond;
		*cond = (loadresCOND *)RESCOND_NEWREQ;
		return nil;
	}
	
	double saveDate = [[dataDic valueForKey:K_RES_MANAGE_SAVEDATE] doubleValue];
	double fileDate = [[dataDic valueForKey:K_RES_MANAGE_FILEDATE] doubleValue];
	if ([NSDate timeIntervalSinceReferenceDate]>saveDate) {
		if (fileDate<1.0) {
			TLog(@"********* 0 has Data But request");
			*cond = (loadresCOND *)RESCOND_NEWREQ;
		}else {
			TLog(@"********* 1 This 's time to request");
			*cond = (loadresCOND *)RESCOND_REPREQ;
		}
		
		
	}else {TLog(@"********* 2 This 's time from Map");
		*cond = (loadresCOND *)RESCOND_DISKTOMAP;
	}
	
	return result;
}
#endif

#ifdef NEW_RES_MAPPING
//1.1.6
-(id)loadResItemUnconditionWithKey:(NSString*) key reSetResident:(BOOL) resident{
	NSURL *url = [NSURL URLWithString:key];
	NSString *relativeKey = key;//[url path];
	
	//V1.0.1 
	//	NSDictionary *dataDic = [_mapTable valueForKey:relativeKey];
	//	if (!dataDic) {
	//		TLog(@"********* 2 Uncondition");
	//		return nil;
	//	}
	
	
	id result = nil;
	result = [_resTable valueForKey:key];
    TLog(@"result = %@",result);
    //    result =  [_self_ _queryFromUrl:key];
    NSMutableDictionary *dataDic = nil;
    dataDic = [_self_ _queryFromUrl:relativeKey];
    //        TLog(@"dataDic = %@",dataDic);
	if (!result) {
		
		//V1.0.2 Renovate getRes Logic 
		
		if (!dataDic) {
			TLog(@"********* 2 Uncondition");
			return nil;
		}
		//////////////////////////////////
		
        
		NSString *fileName = [dataDic valueForKey:K_RES_MANAGE_NAME];
		NSString *fileCont = [dataDic valueForKey:K_RES_MANAGE_CONTENT];
		NSString *fileType = [dataDic valueForKey:K_RES_MANAGE_TYPE];
        //		NSInteger count = [[dataDic valueForKey:K_RES_MANAGE_FAVOR] intValue];
        //		++count;
        //		[dataDic setValue:[NSString stringWithFormat:@"%d",count] forKey:K_RES_MANAGE_FAVOR];
		
		NSString *filePath = [[_fm _getPathOfDirectory:FP_DOC ofFile:nil] stringByAppendingFormat:@"%@/%@.%@",T_RES_PACKAGE,fileName,fileType];
		TLog(@"disk to maping >> filePath=  %@",filePath);
		if ([fileCont isEqualToString:@"text"]||[fileCont isEqualToString:@"application"]){
			if ([fileType isEqualToString:@"xml"]) {
				
				NSData *data = [_fm readDataFrom:filePath];
				//		////TLog(@"data =  %@",data);
				if (data) {//////TLog(@"------- 1");
					id poolTmp = [NSKeyedUnarchiver unarchiveObjectWithData:data];//////TLog(@"------- 2");
					if ([poolTmp isKindOfClass:[NSArray class]]) {
						result = [[NSMutableArray	alloc] initWithArray:poolTmp];
					}else if ([poolTmp isKindOfClass:[NSDictionary class]]) {
						result = [[NSMutableDictionary alloc] initWithDictionary:poolTmp];
					}
					
					
				}
			}
			//			result = _feedTmp;
			//			TLog(@"2. result = %@",result);
		}else if ([fileCont isEqualToString:@"image"]){
			result = [UIImage imageWithContentsOfFile:filePath];
		}else if ([fileCont isEqualToString:@"___"]){
			
		}
        /*_*/		
#ifdef K_CACHE_L2
		[_resTable setValue:result forKey:relativeKey];
#endif		
        /*_*/		
	}
    //	else
    {
        //		NSMutableDictionary *dataDic = [_mapTable valueForKey:relativeKey];
        //		NSMutableDictionary *dataDic = [_self_ _queryFromUrl:relativeKey];
		NSInteger count = [[dataDic valueForKey:K_RES_MANAGE_FAVOR] intValue];
		++count;
		[dataDic setValue:[NSString stringWithFormat:@"%d",count] forKey:K_RES_MANAGE_FAVOR];
        if (resident) {
            
            [dataDic setValue:@"YES"  forKey:K_RES_MANAGE_RESIDENT];	
        }else{
            //            NSMutableDictionary *resiDic = [_self_ _queryFromUrl:relativeKey];//[_mapTable valueForKey:relativeKey];
            [dataDic setValue:@"NO"  forKey:K_RES_MANAGE_RESIDENT];	
        }
	}
	[dataDic release];
    
	
	return result;
}
#else
//<1.1.6
-(id)loadResItemUnconditionWithKey:(NSString*) key reSetResident:(BOOL) resident{
//	NSURL *url = [NSURL URLWithString:key];
	NSString *relativeKey = key;//[url path];
	
	//V1.0.1 
	//	NSDictionary *dataDic = [_mapTable valueForKey:relativeKey];
	//	if (!dataDic) {
	//		TLog(@"********* 2 Uncondition");
	//		return nil;
	//	}
	
	
	id result = nil;
	result = [_resTable valueForKey:key];
	if (!result) {
		
		//V1.0.2 Renovate getRes Logic 
		NSMutableDictionary *dataDic = [_mapTable valueForKey:relativeKey];
		if (!dataDic) {
			TLog(@"********* 2 Uncondition");
			return nil;
		}
		//////////////////////////////////
		
		//		TLog(@"dataDic = %@",dataDic);
		NSString *fileName = [dataDic valueForKey:K_RES_MANAGE_NAME];
		NSString *fileCont = [dataDic valueForKey:K_RES_MANAGE_CONTENT];
		NSString *fileType = [dataDic valueForKey:K_RES_MANAGE_TYPE];
		NSInteger count = [[dataDic valueForKey:K_RES_MANAGE_FAVOR] intValue];
		++count;
		[dataDic setValue:[NSString stringWithFormat:@"%d",count] forKey:K_RES_MANAGE_FAVOR];
//-_-? release		
		NSString *filePath = [[_fm _getPathOfDirectory:FP_DOC ofFile:nil] stringByAppendingFormat:@"%@/%@.%@",T_RES_PACKAGE,fileName,fileType];
//		TLog(@"disk to maping >> filePath=  %@",filePath);
		if ([fileCont isEqualToString:@"text"]||[fileCont isEqualToString:@"application"]){
			if ([fileType isEqualToString:@"xml"]||[fileType isEqualToString:@"json"]) {
				
				NSData *data = [_fm readDataFrom:filePath];
				//		////TLog(@"data =  %@",data);
				if (data) {//////TLog(@"------- 1");
					id poolTmp = [NSKeyedUnarchiver unarchiveObjectWithData:data];//////TLog(@"------- 2");
					if ([poolTmp isKindOfClass:[NSArray class]]) {
						result = [[NSMutableArray	alloc] initWithArray:poolTmp];
					}else if ([poolTmp isKindOfClass:[NSDictionary class]]) {
						result = [[NSMutableDictionary alloc] initWithDictionary:poolTmp];
					}
					
					
				}
			}
			//			result = _feedTmp;
			//			TLog(@"2. result = %@",result);
		}
        else if ([fileCont isEqualToString:@"image"]){
			result = [UIImage imageWithContentsOfFile:filePath];
		}else if ([fileCont isEqualToString:@"___"]){
			
		}
/*_*/		
#ifdef K_CACHE_L2
		[_resTable setValue:result forKey:relativeKey];
#endif		
/*_*/		
	}
	else {
		NSMutableDictionary *dataDic = [_mapTable valueForKey:relativeKey];
		NSInteger count = [[dataDic valueForKey:K_RES_MANAGE_FAVOR] intValue];
		++count;
		[dataDic setValue:[NSString stringWithFormat:@"%d",count] forKey:K_RES_MANAGE_FAVOR];
	}
	
	if (resident) {
		NSMutableDictionary *resiDic = [_mapTable valueForKey:relativeKey];
		[resiDic setValue:@"YES"  forKey:K_RES_MANAGE_RESIDENT];	
	}
	
	return result;//[result autorelease];
}
#endif
-(id)loadResItemUnconditionWithKey:(NSString*) key{TLog(@">>>>>>>>>>>> That's Time");
	id result = nil;
	result = [_self_ loadResItemUnconditionWithKey:key reSetResident:NO];
	return result;
}
//V1.0.2 Renovate getRes Logic
-(id)loadResItemWithKey:(NSString*) key andGetCondition:(loadresCOND**) cond{
	id result = nil;
	result = [_self_ loadResItemWithKey:key andGetCondition:(loadresCOND**) cond reSetResident:NO];
		return result;
}
//V1.0.1
//-(id)loadResItemWithKey:(NSString*) key andGetCondition:(loadresCOND**) cond{
////	loadresCOND &condition = *cond;
//	NSURL *url = [NSURL URLWithString:key];
//	NSString *relativeKey = key;//[url path];
//	id result = nil;
//
//	NSDictionary *dataDic = [_mapTable valueForKey:relativeKey];
//	if (!dataDic) {
//		TLog(@"********* 2");
////		loadresCOND *condd = *cond;
//		*cond = RESCOND_NEWREQ;
//		return nil;
//	}
//
//	double saveDate = [[dataDic valueForKey:K_RES_MANAGE_SAVEDATE] doubleValue];
//	double fileDate = [[dataDic valueForKey:K_RES_MANAGE_FILEDATE] doubleValue];
//	if ([NSDate timeIntervalSinceReferenceDate]>saveDate) {
//		if (fileDate<1.0) {
//			TLog(@"********* 0 has Data But request");
//			*cond = RESCOND_NEWREQ;
//		}else {
//			TLog(@"********* 1 This 's time to request");
//			*cond = RESCOND_REPREQ;
//		}
//
//		
//	}else {TLog(@"********* 2 This 's time from Map");
//		*cond = RESCOND_DISKTOMAP;
//	}
//	result =  [self loadResItemUnconditionWithKey:key];
//	return result;
//}
//1.1.6
#ifdef NEW_RES_MAPPING
-(NSMutableDictionary*)saveResItemUnconditionWithKey:(id) conOrFeed{
	NSMutableDictionary	*newDic = nil;
	KoreConn *conn = nil;
    //	NSDate	*howLongUpdate = nil;
	NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
	NSTimeInterval howLongUpdate = 0;
    //	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    //	NSDateComponents *comps = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:[NSDate date]];
    //	if (_mapTable) 
    {
		if ([conOrFeed isKindOfClass:[KoreConn class]]) {
			
            conn = (KoreConn*)conOrFeed;
            //			[comps setMinute:[comps minute]+K_REQUEST_NEXT_IMG];
            //			howLongUpdate = [gregorian dateFromComponents:comps];
			howLongUpdate =now+K_REQUEST_NEXT_IMG;
			
		}
		else if([conOrFeed isKindOfClass:[KoreFeedEngine class]]) {
			
			KoreFeedEngine *feed = (KoreFeedEngine*)conOrFeed;
			conn = [feed _mainConn];
			
            //			[comps setMinute:[comps minute]+K_REQUEST_NEXT_FEED];
            //			howLongUpdate = [gregorian dateFromComponents:comps];
			howLongUpdate =now+K_REQUEST_NEXT_FEED;
			
		}
        //		newDic = [_mapTable valueForKey:[[conn _url] path]];		TLog(@"1_ [conn _url] path] = %@",[[conn _url] path]);
        newDic = [_self_ _queryFromUrl:[conn _strRequest]];//[_mapTable valueForKey:[conn _strRequest]];		//TLog(@"1_ [conn _strRequest] = %@",[conn _strRequest]);
		if (newDic) {
			[newDic setValue:[NSString stringWithFormat:@"%f",howLongUpdate] forKey:K_RES_MANAGE_SAVEDATE];
			[newDic setValue:[NSString stringWithFormat:@"%f",now] forKey:K_RES_MANAGE_CLEANDATE];
            //			[newDic setValue:[howLongUpdate description] forKey:K_RES_MANAGE_SAVEDATE];
            //			[newDic setValue:[[conn _lastModified] description] forKey:K_RES_MANAGE_FILEDATE];	
			[newDic setValue:[NSString stringWithFormat:@"%f",[conn _lastModifiedSinceRefDate]] forKey:K_RES_MANAGE_FILEDATE];
			TLog(@".....Update.....");
			return nil;
		}
		else {
			newDic = [[NSMutableDictionary alloc] init];														//TLog(@"________ 3");
            //TLog(@"________ 3.1");			
			[newDic setValue:[conn _respContent]  forKey:K_RES_MANAGE_CONTENT];												//TLog(@"________ 3.2");
			[newDic setValue:[conn _respType]  forKey:K_RES_MANAGE_TYPE];														
			
			
            //			[newDic setValue:[[conn _lastModified] description] forKey:K_RES_MANAGE_FILEDATE];										//TLog(@"________ 3.4");
			[newDic setValue:[NSString stringWithFormat:@"%f",[conn _lastModifiedSinceRefDate]] forKey:K_RES_MANAGE_FILEDATE];
			
			
			[newDic setValue:[NSString stringWithFormat:@"%f",howLongUpdate] forKey:K_RES_MANAGE_SAVEDATE];
			[newDic setValue:[NSString stringWithFormat:@"%f",now] forKey:K_RES_MANAGE_CLEANDATE];
            //			[newDic setValue:[howLongUpdate description] forKey:K_RES_MANAGE_SAVEDATE];
			[newDic setValue:@"1" forKey:K_RES_MANAGE_FAVOR];	
			TLog(@".....Create.....");
			return newDic;
		}
        
		
	}
}
#else
//<1.1.6
-(NSMutableDictionary*)saveResItemUnconditionWithKey:(id) conOrFeed{
	NSMutableDictionary	*newDic = nil;
	KoreConn *conn = nil;
//	NSDate	*howLongUpdate = nil;
	NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
	NSTimeInterval howLongUpdate = 0;
//	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//	NSDateComponents *comps = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:[NSDate date]];
	if (_mapTable) {
		if ([conOrFeed isKindOfClass:[KoreConn class]]) {
			
			 conn = (KoreConn*)conOrFeed;
//			[comps setMinute:[comps minute]+K_REQUEST_NEXT_IMG];
//			howLongUpdate = [gregorian dateFromComponents:comps];
			howLongUpdate =now+K_REQUEST_NEXT_IMG;
			
		}
		else if([conOrFeed isKindOfClass:[KoreFeedEngine class]]) {
			
			KoreFeedEngine *feed = (KoreFeedEngine*)conOrFeed;
			conn = [feed _mainConn];
			
//			[comps setMinute:[comps minute]+K_REQUEST_NEXT_FEED];
//			howLongUpdate = [gregorian dateFromComponents:comps];
			howLongUpdate =now+K_REQUEST_NEXT_FEED;
			
		}
//		newDic = [_mapTable valueForKey:[[conn _url] path]];		TLog(@"1_ [conn _url] path] = %@",[[conn _url] path]);
        newDic = [_mapTable valueForKey:[conn _strRequest]];		//TLog(@"1_ [conn _strRequest] = %@",[conn _strRequest]);
		if (newDic) {
			[newDic setValue:[NSString stringWithFormat:@"%f",howLongUpdate] forKey:K_RES_MANAGE_SAVEDATE];
			[newDic setValue:[NSString stringWithFormat:@"%f",now] forKey:K_RES_MANAGE_CLEANDATE];
//			[newDic setValue:[howLongUpdate description] forKey:K_RES_MANAGE_SAVEDATE];
//			[newDic setValue:[[conn _lastModified] description] forKey:K_RES_MANAGE_FILEDATE];	
			[newDic setValue:[NSString stringWithFormat:@"%f",[conn _lastModifiedSinceRefDate]] forKey:K_RES_MANAGE_FILEDATE];
			TLog(@".....Update.....");
			return nil;
		}
		else {
			newDic = [[NSMutableDictionary alloc] init];														//TLog(@"________ 3");
																		//TLog(@"________ 3.1");			
			[newDic setValue:[conn _respContent]  forKey:K_RES_MANAGE_CONTENT];												//TLog(@"________ 3.2");
			[newDic setValue:[conn _respType]  forKey:K_RES_MANAGE_TYPE];														
			
			
//			[newDic setValue:[[conn _lastModified] description] forKey:K_RES_MANAGE_FILEDATE];										//TLog(@"________ 3.4");
			[newDic setValue:[NSString stringWithFormat:@"%f",[conn _lastModifiedSinceRefDate]] forKey:K_RES_MANAGE_FILEDATE];
			
			
			[newDic setValue:[NSString stringWithFormat:@"%f",howLongUpdate] forKey:K_RES_MANAGE_SAVEDATE];
			[newDic setValue:[NSString stringWithFormat:@"%f",now] forKey:K_RES_MANAGE_CLEANDATE];
//			[newDic setValue:[howLongUpdate description] forKey:K_RES_MANAGE_SAVEDATE];
			[newDic setValue:@"1" forKey:K_RES_MANAGE_FAVOR];	
			TLog(@".....Create.....");
			return newDic;
		}

		
	}
}
#endif

-(id)saveResItemWithKey:(id) conOrFeed andType:(respondTYPE) type cachingIs:(BOOL)caching residentIs:(BOOL)resident{
	//#define K_RES_MANAGE_NAME @"_fileName"
	//#define K_RES_MANAGE_CONTENT @"_fileContent"
	//#define K_RES_MANAGE_TYPE @"_fileType"
	//#define K_RES_MANAGE_FILEDATE @"_fileDate"
	//#define K_RES_MANAGE_SAVEDATE @"_saveDate"
	//#define K_RES_MANAGE_FAVOR @"_favor"		

	
		TLog(@"caching________ is = %@",(caching?@"YES":@"NO"));
	id result = nil;
	KoreConn *conn = nil;
	//	NSDate	*howLongUpdate = nil;
	//	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	//	NSDateComponents *comps = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:[NSDate date]];
	
	if (_mapTable) {TLog(@"________ _mapTable has");
		if ([conOrFeed isKindOfClass:[KoreConn class]]) {TLog(@"________ KoreConn has 1");
			conn = (KoreConn*)conOrFeed;
			
			//[comps setMinute:[comps minute]+3];
			//			howLongUpdate = [gregorian dateFromComponents:comps];
			
		}
		else if([conOrFeed isKindOfClass:[KoreFeedEngine class]]) {TLog(@"________ KoreFeedEngine has 1");
			KoreFeedEngine *feed = (KoreFeedEngine*)conOrFeed;
			conn = [feed _mainConn];
			
			//[comps setMinute:[comps minute]+1];
			//			howLongUpdate = [gregorian dateFromComponents:comps];
			
		}
		NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
		
		
		[dateFormatter setDateFormat:@"yyMMddHHmmss"];
		[dateFormatter setLocale:[NSLocale systemLocale]];
#ifdef NEW_TIME_TO_HASH
        KoreCryptonite *kc = [KoreCryptonite _getInstance];
        NSString *tmpFile = [kc md5:[conn _strRequest]];
        NSString *fileName = [NSString stringWithFormat:@"%@%d",tmpFile ,(arc4random()%1000)];											        

#else
		NSString *fileName = [NSString stringWithFormat:@"%@%d",[dateFormatter stringFromDate:[NSDate date]] ,(arc4random()%10000)];											        
#endif
					TLog(@"________ 2 = fileName = %@",fileName);
		
		
		NSString *fileNameExt = [NSString stringWithFormat:@"%@.%@",fileName,[conn _respType]];								TLog(@"________ 5 fileNameExt = %@",fileNameExt);
		id Obj = nil;
        if ([conn _progress]<1.0) {

        }else{
            Obj = [_resTable valueForKey:[conn _strRequest]];
        }

		if (Obj) {
//			[_garBagePool addObject:Obj];
			
		}
		if ([conOrFeed isKindOfClass:[KoreConn class]]) {
			KoreConn *conn = (KoreConn*)conOrFeed;
			//			dispatch_queue_t		qRes = dispatch_queue_create("KAL.RES_MANAGER.queue", NULL);
			//			dispatch_sync(qRes, 
			//						  ^{
			//			[self _saveFileWithName:fileNameExt andData:[conn _receivedData]];	
			//						  }
			//						  );
			//			dispatch_release(qRes);
			if (caching) {
				[self _saveFileWithName:fileNameExt andData:[conn _receivedData]];	
			}
			
/*_*/		
#ifdef K_CACHE_L2
			if (type==RESPTYPE_IMG) {
				
//				UIImage *img = [UIImage imageWithData:[conn _receivedData]];
                UIImage *img = [[UIImage alloc] initWithData:[conn _receivedData]];
				[_resTable setValue:img forKey:[conn _strRequest]];
				
				
			}
            else if(type==RESPTYPE_DATA){
				
				
				[_resTable setValue:[conn _receivedData] forKey:[conn _strRequest]];
			}
#endif
/*_*/			
		}
		else if([conOrFeed isKindOfClass:[KoreFeedEngine class]]) {TLog(@"________ KoreFeedEngine has 2");
//			KoreFeedEngine *feed = (KoreFeedEngine*)conOrFeed;
			
			if (caching) {
				if ([conOrFeed _resultDic]) {
					NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[conOrFeed _resultDic]];
					//			dispatch_queue_t		qRes = dispatch_queue_create("KAL.RES_MANAGER.queue", NULL);
					//			dispatch_sync(qRes, 
					//						  ^{
					//			[self _saveFileWithName:fileNameExt andData:data];	
					//						  }
					//						  );
					//			dispatch_release(qRes);
					
					[self _saveFileWithName:fileNameExt andData:data];
				}


			}
			if (type==RESPTYPE_DIC) {TLog(@"________ KoreFeedEngine RESPTYPE_DIC");
/*_*/		
#ifdef K_CACHE_L2				
//				NSMutableDictionary *muDic = [[NSMutableDictionary alloc] initWithDictionary:[conOrFeed _resultDic]];
				[_resTable setValue:[conOrFeed _resultDic] forKey:[conn _strRequest]];
#endif
/*_*/               
			}
		}
		
//Write Mapping Overhead by url is a key and value is filepath
        
//1.1.6
#ifdef NEW_RES_MAPPING        
        if (caching) {
            NSMutableDictionary *newDic = [_self_ saveResItemUnconditionWithKey:conOrFeed];
            
            [newDic setValue:[conn _strRequest] forKey:K_RES_MANAGE_PK];
            if (newDic&&resident) {
                
                [newDic setValue:@"YES"  forKey:K_RES_MANAGE_RESIDENT];	
                
                
            }else if (newDic) {
                
                [newDic setValue:@"NO"  forKey:K_RES_MANAGE_RESIDENT];	
                
            }
            [newDic setValue:fileName  forKey:K_RES_MANAGE_NAME];	                
            //                    [_mapTable setValue:newDic forKey:[conn _strRequest]];		//TLog(@"2_ [[conn _strRequest] = %@",[conn _strRequest]);
            
            NSArray *infoList = [[NSArray  alloc] initWithObjects:newDic, nil];
            
            [_self_ _insertOrUpdateWithList:infoList];
            [infoList release];
            [newDic release];
        }
#else
//<1.1.6        
			if (caching) {
				NSMutableDictionary *newDic = [_self_ saveResItemUnconditionWithKey:conOrFeed];

				
				if (newDic&&resident) {
					[newDic setValue:fileName  forKey:K_RES_MANAGE_NAME];	
					[newDic setValue:@"YES"  forKey:K_RES_MANAGE_RESIDENT];	
					[_mapTable setValue:newDic forKey:[conn _strRequest]];		//TLog(@"2_ [[conn _strRequest] = %@",[conn _strRequest]);
                    [newDic release];
                    newDic = nil;
				}else if (newDic) {
					[newDic setValue:fileName  forKey:K_RES_MANAGE_NAME];	
					[newDic setValue:@"NO"  forKey:K_RES_MANAGE_RESIDENT];	
					[_mapTable setValue:newDic forKey:[conn _strRequest]];		//TLog(@"2_ [[conn _strRequest] = %@",[conn _strRequest]);
                    [newDic release];
                    newDic = nil;
				}

			}
#endif
		}
#ifdef K_CACHE_L2
	result = [_resTable valueForKey:[conn _strRequest]];
#else
    if (type==RESPTYPE_IMG) {

        UIImage *img = [[UIImage alloc] initWithData:[conn _receivedData]];//[UIImage imageWithData:[conn _receivedData]];
//        [_resTable setValue:img forKey:[conn _strRequest]];
        TLog(@"RESPTYPE_IMG = %@",img);
        result =  img;
    }
    else if(type==RESPTYPE_DATA){
        
        result =  [conn _receivedData];

    }
    else if(type==RESPTYPE_DIC){
        result = [conOrFeed _resultDic];
    }
#endif
	return result;
	
}
-(id)saveResItemWithKey:(id) conOrFeed andType:(respondTYPE) type cachingIs:(BOOL)caching{TLog(@"caching = %@",(caching?@"yes":@"no"));
	id result = nil;
	result = [_self_ saveResItemWithKey:conOrFeed andType:type cachingIs:YES residentIs:NO];
	return result;
}
-(id)saveResItemWithKey:(id) conOrFeed andType:(respondTYPE) type{			//Dic have to contain keyName and fileDate
	id result = nil;
	result = [_self_ saveResItemWithKey:conOrFeed andType:type cachingIs:YES];
	return result;
}
-(void)saveMappingTable{
	dispatch_queue_t		qConn = dispatch_queue_create("KAL.RES_MANAGER.queue", NULL);
	dispatch_sync(qConn, 
				  ^{
					TLog(@"___________________________> runDiskGarbage");	
					  [_self_ _cleanDisk];
					  TLog(@"___________________________> _saveMap");
  					  [_self_ _saveMap];
					  
				  }
				  );
	dispatch_release(qConn);
	
}
-(void)runDiskGarbage{
//	dispatch_queue_t		qConn = dispatch_queue_create("KAL.RES_MANAGER.queue", NULL);
//	dispatch_sync(qConn, 
//				  ^{
//					  [_self_ _cleanDisk];
//
//				  }
//				  );
//	dispatch_release(qConn);
}
-(void)runHeapGarbage{TLog(@"_______ 1");
	[_resTable removeAllObjects];
//	NSArray *keyList = [_resTable allKeys];
//	NSArray *valList = [_resTable allValues];TLog(@"_______ keyCount = %d",[keyList count]);
//	for (NSInteger i=0; i<[keyList count];i++) {
//		id someThing = [_resTable valueForKey:[keyList objectAtIndex:i]];
//		[someThing release];
//	}TLog(@"_______ 2");
//	
////	[_resTable release];
////	_resTable = nil;
//	[keyList removeAllObjects];
//	[valList removeAllObjects];
	TLog(@"_______ 3");
}

//V1.0.6 minor for theOne
-(NSString*)byPassKey:(NSString*)key{
	return @"";
}


//-(void)markCacheUpdateWithKey:(NSString*) key{
//	
//}
#pragma mark -
#pragma mark KoreFeedEngineDelegate
-(void)feedDidFinish:(KoreFeedEngine *) kFeed{
	_feedTmp = [kFeed _resultDic];
//	TLog(@"_feedTmp = %@",_feedTmp);
}
-(void)feedDidFailed:(KoreFeedEngine *) kFeed{
	
}
#pragma mark -
#pragma mark ________________ADDITIONAL
#pragma mark - V1.1.6 main update core resouce from plist to sqlite
-(void)_startUpdateFromInterval{
    
}
-(void)_insertOrUpdateWithList:(NSArray*)list{
    NSString *path = [_fm _getPathOfDirectory:FP_DOC ofFile:[NSString stringWithFormat:@"%@/%@",T_RES_PACKAGE,T_RES_MANAGE_MAPFILE]];
    TLog(@"_insertOrUpdateWithList_path = %@",path);
    [__DBMS__ openDB:path forceOpen:YES];
    
    
    if([__DBMS__ openTBname:@"mapLinkWithSorce" forceOpenCmd:
        @"CREATE TABLE mapLinkWithSorce ( _fromUrl TEXT PRIMARY KEY, _fileName TEXT, _fileContent TEXT, _fileType TEXT, _fileDate TEXT, _saveDate TEXT, _cleanDate TEXT, _favor TEXT, _resident TEXT )"]){TLog(@"______________ _insertOrUpdateWithList");
        [__DBMS__ insertOrReplaceByCmd:@"INSERT OR REPLACE INTO mapLinkWithSorce (  _fromUrl , _fileName, _fileContent, _fileType, _fileDate, _saveDate, _cleanDate, _favor, _resident) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)" fromDataSet:list];        
        
    }
    //    [_dbm_keySearch openDB:path forceOpen:YES];
    //    
    //    
    //    if([_dbm_keySearch openTBname:@"friendList" forceOpenCmd:@"CREATE TABLE friendList (id INTEGER PRIMARY KEY  NOT NULL , user_id INTEGER, friend_id INTEGER, name TEXT, first_name TEXT, last_name TEXT, image TEXT)"]){TLog(@"______________ addHistory");
    //        
    //        [_dbm_keySearch insertOrReplaceByCmd:@"INSERT OR REPLACE INTO friendList (  user_id, friend_id, name, first_name, last_name, image) VALUES (?, ?, ?, ?, ?, ?)" fromDataSet:fList];
    //    }
    
    
}
-(NSMutableDictionary*)_queryFromUrl:(NSString*)url{
//    TLog(@"___From _queryFromUrl = %@",url);
    NSString *path = [_fm _getPathOfDirectory:FP_DOC ofFile:[NSString stringWithFormat:@"%@/%@",T_RES_PACKAGE,T_RES_MANAGE_MAPFILE]];
    [__DBMS__ openDB:path forceOpen:NO];
    
    
    if([__DBMS__ openTBname:@"mapLinkWithSorce" forceOpenCmd:nil]){TLog(@"______________ cataSearch");
        NSMutableArray *result = [[NSMutableArray alloc] init];
        NSString *cmd = [NSString stringWithFormat:@"select * from mapLinkWithSorce where _fromUrl = '%@'",url];// ORDER BY ASC
        //        TLog(@"cmd = %@",cmd);
        [__DBMS__ queryByCmd:cmd forGetType:nil toDic:&result];   
        //        TLog(@"result_list = %@",result);
        if ([result count]>0) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[result objectAtIndex:0]];        
            [result release];
            if ([dic allKeys]>0) {
//                TLog(@"___From Map table = %@",dic);
                return  dic;                
            }else{
                return nil;
            }
            
        }
    }
    return nil;
    
}
@end
