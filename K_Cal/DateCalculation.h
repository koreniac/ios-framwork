//
//  DateCalculation.h
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 11/23/11.
//  Copyright (c) 2011 MonsterMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateCalculation : NSObject{
    
}
+(BOOL)nowInTimeGap:(NSArray*)timelist;
+(NSString*)hourIntimeGap;
+(NSString*)displayDateFromList:(NSArray*)timelist;
+ (NSDate*) convertToUTC:(NSDate*)sourceDate;
+ (NSString*)convertToDateYMD:(NSDate*)date;
+(NSString *)timeSinceFromTime:(NSString *)seconds;
+(NSString *)convertStrDateToMMMddYYYY:(NSString *)dateStr;
+(NSString *)convertStrDateToPrettyTime:(NSString *)dateStr;
+(NSString *)convertStrDateToPrettyTime_V2:(NSString *)dateStr;
+(NSInteger)getGMT;
@end
