//
//  DateCalculation.m
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 11/23/11.
//  Copyright (c) 2011 MonsterMedia. All rights reserved.
//

#import "DateCalculation.h"
static NSMutableString *nowHours = nil;
@implementation DateCalculation
+(BOOL)nowInTimeGap:(NSArray*)timelist{
    BOOL result = NO;
//////////////////////////////////////////////////////////////////////////////////////
//    hours =                 (
//                             {
//                                 day = Thu;
//                                 end = 17;
//                                 start = 10;
//                             },
//                             {
//                                 day = Thu;
//                                 end = 21;
//                                 start = 4;
//                             }
//                             );
//______________________________________________________________________ CheckDate
    NSDateFormatter *    sRFC3339DateFormatter = nil;
//    NSString *                  userVisibleDateTimeString;
    NSDate *                    date = [NSDate date];
	
    // If the date formatters aren't already set up, do that now and cache them 
    // for subsequence reuse.
	
//    if (sRFC3339DateFormatter == nil) {
        NSLocale *                  enUSPOSIXLocale;
		
        sRFC3339DateFormatter = [[NSDateFormatter alloc] init];
        assert(sRFC3339DateFormatter != nil);
		
        enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];
        assert(enUSPOSIXLocale != nil);
		
        [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
        [sRFC3339DateFormatter setDateFormat:@"eee"];//@"eee, dd MMM yyyy HH:mm:ss ZZ"
        NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];
        NSTimeZone* utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
        
        NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date];
        NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date];
        NSTimeInterval gmtInterval = gmtOffset - currentGMTOffset;
        TLog(@"currentGMTOffset  = %d",currentGMTOffset);
        TLog(@"gmtOffset  = %d",gmtOffset);
        TLog(@"gmtInterval = %f",gmtInterval);
        
        
        [sRFC3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:currentGMTOffset]];
        NSString *whatDate = [sRFC3339DateFormatter stringFromDate:[NSDate date]];
        TLog(@"whatDate = %@",whatDate);
        NSInteger count = [timelist count];
    NSString *start = nil;
    NSString *end = nil;
    NSMutableArray *timeList = [[NSMutableArray alloc] init];
        for (NSInteger i=0; i<count; i++) {
            NSDictionary *dic = [timelist objectAtIndex:i];
            if ([whatDate isEqualToString:[dic valueForKey:@"day"]]) {
                result = YES;
                start = [[dic valueForKey:@"start"] retain];
                end = [[dic valueForKey:@"end"] retain];
                NSDictionary *dic  = [[NSDictionary alloc] initWithObjectsAndKeys:start,@"start",end,@"end", nil];
                [timeList addObject:dic];
                [dic release];
            }
        }
//    [whatDate release];
//    whatDate = nil;
    TLog(@">>>>timeList = %@",timeList);
        if (!result) {
//            [currentTimeZone release];
//            [utcTimeZone release];
            [sRFC3339DateFormatter release];
//            [enUSPOSIXLocale release];
            [timeList release];
//            [nowHours release];
            sRFC3339DateFormatter = nil;
            enUSPOSIXLocale = nil;
            timeList = nil;
            currentTimeZone = nil;
            utcTimeZone = nil;
            
            return result;
        }
//    }
	
//______________________________________________________________________ CheckTime    
    
    [sRFC3339DateFormatter setDateFormat:@"HH"];//@"eee, dd MMM yyyy HH:mm:ss ZZ"
    [sRFC3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:currentGMTOffset]];
    NSString *whatTime = [sRFC3339DateFormatter stringFromDate:[NSDate date]];
    TLog(@"whatTime = %@",whatTime);
    NSInteger nowHour = [whatTime intValue];
    NSInteger startHour = [start intValue];
    NSInteger endHour = [end intValue];
    if (nowHour>=startHour&&nowHour<=endHour) {
        result = YES;
        nowHours = [[NSMutableString alloc] initWithString:@""];
        for (NSInteger i=0; i<[timeList count]; i++) {
            NSDictionary *dic = [timeList objectAtIndex:i];
            NSMutableString *start = [[NSMutableString alloc] initWithString:@""];
            NSMutableString *end = [[NSMutableString alloc] initWithString:@""];
            NSArray *startList = [[dic valueForKey:@"start"] componentsSeparatedByString:@"."];
            NSArray *endList = [[dic valueForKey:@"end"] componentsSeparatedByString:@"."];            
            TLog(@"endList = %@",endList);
            if ([startList count]>1) {
                CGFloat minite = [[startList objectAtIndex:1] floatValue];
                NSInteger newMinite = (60*minite)/10;
                [start appendFormat:@"%@:%@",[startList objectAtIndex:0],[NSString stringWithFormat:@"%d",newMinite]];
            }else{
                [start appendFormat:@"%@:00",[startList objectAtIndex:0]];     
            }
            if ([endList count]>1) {
                CGFloat minite = [[endList objectAtIndex:1] floatValue];
                NSInteger newMinite = (60*minite)/10;
                [end appendFormat:@"%@:%@",[endList objectAtIndex:0],[NSString stringWithFormat:@"%d",newMinite]];
            }else{
                [end appendFormat:@"%@:00",[endList objectAtIndex:0]];     
            }           
            if ([start length]<5) {
                [start insertString:@"0" atIndex:0];
            }
            if ([end length]<5) {
                [end insertString:@"0" atIndex:0];
            }
            if (i>0) {
                [nowHours appendString:@", "];
            }
            [nowHours appendString:[NSString stringWithFormat:@"%@ - %@",start,end]];
            [start release];
            [end release];
            [startList release];
            [endList release];
            start = nil;
            end = nil;
            startList = nil;
            endList = nil;

            
        }
    }else{
        result = NO;
        nowHours = nil;
    }
    TLog(@">>>>nowHour = %@",nowHours);
//    [currentTimeZone release];
//    [utcTimeZone release];
    [sRFC3339DateFormatter release];
//    [enUSPOSIXLocale release];
    [timeList release];
//    [nowHours release];
    [whatTime release];
    sRFC3339DateFormatter = nil;
    enUSPOSIXLocale = nil;
    timeList = nil;
    currentTimeZone = nil;
    utcTimeZone = nil;
    whatTime = nil;
//    nowHours = nil;
//////////////////////////////////////////////////////////////////////////////////////
    return  result;
}
+(NSString*)hourIntimeGap{
    NSString *result = [nowHours retain];
    TLog(@"hourIntimeGap_result = %@",result);
    [nowHours release];
    return result;
}
+(NSString*)displayDateFromList:(NSArray*)timelist{
    NSMutableString *result = nil;
    //Group Time
    NSInteger count = [timelist count];
    NSMutableDictionary *timeDic = [[NSMutableDictionary alloc] init];
    for (NSInteger i=0; i<count;i++) {
        NSDictionary *dic = [timelist objectAtIndex:i];
        NSMutableString *start = [[NSMutableString alloc] initWithString:@""];
        NSMutableString *end = [[NSMutableString alloc] initWithString:@""];
        NSArray *startList = [[dic valueForKey:@"start"] componentsSeparatedByString:@"."];
        NSArray *endList = [[dic valueForKey:@"end"] componentsSeparatedByString:@"."];            
        
        if ([startList count]>1) {
            NSInteger minite = [[startList objectAtIndex:1] intValue];
            NSInteger newMinite = (60*minite)/10;
            [start appendFormat:@"%@:%@",[startList objectAtIndex:0],[NSString stringWithFormat:@"%d",newMinite]];
        }else{
            [start appendFormat:@"%@:00",[startList objectAtIndex:0]];     
        }
        if ([endList count]>1) {
            NSInteger minite = [[endList objectAtIndex:1] intValue];
            NSInteger newMinite = (60*minite)/10;
            [end appendFormat:@"%@:%@",[endList objectAtIndex:0],[NSString stringWithFormat:@"%d",newMinite]];
        }else{
            [end appendFormat:@"%@:00",[endList objectAtIndex:0]];     
        }           
        if ([start length]<5) {
            [start insertString:@"0" atIndex:0];
        }
        if ([end length]<5) {
            [end insertString:@"0" atIndex:0];
        }
        NSString *time = [[NSString alloc] initWithFormat:@"%@-%@",start,end];
        NSString *date = [dic valueForKey:@"day"];
        NSString *haveTime = [timeDic valueForKey:time];
                        NSString *newDate = @"";
//        TLog(@"x-x time = %@",time);
        if (haveTime&&[haveTime isKindOfClass:[NSString class]]&&![haveTime isEqualToString:@"(null)"]) {
                            NSArray *dayList = [NSArray arrayWithObjects:@"Mon",@"Tue",@"Wed",@"Thu",@"Fri",@"Sat",@"Sun", nil];
            NSArray *haveTimeList = [haveTime componentsSeparatedByString:@"-"];
            if ([haveTimeList count]>1) {
                NSInteger dayIndexFromFirst = [dayList indexOfObject:[haveTimeList objectAtIndex:0]];
                NSInteger dayIndexFromLast = [dayList indexOfObject:[haveTimeList objectAtIndex:[haveTimeList count]-1]];
                NSInteger dayIndexFromList = [dayList indexOfObject:date];       
                if (dayIndexFromList>dayIndexFromLast) {
                    newDate = [NSString stringWithFormat:@"%@-%@",[haveTimeList objectAtIndex:0],date];
                }else if(dayIndexFromList<dayIndexFromFirst){
                    newDate = [NSString stringWithFormat:@"%@-%@",date,[haveTimeList objectAtIndex:[haveTimeList count]-1]];
                }else{
                    break;
                }
                [timeDic setValue:newDate forKey:time];                   
            }else{

                NSInteger dayIndexFromDic = [dayList indexOfObject:haveTime];
                NSInteger dayIndexFromList = [dayList indexOfObject:date];

                if (dayIndexFromDic>dayIndexFromList) {
                    newDate = [NSString stringWithFormat:@"%@-%@",date,haveTime];
                }else{
                    newDate = [NSString stringWithFormat:@"%@-%@",haveTime,date];                
                }
                [timeDic setValue:newDate forKey:time];                   
            }
//            TLog(@"newDate = %@",newDate);
        }else{
            [timeDic setValue:date forKey:time];            
//                        TLog(@"date = %@",date);
        }

        
    }
    NSArray *allKey = [timeDic allKeys];
    if ([allKey count]>0) {
        result = [[NSMutableString alloc] initWithString:@""];
    }
    for (NSInteger i=0; i<[allKey count];i++) {
        NSString *key = [allKey objectAtIndex:i];
        NSString *value = [timeDic valueForKey:key];
        if (i>0) {
            [result appendString:@",\r\n"];
        }
        [result appendFormat:@"%@ %@",key,value];
    }
//    TLog(@"timeDic = %@",timeDic);
    //Group Date
    
    return result;
}
+ (NSDate*) convertToUTC:(NSDate*)sourceDate
{
    NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone* utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval gmtInterval = gmtOffset - currentGMTOffset;
    
    NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:sourceDate] autorelease];     
    return destinationDate;
}
+ (NSString*)convertToDateYMD:(NSDate*)date{
    NSString *result = nil;
    NSDateFormatter *sRFC3339DateFormatter = [[NSDateFormatter alloc] init];
    assert(sRFC3339DateFormatter != nil);
    
    NSLocale *enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];
    assert(enUSPOSIXLocale != nil);
    
    [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
    [sRFC3339DateFormatter setDateFormat:@"MMM dd, yyyy"];//@"eee, dd MMM yyyy HH:mm:ss ZZ"
//    NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];
//    NSTimeZone* utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
//    
//    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date];
//    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date];
//    NSTimeInterval gmtInterval = gmtOffset - currentGMTOffset;
//    TLog(@"currentGMTOffset  = %d",currentGMTOffset);
//    TLog(@"gmtOffset  = %d",gmtOffset);
//    TLog(@"gmtInterval = %f",gmtInterval);
//    
//    
//    [sRFC3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:currentGMTOffset]];
    result = [sRFC3339DateFormatter stringFromDate:date];
    return  result;
}
+(NSString *)timeSinceFromTime:(NSString *)seconds{
    NSString *result = nil;
    
//    double seconds2 = [seconds doubleValue];
//    NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds2];
//    NSDate *now = [NSDate date];
//    double start = [date timeIntervalSince1970];
//    double end = [now timeIntervalSince1970];
    double end = [[NSDate date] timeIntervalSince1970];
    double start = ([seconds doubleValue]);
    double difference = (end - start);
//    difference = round(difference);
    NSInteger minutes = difference / 60;
    NSInteger hours = minutes / 60;
    NSInteger days = hours / 24;
    NSInteger weeks = days / 7;
    NSInteger months = weeks / 5;
    NSInteger years = months / 12;

    if(difference < 0){
        result = [NSString stringWithFormat:@"%d seconds ago",1];
    }else if(difference < 60){
        result = [NSString stringWithFormat:@"%.0f seconds ago",difference];
    }else if (minutes > 1 && minutes < 60) {
        result = [NSString stringWithFormat:@"%d minutes ago",minutes];
    }else if (hours > 1 && hours < 24) {
        result = [NSString stringWithFormat:@"%d hours ago",hours];
    }else if (days > 1 && days < 7) {
        result = [NSString stringWithFormat:@"%d days ago",days];
    }else if (weeks > 1 && weeks < 5) {
        result = [NSString stringWithFormat:@"%d weeks ago",weeks];
    }else if (months > 1  && months < 12) {
        result = [NSString stringWithFormat:@"%d months ago",months];
    }else if (years > 1 && years < 12) {
        result = [NSString stringWithFormat:@"%d years ago",years];
    }else if(minutes==1){
        result = @"last minute ago";
    }else if(hours==1){
        result = @"last minute ago";
    }else if(days==1){
        result = @"last minute ago";
    }else if(weeks==1){
        result = @"last minute ago";
    }else if(months==1){
        result = @"last minute ago";
    }else if(years==1){
        result = @"last minute ago";
    }
    return result;
}
+(NSString *)convertStrDateToMMMddYYYY:(NSString *)dateStr{
    NSString *result = nil;
    if ([dateStr isKindOfClass:[NSNull class]]) {
        return @"";
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:dateStr];
    NSLog(@"convertFromdate:%@",dateStr); // result date:(null)
//    NSLog(@"convertFromdate:%@",newDateStr); // result date:(null)
    NSLog(@"convertFromdate:%@",date); // result date:(null)
    
    
    NSDateFormatter *sRFC3339DateFormatter = [[NSDateFormatter alloc] init];
    assert(sRFC3339DateFormatter != nil);

    //[NSLocale currentLocale];//
    NSLocale *enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];//en_US_POSIX
    TLog(@"Current Locale: %@", [enUSPOSIXLocale localeIdentifier]);
    assert(enUSPOSIXLocale != nil);

    NSCalendar *cld = [NSCalendar currentCalendar];
    TLog(@"Current NSCalendar: %@", [cld calendarIdentifier]);
    [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
//    [sRFC3339DateFormatter setTimeZone:tz];
    [sRFC3339DateFormatter setCalendar:cld];
    [sRFC3339DateFormatter setDateFormat:@"MMM dd, yyyy"];//@"eee, dd MMM yyyy HH:mm:ss ZZ"
    //    NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];
    //    NSTimeZone* utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    //    
    //    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date];
    //    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date];
    //    NSTimeInterval gmtInterval = gmtOffset - currentGMTOffset;
    //    TLog(@"currentGMTOffset  = %d",currentGMTOffset);
    //    TLog(@"gmtOffset  = %d",gmtOffset);
    //    TLog(@"gmtInterval = %f",gmtInterval);
    //    
    //    
    //    [sRFC3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:currentGMTOffset]];
    result = [sRFC3339DateFormatter stringFromDate:date];
    return  result; 
}
+(NSInteger)getGMT{
    NSInteger result = 0;
    
    TLog(@"NSDate = %@",[[NSDate date] description]);
    NSDateFormatter* dft = [[NSDateFormatter alloc] init];
    [dft setDateFormat:@"Z"]; // error in documentation - the zone in hours should be 'z' not 'Z'
    //    timezoneoffset = (int) [[dft stringFromDate:[NSDate date]] intValue]/100;
    NSString *strGMT = [dft stringFromDate:[NSDate date]];
    TLog(@"format %@",strGMT );
    NSRange rangeSign;
    rangeSign.location = 0;
    rangeSign.length = 1;
    NSRange rangeValue;
    rangeValue.location = 1;
    rangeValue.length = 2;
    NSRange rangeDecimal;
    rangeDecimal.location = 3;
    rangeDecimal.length = 2;
    NSString *signInt = [strGMT substringWithRange:rangeSign];
    NSString *value = [strGMT substringWithRange:rangeValue];
    NSString *deci = [strGMT substringWithRange:rangeDecimal];
    [dft release];
    result = ([value intValue]*60)+[deci intValue];
    if ([signInt isEqualToString:@"-"]) {
        result *= -1;
    }
    return result;
}
+(NSString *)convertStrDateToPrettyTime_V2:(NSString *)dateStr{
    NSString *result = nil;
    TLog(@"dateStr :%@",dateStr);
    TLog(@"getGMT : %d",[DateCalculation getGMT]);
    if (!dateStr||![dateStr isKindOfClass:[NSString class]]) {
        return @"";
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssz':'00"];
    //yyyy-MM-dd'T'HH:mm:ss
    //yyyy-MM-dd
    NSLocale *enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];//en_US_POSIX
    TLog(@"Current Locale: %@", [enUSPOSIXLocale localeIdentifier]);
    assert(enUSPOSIXLocale != nil);
    
    NSCalendar *cld = [NSCalendar currentCalendar];
     NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];//systemTimeZone
    TLog(@"Current NSTimeZone: %@", [currentTimeZone description]);
    TLog(@"Current NSCalendar: %@", [cld calendarIdentifier]);
    [dateFormatter setLocale:enUSPOSIXLocale];
//    [dateFormatter setTimeZone:currentTimeZone];
//    [dateFormatter setCalendar:cld];

    
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:dateStr];
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"ICT"];//EST ICT
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:date];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:date];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:interval sinceDate:date] autorelease];
    NSTimeInterval intervalSince = [destinationDate timeIntervalSince1970];
    NSString *strInterval = [NSString stringWithFormat:@"%f",intervalSince];
    TLog(@"strInterval  =%@",strInterval);
    NSString *pretty = [DateCalculation timeSinceFromTime:strInterval];
    TLog(@"pretty  =%@",pretty);
    result = [[NSString alloc] initWithString:pretty];
    TLog(@"result  =%@",result);
    return result;

}

+(NSString *)convertStrDateToPrettyTime:(NSString *)dateStr{
    NSString *result = nil;
    TLog(@"dateStr :%@",dateStr);
    if (!dateStr||![dateStr isKindOfClass:[NSString class]]) {
        return @"";
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssz':'00"];
    //yyyy-MM-dd'T'HH:mm:ss
    //yyyy-MM-dd
    NSLocale *enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];//en_US_POSIX
    TLog(@"Current Locale: %@", [enUSPOSIXLocale localeIdentifier]);
    assert(enUSPOSIXLocale != nil);
    
    NSCalendar *cld = [NSCalendar currentCalendar];
     NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];//systemTimeZone
    TLog(@"Current NSTimeZone: %@", [currentTimeZone description]);
    TLog(@"Current NSCalendar: %@", [cld calendarIdentifier]);
    [dateFormatter setLocale:enUSPOSIXLocale];
//    [dateFormatter setTimeZone:currentTimeZone];
//    [dateFormatter setCalendar:cld];
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:dateStr];
    TLog(@"convertFromdate:%@",dateStr); // result date:(null)
    //    NSLog(@"convertFromdate:%@",newDateStr); // result date:(null)
    TLog(@"convertFromdate:%@",date); // result date:(null)
    
    
//    NSDateFormatter *sRFC3339DateFormatter = [[NSDateFormatter alloc] init];
//    assert(sRFC3339DateFormatter != nil);
//    
//    //[NSLocale currentLocale];//
//    NSLocale *enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];//en_US_POSIX
//    TLog(@"Current Locale: %@", [enUSPOSIXLocale localeIdentifier]);
//    assert(enUSPOSIXLocale != nil);
//    
//    NSCalendar *cld = [NSCalendar currentCalendar];
//    NSTimeZone* tz = [NSTimeZone localTimeZone];
//    TLog(@"Current NSCalendar: %@", [cld calendarIdentifier]);
//    [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
//    [sRFC3339DateFormatter setTimeZone:tz];
//    [sRFC3339DateFormatter setCalendar:cld];
//    [sRFC3339DateFormatter setDateFormat:@"MMM dd, yyyy"];//@"eee, dd MMM yyyy HH:mm:ss ZZ"
//
//
//    result = [sRFC3339DateFormatter stringFromDate:date];
    NSTimeInterval interval = [date timeIntervalSince1970];
    NSString *strInterval = [NSString stringWithFormat:@"%f",interval];
    TLog(@"strInterval  =%@",strInterval);
    NSString *pretty = [DateCalculation timeSinceFromTime:strInterval];
    TLog(@"pretty  =%@",pretty);
    result = [[NSString alloc] initWithString:pretty];
    TLog(@"result  =%@",result);
    return result;
}
@end
