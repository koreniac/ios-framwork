//
//  GeoCalculation.h
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 11/16/11.
//  Copyright (c) 2011 MonsterMedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#define RATIO_ONE_WIDTHMAP 320.0
#define RATIO_ONE_HEIGHTMAP 920.0
#define DEG_TO_RAD 0.017453292519943295769236907684886
#define EARTH_RADIUS_IN_METERS 6372797.560856
#define YARD_TO_METRE  0.9144
#define METRE_TO_YARD  1.0936133
#define METRE_TO_MILE  0.000621371192

@interface GeoCalculation : NSObject
////~~~~~~~~~~~~~~~~~~~~~~~~~~~ Calculate Distance from real lat long and return Metres
//+(double)DistanceInMeters:(CGPoint) ptSt :(CGPoint)ptNd;
//
////~~~~~~~~~~~~~~~~~~~~~~~~~~~
//+(float)ArcInRadians:(CGPoint) ptSt :(CGPoint)ptNd;

+(double)distanceByHaversineFormula:(CLLocationCoordinate2D)start andStop:(CLLocationCoordinate2D)stop;
+(double)distanceInMiles:(CLLocationCoordinate2D)start andStop:(CLLocationCoordinate2D)stop;
+(double)distanceInMeters:(CLLocationCoordinate2D)start andStop:(CLLocationCoordinate2D)stop;
+(double)distanceInKiloMeters:(CLLocationCoordinate2D)start andStop:(CLLocationCoordinate2D)stop;
+(NSString*)prettyDistanceMeters:(CLLocationCoordinate2D)start andStop:(CLLocationCoordinate2D)stop;
@end
