//
//  GeoCalculation.m
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 11/16/11.
//  Copyright (c) 2011 MonsterMedia. All rights reserved.
//

#import "GeoCalculation.h"

@implementation GeoCalculation
//+(double)DistanceInMeters:(CGPoint) ptSt :(CGPoint)ptNd{
//	float a=[self ArcInRadians:ptSt :ptNd];
//    return EARTH_RADIUS_IN_METERS*a;
//}
//+(float)ArcInRadians:(CGPoint) ptSt :(CGPoint)ptNd{
//    double latitudeArc  = (ptSt.y - ptNd.y) * DEG_TO_RAD;
//    double longitudeArc = (ptSt.x - ptNd.x) * DEG_TO_RAD;
//    double latitudeH = sin(latitudeArc * 0.5);
//    latitudeH *= latitudeH;
//    double lontitudeH = sin(longitudeArc * 0.5);
//    lontitudeH *= lontitudeH;
//    double tmp = cos(ptSt.y*DEG_TO_RAD) * cos(ptNd.y*DEG_TO_RAD);
//	float xxx=2.0 * asin(sqrt(latitudeH + tmp*lontitudeH));
//    return xxx;
//}
+(double)distanceByHaversineFormula:(CLLocationCoordinate2D)start andStop:(CLLocationCoordinate2D)stop{
//    Haversine formula:
//    
//    a = sin²(Δlat/2) + cos(lat1).cos(lat2).sin²(Δlong/2)
//    c = 2.atan2(√a, √(1−a))
    double latitudeArc  = (start.latitude - stop.latitude)* DEG_TO_RAD;
    double longitudeArc = (start.longitude - stop.longitude) * DEG_TO_RAD;
    double latitudeH = sin(latitudeArc * 0.5);
    latitudeH *= latitudeH;
    double lontitudeH = sin(longitudeArc * 0.5);
    lontitudeH *= lontitudeH;
    double tmp = cos(start.latitude*DEG_TO_RAD) * cos(stop.latitude*DEG_TO_RAD);
    double a = latitudeH + tmp*lontitudeH;
	float c=2.0 * atan2(sqrt(a), sqrt(1-a));
    return c;
}
+(double)distanceInMiles:(CLLocationCoordinate2D)start andStop:(CLLocationCoordinate2D)stop{
    double result = 0.0;
    result = [self distanceInMeters:start andStop:stop];
//    result = [self distanceByHaversineFormula:start andStop:stop];    
    return (result*METRE_TO_MILE);
}
+(double)distanceInMeters:(CLLocationCoordinate2D)start andStop:(CLLocationCoordinate2D)stop{
    double result = 0.0;
    result = [self distanceByHaversineFormula:start andStop:stop];
    return  (result*EARTH_RADIUS_IN_METERS);
}
+(double)distanceInKiloMeters:(CLLocationCoordinate2D)start andStop:(CLLocationCoordinate2D)stop{
    double result = 0.0;
    result = [self distanceInMeters:start andStop:stop];
    //    result = [self distanceByHaversineFormula:start andStop:stop];    
    return (result/1000.0);
}
+(NSString*)prettyDistanceMeters:(CLLocationCoordinate2D)start andStop:(CLLocationCoordinate2D)stop{
    NSString *result = nil;
    double dist = [GeoCalculation distanceInMeters:start andStop:stop];
    double div = dist/100.0;
    if (div>0.99999) {
        double realDiv = dist/1000.0;
        if (realDiv>10.0000) {
            result = [NSString stringWithFormat:@"%.f km",realDiv];    
        }else{
            result = [NSString stringWithFormat:@"%.2f km",realDiv];
        }
    }else{
        result = [NSString stringWithFormat:@"%.f m",dist];
    }
    return result;
}
@end
