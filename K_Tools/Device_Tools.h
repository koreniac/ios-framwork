//
//  Device_Tools.h
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 2/14/55 BE.
//  Copyright (c) 2555 MonsterMedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IPAddress.h"
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

@interface Device_Tools : NSObject{
    
}
+(NSString *)deviceIPAdress;
+(NSString *)deviceIPAdress_MD5;
+(NSString *)cleanMac;
//+(NSString*)newUDID;
+(NSString *)getMacMD5;
@end
