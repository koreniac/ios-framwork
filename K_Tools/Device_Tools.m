//
//  Device_Tools.m
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 2/14/55 BE.
//  Copyright (c) 2555 MonsterMedia. All rights reserved.
//

#import "Device_Tools.h"
#import "KoreCryptonite.h"
static NSString *deviceIP = nil;
@implementation Device_Tools
+(NSString *)cleanMac{
    if (deviceIP) {
        [deviceIP release];
        deviceIP = nil;
        return @"nil";
    }
    else {
        return @"nil";
    }
}
+(NSString *)getMacMD5{
    if (deviceIP) {
        return deviceIP;
    }
    

        int                 mgmtInfoBase[6];
        char                *msgBuffer = NULL;
        size_t              length;
        unsigned char       macAddress[6];
        struct if_msghdr    *interfaceMsgStruct;
        struct sockaddr_dl  *socketStruct;
        NSString            *errorFlag = NULL;
        
        // Setup the management Information Base (mib)
        mgmtInfoBase[0] = CTL_NET;        // Request network subsystem
        mgmtInfoBase[1] = AF_ROUTE;       // Routing table info
        mgmtInfoBase[2] = 0;              
        mgmtInfoBase[3] = AF_LINK;        // Request link layer information
        mgmtInfoBase[4] = NET_RT_IFLIST;  // Request all configured interfaces
        
        // With all configured interfaces requested, get handle index
        if ((mgmtInfoBase[5] = if_nametoindex("en0")) == 0) 
            errorFlag = @"if_nametoindex failure";
        else
        {
            // Get the size of the data available (store in len)
            if (sysctl(mgmtInfoBase, 6, NULL, &length, NULL, 0) < 0) 
                errorFlag = @"sysctl mgmtInfoBase failure";
            else
            {
                // Alloc memory based on above call
                if ((msgBuffer = malloc(length)) == NULL)
                    errorFlag = @"buffer allocation failure";
                else
                {
                    // Get system information, store in buffer
                    if (sysctl(mgmtInfoBase, 6, msgBuffer, &length, NULL, 0) < 0)
                        errorFlag = @"sysctl msgBuffer failure";
                }
            }
        }
        
        // Befor going any further...
        if (errorFlag != NULL)
        {
            NSLog(@"Error: %@", errorFlag);
            return errorFlag;
        }
        
        // Map msgbuffer to interface message structure
        interfaceMsgStruct = (struct if_msghdr *) msgBuffer;
        
        // Map to link-level socket structure
        socketStruct = (struct sockaddr_dl *) (interfaceMsgStruct + 1);
        
        // Copy link layer address data in socket structure to an array
        memcpy(&macAddress, socketStruct->sdl_data + socketStruct->sdl_nlen, 6);
        
        // Read from char array into a string object, into traditional Mac address format
        NSString *macAddressString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X", 
                                      macAddress[0], macAddress[1], macAddress[2], 
                                      macAddress[3], macAddress[4], macAddress[5]];
        NSLog(@"Mac Address: %@", macAddressString);
        
        // Release the buffer memory
        free(msgBuffer);
    deviceIP = [[NSString alloc] initWithString:macAddressString];
    
    return deviceIP;
}
+(NSString *)deviceIPAdress_MD5 {
    NSString *result = nil;


    NSString *tmp = [Device_Tools getMacMD5];
    NSArray *macSplite = [tmp componentsSeparatedByString:@":"];
    NSMutableString *sumMac = [[NSMutableString alloc] initWithString:@""];
    for (NSInteger i=0; i<[macSplite count]; i++) {
        NSString *lower = [[macSplite objectAtIndex:i] lowercaseString];
//        NSString *lower = [macSplite objectAtIndex:i];
        [sumMac appendString:lower];
    }
    KoreCryptonite *crypt = [KoreCryptonite _getInstance];
    result = [[NSString alloc] initWithString:[crypt md5:sumMac]];
    TLog(@"deviceIPAdress_MD5 = %@",result);
    return result;
}
+(NSString *)deviceIPAdress {
    return [Device_Tools getMacMD5];   
}
//+(NSString *)deviceIPAdress {
//    if (deviceIP) {
//        return deviceIP;
//    }
//	InitAddresses();
//	GetIPAddresses();
//	GetHWAddresses();
//	
//
//     int i;
////     NSString *deviceIP;
//     for (i=0; i<MAXADDRS; ++i)
//     {
//     static unsigned long localHost = 0x7F000001;		// 127.0.0.1
//     unsigned long theAddr;
//     
//     theAddr = ip_addrs[i];
//     
//     if (theAddr == 0) break;
//     if (theAddr == localHost) continue;
//     
//     TLog(@"deviceIPAdress_[%d]_%s %s %s\n",i, if_names[i], hw_addrs[i], ip_names[i]);
//     }
//     deviceIP = [[NSString alloc] initWithFormat:@"%s", hw_addrs[1]];
//
//	
//    //this will get you the right IP from your device in format like 198.111.222.444. If you use the for loop above you will se that ip_names array will also contain localhost IP 127.0.0.1 that's why I don't use it. Eventualy this was code from mac that's why it uses arrays for ip_names as macs can have multiple IPs
//	return deviceIP;//[NSString stringWithFormat:@"%s", ip_names[1]];
//}
//+(NSString*)newUDID{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *uuidStr =  nil;
//    if ([defaults objectForKey:@"CFUDID"]) {
//        uuidStr = [defaults objectForKey:@"CFUDID"];
//
//    }else{
//        CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
//        //    NSString *uuidStr = (__bridge_transfer NSString *)CFUUIDCreateString(NULL, uuid);
//        uuidStr =  (NSString *)CFUUIDCreateString(NULL, uuid);
//        [defaults setObject:uuidStr forKey:@"CFUDID"];
//        CFRelease(uuid);        
//    }
//
//    return uuidStr;
//}
//+(NSString*)newUDID_V_II{
//    //CFBundleIdentifier
//    NSString *strBundle = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
//    NSString *strMAC = [Device_Tools deviceIPAdress];
//
//    NSString *uuidStr =  nil;
//    KoreCryptonite *crypt = [KoreCryptonite _getInstance];
//    NSString *newStr = [NSString stringWithFormat:@"%@_%@",strBundle,strMAC];
//    uuidStr = [crypt md5:newStr];
//    
//    return uuidStr;
//}
@end
