//
//  Graphics_Tools.h
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 1/24/12.
//  Copyright (c) 2012 MonsterMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Graphics_Tools : NSObject
+ (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize withImage:(UIImage *) originImage;
+ (UIImage *)imageCropByRect:(CGRect)rect withImage:(UIImage *) originImage;
@end
