//
//  Graphics_Tools.m
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 1/24/12.
//  Copyright (c) 2012 MonsterMedia. All rights reserved.
//

#import "Graphics_Tools.h"

@implementation Graphics_Tools
+ (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize withImage:(UIImage *) originImage {
    
    UIImage *sourceImage = originImage;
    UIImage *newImage = nil;
    
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        TLog(@"widthFactor  = %f",widthFactor);
        TLog(@"heightFactor  = %f",heightFactor);
        if (widthFactor < heightFactor) 
            scaleFactor = widthFactor;
        else
            scaleFactor = heightFactor;
        
        TLog(@"scaleFactor  = %f",scaleFactor);
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        TLog(@"scaledWidth  = %f",scaledWidth);
        TLog(@"scaledHeight  = %f",scaledHeight);
        // center the image
        
        if (widthFactor < heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5; 
            thumbnailPoint.y = thumbnailPoint.y-((targetHeight-scaledHeight)/2.0);
        } else if (widthFactor > heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            thumbnailPoint.x = thumbnailPoint.x-((targetWidth-scaledWidth)/2.0);
        }
    }
    CGSize lastTarget = CGSizeMake(scaledWidth, scaledHeight);
    // this is actually the interesting part:
    UIGraphicsBeginImageContext(lastTarget);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if(newImage == nil) NSLog(@"could not scale image");
    
    
    return newImage ;
}
+ (UIImage *)imageCropByRect:(CGRect)rect withImage:(UIImage *) originImage {
    
    UIImage *sourceImage = originImage;
    UIImage *newImage = nil;
//    
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef currentContext = UIGraphicsGetCurrentContext();
//    
//    //create a rect with the size we want to crop the image to
//    //the X and Y here are zero so we start at the beginning of our
//    //newly created context
//    CGRect clippedRect = CGRectMake(0, 0, rect.size.width, rect.size.height);
//    CGContextClipToRect( currentContext, clippedRect);
//    
//    //create a rect equivalent to the full size of the image
//    //offset the rect by the X and Y we want to start the crop
//    //from in order to cut off anything before them
//    CGRect drawRect = CGRectMake(rect.origin.x * -1,
//                                 rect.origin.y * -1,
//                                 sourceImage.size.width,
//                                 sourceImage.size.height);
//    
//    //draw the image to our clipped context using our offset rect
//    CGContextDrawImage(currentContext, drawRect, sourceImage.CGImage);
//    
//    //pull the image from our cropped context
//    newImage = UIGraphicsGetImageFromCurrentImageContext();
//    
//    //pop the context to get back to the default
//    UIGraphicsEndImageContext();
    
    //Note: this is autoreleased


    
    // Create bitmap image from original image data,
    // using rectangle to specify desired crop area
//    CGImageRef imageRef = CGImageCreateWithImageInRect([sourceImage CGImage], rect);
//    newImage = [UIImage imageWithCGImage:imageRef]; 
//    CGImageRelease(imageRef);

    CGFloat scale = 1.0;//[[UIScreen mainScreen] scale];
    
    if (scale>1.0) {        
        rect = CGRectMake(rect.origin.x*scale , rect.origin.y*scale, rect.size.width*scale, rect.size.height*scale);        
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([sourceImage CGImage], rect);
    newImage = [UIImage imageWithCGImage:imageRef]; 
    CGImageRelease(imageRef);
    
    return newImage ;
}
@end
