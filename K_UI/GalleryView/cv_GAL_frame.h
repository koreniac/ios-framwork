//
//  cv_GAL_frame.h
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 4/20/55 BE.
//  Copyright (c) 2555 MonsterMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cv_GAL_frame : UITableViewCell<UIGestureRecognizerDelegate,UIScrollViewDelegate>{
    BOOL _hud;
    UITapGestureRecognizer *tapRecognizer;
}
@property (nonatomic,retain)IBOutlet UIImageView *_iv;
@property (nonatomic,retain)IBOutlet UIView *_vTop;
@property (nonatomic,retain)IBOutlet UIView *_vButtom;
@property (nonatomic,retain)IBOutlet UIScrollView *_sv;

-(void)initFrame;
-(void)setInfo:(NSDictionary*)info;
-(void)setPicture:(UIImage*)image;
-(void)setBg:(UIImage*)bg;
-(void)setIndex:(NSInteger)index;
-(void)setCaption:(NSString*)caption;
-(void)setName:(NSString*)name andDate:(NSString*)date;
-(void)setBusiness:(NSString*)biz;
-(void)resetZoom;
-(void)showIndicator;
-(void)hideIndicator;
-(void)resetHud;
-(void)_hudOn;
-(void)_hudOff;
@end
