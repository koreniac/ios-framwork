//
//  cv_GAL_frame.m
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 4/20/55 BE.
//  Copyright (c) 2555 MonsterMedia. All rights reserved.
//

#import "cv_GAL_frame.h"

@implementation cv_GAL_frame
@synthesize _iv,_vTop,_vButtom,_sv;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
#pragma mark _AC_
-(void)_acTap:(UITapGestureRecognizer*)tap{
    TLog(@"_acTap__");
    if (_hud) {TLog(@"_acTap__on");
        _hud = NO;
        [self _hudOff];
    }else{TLog(@"_acTap__off");
        _hud = YES;
        [self _hudOn];
    }
}
#pragma mark _PRI_
-(void)_hudOn{
    [UIView beginAnimations: nil context: nil]; 
    [UIView setAnimationDuration:0.25];         
    [_vTop setFrame:CGRectMake(0,0,320,70)];
    [_vButtom setFrame:CGRectMake(0,[self frame].size.height-70,320,70)];
    [UIView commitAnimations];    
}
-(void)_hudOff{
    [UIView beginAnimations: nil context: nil]; 
    [UIView setAnimationDuration:0.25];         
    [_vTop setFrame:CGRectMake(0,-100,320,70)];
    [_vButtom setFrame:CGRectMake(0,[self frame].size.height,320,70)];
    [UIView commitAnimations];    
}
-(void)_setLayoutCaption:(NSString*)caption{
    //    CGFloat fontSize = (caption.length>0?23.0:14.0);
    //    NSInteger numLine = 3;
    //    
    //    [_wrapView setFrame:CGRectMake(0, 0, [self frame].size.width-20, fontSize*(numLine+1))];
    //    [_wrapView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
    //    
    //    [[_wrapView layer] setMasksToBounds:YES];
    //    [[_wrapView layer] setCornerRadius:10.0]; 
    //    
    //    
    //	[_wrapView setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height-(_wrapView.frame.size.height/2.0)+10)];
    //	[self setBackgroundColor:[UIColor clearColor]];
    //    
    //    //    _acIn = acIn;
    //    //    [acIn release];
    //    CGFloat fontSizeII = 10.0;
    //    [_lbName setFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*3)+5), [self frame].size.width-50, fontSizeII+5)];
    //    
    //    
    //    [_lbDate setFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*2)+5), [self frame].size.width/2, fontSizeII+5)];
    //    
    //    
    //    [_lbCap setFrame:CGRectMake(10, 5, [self frame].size.width-50, 14.0*3)];
    //    
    //    [_lbBiz setFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*4)+8), [self frame].size.width-50, fontSizeII+5)];
    //    
    
}
#pragma mark _PUB_
-(void)resetHud{
    _hud = YES;  
//    [self _hudOn];
    [_vTop setFrame:CGRectMake(0,0,320,70)];
    [_vButtom setFrame:CGRectMake(0,[self frame].size.height-70,320,70)];
}
-(void)initFrame{
    TLog(@"__initFrame");
    tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_acTap:)] autorelease];
	[tapRecognizer setNumberOfTapsRequired:1];
	[tapRecognizer setDelegate:self];
	[_iv addGestureRecognizer:tapRecognizer];  
    [self resetHud];
    [_sv setMaximumZoomScale:2.0];
	[_sv setMinimumZoomScale:1.0];
	[_sv setDelegate:self];
}
-(void)setInfo:(NSDictionary*)info{

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setPicture:(UIImage*)image{
    [self._iv setContentMode:UIViewContentModeScaleAspectFit];
	[self._iv setImage:image];
    [_sv setContentSize:CGSizeMake(image.size.width,image.size.height)];
    [_sv setMaximumZoomScale:2.0];
	[_sv setMinimumZoomScale:1.0];
//    TLog(@"__________setPicture");
//    [_acIn setAlpha:0.0];
}
-(void)setBg:(UIImage*)bg{
//	[_ivBg setImage:bg];
}
-(void)setIndex:(NSInteger)index{
//	[_lbIndex setText:[NSString stringWithFormat:@"%d",index]];
}
-(void)setName:(NSString*)name andDate:(NSString*)date{
//    if (name.length>0) {
//        //        [[_lbName layer] setMasksToBounds:YES];
//        //        [[_lbName layer] setCornerRadius:10.0]; 
//        
//        [_lbName setText:name];        
//        [_lbName setTextColor:[UIColor whiteColor]];
//        [_lbName setBackgroundColor:[UIColor clearColor]];
//    }else{
//        
//        [_lbName setText:@""];
//    }
//    if (date.length>0) {
//        //        [[_lbDate layer] setMasksToBounds:YES];
//        //        [[_lbDate layer] setCornerRadius:10.0]; 
//        
//        
//        [_lbDate setText:date];        
//        [_lbDate setTextColor:[UIColor whiteColor]];
//        [_lbDate setBackgroundColor:[UIColor clearColor]];
//    }else{
//        
//        
//        [_lbDate setText:@""];
//    }
}
-(void)setCaption:(NSString*)caption{//TLog(@"caption =  %@",caption);
    //    if (caption.length>0) {
    
//    [self _setLayoutCaption:caption];
//    
//    [_lbCap setText:caption];        
//    [_lbCap setTextColor:[UIColor whiteColor]];
    
    //    }else{
    //
    //
    //    }
    
    
    //    [_lbIndex setBackgroundColor:[UIColor clearColor]];
}
-(void)setBusiness:(NSString*)biz{
//    [_lbBiz setText:biz];
//    [_lbBiz setTextColor:[UIColor whiteColor]];
}
-(void)resetZoom{
	[_sv setZoomScale:1.0 animated:YES];
}
-(void)showIndicator{    //[_acIn setAlpha:1.0];
    //    [self addSubview:_acIn];
    //    [_acIn setCenter:CGPointMake([self frame].size.width/2.0, [self frame].size.height/2.0)];
}
-(void)hideIndicator{    //[_acIn setAlpha:0.0];
    //    [_acIn removeFromSuperview];
}
#pragma mark - Zoom
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
//	if ([scrollView zoomScale]<1.0) 
//    {
//		[_iv setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
//	}
	return _iv;
	
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
	
	if ([scrollView zoomScale]<1.00) 
	{
		[_iv setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
	}
	
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
	if ([scrollView zoomScale]<=1.00) 
	{
		[_iv setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
//        		[_iv setCenter:CGPointMake(160,230)];
	}
}
#pragma mark UIGestureRecognizer
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if ([tapRecognizer isEqual:gestureRecognizer]) {
        return YES;
    }
    return NO;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([tapRecognizer isEqual:gestureRecognizer]) {
        return YES;
    }
    return NO;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    if ([tapRecognizer isEqual:gestureRecognizer]) {
        return YES;
    }
    return NO;
}
@end
