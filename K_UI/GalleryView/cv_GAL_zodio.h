//
//  cv_GAL_zodio.h
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 4/24/12.
//  Copyright (c) 2012 MonsterMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTLabel.h"
#import "API_User.h"
#import "CENTRAL_DB.h"
#import "BizLogic.h"
#import "RO_CommentTable.h"
#import "CircleProgress.h"
@class galleryView;
@protocol cv_GAL_zodio_Delegate;
@interface cv_GAL_zodio : UITableViewCell<UIGestureRecognizerDelegate,UIScrollViewDelegate,RTLabelDelegate,API_User_DELEGATE,RO_CommentTableDelegate>{
//    BOOL _hud;
    UITapGestureRecognizer *tapRecognizer;
    NSMutableDictionary *_dicInfo;
    NSString *_strBizName;
        NSString *_strBizAddr;
    NSString *_strUser;
    NSString *_strUserCap;
    NSMutableString *_strUserLike;
    id<cv_GAL_zodio_Delegate> _galDelegate;
    API_User *__AU__;
    BOOL _like;
    CENTRAL_DB *__CTDB__;
    BOOL _isLiking;
    NSString *_userId;
            BizLogic *__BL__;
    CGFloat __h;
    NSString *_strUserEncode;
    CircleProgress *_cirProg;
    CGFloat _currentProg;
}
@property (nonatomic,retain)IBOutlet UIButton *_btComment;
@property (nonatomic,retain)IBOutlet UIButton *_btBiz;
@property (nonatomic,retain)IBOutlet UIView *_vWrapRo;
@property (nonatomic,retain)IBOutlet RO_CommentTable *_ro_comment;
@property (nonatomic,retain)IBOutlet UIImageView *_ivLike;
@property (nonatomic,retain)IBOutlet UIImageView *_ivLikeTop;
@property (nonatomic,retain)IBOutlet UIImageView *_iv;
@property (nonatomic,retain)IBOutlet UIView *_vTop;
@property (nonatomic,retain)IBOutlet UIView *_vButtom;
@property (nonatomic,retain)IBOutlet UIScrollView *_sv;
@property (nonatomic,retain)IBOutlet RTLabel *_lbTop_BizName;
@property (nonatomic,retain)IBOutlet RTLabel *_lbTop_BizAddr;
@property (nonatomic,retain)IBOutlet RTLabel *_lbButtom_UserCap;
@property (nonatomic,retain)IBOutlet RTLabel *_lbButtom_User;
@property (nonatomic,retain)IBOutlet RTLabel *_lbButtom_UserLike;
@property (nonatomic,retain)IBOutlet UIButton *_btLike;
@property (nonatomic,retain)IBOutlet UILabel *_lbPrototype;
@property (nonatomic,retain)galleryView *gall;
@property (nonatomic,retain)id<cv_GAL_zodio_Delegate> _galDelegate;
@property (nonatomic,retain)IBOutlet UIView *_vTouch;
@property (nonatomic,retain)IBOutlet UIImageView *_ivClock;

//-(IBAction)_acLike:(id)sender;
-(void)initFrame;
-(void)setInfo:(NSMutableDictionary*)info;
-(void)setPicture:(UIImage*)image;
-(void)setBg:(UIImage*)bg;
-(void)setIndex:(NSInteger)index;
-(void)setCaption:(NSString*)caption;
-(void)setName:(NSString*)name andDate:(NSString*)date;
-(void)setBusiness:(NSString*)biz;
-(void)resetZoom;
-(void)showIndicator:(CGFloat)prog;
-(void)hideIndicator;
-(void)resetHud;
-(void)_hudOn;
-(void)_hudOff;
-(void)_textRendering;
-(void)callPhotoWith:(NSString*)friendId;



@end

@protocol cv_GAL_zodio_Delegate <NSObject>
@optional
-(void)callUserProfileWith:(NSString*)userId andName:(NSString*)name;
-(void)callBizProfileWith:(NSString*)bizId;
-(void)beginforwardPushView:(UIViewController *)viewController;
@end
