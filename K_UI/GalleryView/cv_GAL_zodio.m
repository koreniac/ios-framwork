//
//  cv_GAL_zodio.m
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 4/24/12.
//  Copyright (c) 2012 MonsterMedia. All rights reserved.
//

#import "cv_GAL_zodio.h"
#import "DateCalculation.h"
#import "galleryView.h"
static     BOOL _hud = YES;
@implementation cv_GAL_zodio

@synthesize _iv,_vTop,_vButtom,_sv;
@synthesize _lbTop_BizName,_lbTop_BizAddr;
@synthesize _lbButtom_UserCap,_lbButtom_User,_lbButtom_UserLike;
@synthesize _btLike;
@synthesize _lbPrototype;
@synthesize gall;
@synthesize _galDelegate;
@synthesize _vTouch;
@synthesize _ivLike,_ivLikeTop;
@synthesize _ro_comment,_vWrapRo,_btBiz,_btComment;
@synthesize _ivClock;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
#pragma mark _AC_
-(void)_acLike:(id)sender{TLog(@"_acLike_acLike");
    __BL__  = [BizLogic getInstance];
    if (![__BL__ checkLogin]) {
        
        [[APP_DELEGATE viewController] presentModalViewController:[APP_DELEGATE _navLogin] animated:YES];
        [[APP_DELEGATE _navLogin] setNavigationBarHidden:YES];
        return;
    }
    else if (![__BL__ checkLoginAndActive]) {
        K_MyAlertWithMessage(K_MSG_ACTIVATE);

        return;
    }
    if (_isLiking){
        return;
    }
//    _isLiking = YES;
//    [sender setUserInteractionEnabled:NO];
//    [sender setAlpha:0.8];

    UIImage *imgLike = nil;
    //    NSInteger likeTotal = [[[dic valueForKey:@"like"] valueForKey:@"like_total"] intValue];
    if (_like) {
//        imgLike = [UIImage imageNamed:@"like_it_inactive"];
                imgLike = [UIImage imageNamed:@"02like-button_no-active.png"];
        _like = NO;

        
    }else{
//        imgLike = [UIImage imageNamed:@"like_it_active"];        
                imgLike = [UIImage imageNamed:@"01like-button_active.png"];        
        _like = YES;

    }

    [_btLike setImage:imgLike forState:UIControlStateNormal];
//__________________

    //    NSInteger likeTotal = [[[dic valueForKey:@"like"] valueForKey:@"like_total"] intValue];
    if (_like) {
        //        imgLike = [UIImage imageNamed:@"like_it_active"];//like_it_active
        imgLike = [UIImage imageNamed:@"01like-button_active.png"];  
        //        imgLike = [UIImage imageNamed:@"02like-button_no-active.png"];
        //        [_btLike setTitle:@"Liked" forState:UIControlStateNormal];
        //        [_btLike setImage:imgLike forState:UIControlStateNormal];
        NSMutableDictionary *you = [[NSMutableDictionary alloc] init];
        __CTDB__ = [CENTRAL_DB  getInstanceWithDelegate:nil];
        [you  setValue:[__CTDB__ getValueForKey:ZDO_CD_KEY_USERID] forKey:@"user_id"];
        [you  setValue:[__CTDB__ getValueForKey:ZDO_CD_KEY_USERNAME] forKey:@"user_name"];
        NSMutableArray *user_like_photo = [_dicInfo valueForKey:@"user_like_photo"];
        if (user_like_photo&&[user_like_photo isKindOfClass:[NSArray class]]) {
            [user_like_photo insertObject:you atIndex:0];
            [you release];
            
            
        }else{
            user_like_photo = [[NSMutableArray alloc] init];
            [user_like_photo addObject:you];
            [you release];
            [_dicInfo setObject:user_like_photo forKey:@"user_like_photo"];
            [user_like_photo release];
        }
        [_dicInfo setValue:[NSNumber numberWithBool:YES] forKey:@"isLike"];
        NSInteger likeCount = [[[_dicInfo valueForKey:@"vote"] valueForKey:@"like"] intValue];
        ++likeCount;
        [[_dicInfo valueForKey:@"vote"] setObject:[NSNumber numberWithInteger:likeCount] forKey:@"like"];        
    }
    else{
        //        imgLike = [UIImage imageNamed:@"like_it_inactive"];        
        //        imgLike = [UIImage imageNamed:@"01like-button_active.png"];        
        imgLike = [UIImage imageNamed:@"02like-button_no-active.png"];
        //        [_btLike setTitle:@"Like" forState:UIControlStateNormal];
        //        [_btLike setImage:imgLike forState:UIControlStateNormal];
        NSMutableArray *user_like_photo = [_dicInfo valueForKey:@"user_like_photo"];
        [user_like_photo removeObjectAtIndex:0];
        [_dicInfo setValue:[NSNumber numberWithBool:NO] forKey:@"isLike"];
        NSInteger likeCount = [[[_dicInfo valueForKey:@"vote"] valueForKey:@"like"] intValue];
        --likeCount;
        [[_dicInfo valueForKey:@"vote"] setObject:[NSNumber numberWithInteger:likeCount] forKey:@"like"];
    }
    [_btLike setImage:imgLike forState:UIControlStateNormal];
    [self setInfo:nil];
//__________________    
    
    
    
    __AU__ = [API_User getInstanceWithDelegate:self];
    [__AU__ setLikeReviewOrPhoto:[_dicInfo valueForKey:@"id"]];

}
-(void)_acTap:(UITapGestureRecognizer*)tap{
//    TLog(@"_acTap__");
//    if (_hud) {TLog(@"_acTap__on");
//        _hud = NO;
//        [self _hudOff];
//    }else{TLog(@"_acTap__off");
//        _hud = YES;
//        [self _hudOn];
//    }
//    [gall allHudOn:_hud];
}
#pragma mark _PRI_
-(void)_hudOn{
    [UIView beginAnimations: nil context: nil]; 
    [UIView setAnimationDuration:0.25];         
//    [_vTop setFrame:CGRectMake(0,0,320,70)];
//    [_vButtom setFrame:CGRectMake(0,[self frame].size.height-70,320,70)];
    [_vTop setAlpha:1.0];
    [_vButtom setAlpha:1.0];
    [UIView commitAnimations];    
}
-(void)_hudOff{
    [UIView beginAnimations: nil context: nil]; 
    [UIView setAnimationDuration:0.25];         
//    [_vTop setFrame:CGRectMake(0,-100,320,70)];
//    [_vButtom setFrame:CGRectMake(0,[self frame].size.height,320,70)];
    [_vTop setAlpha:0.0];
    [_vButtom setAlpha:0.0];
    [UIView commitAnimations];    
}
-(void)_setLayoutCaption:(NSString*)caption{
    //    CGFloat fontSize = (caption.length>0?23.0:14.0);
    //    NSInteger numLine = 3;
    //    
    //    [_wrapView setFrame:CGRectMake(0, 0, [self frame].size.width-20, fontSize*(numLine+1))];
    //    [_wrapView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
    //    
    //    [[_wrapView layer] setMasksToBounds:YES];
    //    [[_wrapView layer] setCornerRadius:10.0]; 
    //    
    //    
    //	[_wrapView setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height-(_wrapView.frame.size.height/2.0)+10)];
    //	[self setBackgroundColor:[UIColor clearColor]];
    //    
    //    //    _acIn = acIn;
    //    //    [acIn release];
    //    CGFloat fontSizeII = 10.0;
    //    [_lbName setFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*3)+5), [self frame].size.width-50, fontSizeII+5)];
    //    
    //    
    //    [_lbDate setFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*2)+5), [self frame].size.width/2, fontSizeII+5)];
    //    
    //    
    //    [_lbCap setFrame:CGRectMake(10, 5, [self frame].size.width-50, 14.0*3)];
    //    
    //    [_lbBiz setFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*4)+8), [self frame].size.width-50, fontSizeII+5)];
    //    
    
}
-(void)_textRendering{
    __h = 0;
    [_btLike setTitleColor:UIColorFromRGB(APP_TEXT_SMALL) forState:UIControlStateNormal];
    [_lbTop_BizName setText:_strBizName];   [_lbTop_BizName setDelegate:self]; 
//    [_lbTop_BizAddr setText:_strBizAddr];  [_lbTop_BizAddr setDelegate:self];  
    CGSize bizSize = [_lbTop_BizName optimumSize];
    if (bizSize.height>40) {
        bizSize.height = 40;
    }else{
        bizSize.height += 10;
    }
    CGRect frameBiz = [_lbTop_BizName frame];
	frameBiz.size.height = (int)bizSize.height+5; // +5 to fix height issue, this should be automatically fixed in iOS5
	[_lbTop_BizName setFrame:frameBiz];
    CGRect frameTop = [_vTop frame];
    if (bizSize.height>frameTop.size.height+5) {
        frameTop.size.height = (int)bizSize.height+5;    
    }else{
        frameTop.size.height = 55.0;
    }
    
    [_vTop setFrame:frameTop];
    
    
    [_lbButtom_UserCap setText:_strUserCap]; [_lbButtom_UserCap setDelegate:self];

    CGSize userCapSize = [_lbButtom_UserCap optimumSize];
	CGRect frame = [_lbButtom_UserCap frame];
	frame.size.height = (int)userCapSize.height+5; // +5 to fix height issue, this should be automatically fixed in iOS5
	[_lbButtom_UserCap setFrame:frame];
    
    [_lbButtom_User setText:_strUserEncode];
    CGSize userNameSize = [_lbButtom_User optimumSize];
    
    [_lbButtom_User setText:_strUser];[_lbButtom_User setDelegate:self];
    CGSize userSize = [_lbButtom_User optimumSize];
    frame = [_lbButtom_User frame];
	frame.size.height = (int)userSize.height; // +5 to fix height issue, this should be automatically fixed in iOS5
	[_lbButtom_User setFrame:frame];
    
    
    
    
    [_lbButtom_UserLike setText:_strUserLike];[_lbButtom_UserLike setDelegate:self];
    CGSize userLikeSize = [_lbButtom_UserLike optimumSize];
    TLog(@"userLikeSize = %f",userLikeSize.height);
	 CGRect frameUserLike = [_lbButtom_UserLike frame];
	frameUserLike.size.height = (int)userLikeSize.height+20; // +5 to fix height issue, this should be automatically fixed in iOS5
	[_lbButtom_UserLike setFrame:frameUserLike];
    TLog(@"frame_H = %f",frameUserLike.size.height);
    
    
    if (_strUserLike.length==0) {
        [_ivLike setAlpha:0.0];
    }else {
        [_ivLike setAlpha:1.0];
    }
    
    NSString *strUserCap = [_dicInfo valueForKey:@"description"];
    __h = [_lbButtom_UserCap frame].origin.y;
    if (strUserCap&&[strUserCap isKindOfClass:[NSString class]]&&![strUserCap isEqualToString:@"(null)"]&&strUserCap.length>0) {
        __h += [_lbButtom_UserCap frame].size.height;
    }
    [_lbButtom_User setFrame:CGRectMake(_lbButtom_User.frame.origin.x,__h,_lbButtom_User.frame.size.width,_lbButtom_User.frame.size.height)];
    __h += [_lbButtom_User frame].size.height;
    __h += 10;
//    [_ivClock setFrame:CGRectMake((_lbButtom_User.frame.origin.x+userNameSize.width),_lbButtom_User.frame.origin.y+(userNameSize.height/2.0),_ivClock.image.size.width,_ivClock.image.size.height)];
    [_ivClock setCenter:CGPointMake((_lbButtom_User.frame.origin.x+userNameSize.width+5),_lbButtom_User.frame.origin.y+(userNameSize.height/2.0)+2)];
//144    
//    if (_strUserLike.length==0) {
//        [_btComment setFrame:CGRectMake(_btComment.frame.origin.x,_lbButtom_UserLike.frame.origin.y,_btComment.frame.size.width,_btComment.frame.size.height)];
//        [_btLike setFrame:CGRectMake(_btLike.frame.origin.x,_lbButtom_UserLike.frame.origin.y-5,_btLike.frame.size.width,_btLike.frame.size.height)];
//        __h += _btComment.frame.size.height+10;
//    }else {
//        [_btComment setFrame:CGRectMake(_btComment.frame.origin.x,__h-10,_btComment.frame.size.width,_btComment.frame.size.height)];
//        [_btLike setFrame:CGRectMake(_btLike.frame.origin.x,__h-15,_btLike.frame.size.width,_btLike.frame.size.height)];
//        __h += _btComment.frame.size.height+10;///2.0;
//    }
    
    [_btComment setFrame:CGRectMake(_btComment.frame.origin.x,__h,_btComment.frame.size.width,_btComment.frame.size.height)];
    [_btLike setFrame:CGRectMake(_btLike.frame.origin.x,__h-5,_btLike.frame.size.width,_btLike.frame.size.height)];
    __h += _btComment.frame.size.height+10;
    
    [_lbButtom_UserLike setFrame:CGRectMake(_lbButtom_UserLike.frame.origin.x,__h,_lbButtom_UserLike.frame.size.width,_lbButtom_UserLike.frame.size.height)];
    [_ivLike setFrame:CGRectMake(_ivLike.frame.origin.x,_lbButtom_UserLike.frame.origin.y+2,_ivLike.frame.size.width,_ivLike.frame.size.height)];
    __h += _lbButtom_UserLike.frame.size.height+5;

    CGFloat _hButtom = __h;//(__h>_vButtom.frame.size.height?__h:_vButtom.frame.size.height);
    [_vButtom setFrame:CGRectMake([_vButtom frame].origin.x,[_vButtom frame].origin.y,[_vButtom frame].size.width,_hButtom)];
//    __h += [_vButtom frame].origin.y;
        __h = [_vButtom frame].origin.y+[_vButtom frame].size.height;
    
    if (__h>_iv.frame.size.height) {
        
    }else{
        __h = _iv.frame.size.height; 
    }
    TLog(@"_textRendering_h 0= %f",__h);
    //add ro_comment
    NSInteger _roIndex = [[_sv subviews] indexOfObject:[_ro_comment view]];
    CGFloat _hTmp = __h;
    CGFloat _hComment = 0;
    UITableView *_ro = nil;
    if (_roIndex != NSNotFound) {
//        _ro = [[_vWrapRo subviews] objectAtIndex:_roIndex];
        [(UITableView*)[_ro_comment view]  reloadData];
         _hComment = [(UITableView*)[_ro_comment view]  contentSize].height;
        [[_ro_comment view]  setFrame:CGRectMake(0, __h, 320, _hComment)];
//        [_vWrapRo setFrame:CGRectMake(0, __h, 320, _hComment)];

//        __h += _hComment;
    }
    
    //
    [_sv setContentSize:CGSizeMake(320,__h+_hComment)];
    [_sv setNeedsDisplay];
//    if (_hComment>0) {
//
//        [_ro  setFrame:CGRectMake(0, 0, 320, _hComment)];
//        [_vWrapRo setFrame:CGRectMake(0, 0, 320, _hComment)];
//        TLog(@"_vWrapRo = %@",_vWrapRo);
//        TLog(@"_ro = %@",_ro);
////        __h += _hComment;
//    }
    TLog(@"_textRendering_h = %f",__h);

    __BL__  = [BizLogic getInstance];
    if (![__BL__ checkLogin]) {
        
//        [_btLike setAlpha:0.0];
//        [_btLike setUserInteractionEnabled:NO];
//        [_ivLikeTop setAlpha:0.0];
    }else
    {
        __CTDB__ = [CENTRAL_DB getInstanceWithDelegate:nil];
//        if ([_userId isEqualToString:[__CTDB__ getValueForKey:ZDO_CD_KEY_USERID]]) {
//            [_btLike setAlpha:0.0];
//            [_btLike setUserInteractionEnabled:NO];
//            [_ivLikeTop setAlpha:0.0];
//        }else
        {
            [_btLike setAlpha:1.0];
            [_btLike setUserInteractionEnabled:YES];
            [_ivLikeTop setAlpha:1.0];
        } 
    }

    
}
#pragma mark _PUB_
-(void)resetHud{
//    _hud = YES;  
    //    [self _hudOn];
//    [_vTop setFrame:CGRectMake(0,0,320,50)];
//    [_vButtom setFrame:CGRectMake(0,[self frame].size.height-70,320,70)];
    [_sv setContentOffset:CGPointMake(0,0)];
}
-(void)initFrame{
    _isLiking = NO;
//    [_vTop setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
    [_vButtom setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
    [_iv setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
//    [_btLike setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
    [_sv setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
    [self setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
    TLog(@"__initFrame");

    tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_acTap:)] autorelease];
	[tapRecognizer setNumberOfTapsRequired:1];
	[tapRecognizer setDelegate:self];
	[_vTouch addGestureRecognizer:tapRecognizer];  
    
    
    [self resetHud];
//    [_sv setMaximumZoomScale:2.0];
//	[_sv setMinimumZoomScale:1.0];
	[_sv setDelegate:self];
    _cirProg = nil;
    _currentProg = 0.0;
}

-(void)setInfo:(NSMutableDictionary*)info{
    if (info) {
        _dicInfo = [info retain];    
        [_ro_comment setMainObjectOfComment:_dicInfo];
        NSInteger _roIndex = [[_sv subviews] indexOfObject:[_ro_comment view]];
        
        if (_roIndex != NSNotFound) {
            //        _ro = [[_vWrapRo subviews] objectAtIndex:_roIndex];
            [(UITableView*)[_ro_comment view]  removeFromSuperview];
            
        }
    }else{
        
    }

    TLog(@"_dicInfo = %@",_dicInfo);
    _like = [[_dicInfo valueForKey:@"isLike"] boolValue];
    UIImage *imgLike = nil;
    [_btLike setUserInteractionEnabled:YES];
    [_btLike addTarget:self action:@selector(_acLike:) forControlEvents:UIControlEventTouchUpInside];
    if (_like) {
//        imgLike = [UIImage imageNamed:@"like_it_active"];//like_it_active
                         imgLike = [UIImage imageNamed:@"01like-button_active.png"];        
//        [_btLike setTitle:@"Liked" forState:UIControlStateNormal];
//        [_btLike setImage:imgLike forState:UIControlStateNormal];
        
    }else{
//        imgLike = [UIImage imageNamed:@"like_it_inactive"];        
    imgLike = [UIImage imageNamed:@"02like-button_no-active.png"];        
//        [_btLike setTitle:@"Like" forState:UIControlStateNormal];
//        [_btLike setImage:imgLike forState:UIControlStateNormal];
        
    }

    [_btLike setImage:imgLike forState:UIControlStateNormal];
    NSString *strBizName = [_dicInfo valueForKey:@"object_name"];
    NSString *strBizId = [_dicInfo valueForKey:@"object_id"];
    NSArray *listBizAddr = [_dicInfo valueForKey:@"object_display_address"];
    NSString *strUserCap = [_dicInfo valueForKey:@"description"];
    NSString *strUser = [[_dicInfo valueForKey:@"from"] valueForKey:@"name"];
    NSString *strUserEncode = [[[_dicInfo valueForKey:@"from"] valueForKey:@"name"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    _strUserEncode = [[NSString alloc] initWithString:strUser];
    NSString *strUserId = [[_dicInfo valueForKey:@"from"] valueForKey:@"id"]; 
    _userId = [strUserId retain];
    NSArray *listUserLike = [_dicInfo valueForKey:@"user_like_photo"];
    
    if (strBizName&&[strBizName isKindOfClass:[NSString class]]&&![strBizName isEqualToString:@"(null)"]) {
        _strBizName = [NSString stringWithFormat:@"<a href='BIZ:%@'><font face='Helvetica-Bold' size=15 color='%@'>%@</font></a>",strBizId,APP_RT_LINK_COLOR,strBizName];//<b>%@</b>
    }
    if (listBizAddr&&[listBizAddr isKindOfClass:[NSArray class]]&&[listBizAddr count]>0) {
        NSMutableString *strBizAddr = [[NSMutableString alloc] initWithString:@""];
        for (NSInteger i=0; i<[listBizAddr count]; i++) {
            NSString *str = [listBizAddr objectAtIndex:i];
            if(str&&[str isKindOfClass:[NSString class]]){
                if (i>0) {
                    [strBizAddr appendString:@","];
                }
                [strBizAddr appendString:str];
            }
        }
        _strBizAddr = [NSString stringWithFormat:@"<font face='Helvetica' size=10 color='%@'>%@</font>",APP_RT_COLOR,strBizAddr];
    }
    if (strUserCap&&[strUserCap isKindOfClass:[NSString class]]&&![strUserCap isEqualToString:@"(null)"]) {
        _strUserCap = [NSString stringWithFormat:@"<font face='Helvetica-Bold' size=13 color='%@'>%@</font>",APP_RT_COLOR,strUserCap];//<b>%@</b>
    }
    if (strUser&&[strUser isKindOfClass:[NSString class]]&&![strUser isEqualToString:@"(null)"]) {
        id from = [_dicInfo valueForKey:@"from"];
        NSString *date = @"";
        if (from&&[from isKindOfClass:[NSDictionary class]]) {

            NSString *createTime = [_dicInfo valueForKey:@"created_time"];
            NSArray *dateList =  [createTime componentsSeparatedByString:@"T"];
            
            
            if (createTime&&createTime.length>0) {
                date = [NSString stringWithFormat:@"%@",[DateCalculation convertStrDateToMMMddYYYY:[dateList objectAtIndex:0]]];
            }

            
        }
        
        _strUser = [NSString stringWithFormat:@"<a href='USER:%@:%@'><font face='Helvetica-Bold' size=13 color='%@'>%@</font></a> <font size=12 color='%@'>took this on %@ </font>",strUserId,strUserEncode,APP_RT_LINK_COLOR,strUser,APP_RT_COLOR,date];
//                _strUser = [NSString stringWithFormat:@"<a href='USER:%@:%@'><font face='Helvetica-Bold' size=13 color='%@'>%@</font></a> <font size=12 color='%@'>      %@</font>",strUserId,strUserEncode,APP_RT_LINK_COLOR,strUser,APP_RT_COLOR,date];
        TLog(@"_strUser = %@",_strUser);//<b>%@</b>
    }
    NSDictionary *dic = nil;
    if (listUserLike&&[listUserLike isKindOfClass:[NSArray class]]&&[listUserLike count]>0) {
        _strUserLike = [[NSMutableString alloc] initWithString:@""];
        __CTDB__ = [CENTRAL_DB  getInstanceWithDelegate:nil];
        NSString *userId = [__CTDB__ getValueForKey:ZDO_CD_KEY_USERID];
        for(NSInteger i = 0;i<[listUserLike count]&&i<10;i++){
            dic = [listUserLike objectAtIndex:i];
            NSString *userName =  [[dic valueForKey:@"user_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            TLog(@"trim_userName = %@",userName);
            TLog(@"user_id = %@",[dic valueForKey:@"user_id"]);
            NSString *userNameEncode =  [userName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            //<a href='http://store.apple.com'><font color='#CCFF00'>link to apple store</font></a>
            [_strUserLike appendFormat:@"<a href='USER:%@:%@'><font face='Helvetica-Bold' size=13 color='%@'>%@%@%@</font></a>",
             [dic valueForKey:@"user_id"],
             userNameEncode,
             APP_RT_LINK_COLOR,
             (i>0?(i==[listUserLike count]-1&&[listUserLike count]==2
                   ?@" and "
                   :(i==[listUserLike count]-1&&[listUserLike count]>10        //@", and "
                     ?@", "
                     :(i==[listUserLike count]-1
                       ?@" and "
                       :@", ")
                     )
                   )
              :@""),
             ([userId isEqualToString:[dic valueForKey:@"user_id"]]?@"You":userName),
             (i==9&&[listUserLike count]>10?@"":@"")];//<b>%@%@</b>

        }
        NSDictionary *vote = [_dicInfo valueForKey:@"vote"];
        TLog(@"[listUserLike count] = %d",[listUserLike count]);
//        if ([listUserLike count]>10&&vote&&[vote isKindOfClass:[NSDictionary class]]) {

        NSInteger likeCount = [[vote valueForKey:@"like"] intValue];
        if (likeCount>=10&&vote&&[vote isKindOfClass:[NSDictionary class]]) {
            likeCount -= 10;
            if (likeCount==1) {
                [_strUserLike appendFormat:@"<font size=12 color='%@'> and %d other person like this</font>",APP_RT_COLOR,likeCount];
            }else if(likeCount==2){
                [_strUserLike appendFormat:@"<font size=12 color='%@'> and %d other people like this</font>",APP_RT_COLOR,likeCount];
            }else if(likeCount>2){
                [_strUserLike appendFormat:@"<font size=12 color='%@'> and %d other people like this</font>",APP_RT_COLOR,likeCount];
            }
        }else{
            [_strUserLike appendFormat:@"<font size=12 color='%@'> like%@ this</font>",APP_RT_COLOR,
             ([[vote valueForKey:@"like"] intValue]==1&&![userId isEqualToString:[dic valueForKey:@"user_id"]]?@"s":@"")];
        }
    }else{
        _strUserLike = [[NSMutableString alloc] initWithString:@""];
        
    }

    [self _textRendering];
    [_ro_comment setCommentTableDelegate:self];
    if (info) {

        [_ro_comment setIDwithcon:[_dicInfo valueForKey:@"id"] withType:ZDO_CMT_PHOTO];           
    }


}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setPicture:(UIImage*)image{
//    [self._iv setContentMode:UIViewContentModeScaleAspectFit];
	[self._iv setImage:image];
//    [self._iv setNeedsDisplay];
//    [_sv setContentSize:CGSizeMake(image.size.width,image.size.height)];
//    [_sv setMaximumZoomScale:2.0];
//	[_sv setMinimumZoomScale:1.0];
    //    TLog(@"__________setPicture");
    //    [_acIn setAlpha:0.0];
}
-(void)setBg:(UIImage*)bg{
    //	[_ivBg setImage:bg];
}
-(void)setIndex:(NSInteger)index{
    //	[_lbIndex setText:[NSString stringWithFormat:@"%d",index]];
}
-(void)setName:(NSString*)name andDate:(NSString*)date{
    //    if (name.length>0) {
    //        //        [[_lbName layer] setMasksToBounds:YES];
    //        //        [[_lbName layer] setCornerRadius:10.0]; 
    //        
    //        [_lbName setText:name];        
    //        [_lbName setTextColor:[UIColor whiteColor]];
    //        [_lbName setBackgroundColor:[UIColor clearColor]];
    //    }else{
    //        
    //        [_lbName setText:@""];
    //    }
    //    if (date.length>0) {
    //        //        [[_lbDate layer] setMasksToBounds:YES];
    //        //        [[_lbDate layer] setCornerRadius:10.0]; 
    //        
    //        
    //        [_lbDate setText:date];        
    //        [_lbDate setTextColor:[UIColor whiteColor]];
    //        [_lbDate setBackgroundColor:[UIColor clearColor]];
    //    }else{
    //        
    //        
    //        [_lbDate setText:@""];
    //    }
}
-(void)setCaption:(NSString*)caption{//TLog(@"caption =  %@",caption);
    //    if (caption.length>0) {
    
    //    [self _setLayoutCaption:caption];
    //    
    //    [_lbCap setText:caption];        
    //    [_lbCap setTextColor:[UIColor whiteColor]];
    
    //    }else{
    //
    //
    //    }
    
    
    //    [_lbIndex setBackgroundColor:[UIColor clearColor]];
}
-(void)setBusiness:(NSString*)biz{
    //    [_lbBiz setText:biz];
    //    [_lbBiz setTextColor:[UIColor whiteColor]];
}
-(void)resetZoom{
//	[_sv setZoomScale:1.0 animated:YES];
}
-(void)showIndicator:(CGFloat)prog{    //[_acIn setAlpha:1.0];
    //    [self addSubview:_acIn];
    //    [_acIn setCenter:CGPointMake([self frame].size.width/2.0, [self frame].size.height/2.0)];
    if (!_cirProg) {
        _cirProg = [[CircleProgress alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        [self addSubview:_cirProg];
        [_cirProg setCenter:CGPointMake(160.0,200.0)];
    }else{
        NSInteger index = [[self subviews] indexOfObject:_cirProg];
        if (index == NSNotFound) {
           [self addSubview:_cirProg]; 
        }
        
    }
    _cirProg.transform = CGAffineTransformMakeScale(1.0,1.0);
    [_cirProg setLineSize:10.0];
    [_cirProg setLineColor:UIColorFromRGB(APP_MAIN_GRAY_CATE)];
   _cirProg.alpha = 1.0;
//    [UIView animateWithDuration:0.2f delay:0.1 options:0         
//                     animations:^{
//                          [_cirProg setProgress:prog];
//                     } 
//                     completion:^(BOOL finished) {
//                         
//                     }
//     ];
    if (_currentProg<prog) {
        _currentProg = prog;    
    }

    
    [UIView beginAnimations: nil context: nil]; 
    [UIView setAnimationDuration:0.25];         
    [_cirProg setProgress:_currentProg];
    [UIView commitAnimations];    
    if (_currentProg>=0.95) {
        _currentProg = 0.0;
    }
}
-(void)hideIndicator{    
    NSInteger index = [[self subviews] indexOfObject:_cirProg];
    if (index != NSNotFound) {
        CGAffineTransform tr = CGAffineTransformScale(_cirProg.transform, 1.2, 1.2);
        CGAffineTransform trMin = CGAffineTransformScale(_cirProg.transform, 0.01, 0.01);
        CGFloat h = _cirProg.frame.size.height;
    [UIView animateWithDuration:0.2f delay:0.1 options:0         
                     animations:^{
                          [_cirProg setProgress:1.0];
                     } 
                     completion:^(BOOL finished) {
//                         [UIView animateWithDuration:0.2f delay:0.1 options:0 
//                                          animations:^{
//                                              _cirProg.transform = tr;
//                                              //                             [_cirProg setLineColor:[UIColor whiteColor]];
//                                              //                         _cirProg.center = CGPointMake(0,h);
//                                          } 
//                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:0.3f delay:0 options:0 
                                                               animations:^{
//                                                                   _cirProg.transform = trMin;
                                                                   _cirProg.alpha = 0.0;
                                                                   //                         _cirProg.center = CGPointMake(0,h);
                                                               } 
                                                               completion:^(BOOL finished) {
                                                                   
                                                                   //                                                  [_cirProg removeFromSuperview];
                                                               }
                                               ];
//                                          }
//                          ];

                     }
     ];
         
    }
        

}
-(void)callPhotoWith:(NSString*)friendId{
    __AU__ = [API_User getInstanceWithDelegate:self];
    [__AU__ deepForPhotoWith:friendId];
}

#pragma mark - Zoom
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    //	if ([scrollView zoomScale]<1.0) 
    //    {
    //		[_iv setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
    //	}
	return _iv;
	
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
	
	if ([scrollView zoomScale]<1.00) 
	{
		[_iv setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
	}
	
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
	if ([scrollView zoomScale]<=1.00) 
	{
		[_iv setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
        //        		[_iv setCenter:CGPointMake(160,230)];
	}
}
#pragma mark UIGestureRecognizer
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if ([tapRecognizer isEqual:gestureRecognizer]) {
        return YES;
    }
    return NO;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([tapRecognizer isEqual:gestureRecognizer]) {
        return YES;
    }
    return NO;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    if ([tapRecognizer isEqual:gestureRecognizer]) {
        return YES;
    }
    return NO;
}
#pragma mark - RTLabelDelegate
- (void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSURL*)url{
    
//    if (!_isLiking)
    {
        NSArray *type = [[url description] componentsSeparatedByString:@":"];
        NSString *objType = [type objectAtIndex:0];
        NSString *objId = [type objectAtIndex:1];
        if ([objType isEqualToString:@"USER"]) {
            NSString *name = [[type objectAtIndex:2] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [_galDelegate callUserProfileWith:objId andName:name];
        }else if ([objType isEqualToString:@"BIZ"]) {
            [_galDelegate callBizProfileWith:objId];        
        }        
    }

}
#pragma mark API_User_DELEGATE
-(void)userApiNotAvailable{
    K_MyAlertWithHUDIndicatorStop();
}
-(void)userResult:(NSMutableDictionary*)value ofDeepPhoto:(BOOL)status{
    if (!status) {
        TLog(@"FAILED: ofSetLikeReviewOrPhoto = %@",value);
        NSString *message = [value valueForKey:@"message"];
        if (message&&[message isKindOfClass:[NSString class]]&&message.length>4) {
            K_MyAlertWithMessage([value valueForKey:@"message"]);            
        }else{
            K_MyAlertWithMessage(K_MSG_TIMEOUT);            
        }
        return;
    }
    NSDictionary *dic = [value valueForKey:@"data"];
    [self setInfo:dic];
}
-(void)userResult:(NSMutableDictionary*)value ofSetLikeReviewOrPhoto:(BOOL)status{
    _isLiking = NO;
    [_btLike setUserInteractionEnabled:YES];
    [_btLike setAlpha:1.0];
    if (!status) {
        TLog(@"FAILED: ofSetLikeReviewOrPhoto = %@",value);
        NSString *message = [value valueForKey:@"message"];
        if (message&&[message isKindOfClass:[NSString class]]&&message.length>4) {
            K_MyAlertWithMessage([value valueForKey:@"message"]);            
        }else{
            K_MyAlertWithMessage(K_MSG_TIMEOUT);            
        }
        return;
    }
//    TLog(@"ofSetLikeReviewOrPhoto_value = %@",value);
//    NSString *message_code = [value valueForKey:@"message_code"];
//    if (message_code&&[message_code isKindOfClass:[NSString class]]&&[message_code isEqualToString:@"like"]){
//        _like = YES;
//        
//    }else{
//        _like = NO;
//
//    }
    
    
//    UIImage *imgLike = nil;
//    //    NSInteger likeTotal = [[[dic valueForKey:@"like"] valueForKey:@"like_total"] intValue];
//    if (_like) {
////        imgLike = [UIImage imageNamed:@"like_it_active"];//like_it_active
//        imgLike = [UIImage imageNamed:@"01like-button_active.png"];  
////        imgLike = [UIImage imageNamed:@"02like-button_no-active.png"];
////        [_btLike setTitle:@"Liked" forState:UIControlStateNormal];
////        [_btLike setImage:imgLike forState:UIControlStateNormal];
//        NSMutableDictionary *you = [[NSMutableDictionary alloc] init];
//        __CTDB__ = [CENTRAL_DB  getInstanceWithDelegate:nil];
//        [you  setValue:[__CTDB__ getValueForKey:ZDO_CD_KEY_USERID] forKey:@"user_id"];
//        [you  setValue:[__CTDB__ getValueForKey:ZDO_CD_KEY_USERNAME] forKey:@"user_name"];
//        NSMutableArray *user_like_photo = [_dicInfo valueForKey:@"user_like_photo"];
//        if (user_like_photo&&[user_like_photo isKindOfClass:[NSArray class]]) {
//            [user_like_photo insertObject:you atIndex:0];
//            [you release];
//            
//
//        }else{
//            user_like_photo = [[NSMutableArray alloc] init];
//            [user_like_photo addObject:you];
//            [you release];
//            [_dicInfo setObject:user_like_photo forKey:@"user_like_photo"];
//            [user_like_photo release];
//        }
//        [_dicInfo setValue:[NSNumber numberWithBool:YES] forKey:@"isLike"];
//        NSInteger likeCount = [[[_dicInfo valueForKey:@"vote"] valueForKey:@"like"] intValue];
//        ++likeCount;
//        [[_dicInfo valueForKey:@"vote"] setObject:[NSNumber numberWithInteger:likeCount] forKey:@"like"];        
//    }
//    else{
////        imgLike = [UIImage imageNamed:@"like_it_inactive"];        
////        imgLike = [UIImage imageNamed:@"01like-button_active.png"];        
//        imgLike = [UIImage imageNamed:@"02like-button_no-active.png"];
////        [_btLike setTitle:@"Like" forState:UIControlStateNormal];
////        [_btLike setImage:imgLike forState:UIControlStateNormal];
//        NSMutableArray *user_like_photo = [_dicInfo valueForKey:@"user_like_photo"];
//        [user_like_photo removeObjectAtIndex:0];
//        [_dicInfo setValue:[NSNumber numberWithBool:NO] forKey:@"isLike"];
//        NSInteger likeCount = [[[_dicInfo valueForKey:@"vote"] valueForKey:@"like"] intValue];
//        --likeCount;
//        [[_dicInfo valueForKey:@"vote"] setObject:[NSNumber numberWithInteger:likeCount] forKey:@"like"];
//    }
//    [_btLike setImage:imgLike forState:UIControlStateNormal];
//    [self setInfo:nil];

}
#pragma mark _RO_CommentTableDelegate
-(void)commentIsLoaded{
//    [_tbv reloadData];
    
    if ([[_sv subviews] indexOfObject:[_ro_comment view]]!= NSNotFound) 
    {
//        [[_ro_comment view] removeFromSuperview];
    }else{
        [_sv addSubview:[_ro_comment view]];  
    }
  
    [(UITableView*)[_ro_comment view] reloadData];
    CGFloat _hComment = [(UITableView*)[_ro_comment view] contentSize].height;
    [[_ro_comment view] setFrame:CGRectMake(0, __h, 320, _hComment)];

    //
    [_sv setContentSize:CGSizeMake(320,__h+_hComment)];
    [_sv setNeedsDisplay];
}

-(void)willPushViewController:(UIViewController *)thisView {
//    [[self navigationController] pushViewController:thisView animated:YES];
    [self beginforwardPushView:thisView];
}

-(void)beginforwardPushView:(UIViewController *)viewController {
    if (_galDelegate) {
        [_galDelegate beginforwardPushView:viewController];
    }
}
@end
