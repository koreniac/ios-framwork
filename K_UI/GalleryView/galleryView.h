//
//  galleryView.h
//  iFriendBuuks
//
//  Created by Naruphon Sirimasrungsee on 5/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "photoFrame.h"
#import "cv_GAL_frame.h"

@protocol galleryViewDelegate;
@protocol galleryViewDataSource;
@interface galleryView : UIViewController<UIScrollViewDelegate> {
	UIScrollView *_svGallery;
	id<galleryViewDataSource> __gvDS__;
	id<galleryViewDelegate> __gvDG__;
	CGSize _viewSize;
	CGSize _matrixSize;
	NSInteger _column;
	NSInteger _row;
	
	NSInteger _currentColumn;
	NSInteger _currentRow;
	
	NSInteger _currentIndex;
    BOOL    __MEM_WARN;
}
@property (nonatomic,retain)IBOutlet 	UIScrollView *_svGallery;
-(void)_init;
-(void)_clear;
-(void)setDelgate:(id<galleryViewDelegate>)del andDataSource:(id<galleryViewDataSource>)dat;
-(void)setSize:(CGSize)viewSize andMatrix:(NSInteger)column X:(NSInteger)row andMatrixSize:(CGSize)matrixSize;
-(UIView*)getPageForHorizonIndex:(NSInteger)horiIndex andVerticalIndex:(NSInteger)vertIndex;
-(NSInteger)getCurrentColumn;
-(NSInteger)getCurrentRow;
-(void)showImageAtIndex:(NSInteger) imgId;
-(NSInteger)getCurrentIndex;
-(void)allHudOn:(BOOL)on;
-(void)clearFram;
@end


@protocol galleryViewDataSource<NSObject>
-(NSInteger)_numberOfHorizonPage;
-(NSInteger)_numberOfVerticalPage;

@end
@protocol galleryViewDelegate<NSObject>
-(void)_pageForHorizonAtIndex:(NSInteger)horizonIndex andVerticalAtIndex:(NSInteger)verticalIndex withImageView:(UIView*)ivInstance;
-(void)_currentAtIndex:(NSInteger) index withImageView:(UIView*)ivInstance;
-(void)_tapped;

@end