//
//  galleryView.m
//  iFriendBuuks
//
//  Created by Naruphon Sirimasrungsee on 5/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "galleryView.h"
#import "cv_GAL_zodio.h"



@implementation galleryView
@synthesize _svGallery;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
-(void)awakeFromNib{
	[self _init];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
		[self _init];
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    
    _svGallery = [[UIScrollView alloc] initWithFrame:[self view].frame];            
    [[self view] addSubview:_svGallery];
    [_svGallery setPagingEnabled:YES];
    [_svGallery setShowsHorizontalScrollIndicator:NO];
    [_svGallery setShowsVerticalScrollIndicator:NO];
    
    [_svGallery setDelegate:(id)self];
    _svGallery.delaysContentTouches = YES;
    _svGallery.canCancelContentTouches = YES;
    [_svGallery setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
        [[self view] setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
//    [_svGallery setFrame:[self view].frame];
    if (__MEM_WARN) {
        __MEM_WARN = NO;
        [self setDelegate:nil andDataSource:nil];
        [self setSize:CGSizeZero andMatrix:0 X:0 andMatrixSize:CGSizeZero];
    }

}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    __MEM_WARN = YES;
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
            [self _clear];
}


- (void)dealloc {
    [self _clear];
    [super dealloc];
}
#pragma mark - Private
-(void)_init{
//	_svGallery =  nil;
    __MEM_WARN = NO;
}
-(void)_clear{TLog(@"_svGallery_clear");
//    [_svGallery release];
//    _svGallery = nil;
}
-(void)tapped:(id)sender {
	[__gvDG__ _tapped];
}
#pragma mark - Public
-(void)clearFram{
    _currentColumn = 0;
    NSInteger count = [[_svGallery subviews] count];
    TLog(@"_svGallery_count = %d",count);
//    TLog(@"[_svGallery subviews] = %@",[_svGallery subviews]);
    for (NSInteger i=0;i<count ;i++) {
        cv_GAL_zodio *fram = [[_svGallery subviews] objectAtIndex:0];
        [fram removeFromSuperview];
        fram = nil;
    }
    //    TLog(@"_svGallery_count____Fin");		_currentColumn = 0;
//    for (NSInteger i=0; i< [[_svGallery subviews] count]; i++) {
//        photoFrame *iv = [[_svGallery subviews] objectAtIndex:i];
//        [iv setPicture:nil];
//        
//    }
    [_svGallery setNeedsDisplay];
}
-(void)setDelgate:(id<galleryViewDelegate>)del andDataSource:(id<galleryViewDataSource>)dat{
    if (del&&dat) {
        __gvDG__ = del;
        __gvDS__ = dat;
        [_svGallery setMaximumZoomScale:1.0];
        [_svGallery setMinimumZoomScale:1.0];
//        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
//        [tapRecognizer setNumberOfTapsRequired:1];
//        [tapRecognizer setDelegate:(id)self];
//        [_svGallery addGestureRecognizer:tapRecognizer];        
    }

}
-(void)setSize:(CGSize)viewSize andMatrix:(NSInteger)column X:(NSInteger)row andMatrixSize:(CGSize)matrixSize{
    TLog(@"^__^ setSize");
//	if ([[_svGallery subviews] count]>0) {
//		return;
//	}
    if (viewSize.width==0) {
        return;
    }
	NSInteger dataColumn = [__gvDS__ _numberOfHorizonPage];
	NSInteger dataRow = [__gvDS__ _numberOfVerticalPage];
	TLog(@"^__^ dataColumn = %d",dataColumn);
	TLog(@"^__^ dataRow = %d",dataRow);	
	_viewSize = viewSize;
	_matrixSize = matrixSize;
	[[self view] setFrame:CGRectMake(0, 0, _viewSize.width, _viewSize.height)];
	[_svGallery setFrame:CGRectMake(0, 0, _viewSize.width, _viewSize.height)];
	if (dataColumn<=2) {
		[_svGallery setContentSize:CGSizeMake(_viewSize.width+(_matrixSize.width*(dataColumn-1)), _viewSize.height/*+(_matrixSize.height*2)*/)];	
	}else if(dataColumn>2){
		[_svGallery setContentSize:CGSizeMake(_viewSize.width+(_matrixSize.width*2)+10, _viewSize.height/*+(_matrixSize.height*2)*/)];
	}

//	[_svGallery setContentSize:CGSizeMake(_viewSize.width+(_matrixSize.width*2), _viewSize.height+(_matrixSize.height*2))];
	[_svGallery setContentOffset:CGPointMake(0,0 /*_matrixSize.height*/)];
	_column = column;
	_row = row;
	if (dataColumn>3) {
		dataColumn = 3;
	}
	for (NSInteger i=0; i<(3-(3-dataColumn)); i++) {NSLog(@"___ i = %d",i);
//		photoFrame *iv = [[photoFrame alloc] initWithFrame:CGRectMake((i*_matrixSize.width), 0/*_matrixSize.height*/, _matrixSize.width, _matrixSize.height)];

        cv_GAL_zodio *iv = [[cv_GAL_zodio alloc] _initCellWithNib];

        [iv initFrame];
        [iv setGall:self];
        [iv setFrame:CGRectMake((i*iv.frame.size.width), 0/*_matrixSize.height*/, iv.frame.size.width, iv.frame.size.height)];

		[_svGallery addSubview:iv];
//		cv_GAL_zodio *ivInstance = [[_svGallery subviews] objectAtIndex:i];
//		[__gvDG__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:ivInstance];

	}
	


}
-(UIView*)getPageForHorizonIndex:(NSInteger)horiIndex andVerticalIndex:(NSInteger)vertIndex{
	NSInteger subviewIndex = horiIndex-_currentColumn;
	TLog(@"subviewIndex = %d",subviewIndex);
	if (subviewIndex>2||subviewIndex<0) {
		return nil;
	}
//	photoFrame *tmp = [[_svGallery subviews] objectAtIndex:subviewIndex];
    cv_GAL_zodio *tmp = [[_svGallery subviews] objectAtIndex:subviewIndex];
    TLog(@"tmp = %@",tmp);
	[tmp resetZoom];
	return tmp;
	
}
-(NSInteger)getCurrentColumn{
	return _currentColumn;
}
-(NSInteger)getCurrentRow{
	return _currentRow;
}
-(NSInteger)getCurrentIndex{
    return _currentIndex;
}
-(void)showImageAtIndex:(NSInteger) imgId{
        TLog(@"^__^ showImageAtIndex");
    TLog(@"__gvDS__ = %@",__gvDS__);

	NSInteger dataColumn = [__gvDS__ _numberOfHorizonPage];
	NSInteger dataRow = [__gvDS__ _numberOfVerticalPage];
	if (dataColumn==0) {
        return;
    }
    TLog(@"^__^ dataColumn = %d",dataColumn);
	TLog(@"^__^ dataRow = %d",dataRow);	
	TLog(@"imgId = %d",imgId);
	TLog(@"dataColumn-3 = %d",(dataColumn-3));
	if (imgId<2) {
        TLog(@"[[_svGallery subviews] count] = %d",[[_svGallery subviews] count]);
		_currentColumn = 0;
		for (NSInteger i=0; i< [[_svGallery subviews] count]; i++) {

            cv_GAL_zodio *iv = [[_svGallery subviews] objectAtIndex:i];

			[__gvDG__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:iv];
//			if (i==0) {
//				[__gvDG__ _currentAtIndex:i+_currentColumn];
//			}
			
		}
		[_svGallery setContentOffset:CGPointMake(_matrixSize.width*imgId, 0)];
	}
	else if (imgId==dataColumn-1) {
		_currentColumn = dataColumn-3;
		for (NSInteger i=0; i< 3; i++) {
            
            cv_GAL_zodio *iv = [[_svGallery subviews] objectAtIndex:i];

			[__gvDG__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:iv];
//			if (i==0) {
//				[__gvDG__ _currentAtIndex:i+_currentColumn];
//			}
			
		}

		[_svGallery setContentOffset:CGPointMake(_matrixSize.width*2, 0)];
	}
	else {
		_currentColumn = imgId-1;
		
		if (_currentColumn>0) {//go right		--
			
			for (NSInteger i=0; i< 3; i++) {
				photoFrame *iv = [[_svGallery subviews] objectAtIndex:i];
				[__gvDG__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:iv];
//				if (i==0) {
//					[__gvDG__ _currentAtIndex:i+_currentColumn];
//				}
				
			}
			[_svGallery setContentOffset:CGPointMake(_matrixSize.width, 0)];
		}else if (_currentColumn<(dataColumn-1)) {//go left	++
			
			for (NSInteger i=0; i< 3; i++) {
				photoFrame *iv = [[_svGallery subviews] objectAtIndex:i];
				[__gvDG__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:iv];
//				if (i==0) {
//					[__gvDG__ _currentAtIndex:i+_currentColumn];
//				}
				
			}
			[_svGallery setContentOffset:CGPointMake(_matrixSize.width, 0)];
		}
	}

}
-(void)allHudOn:(BOOL)on{
    for (NSInteger i=0; i< [[_svGallery subviews] count]; i++) {
		cv_GAL_zodio *iv = [[_svGallery subviews] objectAtIndex:i];
        //        [iv setCaption:@""];

        if (on) {
            [iv _hudOn];
        }else{
            [iv _hudOff];
        }
		
	}
}
# pragma mark UIScrollViewDelegate
//- (BOOL)touchesShouldCancelInContentView:(UIView *)view{
//    return YES;
//}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {//////NSLog(@"~~~~~~~~~~ scrollViewDidScroll");
	NSInteger dataColumn = [__gvDS__ _numberOfHorizonPage];
//	NSInteger dataRow = [__gvDS__ _numberOfVerticalPage];
//	if (dataColumn==2) {
//		[_svGallery setContentSize:CGSizeMake(_viewSize.width+(_matrixSize.width*1), _viewSize.height+(_matrixSize.height*2))];	
//	}else if(dataColumn>2){
//		[_svGallery setContentSize:CGSizeMake(_viewSize.width+(_matrixSize.width*2), _viewSize.height+(_matrixSize.height*2))];
//	}
//	if (dataColumn==2) {
//		[_svGallery setContentSize:CGSizeMake(_viewSize.width+(_matrixSize.width*1), _viewSize.height/*+(_matrixSize.height*2)*/)];	
//	}else if(dataColumn>2){
//		[_svGallery setContentSize:CGSizeMake(_viewSize.width+(_matrixSize.width*2), _viewSize.height/*+(_matrixSize.height*2)*/)];
//	}	
	CGFloat horiPage = 	([scrollView contentOffset].x/_matrixSize.width);
//	CGFloat vertiPage = 	([scrollView contentOffset].y/_matrixSize.height);
	NSInteger hpFloor = (NSInteger)floor(horiPage);
	NSInteger hpCeil = (NSInteger)ceil(horiPage);
	NSInteger hpRound = (NSInteger)round(horiPage);
	NSInteger tmp = 0;
	if ((horiPage<0.00&&hpCeil==0)&&_currentColumn>0) {//go right		--
		--_currentColumn;
		for (NSInteger i=0; i< [[_svGallery subviews] count]; i++) {
			photoFrame *iv = [[_svGallery subviews] objectAtIndex:i];
            [iv resetHud];
//			[iv resetZoom];
			[__gvDG__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:iv];
//			if (i==0) {
//				[__gvDG__ _currentAtIndex:i+_currentColumn];
//			}
			
		}
		tmp = hpFloor;
		[scrollView setContentOffset:CGPointMake(_matrixSize.width, 0)];
	}else if ((horiPage>2.00&&hpFloor==2)&&_currentColumn<(dataColumn-3)) {//go left	++
		++_currentColumn;
		for (NSInteger i=0; i< [[_svGallery subviews] count]; i++) {
			photoFrame *iv = [[_svGallery subviews] objectAtIndex:i];
                        [iv resetHud];
//			[iv resetZoom];
			[__gvDG__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:iv];
//			if (i==0) {
//				[__gvDG__ _currentAtIndex:i+_currentColumn];
//			}
			
		}
				tmp = hpCeil;
		[scrollView setContentOffset:CGPointMake(_matrixSize.width, 0)];
	}
	_currentIndex = hpRound+_currentColumn;

//	for (NSInteger i=0; i< 3; i++) {
//		photoFrame *iv = [[_svGallery subviews] objectAtIndex:i];
//		[iv resetZoom];
//	
//	}
	
	
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{//////NSLog(@"~~~~~~~~~~ scrollViewDidEndDecelerating");
//	NSInteger dataColumn = [__gvDS__ _numberOfHorizonPage];
//	NSInteger dataRow = [__gvDS__ _numberOfVerticalPage];
	
//	CGFloat horiPage = 	([scrollView contentOffset].x/_matrixSize.width);
//	CGFloat vertiPage = 	([scrollView contentOffset].y/_matrixSize.height);
//	NSInteger hpFloor = (NSInteger)floor(horiPage);
//	NSInteger hpCeil = (NSInteger)ceil(horiPage);
//	NSInteger hpRound = (NSInteger)round(horiPage);
    photoFrame *ivCur = [[_svGallery subviews] objectAtIndex:(_currentIndex%[[_svGallery subviews] count])];
//    [ivCur setCaption:@""];
//    [ivCur setPicture:nil];
			[__gvDG__ _currentAtIndex:_currentIndex withImageView:(UIView*)ivCur];
	for (NSInteger i=0; i< [[_svGallery subviews] count]; i++) {
		cv_GAL_zodio *iv = [[_svGallery subviews] objectAtIndex:i];
//        [iv setCaption:@""];
        [iv resetHud];
		[iv resetZoom];
		
	}
	//if ((horiPage<0.0000&&hpCeil==0)&&_currentColumn>0) {//go right		--
//
//		for (NSInteger i=0; i< 3; i++) {
//			photoFrame *iv = [[_svGallery subviews] objectAtIndex:i];
//			[iv resetZoom];
//
//		}
//
//	}else if ((horiPage>2.0000&&hpFloor==2)&&_currentColumn<(dataColumn-3)) {//go left	++
//
//		for (NSInteger i=0; i< 3; i++) {
//			photoFrame *iv = [[_svGallery subviews] objectAtIndex:i];
//			[iv resetZoom];
//		}
//
//	}

}
#pragma mark - Zoom
//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
//	return [[scrollView subviews] objectAtIndex:_currentIndex];
//	
//}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
	
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
	
}

@end
