//
//  photoFrame.h
//  iFriendBuuks
//
//  Created by Naruphon Sirimasrungsee on 5/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface photoFrame : UIView<UIScrollViewDelegate> {
	UIImageView *_ivPicture;
	UILabel *_lbIndex;
    	UILabel *_lbCap;
    UILabel *_lbName;
    UILabel *_lbDate;
	UIImageView *_ivBg;
	UIScrollView *_sv;
    UIActivityIndicatorView *_acIn;
    NSString *_name;
    NSString *_date;
    UILabel *_lbBiz;
    UIView *_wrapView;
}
//@property (nonatomic,retain)	UIImageView *_ivPicture;
//@property (nonatomic,retain)	UILabel *_lbIndex;
//@property (nonatomic,retain)	UIImageView *_ivBg;

-(void)_init;
-(void)_clear;
-(void)setPicture:(UIImage*)image;
-(void)setBg:(UIImage*)bg;
-(void)setIndex:(NSInteger)index;
-(void)setCaption:(NSString*)caption;
-(void)setName:(NSString*)name andDate:(NSString*)date;
-(void)setBusiness:(NSString*)biz;
-(void)resetZoom;
-(void)showIndicator;
-(void)hideIndicator;
@end
