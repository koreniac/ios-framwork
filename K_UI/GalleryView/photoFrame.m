//
//  photoFrame.m
//  iFriendBuuks
//
//  Created by Naruphon Sirimasrungsee on 5/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "photoFrame.h"


@implementation photoFrame

//@synthesize 	_ivPicture;
//@synthesize 	_lbIndex;
//@synthesize 	_ivBg;
- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
		[self _init];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/

- (void)dealloc {
    [super dealloc];
}
-(void)_clear{
    [_acIn removeFromSuperview];
    [_ivPicture removeFromSuperview];
    [_lbIndex removeFromSuperview];
    [_ivBg removeFromSuperview];
    [_sv removeFromSuperview];

    _ivPicture = nil;
    _lbIndex = nil;
    _ivBg = nil;
    _sv = nil;
    _acIn = nil;
}
-(void)_setLayoutCaption:(NSString*)caption{
    CGFloat fontSize = (caption.length>0?23.0:14.0);
    NSInteger numLine = 3;

    [_wrapView setFrame:CGRectMake(0, 0, [self frame].size.width-20, fontSize*(numLine+1))];
    [_wrapView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
    
    [[_wrapView layer] setMasksToBounds:YES];
    [[_wrapView layer] setCornerRadius:10.0]; 
    

	[_wrapView setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height-(_wrapView.frame.size.height/2.0)+10)];
	[self setBackgroundColor:[UIColor clearColor]];
    
    //    _acIn = acIn;
    //    [acIn release];
    CGFloat fontSizeII = 10.0;
    [_lbName setFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*3)+5), [self frame].size.width-50, fontSizeII+5)];
    
    
    [_lbDate setFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*2)+5), [self frame].size.width/2, fontSizeII+5)];
    
    
    [_lbCap setFrame:CGRectMake(10, 5, [self frame].size.width-50, 14.0*3)];
    
    [_lbBiz setFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*4)+8), [self frame].size.width-50, fontSizeII+5)];
    

}
-(void)_init{
	
//	 _ivBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [self frame].size.width, [self frame].size.height)];
//	[_ivBg setBackgroundColor:[UIColor cyanColor]];
//	[_ivBg setContentMode:UIViewContentModeScaleToFill];
	
	_ivPicture = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [self frame].size.width, [self frame].size.height)];
	[_ivPicture setBackgroundColor:[UIColor clearColor]];
	[_ivPicture setContentMode:UIViewContentModeScaleAspectFill];
	

//	_lbIndex = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [self frame].size.width-20, fontSize*(numLine+1))];
//    //UIBaselineAdjustmentAlignCenters
//    //UIBaselineAdjustmentAlignBaselines
//    //UIBaselineAdjustmentNone
//	[_lbIndex setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];
//	[_lbIndex setTextAlignment:UITextAlignmentCenter];
//	[_lbIndex setBackgroundColor:[UIColor blackColor]];
//	[_lbIndex setTextColor:[UIColor darkGrayColor]];
//	[_lbIndex setFont:[UIFont systemFontOfSize:fontSize]];
//	[_lbIndex setNumberOfLines:numLine];
//    [_lbIndex setAlpha:0.7];
    
    //_wrapView
    CGFloat fontSize = 23.0;
    NSInteger numLine = 3;
    if (!_wrapView) {
        _wrapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [self frame].size.width-20, fontSize*(numLine+1))];
    }
    [_wrapView setFrame:CGRectMake(0, 0, [self frame].size.width-20, fontSize*(numLine+1))];
    [_wrapView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
    
    [[_wrapView layer] setMasksToBounds:YES];
    [[_wrapView layer] setCornerRadius:10.0]; 
    
	_sv  = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [self frame].size.width, [self frame].size.height)];
	[_sv setMaximumZoomScale:2.0];
	[_sv setMinimumZoomScale:1.0];
	[_sv setDelegate:self];
	[_sv setBounces:NO];
    
	[_sv setShowsVerticalScrollIndicator:NO];
	[_sv setShowsHorizontalScrollIndicator:NO];
	[_sv addSubview:_ivPicture];
    //	[self addSubview:_ivBg];
    
	
    //	[self addSubview:_ivPicture];
    [self addSubview:_sv];
    [self addSubview:_wrapView];
	[_wrapView setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height-(_wrapView.frame.size.height/2.0)+10)];
	[self setBackgroundColor:[UIColor clearColor]];
    
    //    UIActivityIndicatorView *acIn = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _acIn = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    
    
    [self addSubview:_acIn];
    [_acIn setCenter:CGPointMake([self frame].size.width/2.0, [self frame].size.height/2.0)];
    [_acIn startAnimating];
    //    _acIn = acIn;
    //    [acIn release];
    CGFloat fontSizeII = 10.0;
    _lbName = [[UILabel alloc] initWithFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*3)+5), [self frame].size.width-50, fontSizeII+5)];
    //UIBaselineAdjustmentAlignCenters
    //UIBaselineAdjustmentAlignBaselines
    //UIBaselineAdjustmentNone
	[_lbName setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];
	[_lbName setTextAlignment:UITextAlignmentLeft];
	[_lbName setBackgroundColor:[UIColor clearColor]];
	[_lbName setTextColor:[UIColor whiteColor]];
	[_lbName setFont:[UIFont systemFontOfSize:fontSizeII]];
	[_lbName setNumberOfLines:1];
    
    
    _lbDate = [[UILabel alloc] initWithFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*2)+5), [self frame].size.width/2, fontSizeII+5)];
    //UIBaselineAdjustmentAlignCenters
    //UIBaselineAdjustmentAlignBaselines
    //UIBaselineAdjustmentNone
	[_lbDate setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];
	[_lbDate setTextAlignment:UITextAlignmentLeft];
	[_lbDate setBackgroundColor:[UIColor clearColor]];
	[_lbDate setTextColor:[UIColor whiteColor]];
	[_lbDate setFont:[UIFont systemFontOfSize:fontSizeII]];
	[_lbDate setNumberOfLines:1];
    
    _lbCap = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, [self frame].size.width-50, 14.0*3)];
    //UIBaselineAdjustmentAlignCenters
    //UIBaselineAdjustmentAlignBaselines
    //UIBaselineAdjustmentNone
	[_lbCap setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];
	[_lbCap setTextAlignment:UITextAlignmentCenter];
	[_lbCap setBackgroundColor:[UIColor clearColor]];
	[_lbCap setTextColor:[UIColor whiteColor]];
	[_lbCap setFont:[UIFont systemFontOfSize:fontSizeII+4.0]];
	[_lbCap setNumberOfLines:2];
    [_lbCap setLineBreakMode:UILineBreakModeTailTruncation];
    
    _lbBiz = [[UILabel alloc] initWithFrame:CGRectMake(10, _wrapView.frame.size.height-((fontSizeII*4)+5), [self frame].size.width-50, fontSizeII+5)];
    //UIBaselineAdjustmentAlignCenters
    //UIBaselineAdjustmentAlignBaselines
    //UIBaselineAdjustmentNone
	[_lbBiz setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];
	[_lbBiz setTextAlignment:UITextAlignmentLeft];
	[_lbBiz setBackgroundColor:[UIColor clearColor]];
	[_lbBiz setTextColor:[UIColor whiteColor]];
	[_lbBiz setFont:[UIFont systemFontOfSize:fontSizeII+2]];
	[_lbBiz setNumberOfLines:1];
    
    [_wrapView addSubview:_lbName];
    [_wrapView addSubview:_lbDate];
    [_wrapView addSubview:_lbBiz];
    [_wrapView addSubview:_lbCap];
    

    

}
-(void)setPicture:(UIImage*)image{
    [_ivPicture setContentMode:UIViewContentModeScaleAspectFit];
	[_ivPicture setImage:image];
	NSLog(@"_ivPicture = %@",_ivPicture);
//    [_acIn removeFromSuperview];
//    [[self subviews] removeLastObject];
    [_acIn setAlpha:0.0];
}
-(void)setBg:(UIImage*)bg{
	[_ivBg setImage:bg];
}
-(void)setIndex:(NSInteger)index{
	[_lbIndex setText:[NSString stringWithFormat:@"%d",index]];
}
-(void)setName:(NSString*)name andDate:(NSString*)date{
    if (name.length>0) {
//        [[_lbName layer] setMasksToBounds:YES];
//        [[_lbName layer] setCornerRadius:10.0]; 
        
        [_lbName setText:name];        
        [_lbName setTextColor:[UIColor whiteColor]];
        [_lbName setBackgroundColor:[UIColor clearColor]];
    }else{
        
        [_lbName setText:@""];
    }
    if (date.length>0) {
//        [[_lbDate layer] setMasksToBounds:YES];
//        [[_lbDate layer] setCornerRadius:10.0]; 
        

        [_lbDate setText:date];        
        [_lbDate setTextColor:[UIColor whiteColor]];
        [_lbDate setBackgroundColor:[UIColor clearColor]];
    }else{
        

        [_lbDate setText:@""];
    }
}
-(void)setCaption:(NSString*)caption{//TLog(@"caption =  %@",caption);
//    if (caption.length>0) {

    [self _setLayoutCaption:caption];

        [_lbCap setText:caption];        
        [_lbCap setTextColor:[UIColor whiteColor]];

//    }else{
//
//
//    }


//    [_lbIndex setBackgroundColor:[UIColor clearColor]];
}
-(void)setBusiness:(NSString*)biz{
    [_lbBiz setText:biz];
    [_lbBiz setTextColor:[UIColor whiteColor]];
}
-(void)resetZoom{
	[_sv setZoomScale:1.0 animated:YES];
}
-(void)showIndicator{    [_acIn setAlpha:1.0];
//    [self addSubview:_acIn];
//    [_acIn setCenter:CGPointMake([self frame].size.width/2.0, [self frame].size.height/2.0)];
}
-(void)hideIndicator{    [_acIn setAlpha:0.0];
//    [_acIn removeFromSuperview];
}
#pragma mark - Zoom
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
//	if ([scrollView zoomScale]<1.0) {
//		[_ivPicture setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
//	}
	return _ivPicture;
	
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
	
	if ([scrollView zoomScale]<1.00) 
	{
		[_ivPicture setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
	}
	
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
	if ([scrollView zoomScale]<=1.00) 
	{
		[_ivPicture setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
	}
}
@end
