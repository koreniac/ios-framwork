//
//  gridView.h
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 2/7/12.
//  Copyright (c) 2012 MonsterMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "springIcon.h"
@protocol gridViewDelegate;
@protocol gridViewDataSource;


@interface gridView : UIViewController <UIGestureRecognizerDelegate,UIScrollViewDelegate>{
    id<gridViewDelegate> _DG;
    id<gridViewDataSource> _DS;
    NSInteger _numItemInColumn;
    NSInteger _numItemInRow;
    NSInteger _numItem;
    CGFloat _contentWidth;
        CGFloat _contentHeight;
    CGSize _itemSize;
    NSInteger _offsetIndexX;
    NSInteger _offsetIndexY;
    UIEdgeInsets _boundary;
    NSInteger _spaceW;
    NSInteger _spaceH;
    NSInteger _realRow;
    NSInteger _realCol;
    NSInteger _currentRow;
    NSInteger _currentCol;
    NSInteger _carryRow;
    NSInteger _carryCol;
    CGFloat _lastY;
    BOOL __MEM_WARN;
}
@property (nonatomic,retain)IBOutlet 	UIScrollView *_svGrid;

-(void)_init;
-(void)_clear;

-(void)setDelgate:(id<gridViewDelegate>)del andDataSource:(id<gridViewDataSource>)dat;
//-(void)setSize:(CGSize)viewSize andMatrix:(NSInteger)column X:(NSInteger)row andMatrixSize:(CGSize)matrixSize;
-(void)setItemSize:(CGSize)size andSpaceWidth:(NSInteger)width andSpaceHeight:(NSInteger)height;
-(void)setBoundaryTop:(NSInteger)top Bottom:(NSInteger)bottom Left:(NSInteger)left Right:(NSInteger)right;
-(void)reloadGrid;
-(id)itemForSequenceAtIndex:(NSInteger)index;
-(void)cleanItem;
@end

@protocol gridViewDataSource<NSObject>
-(NSInteger)numberOfHorizonItemMax;
-(NSInteger)numberOfVerticalItemMax;
-(NSInteger)numberOfItem;
@end

@protocol gridViewDelegate<NSObject>
-(CGSize)itemWidthHeightForSequence:(id)item atIndex:(NSInteger)index;
-(void)itemForSequence:(id)item atIndex:(NSInteger)index;
-(void)itemSelectedAtIndex:(NSInteger)index;
//-(void)pageForHorizonAtIndex:(NSInteger)horizonIndex andVerticalAtIndex:(NSInteger)verticalIndex withImageView:(UIView*)ivInstance;
//-(void)currentAtIndex:(NSInteger) index withImageView:(UIView*)ivInstance;
//-(void)tapped;

@end