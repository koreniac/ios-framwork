//
//  gridView.m
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 2/7/12.
//  Copyright (c) 2012 MonsterMedia. All rights reserved.
//

#import "gridView.h"

@implementation gridView
@synthesize _svGrid;
-(void)dealloc{
    [self _clear];
    [super dealloc];
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self _init];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    TLog(@"GridView_didReceiveMemoryWarning_");
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    [self _clear];
    __MEM_WARN = YES;
    

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{TLog(@"GridView_viewDidLoad_");
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (__MEM_WARN) 
    {
        __MEM_WARN = NO;
//        [self reloadGrid];
    }
    
}

- (void)viewDidUnload
{TLog(@"GridView_viewDidUnload_");
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [_svGrid release];
    _svGrid = nil;

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark _AC_
-(void)_acClick:(id)sender{
    [_DG itemSelectedAtIndex:[sender tag]];
}
#pragma mark _PRI_
-(void)_init{
    _lastY = 0;
    _DG = nil;
    _DS = nil;
    _offsetIndexX = 0;
    _offsetIndexY = 0;
    _currentRow = 0;
    _currentCol = 0;
    __MEM_WARN = NO;
}
-(void)_clear{
    [_svGrid release];
    _svGrid = nil;
}
#pragma mark _PUB_
-(void)setDelgate:(id<gridViewDelegate>)del andDataSource:(id<gridViewDataSource>)dat{
    _DG = del;
    _DS = dat;
}
-(void)setItemSize:(CGSize)size andSpaceWidth:(NSInteger)width andSpaceHeight:(NSInteger)height{
    _itemSize = size;
    _spaceW = width;
    _spaceH = height;
}
//-(void)setSize:(CGSize)viewSize andMatrix:(NSInteger)column X:(NSInteger)row andMatrixSize:(CGSize)matrixSize{
//    _itemSize = matrixSize;
//}
-(void)setBoundaryTop:(NSInteger)top Bottom:(NSInteger)bottom Left:(NSInteger)left Right:(NSInteger)right{
    _boundary = UIEdgeInsetsMake(top, left, bottom, right);
}
-(void)reloadGrid{TLog(@"__________ 1 ***");
    if (!_DG||!_DS) {
        return;
    }TLog(@"__________ 2 ***");
    if ([[[self view] subviews] count]>0) {
        UIScrollView *tmp = [[[self view] subviews] objectAtIndex:0];
        [tmp removeFromSuperview];
    }else{

    }
    TLog(@"__________ 3 ***");
    _svGrid = [[UIScrollView alloc] initWithFrame:[self view].frame];            
    [[self view] addSubview:_svGrid];
    TLog(@"__________ 4 ***");
//    UIScrollView *tmp = [[UIScrollView alloc] initWithFrame:[self view].frame];        
//    _svGrid = tmp;
//    
//    [tmp release];

    [_svGrid setDelegate:self];

    [_svGrid setFrame:CGRectMake([self view].frame.origin.x+_boundary.left, [self view].frame.origin.y+_boundary.top, [self view].frame.size.width-(_boundary.left+_boundary.right), [self view].frame.size.height-(_boundary.top+_boundary.bottom))];
    //get basic info
    _numItem = [_DS numberOfItem];TLog(@"GV__ _numItem = %d",_numItem);
    _numItemInRow = [_DS numberOfHorizonItemMax];TLog(@"GV__ _numItemInRow = %d",_numItemInRow);
    _numItemInColumn = ceil((CGFloat)_numItem/(CGFloat)_numItemInRow);//[_DS numberOfVerticalItemMax];
    TLog(@"GV__ _numItemInColumn = %d",_numItemInColumn);
    _contentWidth = (_itemSize.width*_numItemInRow)+(_spaceW*(_numItemInRow-1));
    _contentHeight = (_itemSize.height*(_numItemInColumn))+(_spaceH*(_numItemInColumn+5));;
    [_svGrid setContentSize:CGSizeMake(_contentWidth, _contentHeight)];
    _realRow = _numItemInColumn;
    _realCol = _numItemInRow;

    if (_contentHeight>[_svGrid frame].size.height) {
        for (NSInteger i=0; i<_numItemInColumn; i++) {
            CGFloat h = (_itemSize.height*i)+(_spaceH*(i-1));
            CGFloat difH = h-_svGrid.frame.size.height;
            if (difH>0) {
                _carryRow = (difH<_itemSize.height?1:0);
                _realRow = i+_carryRow;
                break;
            }
        }
    }
    if (_contentWidth>[_svGrid frame].size.width) {
        for (NSInteger i=0; i<_numItemInRow; i++) {
            CGFloat w = (_itemSize.width*i)+(_spaceW*(i-1));
            CGFloat difW = w-_svGrid.frame.size.width;
            if (difW>0) {
                _carryCol = (difW<_itemSize.height?1:0);
                _realCol = i+_carryCol;
                break;
            }
        }
    }

    TLog(@"GV__ _realRow = %d",_realRow);
    TLog(@"GV__ _realCol = %d",_realCol);
    NSInteger sumReal = 0;
    TLog(@"GV__ [_svGrid subviews] = %@",[_svGrid subviews]);
    if (_numItem<=_numItemInRow) {
        sumReal = _numItem;
    }else{
        sumReal = _realRow*_realCol;    
    }
    TLog(@"GV__ sumReal = %d",sumReal);
    for (NSInteger i=0; i<sumReal; i++) {
//        UIImageView *iv = nil;
        springIcon *iv = nil;
        if ([[_svGrid subviews] count]>i) {
            iv = [[_svGrid subviews] objectAtIndex:i];
        }else{

            springIcon *tmp = [[springIcon alloc] initWithFrame:CGRectMake(0, 0, _itemSize.width, _itemSize.height)];
            [tmp setIconMode:SIM_BUT];
            [[tmp btIcon] addTarget:self action:@selector(_acClick:) forControlEvents:UIControlEventTouchUpInside];
            [[tmp btIcon] setTag:i];
//            UIImageView *tmp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _itemSize.width, _itemSize.height)];
            [_svGrid addSubview:tmp];

            iv = [[_svGrid subviews] objectAtIndex:i];
            [tmp release];
        }

        CGFloat x = ((i%_realCol)*_itemSize.width)+(_spaceW*(i%_realCol));
        CGFloat y = ((i/_realCol)*_itemSize.height)+(_spaceH*(i/_realCol));
        x += _itemSize.width/2;
        y += _itemSize.height/2;
        [iv setCenter:CGPointMake(x, y)];
        if(i<_numItem){
            [_DG itemForSequence:iv atIndex:i];
        }
        TLog(@"__iv = %@",iv);
    }
    //setcontent size
    
}
-(id)itemForSequenceAtIndex:(NSInteger)index{
//    UIImageView *result = nil;
        springIcon *result = nil;
    TLog(@"___currentRow(%d) = %d",index,_currentRow);
    TLog(@"____realCol(%d) = %d",index,_realCol);
    TLog(@"____realRow(%d) = %d",index,_realRow);

    if (index>=(_currentRow*_realCol)&&index<((_currentRow+_realRow)*_realCol)) {
        NSInteger dataRow = (index/_realCol)%_realRow;
        NSInteger dataCol = index%_realCol;
        NSInteger whatsItem = (dataRow*_realCol)+dataCol;
        TLog(@"___currentRow(dataRow) = %d",dataRow);
        TLog(@"___currentRow(dataCol) = %d",dataCol);
        TLog(@"___currentRow(whatsItem) = %d",_currentRow);
        result = [[_svGrid subviews] objectAtIndex:whatsItem];        
    }

    return result;
}
-(void)cleanItem{
//    NSInteger   count  = [[_svGrid subviews] count];
//    for (NSInteger i=0; i<count; i++) {
//        id tmp = [[_svGrid subviews] objectAtIndex:0];
//        [tmp removeFromSuperview];
//        tmp = nil;
//    }
    [_svGrid removeFromSuperview];
}
# pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
        CGFloat allH = (_itemSize.height+_spaceH);
    CGFloat difLastY = _svGrid.contentOffset.y-_lastY;
     _lastY = _svGrid.contentOffset.y;
//    TLog(@"_svGrid.contentOffset.y = %f",_svGrid.contentOffset.y);
//    TLog(@"_svGrid.contentInset.top = %f",_svGrid.contentInset.top);
//    TLog(@"_svGrid.contentInset.bottom = %f",_svGrid.contentInset.bottom);
    if (_svGrid.contentOffset.y<-(allH*1)||_svGrid.contentOffset.y>((_svGrid.contentSize.height-_svGrid.frame.size.height)+(allH*1))) {
        return;
    }
    

    CGFloat currentRow = _svGrid.contentOffset.y/allH;
    CGFloat floorRow = floor(currentRow);
    CGFloat ceilRow = ceilf(currentRow);
    TLog(@"currentRow = %f",currentRow);
    TLog(@"___floorRow = %f",floorRow);
    TLog(@"ceilRow = %f",ceilRow);
    TLog(@"___currentRow = %f",_currentRow);
    NSInteger difFloor = (NSInteger)(floorRow-_currentRow);
    NSInteger difCeil = (NSInteger)(_currentRow-ceilRow)+1;
    if ((NSInteger)floorRow>_currentRow&&_svGrid.contentOffset.y<((_svGrid.contentSize.height-_svGrid.frame.size.height)-(_itemSize.height*(_carryRow)))) {
//    if ((NSInteger)floorRow>_currentRow&&_svGrid.contentOffset.y<((_svGrid.contentSize.height-_svGrid.frame.size.height))) {
        TLog(@"______________GV Row Down");
        TLog(@"_______________GV realRow = %d",_realRow);

        TLog(@"__GV dif = %d",difFloor);

//        TLog(@"_______________GV whatsRow = %d",whatsRow);
//        TLog(@"_______________GV moveRow = %d",moveRow);
        CGFloat halfH = _itemSize.height/2;
                CGFloat halfW = _itemSize.width/2;
        for (NSInteger d = 0; d<difFloor; d++) {
            NSInteger whatsRow = ((_currentRow%_realRow)*_realCol);
            NSInteger moveRow = ((_realRow+_currentRow)*_realCol);
            for (NSInteger i=0; i<_realCol; i++) {
                
//                UIImageView *iv = [[_svGrid subviews] objectAtIndex:(i+whatsRow)];
                springIcon *iv = [[_svGrid subviews] objectAtIndex:(i+whatsRow)];                
                NSInteger j = moveRow+i;
                CGFloat x = ((j%_realCol)*_itemSize.width)+(_spaceW*(j%_realCol));
                CGFloat y = ((j/_realCol)*_itemSize.height)+(_spaceH*(j/_realCol));
                x += halfW;
                y += halfH;
                [[iv btIcon] setTag:j];
                [iv setCenter:CGPointMake(x, y)];
                //            TLog(@"______________GV x = %f",x);
                //            TLog(@"______________GV y = %f",y);
//                [[iv btIcon] setBackgroundImage:nil forState:UIControlStateNormal];
                [[iv ivIcon] setImage:nil];
                [[iv ivFram] setImage:nil];
                [[iv lbIcon] setText:@""];
                if(j<_numItem){
                    [[iv ivFram] setImage:[UIImage imageNamed:@"rm_fram.png"]];
                    [_DG itemForSequence:iv atIndex:j];
                }
            }
            ++_currentRow;    TLog(@"__________ currentRow 1");        
        }

    }
    else if(ceilRow<=_currentRow&&_svGrid.contentOffset.y>0){//(_itemSize.height*_carryRow)
//    else if(_svGrid.contentOffset.y<=(_currentRow*allH)&&_svGrid.contentOffset.y>0){//(_itemSize.height*_carryRow)
        for (NSInteger d = 0; d<difCeil; d++) {
            --_currentRow;
    //        TLog(@"______________GV Row Up");
    //        TLog(@"_______________GV realRow = %d",_realRow);
            NSInteger whatsRow = (((_currentRow+(_realRow))%_realRow)*_realCol);
            NSInteger moveRow = ((_currentRow)*_realCol);
    //        TLog(@"______________GV WhatRow = %d",whatsRow);
            for (NSInteger i=0; i<_realCol; i++) {

//                UIImageView *iv = [[_svGrid subviews] objectAtIndex:(i+whatsRow)];
                springIcon *iv = [[_svGrid subviews] objectAtIndex:(i+whatsRow)];
                NSInteger j = moveRow+i;
                CGFloat x = ((j%_realCol)*_itemSize.width)+(_spaceW*(j%_realCol));
                CGFloat y = ((j/_realCol)*_itemSize.height)+(_spaceH*(j/_realCol));
                x += _itemSize.width/2;
                y += _itemSize.height/2;
                [[iv btIcon] setTag:j];
                [iv setCenter:CGPointMake(x, y)];
//                [[iv btIcon] setBackgroundImage:nil forState:UIControlStateNormal];
                [[iv ivIcon] setImage:nil];
//                [iv setImage:nil];
    //            TLog(@"______________GV x = %f",x);
    //            TLog(@"______________GV y = %f",y);
                [[iv ivFram] setImage:nil];   
                [[iv lbIcon] setText:@""];
                if(j<_numItem){
                    [[iv ivFram] setImage:[UIImage imageNamed:@"rm_fram.png"]];
                    [_DG itemForSequence:iv atIndex:j];
                }
            }
        }
        
    }
    else if (difLastY>=(_itemSize.height*(_carryRow))&&floorRow>_currentRow&&_svGrid.contentOffset.y>=(_svGrid.contentSize.height-_svGrid.frame.size.height-(_itemSize.height*(_carryRow)))) {
        for (NSInteger d = 0; d<difFloor; d++) {
            NSInteger whatsRow = ((_currentRow%_realRow)*_realCol);
            NSInteger moveRow = ((_realRow+_currentRow)*_realCol);
            for (NSInteger i=0; i<_realCol; i++) {
                
                //                UIImageView *iv = [[_svGrid subviews] objectAtIndex:(i+whatsRow)];
                springIcon *iv = [[_svGrid subviews] objectAtIndex:(i+whatsRow)];                
                NSInteger j = moveRow+i;
                CGFloat x = ((j%_realCol)*_itemSize.width)+(_spaceW*(j%_realCol));
                CGFloat y = ((j/_realCol)*_itemSize.height)+(_spaceH*(j/_realCol));
                x += _itemSize.width/2;
                y += _itemSize.height/2;
                [[iv btIcon] setTag:j];
                [iv setCenter:CGPointMake(x, y)];
                //            TLog(@"______________GV x = %f",x);
                //            TLog(@"______________GV y = %f",y);
//                [[iv btIcon] setBackgroundImage:nil forState:UIControlStateNormal];
                [[iv ivIcon] setImage:nil];
                                [[iv ivFram] setImage:nil];
                [[iv lbIcon] setText:@""];
                if(j<_numItem&&j>=0){
                    [[iv ivFram] setImage:[UIImage imageNamed:@"rm_fram.png"]];
                    [_DG itemForSequence:iv atIndex:j];
                }
            }
            ++_currentRow;         TLog(@"__________ currentRow 2");           
        }
    }
    else if(fabs(difLastY)>=(_itemSize.height*(_carryRow+1))&&ceilRow<=_currentRow&&_svGrid.contentOffset.y<=0&&difCeil>2){//(_itemSize.height*_carryRow)
        //    else if(_svGrid.contentOffset.y<=(_currentRow*allH)&&_svGrid.contentOffset.y>0){//(_itemSize.height*_carryRow)
        for (NSInteger d = 0; d<difCeil; d++) {
            --_currentRow;
            //        TLog(@"______________GV Row Up");
            //        TLog(@"_______________GV realRow = %d",_realRow);
            NSInteger whatsRow = (((_currentRow+(_realRow))%_realRow)*_realCol);
            NSInteger moveRow = ((_currentRow)*_realCol);
            //        TLog(@"______________GV WhatRow = %d",whatsRow);
            for (NSInteger i=0; i<_realCol; i++) {
                
                //                UIImageView *iv = [[_svGrid subviews] objectAtIndex:(i+whatsRow)];
                springIcon *iv = [[_svGrid subviews] objectAtIndex:(i+whatsRow)];
                NSInteger j = moveRow+i;
                CGFloat x = ((j%_realCol)*_itemSize.width)+(_spaceW*(j%_realCol));
                CGFloat y = ((j/_realCol)*_itemSize.height)+(_spaceH*(j/_realCol));
                x += _itemSize.width/2;
                y += _itemSize.height/2;
                [[iv btIcon] setTag:j];
                [iv setCenter:CGPointMake(x, y)];
//                [[iv btIcon] setBackgroundImage:nil forState:UIControlStateNormal];
                [[iv ivIcon] setImage:nil];
                //                [iv setImage:nil];
                //            TLog(@"______________GV x = %f",x);
                //            TLog(@"______________GV y = %f",y);
                                [[iv ivFram] setImage:nil];
                [[iv lbIcon] setText:@""];
                if(j<_numItem&&j>=0){
                    [[iv ivFram] setImage:[UIImage imageNamed:@"rm_fram.png"]];
                    [_DG itemForSequence:iv atIndex:j];
                }
            }
        }
        _currentRow = 0;
        
    }
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
}
#pragma mark - Zoom
//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
//	return [[scrollView subviews] objectAtIndex:_currentIndex];
//	
//}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
	
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
	
}
@end
