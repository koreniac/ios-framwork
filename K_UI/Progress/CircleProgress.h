//
//  CircleProgress.h
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 7/10/55 BE.
//  Copyright (c) 2555 MonsterMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleProgress : UIView{
    CGFloat _prog;
}
@property (nonatomic,retain)UIColor *lineColor;
@property (assign)CGFloat lineSize;
-(void)setProgress:(CGFloat)prog;
@end
