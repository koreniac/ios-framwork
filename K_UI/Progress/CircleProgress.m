//
//  CircleProgress.m
//  Zodio
//
//  Created by Naruphon Sirimasrungsee on 7/10/55 BE.
//  Copyright (c) 2555 MonsterMedia. All rights reserved.
//

#import "CircleProgress.h"

@implementation CircleProgress
@synthesize lineSize,lineColor;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
        lineSize = 1.0;
        lineColor = [UIColor blackColor];
    }
    return self;
}
-(void)setProgress:(CGFloat)prog{
    [UIView beginAnimations: nil context: nil]; 
    [UIView setAnimationDuration:0.25];         
    _prog = prog;
    [self setNeedsDisplay];
    [UIView commitAnimations];     

}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextTranslateCTM(context, 0, rect.size.height);
//    CGContextScaleCTM(context, 1, -1);



    CGPoint pnt = CGPointMake((rect.size.width)/2.0,(rect.size.height)/2.0);
    CGFloat rad = (rect.size.width-lineSize)/2.0;
    CGFloat angleStart = -90;
    UIBezierPath *border = [UIBezierPath bezierPathWithArcCenter:pnt radius:rad startAngle:(angleStart)*DEG_TO_RAD endAngle:((_prog+(angleStart/360))*360)*DEG_TO_RAD clockwise:YES];

    [border setLineWidth:lineSize];
    [lineColor setStroke];
    [border stroke];
    
//    UIGraphicsEndImageContext();
    

    

}


@end
