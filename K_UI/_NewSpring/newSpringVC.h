//
//  newSpringVC.h
//  theOne
//
//  Created by Naruphon Sirimasrungsee on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "springBoard.h"
#import "springIcon.h"
typedef enum{
	NS_NONE = 0,
	NS_INIT = 1,
	NS_START = 2,
	NS_PAUSE = 3,
	NS_STOP = 4,
	
}newSpringStage;



typedef enum{
	SM_NONE = 0,
	SM_ICON = 1,
	SM_THUMB = 2,
	SM_ICON_THUMB = 3,
	
}springMode;


@protocol NEW_SPRING_DELEGATE;
@protocol NEW_SPRING_DATASOURCE;
@interface newSpringVC : UIViewController<springBoardDelegate> {
	UIScrollView *_sv_;
	id<NEW_SPRING_DELEGATE>_springDelegate_;
	id<NEW_SPRING_DATASOURCE>_springData_;
	NSInteger _currentPage_;
	NSInteger _currentRow_;
	NSInteger _currentThumb_;
	
	NSInteger dataColumn;// = [_springData_ numberOfPage];
	
	
	
	CGSize _viewSize;// = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
	CGSize _matrixSize;// = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
	
	
	CGFloat _spaceLeft;
	CGFloat _spaceRight;
	CGFloat _spaceTop;
	CGFloat _spaceBottom;
	CGFloat _difW;
	CGFloat _difH;
	CGFloat _gapW;
	CGFloat _gapH;
	
	NSInteger _thumbRow;
	NSInteger _thumbColumn;
	newSpringStage _sStage;
	springIcon *_thumbDidSelect;
	
		NSTimeInterval _lastClick;
	NSInteger _pageId_;
	UIPageControl *_pageControl;
    BOOL _showPage;
    springIconMode _iconMode;
    CGSize _iconSize;
    
}
@property  (nonatomic,retain)IBOutlet	UIScrollView *_sv_;
@property  (nonatomic,retain)IBOutlet 	UIPageControl *_pageControl;

-(void)_init;
-(void)_clear;
-(void)setSpringDelegate:(id<NEW_SPRING_DELEGATE>)delegate andDataSource:(id<NEW_SPRING_DATASOURCE>)data;
-(void)setIconMode:(springIconMode)mode;

-(void)cleanPage:(NSInteger)pageId;
-(void)reloadAllPage;
-(void)reloadPageIndex:(NSInteger)pageId;
-(void)reloadPageIndex:(NSInteger)pageId andThumbIndex:(NSInteger)thumbId;
-(void)_reloadPageIndex:(NSInteger)pageId andThumbIndex:(NSInteger)thumbId withSpring:(springBoard*)ivInstance;

-(void)setSpaceLeft:(CGFloat)left Right:(CGFloat)right Top:(CGFloat)top Bottom:(CGFloat)bot;
-(void)setThumbRow:(NSInteger)row andColumn:(NSInteger)col;
//-(void)setThumbSizeW:(CGFloat)w andSizeH:(NSInteger)h;
-(void)hidePageControl:(BOOL)show;
-(void)setIconSize:(CGSize)size;
-(springIcon*)iconFromPage:(NSInteger)page atIndex:(NSInteger)index;
@end

@protocol NEW_SPRING_DATASOURCE
-(NSInteger)numberOfPage;
-(NSInteger)numberOfIndexOnPage:(NSInteger)page;
@end
@protocol NEW_SPRING_DELEGATE
-(void)thumbForIndex:(NSInteger)index onPage:(NSInteger)page atView:(springIcon*)icon;
-(void)thumbDidSelectForIndex:(NSInteger)index onPage:(NSInteger)page atView:(springIcon*)icon;
@end