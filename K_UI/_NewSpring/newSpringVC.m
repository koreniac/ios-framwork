//
//  newSpringVC.m
//  theOne
//
//  Created by Naruphon Sirimasrungsee on 5/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "newSpringVC.h"


@implementation newSpringVC
@synthesize _sv_;
@synthesize _pageControl;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
-(void)awakeFromNib{
    [self _init];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
        [self _init];
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	[self reloadAllPage];
//	[_svGallery setMaximumZoomScale:2.0];
//	[_svGallery setMinimumZoomScale:0.5];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
        [self _clear];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [self _clear];
    [super dealloc];
}

#pragma mark - Private
-(void)_clear{
//    NSInteger count = [[_sv_ subviews] count];
//    for (NSInteger i=0; i<count; i++) {
//        [self cleanPage:0];        
//    }

    [_sv_ removeFromSuperview];
    
    [_thumbDidSelect release];
	
    [_pageControl release];
    
    _sv_ = nil;
    _thumbDidSelect = nil;
    _pageControl = nil;    
}
-(void)_init{
	_currentPage_ = 0;
	_currentRow_ = 0;
	
	_spaceLeft = 0.0;
	_spaceRight = 0.0;
	_spaceTop = 0.0;
	_spaceBottom = 0.0;
	_difW = 0.0;
	_difH = 0.0;
	_gapW = 0.0;
	_gapH = 0.0;
	
	_thumbRow = 0;
	_thumbColumn = 0;
    _showPage = NO;
    [_pageControl setHidden:YES];
    _iconMode = SIM_IMG;
    _iconSize = CGSizeMake(80.0, 80.0);
}
-(NSInteger)_findIconIdFromPoint:(CGPoint) pnt inPage:(NSInteger)pageId{
	NSInteger column = ((NSInteger)((pnt.x-_spaceLeft/*-(pageId*_width)*/)/_gapW));
	NSInteger row = ((NSInteger)(pnt.y-_spaceTop)/_gapH);
	TLog(@"column = %d	row = %d",column,row);
	NSInteger result = column+(row*_thumbColumn);
	TLog(@"result =  %d",result);
	return result;
}
-(springIcon*)_getIconFrom:(CGPoint) pnt iconIdIs:(NSInteger**)iconId{
	if ([[_sv_ subviews] count]>0) {
		NSInteger itemAmount = [_springData_ numberOfIndexOnPage:_pageId_];
		*iconId = [self _findIconIdFromPoint:pnt inPage:_pageId_];
		//TLog(@"iconId>> %d >= %d << [iconPool count]",(NSInteger)iconId,[iconPool count]);
		

		if (*iconId>=itemAmount||*iconId<0) {
			return nil;
		}
		springBoard *iconPool = [[_sv_ subviews] objectAtIndex:_pageId_];
		return [[iconPool subviews] objectAtIndex:*iconId];
	}else {
		return nil;
	}
	
	
}
#pragma mark - Public

-(void)setSpringDelegate:(id<NEW_SPRING_DELEGATE>)delegate andDataSource:(id<NEW_SPRING_DATASOURCE>)data{
	_springDelegate_ = delegate;
		_springData_ = data;
}
-(void)setIconMode:(springIconMode)mode{
    _iconMode = mode;
}
-(void)cleanPage:(NSInteger)pageId{
    springBoard *ivInstance = nil;


		ivInstance = [[_sv_ subviews] objectAtIndex:pageId];

    if (ivInstance&&[[ivInstance subviews] count]>0) {
            NSInteger count = [[ivInstance subviews] count];
        TLog(@"cleanPage_count = %d",count);

//        for (NSInteger i=0; i<count; i++) {
//            springIcon *icon = [[ivInstance subviews] objectAtIndex:0];
//            [icon removeFromSuperview];  
////            TLog(@"retain Count = %d",[icon retainCount]);
////            icon = nil;
//        }
        [ivInstance removeFromSuperview];
        ivInstance = nil;

    }
}
-(void)reloadAllPage{
	NSInteger dataColumn = [_springData_ numberOfPage];
	if (dataColumn==0) {
		return;
	}
	
	[_pageControl setNumberOfPages:dataColumn];
	[_pageControl setCenter:CGPointMake([self view].frame.size.width/2, [self view].frame.size.height-7)];
	[_pageControl setCurrentPage:0];
	TLog(@"NS_dataColumn = %d",dataColumn);
	
	 _viewSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
	 _matrixSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
	
	[_sv_ setFrame:CGRectMake(0, 0, _matrixSize.width,_matrixSize.height)];
	if (dataColumn<=2) {
		[_sv_ setContentSize:CGSizeMake(_viewSize.width+(_matrixSize.width*(dataColumn-1)), _viewSize.height/*+(_matrixSize.height*2)*/)];	
	}else if(dataColumn>2){
		[_sv_ setContentSize:CGSizeMake(_viewSize.width+(_matrixSize.width*2), _viewSize.height/*+(_matrixSize.height*2)*/)];
	}
	
	//	[_svGallery setContentSize:CGSizeMake(_viewSize.width+(_matrixSize.width*2), _viewSize.height+(_matrixSize.height*2))];
	[_sv_ setContentOffset:CGPointMake(0,0 /*_matrixSize.height*/)];
	//	_column = column;
	//	_row = row;
	if (dataColumn>3) {
		dataColumn = 3;
	}
	for (NSInteger i=0; i<(3-(3-dataColumn)); i++) {NSLog(@"NS____ i = %d",i);
		[self reloadPageIndex:i andThumbIndex:NSNotFound];
	}
	TLog(@"NS____ _sv_ = %@",_sv_);
		TLog(@"NS____ _sv_subviews = %@",[_sv_ subviews]);
	[_sv_ setNeedsDisplay];
	
}
-(void)_reloadPageIndex:(NSInteger)pageId andThumbIndex:(NSInteger)thumbId withSpring:(springBoard*)ivInstance{
    springIcon *icon = nil;
    springIcon *iconTmp = nil;
    CGFloat x = _spaceLeft+((thumbId%_thumbColumn)*_gapW)+(_gapW/2);
    CGFloat y = ((thumbId/_thumbColumn)*(_gapH+_spaceTop))+(_gapH/2)+_spaceTop;
    TLog(@"[ivInstance subviews] count] = %d",[[ivInstance subviews] count]);
        TLog(@"NS_  j(%d) x = %f		y = %f",thumbId,x,y);
    if ([[ivInstance subviews] count]>thumbId) {
        icon = [[ivInstance subviews] objectAtIndex:thumbId];
            [icon setCenter:CGPointMake(x, y)];
            [icon setHidden:NO];
            [_springDelegate_ thumbForIndex:thumbId onPage:pageId atView:icon];
    }else {
        icon = [[springIcon alloc] initWithFrame:CGRectMake(0, 0, _iconSize.width, _iconSize.height)];
        [icon setIconMode:_iconMode];
        if (_iconMode==SIM_BUT) {
            
            [[icon btIcon] addTarget:self action:@selector(_acClick:) forControlEvents:UIControlEventTouchUpInside];
            [[icon btIcon] setTag:thumbId];
        }
        [icon setCenter:CGPointMake(x, y)];
            [icon setHidden:NO];
            [_springDelegate_ thumbForIndex:thumbId onPage:pageId atView:icon];
        [ivInstance addSubview:icon];
        [icon release];
        icon = nil;
    }

}
-(void)reloadPageIndex:(NSInteger)pageId andThumbIndex:(NSInteger)thumbId{
	NSInteger i= pageId;
	NSInteger dataRow = [_springData_ numberOfIndexOnPage:i];
	NSInteger itemAmount = _thumbColumn*_thumbRow;
	NSInteger offY  = 0;
	if (dataRow==0) {
		return;
	}else if (dataRow>=(itemAmount-_thumbColumn)) {
		
		offY = (NSInteger)(ceil((CGFloat)dataRow/(CGFloat)_thumbColumn));
//        ++offY;
        TLog(@"NS_offY = %d",offY);
//		offY +=2;
		//offY -= _thumbColumn;	TLog(@"NS_offY = %d",offY);
//		dataRow = itemAmount;
	}
	TLog(@"NS_dataRow = %d",dataRow);
	

	TLog(@"NS_offY = %d",offY);
	TLog(@"[[_sv_ subviews] count] = %d",[[_sv_ subviews] count]);
	TLog(@"iiiiiiiii = %d",i);
	springBoard *ivInstance = nil;
	springBoard *iv = nil;
	if ([[_sv_ subviews] count]>i) {
		ivInstance = [[_sv_ subviews] objectAtIndex:i];
	}else {
		iv = [[springBoard alloc] initWithFrame:CGRectMake( _matrixSize.width*i, 0/*_matrixSize.height*/, _matrixSize.width, _matrixSize.height)];
        ivInstance = iv;
		[_sv_ addSubview:iv];	
//		ivInstance = [[_sv_ subviews] objectAtIndex:i];
		[ivInstance setBackgroundColor:[UIColor clearColor]];
		
		[iv release];
        iv = nil;
	}
	
//	[ivInstance setContentSize:CGSizeMake(_matrixSize.width,  _matrixSize.height+(_iconSize.height*offY)+20)];
    TLog(@"_iconSize.height = %f",_iconSize.height);
    TLog(@"(_iconSize.height*offY) = %f",(_iconSize.height*offY));
    if (offY==_thumbColumn) {TLog(@"__1");
        [ivInstance setContentSize:CGSizeMake(_matrixSize.width,  _matrixSize.height)];
        [ivInstance setScrollEnabled:NO];
        [_sv_ setScrollEnabled:NO];
        if (_iconMode==SIM_BUT) {
          [_sv_ setScrollEnabled:NO];  
        }
    }else{TLog(@"__2");
        [ivInstance setContentSize:CGSizeMake(_matrixSize.width,  ((_iconSize.height+_spaceTop)*offY)+(offY==0?0:_spaceBottom))];
    }
    
//    [ivInstance setContentSize:CGSizeMake(_matrixSize.width,  ((_iconSize.height+_spaceTop)*offY)+(offY==0?0:_spaceBottom))];
	[ivInstance set_sbDelegage:self];
	[ivInstance setTag:100+i];
	if(offY==2){																	
		//		[ivInstance setContentOffset:CGPointMake(0, 80)];
	}
	if (thumbId==NSNotFound) {
        for (NSInteger clear=0; clear<[[ivInstance subviews] count]; clear++) {
            springIcon *icon = [[ivInstance subviews] objectAtIndex:clear];
            [icon setHidden:YES];
            
        }
        for (NSInteger j=0; j<dataRow; j++) {
            //		springIcon *icon = nil;
            //		if ([[ivInstance subviews] count]>j) {
            //			icon = [[ivInstance subviews] objectAtIndex:j];
            //		}else {
            //			icon = [[springIcon alloc] initWithFrame:CGRectMake(0, 0, _iconSize.width, _iconSize.height)];
            //			[icon setIconMode:_iconMode];
            //            if (_iconMode==SIM_BUT) {
            //
            //                [[icon btIcon] addTarget:self action:@selector(_acClick:) forControlEvents:UIControlEventTouchUpInside];
            //                [[icon btIcon] setTag:j];
            //            }
            //			[ivInstance addSubview:icon];
            //			[icon release];
            //		}
            //		[icon setHidden:NO];
            //		
            //		
            //		//		[icon setBackgroundColor:[UIColor orangeColor]];
            //		//			NSMutableDictionary *newInfo = [[NSMutableDictionary alloc] init];
            //		//			
            //		//			[newInfo setValue:[info valueForKey:@"create_date"] forKey:@"create_date"];
            //		//			[newInfo setValue:[info valueForKey:@"image_preview_path"] forKey:@"image_preview_path"];
            //		//			[newInfo setValue:[info valueForKey:@"image_thumb_125x125"] forKey:@"image_thumb_125x125"];
            //		
            //		//			[result setOnlyDic:newInfo];// withDelegate:self];
            //		
            //		
            //		
            //		CGFloat x = _spaceLeft+((j%_thumbColumn)*_gapW)+(_gapW/2);
            ////		CGFloat y = _spaceTop+((j/_thumbColumn)*_gapH)+((_gapH/2)+_spaceTop);
            //		CGFloat y = ((j/_thumbColumn)*(_gapH+_spaceTop))+(_gapH/2)+_spaceTop;
            //		TLog(@"NS_  j(%d) x = %f		y = %f",j,x,y);
            //		//		[icon setFrame:CGRectMake(x, y, 80, 80)];
            //		[icon setCenter:CGPointMake(x, y)];
            //		[_springDelegate_ thumbForIndex:j onPage:i atView:icon];
            
            [self _reloadPageIndex:i andThumbIndex:j withSpring:ivInstance];
        }
    }
    else{
        [self _reloadPageIndex:i andThumbIndex:thumbId withSpring:ivInstance];
    }

	[ivInstance setNeedsDisplay];
	TLog(@"NS_ivInstance = %@",ivInstance);
    TLog(@"NS_ivInstance subview= %@",[(UIScrollView*)ivInstance subviews]);
	//		[__gvDS__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:ivInstance];
	
	
}
-(void)reloadPageIndexTileBase:(NSInteger)pageId{//For TileBase
	NSInteger i= pageId;
	NSInteger dataRow = [_springData_ numberOfIndexOnPage:i];
	NSInteger itemAmount = _thumbColumn*_thumbRow;
	NSInteger offY  = 0;
	if (dataRow==0) {
		return;
	}else if (dataRow>itemAmount) {

		 offY = (NSInteger)ceil((dataRow-itemAmount)/(CGFloat)_thumbColumn);
		dataRow = itemAmount;
	}
	TLog(@"NS_dataRow = %d",dataRow);
	
	if (offY>2) {
		offY = 2;
	}
	TLog(@"NS_offY = %d",offY);
	springBoard *ivInstance = nil;
	springBoard *iv = nil;
	if ([[_sv_ subviews] count]>i) {
		ivInstance = [[_sv_ subviews] objectAtIndex:i];
	}else {
		iv = [[springBoard alloc] initWithFrame:CGRectMake(0, 0/*_matrixSize.height*/, _matrixSize.width, _matrixSize.height)];
		[_sv_ addSubview:iv];	
		ivInstance = [[_sv_ subviews] objectAtIndex:i];
		[ivInstance setTag:100+i];
		[iv release];
	}
	
//	[ivInstance setContentSize:CGSizeMake(_matrixSize.width,  _matrixSize.height+(_iconSize.height*offY)+20)];
    [ivInstance setContentSize:CGSizeMake(_matrixSize.width,  (_iconSize.height*offY))];
	[ivInstance setDelegate:self];
	if(offY==2){																	
//		[ivInstance setContentOffset:CGPointMake(0, 80)];
	}
	
	for (NSInteger j=0; j<dataRow+(offY*_thumbColumn); j++) {
		springIcon *icon = nil;
		if ([[ivInstance subviews] count]>j) {
			icon = [[ivInstance subviews] objectAtIndex:j];
		}else {
			icon = [[springIcon alloc] initWithFrame:CGRectMake(0, 0, _iconSize.width, _iconSize.height)];
            [icon setIconMode:_iconMode];
            if (_iconMode==SIM_BUT) {
                
                [[icon btIcon] addTarget:self action:@selector(_acClick:) forControlEvents:UIControlEventTouchUpInside];
                [[icon btIcon] setTag:j];
            }
			[ivInstance addSubview:icon];

		}
		
		
//		[icon setBackgroundColor:[UIColor orangeColor]];
		//			NSMutableDictionary *newInfo = [[NSMutableDictionary alloc] init];
		//			
		//			[newInfo setValue:[info valueForKey:@"create_date"] forKey:@"create_date"];
		//			[newInfo setValue:[info valueForKey:@"image_preview_path"] forKey:@"image_preview_path"];
		//			[newInfo setValue:[info valueForKey:@"image_thumb_125x125"] forKey:@"image_thumb_125x125"];
		
		//			[result setOnlyDic:newInfo];// withDelegate:self];
		

		
		CGFloat x = _spaceLeft+((j%_thumbColumn)*_gapW)+(_gapW/2);
		CGFloat y = _spaceTop+((j/_thumbColumn)*_gapH)+(_gapH/2);
		TLog(@"NS_  j(%d) x = %f		y = %f",j,x,y);
//		[icon setFrame:CGRectMake(x, y, 80, 80)];
		[icon setCenter:CGPointMake(x, y)];
		[_springDelegate_ thumbForIndex:j onPage:i atView:icon];
        if ([[ivInstance subviews] count]>j) {
			
		}else {
            [icon release];
            icon = nil;
        }
		
	}
	[ivInstance setNeedsDisplay];
	TLog(@"NS_ivInstance = %@",ivInstance);
	//		[__gvDS__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:ivInstance];

	
}
-(void)setSpaceLeft:(CGFloat)left Right:(CGFloat)right Top:(CGFloat)top Bottom:(CGFloat)bot{
	_spaceLeft = left;
	_spaceRight = right;
	_spaceTop = top;
	_spaceBottom = bot;
	
	_difW = self.view.frame.size.width-(_spaceRight+_spaceLeft);
	_difH = self.view.frame.size.height-(_spaceBottom+_spaceTop);
    TLog(@"self.view.frame.size.height = %f",self.view.frame.size.height);
	
}
-(void)setThumbRow:(NSInteger)row andColumn:(NSInteger)col{
	_thumbRow = row;
	_thumbColumn = col;
	
	_gapW = _difW/(CGFloat)_thumbColumn;
	_gapH = _difH/(CGFloat)_thumbRow;
//    _gapH -= _iconSize.height;
	TLog(@"_gapW = %f",_gapW);
        TLog(@"_thumbRow = %d",_thumbRow);
    TLog(@"_difH = %f",_difH);
		TLog(@"_gapH = %f",_gapH);
}
-(void)hidePageControl:(BOOL)show{
        [_pageControl setHidden:show];
}
-(void)setIconSize:(CGSize)size{
    _iconSize = size;
}
-(springIcon*)iconFromPage:(NSInteger)page atIndex:(NSInteger)index{
    springBoard *board =  [[_sv_ subviews] objectAtIndex:page];
    return [[board subviews] objectAtIndex:index];
}
#pragma mark _AC_
-(void)_acClick:(id)sender{
    TLog(@"sender = %@",sender);
    springBoard *iconPool = [[_sv_ subviews] objectAtIndex:_pageId_];
    _thumbDidSelect =  [[iconPool subviews] objectAtIndex:[sender tag]];

    [_springDelegate_ thumbDidSelectForIndex:[sender tag] onPage:_pageId_ atView:_thumbDidSelect];
}
#pragma mark springBoard_Delegate
-(void)sbTouchBegin:(CGPoint)point withEvent:(UIEvent *)event{TLog(@"_______ sbTouchBegin x = %f	y = %f",point.x,point.y);
	_lastClick = [NSDate timeIntervalSinceReferenceDate];
	NSInteger *iconId;
	_thumbDidSelect = [self _getIconFrom:point iconIdIs:&iconId];		TLog(@"iconId  = (%d) iconInstance = %@",iconId,_thumbDidSelect);
	
	_currentThumb_ = iconId; 
	if (_sStage==NS_NONE) {
		_sStage = NS_INIT;
		
		
	}
}
-(void)sbTouchMove:(CGPoint)point withEvent:(UIEvent *)event{
//	_lastPoint = CGPointMake(point.x, point.y);

}
-(void)sbTouchEnd:(CGPoint)point withEvent:(UIEvent *)event{TLog(@"sbTouchEnd springBoardVC");
	
	TLog(@"[NSDate timeIntervalSinceReferenceDate] = %f",[NSDate timeIntervalSinceReferenceDate]);
	TLog(@"_lastClick+0.5 = %f",(_lastClick+0.5));
	if (_sStage==NS_INIT) {
		_sStage = NS_NONE;
		
		if (([NSDate timeIntervalSinceReferenceDate])<(_lastClick+0.3)) {TLog(@"SS_INIT now+0.3");
			TLog(@"_thumbDidSelect = %@",_thumbDidSelect);
			if ([_thumbDidSelect iconStage:SIS_NORMAL]) {TLog(@"iconStage:SIS_NORMAL");
				
				if ([_thumbDidSelect chkDisable]) {
					
				}else {
					NSInteger index = [self _findIconIdFromPoint:point inPage:_pageId_];
					if (index==_currentThumb_) {
						[_springDelegate_ thumbDidSelectForIndex:_currentThumb_ onPage:_pageId_ atView:_thumbDidSelect];
					}
					
					
				}
				
			}
			
			
		}
	}
	
	
	
	
	
	
	
}


# pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {//////NSLog(@"~~~~~~~~~~ scrollViewDidScroll");

	if ([scrollView isKindOfClass:[springBoard class]]) {
		NSInteger i= [scrollView tag]-100;
		NSInteger itemNum = [_springData_ numberOfIndexOnPage:i]; 
		NSInteger dataRow = [_springData_ numberOfIndexOnPage:i];
		NSInteger rowNum = (NSInteger)ceil(dataRow/(CGFloat)_thumbColumn);//dataRow/_thumbColumn;
		TLog(@"NS_ rowNum = %d",rowNum);
		NSInteger itemAmount = _thumbColumn*_thumbRow;
		NSInteger offY  = 0;
		if (dataRow==0) {
			return;
		}else if (dataRow>itemAmount) {
			
			offY = (NSInteger)ceil((dataRow-itemAmount)/(CGFloat)_thumbColumn);
			dataRow = itemAmount;
		}
		TLog(@"NS_dataRow = %d",dataRow);
		
		if (offY>2) {
			offY = 2;
		}
		TLog(@"NS_offY = %d",offY);
		springBoard *ivInstance = nil;
		springBoard *iv = nil;
		ivInstance = (springBoard*)scrollView;
		

		
//		[ivInstance setContentSize:CGSizeMake(_matrixSize.width,  _matrixSize.height+(80*offY))];
		
		CGFloat horiPage = 	([scrollView contentOffset].x/_sv_.frame.size.width);
		CGFloat vertiPage = 	([scrollView contentOffset].y/_iconSize.width);
				TLog(@"NS_vertiPage = %f",vertiPage);
		TLog(@"_currentRow_ = %d",_currentRow_);
		NSInteger hpFloor = (NSInteger)floor(horiPage);
		NSInteger hpCeil = (NSInteger)ceil(horiPage);
		NSInteger hpRound = (NSInteger)round(horiPage);
		
		
		NSInteger vpFloor = (NSInteger)floor(vertiPage);
		NSInteger vpCeil = (NSInteger)ceil(vertiPage);
		NSInteger vpRound = (NSInteger)round(vertiPage);		
		if ((vertiPage<0.0000&&vpCeil==0)&&_currentRow_>0) {//go bottom		--
			--_currentRow_;
//__________________________________________________________________________________
			TLog(@"NS_ bot_currentRow_ = %d",_currentRow_);
			for (NSInteger j=0; j<dataRow+(offY*_thumbColumn); j++) {
				springIcon *icon = nil;
				if ([[ivInstance subviews] count]>j) {
					icon = [[ivInstance subviews] objectAtIndex:j];
				}
				
				CGFloat x = _spaceLeft+((j%_thumbColumn)*_gapW)+(_gapW/2);
				CGFloat y = _spaceTop+((j/_thumbColumn)*_gapH)+(_gapH/2);
				TLog(@"NS_  j(%d) x = %f		y = %f",j,x,y);
				//		[icon setFrame:CGRectMake(x, y, 80, 80)];
				[icon setCenter:CGPointMake(x, y)];
				NSInteger itemId = j+(_currentRow_*_thumbColumn);
				if (itemId<itemNum) {
					[icon setHidden:NO];
					[_springDelegate_ thumbForIndex:itemId onPage:i atView:icon];	
				}else {
					[icon setHidden:YES];
				}
			}
//__________________________________________________________________________________			
			[scrollView setContentOffset:CGPointMake(0, 80.0)];//-_-!
//            [scrollView setContentOffset:CGPointMake(0, 0)];
		}else if ((vertiPage>2.0000&&vpFloor==2)&&_currentRow_<(rowNum-6)) {//go top	++
			++_currentRow_;
//__________________________________________________________________________________
			TLog(@"NS_ top_currentRow_ = %d",_currentRow_);	
			for (NSInteger j=0; j<dataRow+(offY*_thumbColumn); j++) {
				springIcon *icon = nil;
				if ([[ivInstance subviews] count]>j) {
					icon = [[ivInstance subviews] objectAtIndex:j];
				}

				CGFloat x = _spaceLeft+((j%_thumbColumn)*_gapW)+(_gapW/2);
				CGFloat y = _spaceTop+((j/_thumbColumn)*_gapH)+(_gapH/2);
				TLog(@"NS_  j(%d) x = %f		y = %f",j,x,y);
				//		[icon setFrame:CGRectMake(x, y, 80, 80)];
				[icon setCenter:CGPointMake(x, y)];
				NSInteger itemId = j+(_currentRow_*_thumbColumn);
				if (itemId<itemNum) {
					[icon setHidden:NO];
					[_springDelegate_ thumbForIndex:itemId onPage:i atView:icon];	
				}else {
					[icon setHidden:YES];
				}
			}
			
			
			
//__________________________________________________________________________________
			[scrollView setContentOffset:CGPointMake(0, 80.0)];//-_-!
//            [scrollView setContentOffset:CGPointMake(0, 0)];
		}
		
		
		


		
	}
	else if ([scrollView isKindOfClass:[UIScrollView class]]) {
		NSInteger dataColumn = [_springData_ numberOfPage];
		
		
		
		CGFloat horiPage = 	([scrollView contentOffset].x/_sv_.frame.size.width);
		CGFloat vertiPage = 	([scrollView contentOffset].y/_sv_.frame.size.height);
		_pageId_ = (NSInteger)horiPage;
		[_pageControl setCurrentPage:_pageId_];
		NSInteger hpFloor = (NSInteger)floor(horiPage);
		NSInteger hpCeil = (NSInteger)ceil(horiPage);
		NSInteger hpRound = (NSInteger)round(horiPage);
		
		if ((horiPage<0.0000&&hpCeil==0)&&_currentPage_>0) {//go right		--
			--_currentPage_;
			for (NSInteger i=0; i< 3; i++) {
				NSInteger dataRow = [_springData_ numberOfIndexOnPage:i];
				springBoard *board = [[_sv_ subviews] objectAtIndex:i];
				//			for (NSInteger j=0; j<15; j++) {
				//				springIcon *icon = [[board subviews] objectAtIndex:j];
				//				[_springDelegate_ thumbForIndex:j+(atRow*3) onPage:i atView:icon];
				//			}
				
			}
			[scrollView setContentOffset:CGPointMake(_sv_.frame.size.width, 0)];
		}else if ((horiPage>2.0000&&hpFloor==2)&&_currentPage_<(dataColumn-3)) {//go left	++
			++_currentPage_;
			for (NSInteger i=0; i< 3; i++) {
				springBoard *board = [[_sv_ subviews] objectAtIndex:i];
			}
			[scrollView setContentOffset:CGPointMake(_sv_.frame.size.width, 0)];
		}
	}
	
	
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{//////NSLog(@"~~~~~~~~~~ scrollViewDidEndDecelerating");
	//	CGFloat horiPage = 	([sender contentOffset].x/[sender frame].size.width);
	//	CGFloat vertiPage = 	([sender contentOffset].y/[sender frame].size.height);
//	NSInteger dataColumn = [__gvDS__ _numberOfHorizonPage];
//	NSInteger dataRow = [__gvDS__ _numberOfVerticalPage];
//	
//	NSInteger horiPage = 	(NSInteger)([scrollView contentOffset].x/_matrixSize.width);
//	NSInteger vertiPage = 	(NSInteger)([scrollView contentOffset].y/_matrixSize.height);
	//	if (horiPage==0&&_currentColumn>0) {//go right		--
	//		--_currentColumn;
	//		for (NSInteger i=0; i< 3; i++) {
	//			UIImageView *iv = [[_svGallery subviews] objectAtIndex:i];
	//			[__gvDS__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:iv];
	//		}
	//			[scrollView setContentOffset:CGPointMake(_matrixSize.width, 0)];
	//	}else if (horiPage==2&&_currentColumn<(dataColumn-3)) {//go left	++
	//		++_currentColumn;
	//		for (NSInteger i=0; i< 3; i++) {
	//			UIImageView *iv = [[_svGallery subviews] objectAtIndex:i];
	//			[__gvDS__ _pageForHorizonAtIndex:(i+_currentColumn) andVerticalAtIndex:_currentRow	withImageView:iv];
	//		}
	//			[scrollView setContentOffset:CGPointMake(_matrixSize.width, 0)];
	//	}
	
}
#pragma mark - Zoom
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
	return [[scrollView subviews] objectAtIndex:0];
	
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
	
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
	
}

@end
