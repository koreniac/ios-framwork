//
//  springBoard.h
//  theOne
//
//  Created by Naruphon Sirimasrungsee on 1/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol springBoardDelegate;
@interface springBoard : UIScrollView {
	id<springBoardDelegate> _sbDelegage;
}
@property (nonatomic,retain)	id<springBoardDelegate> _sbDelegage;
@end


@protocol springBoardDelegate
-(void)sbTouchBegin:(CGPoint)point withEvent:(UIEvent *)event;
-(void)sbTouchMove:(CGPoint)point withEvent:(UIEvent *)event;
-(void)sbTouchEnd:(CGPoint)point withEvent:(UIEvent *)event;
@end