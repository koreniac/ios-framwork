//
//  springBoard.m
//  theOne
//
//  Created by Naruphon Sirimasrungsee on 1/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "springBoard.h"


@implementation springBoard

@synthesize _sbDelegage;
- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
		
		[self setShowsVerticalScrollIndicator:NO];
		[self setShowsHorizontalScrollIndicator:NO];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/

- (void)dealloc {TLog(@"dealloc__springBoard");
    [super dealloc];
}
//-(BOOL)touchesShouldCancelInContentView:(UIView *)view{
//	return NO;
//}

#pragma mark UIResponder
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{TLog(@"touchesBegan.... SB ");
	[super touchesBegan:touches withEvent:event];
	UITouch *touch = [touches anyObject];
	if([touch tapCount]==2){//--- double click
		
	}
	
	CGPoint pt = [touch locationInView:self];//--- touch position
	if (_sbDelegage) {
		[_sbDelegage sbTouchBegin:pt withEvent:event];		
	}

	
	
	//	NSLog(@"touchesBegan pt.x = %f",pt.x);
	//	NSLog(@"touchesBegan pt.y = %f",pt.y);
	//	NSLog(@"indexPath page = %d  index = %d",_idPath.section,_idPath.row);
	//	if (pt.x>[_ivDelete frame].origin.x&&pt.x<([_ivDelete frame].origin.x+[_ivDelete frame].size.width)
	//		&&pt.y>[_ivDelete frame].origin.y&&pt.y<([_ivDelete frame].origin.y+[_ivDelete frame].size.height)) {
	//		NSLog(@"deletet");
	//		[_iconDelegate iconDidDelete:self];
	//	}
	//	[self _startEdit];
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{TLog(@"touchesMoved.... SB   = %@",_sbDelegage);
	UITouch *touch = [touches anyObject];
	
	
	CGPoint pt = [touch locationInView:self];//--- touch position
		if (_sbDelegage) {
			[_sbDelegage sbTouchMove:pt withEvent:event];
		}
	
	//	if ([self hitTest:pt withEvent:event]&&[self _checkSelfState:IS_EDIT]) 
	//	{
	//		NSLog(@">> touchesMoved pt.x = %f",pt.x);
	//		NSLog(@">> touchesMoved pt.y = %f",pt.y);
	//		NSLog(@">> indexPath page = %d  index = %d",_idPath.section,_idPath.row);
	////		[self setCenter:CGPointMake(pt.x,pt.y)];	
	//		
	//		self.transform = CGAffineTransformMakeTranslation(_currentPoint.x, _currentPoint.y);
	////		[self setNeedsDisplay];
	//	}
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{TLog(@"touchesEnded.... SB ");
	[super touchesEnded:touches withEvent:event];
	UITouch *touch = [touches anyObject];
	if([touch tapCount]==2){//--- double click
		
	}
	
	CGPoint pt = [touch locationInView:self];//--- touch position
		if (_sbDelegage) {
			[_sbDelegage sbTouchEnd:pt withEvent:event];
		}
	//	if (pt.x>[_ivDelete frame].origin.x&&pt.x<([_ivDelete frame].origin.x+[_ivDelete frame].size.width)
	//		&&pt.y>[_ivDelete frame].origin.y&&pt.y<([_ivDelete frame].origin.y+[_ivDelete frame].size.height)) {NSLog(@"deletet");
	//		
	//		[_iconDelegate iconDidDelete:self];
	//	}
	
}



@end
