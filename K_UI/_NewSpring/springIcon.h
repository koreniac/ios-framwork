//
//  springIcon.h
//  theOne
//
//  Created by Naruphon Sirimasrungsee on 1/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
	SIS_NONE = 0,
	SIS_NORMAL = 0xf010,
	SIS_EDIT =0x0f00,
	SIS_DELETE = 0x0f01,
	SIS_LOADING = 0x0f02,
}springIconStage;

typedef enum{
	SIM_NONE = 0,
	SIM_IMG = 1,
	SIM_BUT = 2,

}springIconMode;
@protocol springIconDelegate;

@interface springIcon : UIView {//<NSCopying, NSCoding>
	NSDictionary *_info;
	UILabel *lbIcon;
	UIImageView *ivIcon;
    UIImageView *ivFram;
//	UIImageView *ivDelete;
	UIButton *ivDelete;
	id<springIconDelegate> _iconDelegate;
	springIconStage _iconStage;
	
	CGAffineTransform _tranScale;
	BOOL _disable;
    UIButton *btIcon;
    springIconMode _iconMode; 
}
@property (nonatomic,retain) NSDictionary *_info;
@property (	nonatomic,retain)IBOutlet 	UILabel *lbIcon;
@property (	nonatomic,retain)IBOutlet 	UIImageView *ivIcon;
@property (	nonatomic,retain)IBOutlet UIButton *ivDelete;
@property (	nonatomic,retain)IBOutlet     UIButton *btIcon;
@property (nonatomic,retain)UIImageView *ivFram;
-(void)_init;
+(BOOL)allStage:(springIconStage)checkStage;
+(void)_allIconNormal;
+(void)_allIconEdit;
-(void)setUp:(NSDictionary*) info;// withDelegate:(id<springIconDelegate>) delegate;
-(void)setOnlyDic:(NSDictionary*)dic;
-(void)setOnlyDic:(NSDictionary*)dic withFrame:(CGRect)rect andFontSize:(CGFloat)fontSize;
-(void)setIconDelegate:(id<springIconDelegate>)delegate;

-(BOOL)iconStage:(springIconStage)checkStage;
-(void)iconNormal;
-(void)iconEdit;
-(void)iconDelete;

-(void)iconScale:(CGFloat)w andH:(CGFloat)h;
-(void)iconRotate:(CGFloat) degree;
-(void)iconTranSlate:(CGFloat) x andY:(CGFloat)y;
-(void)iconDukdik:(CGFloat) dudic;

-(void)enable;
-(void)disable;
-(BOOL)chkDisable;
-(void)setIconMode:(springIconMode)mode;
@end


@protocol springIconDelegate
-(void)iconTouchBegin:(CGPoint)point withIcon:(springIcon*) icon;
-(void)iconTouchMove:(CGPoint)point withIcon:(springIcon*) icon;
-(void)iconTouchEnd:(CGPoint)point withIcon:(springIcon*) icon;

-(void)iconDidSelected:(springIcon*) icon;
-(void)iconDidEdit:(springIcon*) icon;
-(void)iconDidMove:(springIcon*) icon;
-(void)iconDidDelete:(springIcon*) icon;
@end