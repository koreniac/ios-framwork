//
//  springIcon.m
//  theOne
//
//  Created by Naruphon Sirimasrungsee on 1/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "springIcon.h"


static UIViewContentMode _uivctm = UIViewContentModeScaleAspectFit;
static springIconStage _iconStageAll = SIS_NORMAL;
@implementation springIcon 
@synthesize _info;
@synthesize ivIcon,ivDelete,lbIcon,btIcon;
@synthesize ivFram;
- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
		[self _init];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/

- (void)dealloc {TLog(@"dealloc__springIcon");
    if (_info) {
        [_info release];
        _info = nil;
    }
    if (lbIcon) {
        [lbIcon release];
        lbIcon = nil;
    }
    if (ivIcon) {
        [ivIcon release];
        ivIcon = nil;
    }
    if (ivDelete) {
        [ivDelete release];
        ivDelete = nil;
    }
    
//    if (btIcon) {
//        [btIcon release];
//        btIcon = nil;
//    }
    [super dealloc];
}
#pragma mark -
-(void)_init{
	_disable = NO;
	ivIcon = nil;
	lbIcon = nil;
    ivFram = nil;
	ivDelete = nil;
    _iconMode = SIM_IMG;
    btIcon = nil;
}

#pragma mark Function
+(BOOL)allStage:(springIconStage)checkStage{
//	TLog(@"checkStage = %@",(checkStage==SIS_NORMAL?@"SIS_NORMAL":(checkStage==SIS_EDIT?@"SIS_EDIT":@"SIS_DELETE")));
//	TLog(@"_iconStageAll = %@",(_iconStageAll==SIS_NORMAL?@"SIS_NORMAL":(_iconStageAll==SIS_EDIT?@"SIS_EDIT":@"SIS_DELETE")));
	
//	if((_iconStageAll&checkStage)!=0){TLog(@">>>>>>>>Match");
//		return YES;
//	}return NO;
	if ((_iconStageAll&checkStage)!=0) {
		return YES;
	}return NO;
}
+(void)_allIconNormal{
	_iconStageAll = SIS_NORMAL;
}
+(void)_allIconEdit{
	_iconStageAll = SIS_EDIT;
}
-(void)setIconDelegate:(id<springIconDelegate>)delegate {
	_iconDelegate = delegate;
}
-(void)setOnlyDic:(NSDictionary*)dic withFrame:(CGRect)rect andFontSize:(CGFloat)fontSize{
	_iconStage = SIS_NORMAL;
	if (dic) {
		_info = [dic retain];		
	}
    TLog(@"_iconMode = %d",_iconMode);
    if (_iconMode==SIM_IMG) {
        if (!ivIcon) {
            ivIcon = [[UIImageView alloc] initWithFrame:rect];	
            [ivIcon setContentMode:_uivctm];
                [self addSubview:ivIcon];
//            [ivIcon release];
//            ivIcon = nil;
        }
    }else if(_iconMode==SIM_BUT){
//        if (!btIcon) 
        {
            NSString *code = [dic valueForKey:@"code"];
            if (!ivFram&&(!code||[code isEqualToString:@"(null)"])) {
                ivFram = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rm_fram.png"]];
                [ivFram setContentMode:_uivctm];
                [ivFram setFrame:rect];
                [self addSubview:ivFram];                
                if (!ivIcon) {
                    ivIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, rect.size.width-20, rect.size.height-20)];	
                    [ivIcon setContentMode:_uivctm];
                    [self addSubview:ivIcon];                
                }
            }else{
                if (!ivIcon) {
                    ivIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 80, 80)];	
                    [ivIcon setContentMode:UIViewContentModeCenter];
                    [self addSubview:ivIcon];                
                }
            }


//            if(!btIcon)
            {
//                btIcon = [[UIButton alloc] initWithFrame:[self frame]];
                [btIcon setContentMode:_uivctm];
                [btIcon setFrame:rect];
               
            }

//            btIcon = [UIButton buttonWithType:UIButtonTypeCustom];

//            TLog(@"btIcon = %@",btIcon);
//            [btIcon release];
//            btIcon = nil;
        }        
    }


	if (!lbIcon) {
		lbIcon = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height-(fontSize*1.5), self.frame.size.width, ((fontSize*3)+2))];	
			[self addSubview:lbIcon];

        [lbIcon release];
	}

	[lbIcon setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];
	[lbIcon setNumberOfLines:3];
    [lbIcon setTextAlignment:UITextAlignmentCenter];
    
	//ivDelete = [[UIImageView alloc] initWithFrame:CGRectMake(50, -3, 33, 33)];

//Current Not Use	
//	if (!ivDelete) {
//		ivDelete = [UIButton buttonWithType:UIButtonTypeCustom];
//		[ivDelete setFrame:CGRectMake(50, -3, 33, 33)];
//		[ivDelete addTarget:self action:@selector(iconDelete) forControlEvents:UIControlEventTouchUpInside];
//			[self addSubview:ivDelete];
//	[ivDelete setBackgroundImage:[UIImage imageNamed:@"bt_deletedownload.png"] forState:UIControlStateNormal];
//	[ivDelete setHidden:YES];
//	}
	if (_iconMode==SIM_IMG) {
        [ivIcon setContentMode: _uivctm];
    }else if(_iconMode==SIM_BUT){
        [ivIcon setContentMode: _uivctm];
        [btIcon setContentMode:_uivctm];
    }



//	[ivIcon setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];

	
	[lbIcon setTextAlignment:UITextAlignmentCenter];
	[lbIcon setBackgroundColor:[UIColor clearColor]];
	[lbIcon setTextColor:[UIColor blackColor]];
	[lbIcon setFont:[UIFont systemFontOfSize:fontSize]];
    [lbIcon setLineBreakMode:UILineBreakModeWordWrap];
//	[lbIcon setText:@"-_-"];
}
-(void)setOnlyDic:(NSDictionary*)dic{
//	[self setOnlyDic:dic withFrame:CGRectMake(16, 14, 50, 50) andFontSize:14.0];
	[self setOnlyDic:dic withFrame:CGRectMake(0, 0, 75, 75) andFontSize:12.0];
} 
-(void)setUp:(NSDictionary*) info{// withDelegate:(id<springIconDelegate>) delegate{
//	_iconDelegate = delegate;
//	[self setBackgroundColor:[UIColor blackColor]];
	_iconStage = SIS_NORMAL;
	_info = [info copy];
	ivIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 60, 60)];
	lbIcon = [[UILabel alloc] initWithFrame:CGRectMake(-3, 60, self.frame.size.width, 20)];
//	[lbIcon setNumberOfLines:2];
	[ivIcon setImage:[UIImage imageNamed:[_info valueForKey:@"abbr"]]];
	[lbIcon setText:[_info valueForKey:@"name"]];
	[ivIcon setContentMode: _uivctm];
	
	[self addSubview:ivIcon];
	[self addSubview:lbIcon];
	
	
	[lbIcon setTextAlignment:UITextAlignmentCenter];
	[lbIcon setBackgroundColor:[UIColor clearColor]];
	[lbIcon setTextColor:[UIColor whiteColor]];
	[lbIcon setFont:[UIFont systemFontOfSize:14.0]];
    
//	[ivIcon setFrame:CGRectMake(10, 0, 60, 60)];
//	[lbIcon setFrame:CGRectMake(0, 60, 80, 20)];
}
-(BOOL)iconStage:(springIconStage)checkStage{
//	TLog(@"iconStage checkStage = %@",(checkStage==SIS_NORMAL?@"SIS_NORMAL":(checkStage==SIS_EDIT?@"SIS_EDIT":@"SIS_DELETE")));
//	TLog(@"iconStage _iconStageAll = %@",(_iconStageAll==SIS_NORMAL?@"SIS_NORMAL":(_iconStageAll==SIS_EDIT?@"SIS_EDIT":@"SIS_DELETE")));
	
	
	if ((_iconStage&checkStage)!=0) {
		return YES;
		//if((_iconStage&SIS_DELETE)!=0&&(checkStage&SIS_DELETE)!=0){
//			return YES;
//		}else if((_iconStage&SIS_LOADING)!=0&&(checkStage&SIS_LOADING)!=0){
//			return YES;
//		}else if((_iconStage&SIS_EDIT)!=0&&(checkStage&SIS_EDIT)!=0){
//			return YES;
//		}else if((_iconStage&SIS_NORMAL)!=0&&(checkStage&SIS_NORMAL)!=0){
//			return YES;
//		}else {
//			return NO;
//		}
	}
	return NO;
}
-(void)iconNormal{
//	[self iconScale:1.0 andH:1.0];
		[ivDelete setHidden:YES];
	_iconStage = SIS_NORMAL;
	[springIcon _allIconNormal];
					
}
-(void)iconEdit{
		
	_iconStage = SIS_EDIT;
	[springIcon _allIconEdit];
	if (![[_info valueForKey:@"editable"] isEqualToString:@"MOVE"]) {
		[ivDelete setHidden:NO];
	}
	
	

}
-(void)iconDelete{
//	_iconStage = SIS_DELETE;
//	NSInteger index = [self _findIconIdFromPoint:_lastPoint inPage:_currentPage];
	[_iconDelegate iconDidDelete:self];// atIndex:index];
}

-(void)iconScale:(CGFloat)w andH:(CGFloat)h{

	
	_tranScale = CGAffineTransformMakeScale(w, h);
	[UIView beginAnimations: nil context: nil]; 
	[UIView setAnimationDuration:0.1]; 
	[self setTransform:_tranScale];
	[UIView commitAnimations]; 
	
//	if ([self iconStage:SIS_EDIT]) {
//		[ivDelete setHidden:NO];
//	}else{
//		[ivDelete setHidden:YES];
//	}
}
-(void)iconRotate:(CGFloat) degree{
	CGAffineTransform tran = CGAffineTransformMakeRotation(DEG_TO_RAD*degree);
	[self setTransform:tran];
}
-(void)iconTranSlate:(CGFloat) x andY:(CGFloat)y{
	CGAffineTransform tran = CGAffineTransformMakeTranslation(x, y);
	[self setTransform:tran];
}
-(void)iconDukdik:(CGFloat) dudic{
	CGAffineTransform tran = CGAffineTransformMakeRotation(DEG_TO_RAD*dudic);
	tran = CGAffineTransformTranslate(tran,0,(dudic*((rand()%2)-1)));
	[UIView beginAnimations: nil context: nil]; 
	[UIView setAnimationDuration:0.08]; 
	[self setTransform:tran];
	[UIView commitAnimations]; 

}
-(void)enable{
	_disable = NO;
	[self setUserInteractionEnabled:YES];
	[self setAlpha:1.0];		
}
-(void)disable{
	_disable = YES;
	[self setUserInteractionEnabled:NO];
	[self setAlpha:0.5];		
}
-(BOOL)chkDisable{
	return _disable;
}
-(void)setIconMode:(springIconMode)mode{
    _iconMode = mode;
    if (!btIcon) {
        btIcon = [[UIButton alloc] initWithFrame:[self frame]];//[UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:btIcon]; 
//        [btIcon setFrame:rect];
//        [self addSubview:btIcon];
    }   
}
//#pragma mark NSCoding
//- (id)initWithCoder:(NSCoder *)decoder{
//	if (self = [super init])
//	{
//
//		self._info = [decoder decodeObjectForKey:@"INFO"];
//
//	} 
//	return self;
//}
//- (void)encodeWithCoder:(NSCoder *)encoder{//NSLog(@"encodeWithCoder__1");
//	
//
//	[encoder encodeObject:_info forKey:@"INFO"];//NSLog(@"encodeWithCoder__1.5");
//	
//	//NSLog(@"encodeWithCoder__2");
//}
#pragma mark --------------------------------
-(void)_proc{
//	NSTimeInterval now = [[NSDate date] timeIntervalSinceReferenceDate];
//	
//	
//	if(_controlStep< now ){NSLog(@"~~~~~~~~~~~~~~~~~~~~~ now = %f",now);
//		//		_controlStep = now+	_waitLoop;
//		
//		// do step
//		if ([self _checkSelfState:IS_NOR]) {NSLog(@"~~~IS_NOR");
//			//set default Transition
//			[_container setScrollEnabled:NO];
//			[appIcon _setGroupState:IS_EDIT];
//		}else if ([self _checkSelfState:IS_EDIT]) {NSLog(@"~~~IS_EDIT");					//Duk Dik
//			NSLog(@"_currentPoint = %f,%f",_currentPoint.x,_currentPoint.y);
//			//[self setCenter:CGPointMake(_currentPoint.x,_currentPoint.y)];	
//		}
//		
//	}
}
#pragma mark UIResponder
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{NSLog(@"touchesBegan.... AppIcon = %@",[self _info]);
//	[super touchesBegan:touches withEvent:event];
//	UITouch *touch = [touches anyObject];
//	if([touch tapCount]==2){//--- double click
//		
//	}
//	
//	CGPoint pt = [touch locationInView:self];//--- touch position
////	[_iconDelegate iconTouchBegin:pt withIcon:self];
//	//	NSLog(@"touchesBegan pt.x = %f",pt.x);
//	//	NSLog(@"touchesBegan pt.y = %f",pt.y);
//	//	NSLog(@"indexPath page = %d  index = %d",_idPath.section,_idPath.row);
//	//	if (pt.x>[_ivDelete frame].origin.x&&pt.x<([_ivDelete frame].origin.x+[_ivDelete frame].size.width)
//	//		&&pt.y>[_ivDelete frame].origin.y&&pt.y<([_ivDelete frame].origin.y+[_ivDelete frame].size.height)) {
//	//		NSLog(@"deletet");
//	//		[_iconDelegate iconDidDelete:self];
//	//	}
//	//	[self _startEdit];
//}
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
//	UITouch *touch = [touches anyObject];
//
//	
//	CGPoint pt = [touch locationInView:self];//--- touch position
//	
////	_currentPoint = pt;
////		[_iconDelegate iconTouchMove:pt withIcon:self];
////	if ([self hitTest:pt withEvent:event]&&[self _checkSelfState:IS_EDIT]) 
////	{
////		NSLog(@">> touchesMoved pt.x = %f",pt.x);
////		NSLog(@">> touchesMoved pt.y = %f",pt.y);
////		NSLog(@">> indexPath page = %d  index = %d",_idPath.section,_idPath.row);
//////		[self setCenter:CGPointMake(pt.x,pt.y)];	
////		
////		self.transform = CGAffineTransformMakeTranslation(_currentPoint.x, _currentPoint.y);
//////		[self setNeedsDisplay];
////	}
//}
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{NSLog(@"touchesEnded.... AppIcon");
//	[super touchesEnded:touches withEvent:event];
//	UITouch *touch = [touches anyObject];
//	if([touch tapCount]==2){//--- double click
//		
//	}
//	
//	CGPoint pt = [touch locationInView:self];//--- touch position
////		[_iconDelegate iconTouchEnd:pt withIcon:self];
////	if (pt.x>[_ivDelete frame].origin.x&&pt.x<([_ivDelete frame].origin.x+[_ivDelete frame].size.width)
////		&&pt.y>[_ivDelete frame].origin.y&&pt.y<([_ivDelete frame].origin.y+[_ivDelete frame].size.height)) {NSLog(@"deletet");
////		
////		[_iconDelegate iconDidDelete:self];
////	}
//	
//}

@end
