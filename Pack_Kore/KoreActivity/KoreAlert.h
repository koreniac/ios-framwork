//
//  MyAlert.h
//  iTruck
//
//  Created by tu on 6/4/2552.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#define kAuthenticating @"System operating"	//@"Sending"///@"กำลังเข้าสู่ระบบ"
#define kWaitingMessage @"Please wait"	//@"กรุณารอสักครู่..."

BOOL _activityIndicatorCheck;
static NSInteger _countHUD = 0;
UIAlertView *alertViewProgress;
static MBProgressHUD *__HUD = nil;
static MBProgressHUD *__HUD_II = nil;
void K_MyAlertWithHUDProgressShow(UIView *dependOn,NSString *title,CGFloat progress);
void K_MyAlertWithHUDIndicatorShow(UIView *dependOn,NSString *title);
void K_MyAlertWithHUDIndicatorStop();

void K_MyAlertWithHUDIndicatorShowII(UIView *dependOn,NSString *title);
void K_MyAlertWithHUDIndicatorStopII();

void K_MyAlertWithTitleAndActionOne(id delegate, NSString *title,NSString* acOne,NSString *okButton);
void K_MyAlertWithTitleAndAction(id delegate, NSString *title,NSString* acOne, NSString* acTwo,NSString *okButton);
void K_MyAlertWithError(NSError *error);

void K_MyAlertWithMessageAndOkButton(NSString *message,NSString *okButton);
void K_MyAlertWithMessage(NSString *message);
void K_MyAlertWithMessageSetPoint(NSString *message, CGPoint pnt);
void K_MyAlertWithTitleAndMessage(NSString*title,NSString *message);
void K_MyAlertWithMessageAndDelegate(NSString *message, id delegate,NSInteger butNum);
void K_MyAlertWithTitleAndMessageAndDelegate(NSString*title, NSString *message, id delegate,NSInteger butNum);
void K_MyAlertWithTitleAndMessageAndDelegateWithButtonString(NSString*title, NSString *message, id delegate,NSInteger butNum, NSString *strOk,NSString *strCancel);
UIAlertView* K_MyAlertWithMessageAndDelegateAndTextField(NSString *message, id delegate,NSInteger butNum,UITextField *textfield);

void K_MyAlertWithActivityIndicatorStop();
void K_MyAlertWithActivityIndicatorShow();
void K_MyAlertWithActivityIndicatorShowWihtMessage(NSString *title, NSString *message);
void K_MyAlertWithProgressIndicatorShowWithTitle(NSString *title, UIProgressView *progressIndicator);

void K_MyAlertWithTable( id delegate,NSString *title, UITableView *tbView,NSString *okButton, NSString *cancelButton);