//
//  MyAlert.m
//  iTruck
//
//  Created by tu on 6/4/2552.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "KoreAlert.h"

#define buttonOKTitle @"OK"	//@"ตกลง"
#define buttonCancelTitle @"Cancel"	//@"ยกเลิก"
#define kAppTitle @""	//_APP_NAME
static NSInteger _popCount = 0;

//_APP_NAME
//@"mobileTV"
void K_MyAlertWithHUDProgressShow(UIView *dependOn,NSString *title,CGFloat progress){
    if (!__HUD) {
        
        if (dependOn) {
            __HUD = [[MBProgressHUD alloc] initWithView:dependOn];
            [dependOn addSubview:__HUD];
        }else{
            __HUD = [[MBProgressHUD alloc] initWithView:[APP_DELEGATE window]];
            [[APP_DELEGATE window] addSubview:__HUD];            
        }
        
        
        if (title) {
            __HUD.labelText = title;
        }else{
            __HUD.labelText = @"Connecting";
        }
        __HUD.mode = MBProgressHUDModeDeterminate;
        
//        __HUD.minSize = CGSizeMake(135.f, 135.f);
//        [__HUD show:YES];
            [__HUD show:YES];
    }
    
//    [__HUD setNeedsDisplay];
//    usleep(50000);

    __HUD.progress = progress;
}
void K_MyAlertWithHUDIndicatorShowWithView(UIView *dependOn,NSString *title,UIView *view){
    if (!__HUD) {
        
        if (dependOn) {
            __HUD = [[MBProgressHUD alloc] initWithView:dependOn];
            [dependOn addSubview:__HUD];
        }else{
            __HUD = [[MBProgressHUD alloc] initWithView:[APP_DELEGATE window]];
            [[APP_DELEGATE window] addSubview:__HUD];            
        }
        
        
        if (title) {
            __HUD.labelText = title;
        }else{
            __HUD.labelText = @"Connecting";
        }
        __HUD.mode = MBProgressHUDModeCustomView;
        __HUD.minSize = CGSizeMake(135.f, 135.f);
        [__HUD show:YES];
        [__HUD addSubview:view];
    }

}
void K_MyAlertWithHUDIndicatorShow(UIView *dependOn,NSString *title){
//    __HUD = [[MBProgressHUD alloc] initWithView:dependOn];
//	[dependOn addSubview:__HUD];
    
    if (!__HUD) {
            ++_countHUD;
        if (dependOn) {
            __HUD = [[MBProgressHUD alloc] initWithView:dependOn];
            [dependOn addSubview:__HUD];
        }else{
            __HUD = [[MBProgressHUD alloc] initWithView:[APP_DELEGATE window]];
            [[APP_DELEGATE window] addSubview:__HUD];            
        }

        
        if (title) {
            __HUD.labelText = title;
        }else{
                __HUD.labelText = @"Connecting";
        }

        __HUD.minSize = CGSizeMake(135.f, 135.f);
        [__HUD show:YES];
    }else {
        [__HUD removeFromSuperview];
        if (dependOn) {

            [dependOn addSubview:__HUD];
        }else{

            [[APP_DELEGATE window] addSubview:__HUD];            
        }
    }
}
void K_MyAlertWithHUDIndicatorStop(){
    --_countHUD;
    if (__HUD&&_countHUD<=0) {
        _countHUD = 0;
        [__HUD removeFromSuperview];
//        [__HUD hide:YES];    
        [__HUD release];
        __HUD = nil;
    }

}

void K_MyAlertWithHUDIndicatorShowII(UIView *dependOn,NSString *title){
    //    __HUD = [[MBProgressHUD alloc] initWithView:dependOn];
    //	[dependOn addSubview:__HUD];
    if (!__HUD_II) {
        
        if (dependOn) {
            __HUD_II = [[MBProgressHUD alloc] initWithView:dependOn];
            [dependOn addSubview:__HUD];
        }else{
            __HUD_II = [[MBProgressHUD alloc] initWithView:[APP_DELEGATE window]];
            [[APP_DELEGATE window] addSubview:__HUD];            
        }
        
        
        if (title) {
            __HUD_II.labelText = title;
        }else{
            __HUD_II.labelText = @"Connecting";
        }
        
        __HUD_II.minSize = CGSizeMake(135.f, 135.f);
        [__HUD_II show:YES];
    }
}
void K_MyAlertWithHUDIndicatorStopII(){
    if (__HUD_II) {
        [__HUD_II removeFromSuperview];
        //        [__HUD hide:YES];    
        [__HUD_II release];
        __HUD_II = nil;
    }
    
}


void K_MyAlertWithError(NSError *error)
{
    NSString *message = [NSString stringWithFormat:@"Error! %@ %@", [error localizedDescription],[error localizedFailureReason]];
	K_MyAlertWithMessage (message);
}
void K_MyAlertWithTitleAndActionOne(id delegate, NSString *title,NSString* acOne,NSString *okButton){
	UIActionSheet *_asAction = [[UIActionSheet alloc] initWithTitle:title delegate:delegate cancelButtonTitle:okButton destructiveButtonTitle:nil otherButtonTitles:acOne,nil];			
	[_asAction setFrame:CGRectMake(0, 0, [delegate view].frame.size.width, [delegate view].frame.size.height-200)];
	
	[_asAction showInView:[delegate view]];


}
void K_MyAlertWithTitleAndAction(id delegate, NSString *title,NSString* acOne, NSString* acTwo,NSString *okButton){
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:nil delegate:delegate cancelButtonTitle:okButton otherButtonTitles: acOne,acTwo,nil];
	[alert show];
	[alert release];	
}
void K_MyAlertWithMessageAndOkButton(NSString *message,NSString *okButton)
{//++_popCount;
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kAppTitle message:message delegate:nil cancelButtonTitle:okButton otherButtonTitles: nil];
	[alert show];
	[alert release];
}
void K_MyAlertWithMessage(NSString *message)
{//++_popCount;
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kAppTitle message:message delegate:nil cancelButtonTitle:buttonOKTitle otherButtonTitles: nil];
	[alert show];
	[alert release];
}
void  K_MyAlertWithMessageSetPoint(NSString *message, CGPoint pnt)
{//++_popCount;
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kAppTitle message:message delegate:nil cancelButtonTitle:buttonOKTitle otherButtonTitles: nil];
	[alert setCenter:pnt];
	[alert show];
	[alert release];
}
void K_MyAlertWithTitleAndMessage(NSString*title,NSString *message)
{//++_popCount;
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:buttonOKTitle otherButtonTitles: nil];
	[alert show];
	[alert release];
}


void K_MyAlertWithMessageAndDelegate(NSString *message, id delegate,NSInteger butNum)
{//++_popCount;
	UIAlertView *alert = nil;
	if (butNum==1) {
		alert = [[UIAlertView alloc] initWithTitle:kAppTitle message:message delegate:delegate cancelButtonTitle:buttonOKTitle otherButtonTitles: nil];
	}else if (butNum==2) {
		alert = [[UIAlertView alloc] initWithTitle:kAppTitle message:message delegate:delegate cancelButtonTitle:buttonOKTitle  otherButtonTitles:buttonCancelTitle, nil];
	}
	[alert show];
	[alert release];
}
void K_MyAlertWithTitleAndMessageAndDelegate(NSString*title, NSString *message, id delegate,NSInteger butNum)
{//++_popCount;
	UIAlertView *alert = nil;
	if (butNum==1) {
		alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:buttonOKTitle otherButtonTitles: nil];
	}else if (butNum==2) {
		alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:buttonOKTitle  otherButtonTitles:buttonCancelTitle, nil];
	}
	[alert show];
	[alert release];
}
void K_MyAlertWithTitleAndMessageAndDelegateWithButtonString(NSString*title, NSString *message, id delegate,NSInteger butNum, NSString *strOk,NSString *strCancel){
    UIAlertView *alert = nil;
	if (butNum==1) {
		alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:strOk otherButtonTitles: nil];
	}else if (butNum==2) {
		alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:strOk  otherButtonTitles:strCancel, nil];
	}
	[alert show];
	[alert release];
}
UIAlertView* K_MyAlertWithMessageAndDelegateAndTextField(NSString *title, id delegate,NSInteger butNum,UITextField *textfield)
{//++_popCount;
	UIAlertView *alert = nil;
//	textfield = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
	[textfield setDelegate:delegate];
	[textfield becomeFirstResponder];
	[textfield setEnablesReturnKeyAutomatically:YES];
	[textfield setReturnKeyType:UIReturnKeyDone];
	[textfield setClearButtonMode:UITextFieldViewModeWhileEditing];
	if (butNum==1) {
		alert = [[UIAlertView alloc] initWithTitle:title message:@"blank for TextField" delegate:delegate cancelButtonTitle:buttonOKTitle otherButtonTitles: nil];
	}else if (butNum==2) {
		alert = [[UIAlertView alloc] initWithTitle:title message:@"blank for TextField" delegate:delegate cancelButtonTitle:buttonOKTitle  otherButtonTitles:buttonCancelTitle, nil];
	}
	[textfield setBackgroundColor:[UIColor whiteColor]];
	[alert addSubview:textfield];
//	[tf setCenter:CGPointMake(160, alert.frame.size.height/2)];
//	CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 100.0);
//	[alert setTransform: moveUp];
	[alert setCenter:CGPointMake(160.0, 100.0)];
	[alert show];
	
//	[alert release];
//	[tf release];
	return [alert autorelease];
}

void K_MyAlertWithActivityIndicatorStop()
{	_activityIndicatorCheck = NO;
	--_popCount;
//	if (_popCount>0) {
//		return;
//	}
	if ([alertViewProgress retainCount]>0)
	{
		[alertViewProgress dismissWithClickedButtonIndex:0 animated:NO];
//		[alertViewProgress release];
	}
}

void K_MyAlertWithActivityIndicatorShow()
{++_popCount;
	//if (!_activityIndicatorCheck)
	{
		

	_activityIndicatorCheck = YES;
	UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	[activityIndicatorView startAnimating];
	activityIndicatorView.frame = CGRectMake(120.0, 72.0, 35.0, 35.0);
	alertViewProgress = [[UIAlertView alloc] initWithTitle:kAuthenticating message:kWaitingMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
	[alertViewProgress addSubview:activityIndicatorView];
	[activityIndicatorView release];
	
	[alertViewProgress show];
	}
}

void K_MyAlertWithActivityIndicatorShowWihtMessage(NSString *title, NSString *message)
{++_popCount;
	if ([alertViewProgress retainCount]<2) 
	{
		if ([alertViewProgress retainCount]>0) {
			[alertViewProgress release];
		}
	_activityIndicatorCheck = YES;
	UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	[activityIndicatorView startAnimating];
	activityIndicatorView.frame = CGRectMake(120.0,  72.0, 35.0, 35.0);
	alertViewProgress = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
	[alertViewProgress addSubview:activityIndicatorView];
	[activityIndicatorView release];
	
	[alertViewProgress show];
	}

}

void K_MyAlertWithProgressIndicatorShowWithTitle(NSString *title, UIProgressView *progressIndicator)
{++_popCount;
//	if (_activityIndicatorCheck) {
//		return;
//	}
//	_activityIndicatorCheck =  YES;
	
	progressIndicator.frame = CGRectMake(20.0, 85.0, 250.0, 20.0);
	progressIndicator.progress = 0.0;
	alertViewProgress = [[UIAlertView alloc] initWithTitle:title message:@"UpPlease Wait..." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
	[alertViewProgress addSubview:progressIndicator];
	[progressIndicator release];
	
	[alertViewProgress show];
}
//nOt good
void K_MyAlertWithTable( id delegate,NSString *title, UITableView *tbView,NSString *okButton, NSString *cancelButton)
{

	{
		UIView	*view = [[UIView alloc] initWithFrame:CGRectMake(20, 40, 240.0, 79.0)];
		[tbView setFrame:CGRectMake(0.0, 0.0, view.frame.size.width, view.frame.size.height)];		
		[view addSubview:tbView];
		alertViewProgress = [[UIAlertView alloc] initWithTitle:title message:nil delegate:delegate cancelButtonTitle:okButton otherButtonTitles:cancelButton,@"",@"",nil];
		
		[alertViewProgress addSubview:view];

		[alertViewProgress setClipsToBounds:YES];
		[alertViewProgress show];
	}
}
void K_MyAlertWithView( id delegate,NSString *title, UIView *view,NSString *okButton, NSString *cancelButton)
{
	
	{

		

		alertViewProgress = [[UIAlertView alloc] initWithTitle:title message:@"blank for TextField \n blank for TextField \n blank for TextField" delegate:delegate cancelButtonTitle:okButton otherButtonTitles:cancelButton,nil];
		
		[alertViewProgress addSubview:view];
		[view setFrame:CGRectMake(10.0, 40.0, alertViewProgress.frame.size.width-10.0, alertViewProgress.frame.size.height-(40*2))];		
		[alertViewProgress setClipsToBounds:YES];
		[alertViewProgress show];
	}
}