//
//  wcActivities.h
//  WorldCup
//
//  Created by Naruphon Sirimasrungsee on 3/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


#define WCAC_POS CGPointMake(160, 240)
#define LOADING_0 @"preloader_0"
#define LOADING_FRAME_VIEW CGRectMake(0, 0, 320, 120)
#define LOADING_FRAME_CELL CGRectMake(0, 0, 320, 44)
@interface wcActivities : UIView {
	UIActivityIndicatorView *_ac;
	UIImageView *_ivLoading;
	NSMutableArray *_arrLoading;

}
@property (nonatomic,retain) IBOutlet UIImageView	 *_ivLoading;
@property (nonatomic,retain)IBOutlet UIActivityIndicatorView *_ac;


-(void)_start;
-(void)_stop;
-(void)_reSize;
-(void)_startCell;
@end
