//
//  wcActivities.m
//  WorldCup
//
//  Created by Naruphon Sirimasrungsee on 3/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "wcActivities.h"


@implementation wcActivities
@synthesize _ac;
@synthesize _ivLoading;

-(void)_reSize{
	[self setFrame:LOADING_FRAME_VIEW];
	[_ivLoading setCenter:CGPointMake([self frame].size.width/2, [self frame].size.height/2)];
}
-(void)_startCell{
	[self _start];
	[self setFrame:LOADING_FRAME_CELL];
	[_ivLoading setCenter:CGPointMake([self frame].size.width/2, [self frame].size.height/2)];
}
-(void)_start{
	//[_ac setHidden:NO];
	//[_ac startAnimating];
	////////TLog(@"_arrLoading = %@",_arrLoading);
//	[_ivLoading startAnimating];
	[self setHidden:NO];
	
}
-(void)_stop{
//	[_ac stopAnimating];

	[self setHidden:YES];
//	[self setNeedsDisplay];
//	[_ivLoading stopAnimating];
}
- (void)awakeFromNib{////////TLog(@"------------ awakeFromNib");
	//[_ac setHidden:YES];
	
	_arrLoading = [[NSMutableArray alloc] init];
	for(NSInteger i=0;i<3;i++){
	//////TLog(@"name = %@",[NSString stringWithFormat:@"%@%d.png",LOADING_0,(i+1)]);
		UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@%d.png",LOADING_0,(i+1)]];
		
		[_arrLoading addObject:img];
	}
	[_ivLoading setAnimationImages:_arrLoading];
	[_ivLoading setAnimationDuration:0.3];
		[_ivLoading startAnimating];
//	[self _reSize];
}
- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
		//[_ac setHidden:YES];
		_arrLoading = [[NSMutableArray alloc] init];
		for(NSInteger i=0;i<3;i++){
			//	//////TLog(@"name = %@",[NSString stringWithFormat:@"%@%d.png",LOADING_0,(i+1)]);
			UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@%d.png",LOADING_0,(i+1)]];
			
			[_arrLoading addObject:img];
		}
		[_ivLoading setAnimationImages:_arrLoading];
		[_ivLoading setAnimationDuration:0.2];
				[_ivLoading startAnimating];
		[self _reSize];
		
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    // Drawing code
}


- (void)dealloc {
    [super dealloc];
}


@end
