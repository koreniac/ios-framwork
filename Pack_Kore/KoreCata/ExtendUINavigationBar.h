//
//  ExtendUINavigationBar.h
//  TrueMovie
//
//  Created by Naruphon Sirimasrungsee on 8/13/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ExtendUINavigationBar : UINavigationBar {

	UIImage *_bgImg;
	UIImage *_backButton;
	UILabel *_lbTitle;
	CGFloat _size;
	BOOL	_sizeDependNib;
}
-(void)_setSizeDependNib;
-(void)_setBackgroundImage:(UIImage *) bg;
-(void)_setBackButton:(UIImage *) back;
-(void)_setTitle:(NSString*) title withColor:(UIColor*) col;
-(void)_setTitle:(NSString*) title withColor:(UIColor*) col andSize:(CGFloat) size;
-(void)_setTitleImage:(UIImage*) img;
-(void)_setFontSize:(CGFloat) size;
-(void)_setTitleView:(UIView*) titleView;

@end
