//
//  ExtendUINavigationBar.m
//  TrueMovie
//
//  Created by Naruphon Sirimasrungsee on 8/13/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "ExtendUINavigationBar.h"


@implementation ExtendUINavigationBar


- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
    [super dealloc];
}
-(void)awakeFromNib{
	[super awakeFromNib];
	_lbTitle = nil;
	[[self topItem] setTitle:@""];
	_size = 24.0;
	_sizeDependNib = NO;
}
-(void)_setSizeDependNib{
	_sizeDependNib= YES;
}
-(void)_setFontSize:(CGFloat) size{
	_size = size;
}
-(void)_setBackButton:(UIImage *) back{
	_backButton = back;
}
-(void)_setBackgroundImage:(UIImage *) bg{//////TLog(@"___ _setBackgroundImage");
#ifdef USE_UI_CAT
	//////TLog(@"___ USE_UI_CAT");
	_bgImg = bg;//[bg retain];
	if(bg == NULL){ //might be called with NULL argument
		return;
	}
	UIImageView *aTabBarBackground = [[UIImageView alloc]initWithImage:bg];
	if (_sizeDependNib) {
		aTabBarBackground.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
	}else {
		aTabBarBackground.frame = CGRectMake(0,0,320,46);		
	}

	//

	//	[self addSubview:aTabBarBackground];
	//	[self sendSubviewToBack:aTabBarBackground];
	
	[self insertSubview:aTabBarBackground atIndex:0];//////TLog(@"______________ insertSubview");
	[aTabBarBackground release];
#endif
}
-(void)_setTitle:(NSString*) title withColor:(UIColor*) col andSize:(CGFloat) size{
#ifdef USE_UI_CAT
	//[[self topItem] setTitle:@""];
	//	[[self topItem] setTitle:title];
	////TLog(@"_size = %f",size);
	UIColor *newCol = col;
	CGRect frame;
	if (_sizeDependNib) {
		frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
	}else {
		frame = CGRectMake(0,0,320,44);		
	}
	
	if(!_lbTitle)
	{
		if (_lbTitle) {
			[_lbTitle removeFromSuperview];
		}
		_lbTitle = [[UILabel alloc] initWithFrame:frame];
		[self addSubview:_lbTitle];
		//	[self insertSubview:_lbTitle atIndex:0];
	}
	
	[_lbTitle setText:@""];
	[_lbTitle setText:title];
	if (!newCol) {
		newCol = [UIColor blackColor];
	}
	[_lbTitle setTextColor:newCol];
	[_lbTitle setTextAlignment:UITextAlignmentCenter];
	[_lbTitle setBackgroundColor:[UIColor clearColor]];
	//	[lbTitle setAdjustsFontSizeToFitWidth:YES];
	[_lbTitle setShadowColor:[UIColor grayColor]];
	[_lbTitle setShadowOffset:CGSizeMake(0.5, 0.5)];
	//	[lbTitle setMinimumFontSize:10];
	//UIFont *fon = [[UIFont alloc] fontWithSize:55.0];
	[_lbTitle setFont:[UIFont boldSystemFontOfSize:size]];
	
	//	if(!_navTitle){
	//		_navTitle = [[UINavigationItem alloc] initWithTitle:title];
	//		[self pushNavigationItem:_navTitle animated:YES];
	//	}
	
#endif	
	
}
- (void)_setTitle:(NSString*) title withColor:(UIColor*) col{
	[self _setTitle:title withColor:col andSize:_size];
}
-(void)_setTitleImage:(UIImage*) img{
#ifdef USE_UI_CAT
	UIImageView *imgv = [[UIImageView alloc] initWithImage:img];
	[[self topItem] setTitleView:imgv];
#endif
}
-(void)_setTitleView:(UIView*) titleView{
#ifdef USE_UI_CAT

	[[self topItem] setTitleView:titleView];
#endif
}


@end
