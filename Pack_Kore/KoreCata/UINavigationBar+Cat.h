//
//  UINavigationBar+Cat.h
//  WorldCup
//
//  Created by Naruphon Sirimasrungsee on 3/24/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//



#import <Foundation/Foundation.h>


@interface UINavigationBar (Cat)
	UIImage *_bgImg;
	UIImage *_backButton;
	UILabel *_lbTitle;
	CGFloat _size;

-(void)_setBackgroundImage:(UIImage *) bg;
-(void)_setBackButton:(UIImage *) back;
-(void)_setTitle:(NSString*) title withColor:(UIColor*) col;
-(void)_setTitle:(NSString*) title withColor:(UIColor*) col andSize:(CGFloat) size;
-(void)_setTitleImage:(UIImage*) img;
-(void)_setFontSize:(CGFloat) size;
@end
