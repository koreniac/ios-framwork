//
//  UINavigationBar+Cat.m
//  WorldCup
//
//  Created by Naruphon Sirimasrungsee on 3/24/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "UINavigationBar+Cat.h"

//static UILabel *_lbTitle = nil;
//static UINavigationItem *_navTitle = nil;
@implementation UINavigationBar (Cat)

-(void)awakeFromNib{
	[super awakeFromNib];
	_lbTitle = nil;
	[[self topItem] setTitle:@""];
	_size = 24.0;
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 30200

	UIDevice *deviceInfo = [UIDevice currentDevice];
	if(deviceInfo.userInterfaceIdiom == UIUserInterfaceIdiomPad)
	{
		[self setFrame:CGRectMake(0, 0, 704, 44)];
	}else {
		
	}
	
	
#endif
}
-(void)_setFontSize:(CGFloat) size{
	_size = size;
}
-(void)_setBackButton:(UIImage *) back{
	_backButton = back;
}
-(void)_setBackgroundImage:(UIImage *) bg{//////TLog(@"___ _setBackgroundImage");
	#ifdef USE_UI_CAT
	//////TLog(@"___ USE_UI_CAT");
	_bgImg = bg;//[bg retain];
	if(bg == NULL){ //might be called with NULL argument
		return;
	}
	UIImageView *aTabBarBackground = [[UIImageView alloc]initWithImage:bg];
	aTabBarBackground.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
//	[self addSubview:aTabBarBackground];
//	[self sendSubviewToBack:aTabBarBackground];
	
	[self insertSubview:aTabBarBackground atIndex:0];//////TLog(@"______________ insertSubview");
	[aTabBarBackground release];
#endif
}
-(void)_setTitle:(NSString*) title withColor:(UIColor*) col andSize:(CGFloat) size{
#ifdef USE_UI_CAT
	//[[self topItem] setTitle:@""];
	//	[[self topItem] setTitle:title];
	////TLog(@"_size = %f",size);
	UIColor *newCol = col;
	CGRect frame = CGRectMake(0.0, 0.0, self.bounds.size.width, self.bounds.size.height);
	if(!_lbTitle)
	{
		if (_lbTitle) {
			[_lbTitle removeFromSuperview];
		}
		_lbTitle = [[UILabel alloc] initWithFrame:frame];
		[self addSubview:_lbTitle];
		//	[self insertSubview:_lbTitle atIndex:0];
	}
	
	[_lbTitle setText:@""];
	[_lbTitle setText:title];
	if (!newCol) {
		newCol = [UIColor blackColor];
	}
	[_lbTitle setTextColor:newCol];
	[_lbTitle setTextAlignment:UITextAlignmentCenter];
	[_lbTitle setBackgroundColor:[UIColor clearColor]];
	//	[lbTitle setAdjustsFontSizeToFitWidth:YES];
	[_lbTitle setShadowColor:[UIColor grayColor]];
	[_lbTitle setShadowOffset:CGSizeMake(0.5, 0.5)];
	//	[lbTitle setMinimumFontSize:10];
	//UIFont *fon = [[UIFont alloc] fontWithSize:55.0];
	[_lbTitle setFont:[UIFont boldSystemFontOfSize:size]];
	
	//	if(!_navTitle){
	//		_navTitle = [[UINavigationItem alloc] initWithTitle:title];
	//		[self pushNavigationItem:_navTitle animated:YES];
	//	}
	
#endif	
	
}
- (void)_setTitle:(NSString*) title withColor:(UIColor*) col{
	[self _setTitle:title withColor:col andSize:_size];
}
-(void)_setTitleImage:(UIImage*) img{
#ifdef USE_UI_CAT
	UIImageView *imgv = [[UIImageView alloc] initWithImage:img];
	[[self topItem] setTitleView:imgv];
#endif
}
//- (void)drawRect:(CGRect)rect {
//	// Drawing code
//	UIImage *bg = [_bgImg retain];// [[UIImage imageNamed:@"NavBar2.png"] retain];
//	UIImage *backB = [_backButton retain];
//	
//	CGRect bbRect = CGRectMake(rect.origin.x+5 , rect.origin.y+5, backB.size.width, backB.size.height);
//	[bg drawInRect:rect];
//	[backB drawInRect:bbRect];
//	
//	
//	
//	[bg release];
//	[backB release];
//}
@end
