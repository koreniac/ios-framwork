//
//  UITabBar+Cat.h
//  mobileTV
//
//  Created by Naruphon Sirimasrungsee on 6/4/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//
// Howto
//		NSArray *arr = [[NSArray alloc] initWithObjects:@"bt_home_01.png",@"bt_home_02.png",@"bt_movie_01.png",@"bt_movie_02.png",@"bt_setting_01.png",@"bt_setting_02.png",nil];
//		[[_tbController tabBar] setTabItemList:arr];
//		[[_tbController tabBar] setBgImage:[UIImage imageNamed:@"menu_bar.png"]];
//		[_tbController setDelegate:self];
//		[[_tbController tabBar] setSelectedId:0];
//
//		#pragma mark -
//		#pragma mark UITabBarControllerDelegate
//		- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
//			if([tabBarController isEqual:_tbController]){
//				[[_tbController tabBar] setSelectedId:[_tbController selectedIndex]];
//				
//			}
//		}
#import <Foundation/Foundation.h>


@interface UITabBar (Cat) 
NSMutableArray *_tabItemList;
NSArray *_tabRes;
NSInteger _tabId;
UIView *v;

- (void)recolorItemsWithColor:(UIColor *)color shadowColor:(UIColor *)shadowColor shadowOffset:(CGSize)shadowOffset shadowBlur:(CGFloat)shadowBlur; 
- (void)setBgColor:(UIColor*) color;
- (void)setBgImage:(UIImage*) img;
-(void)setTabItemList:(NSArray*) list;
-(void)setSelectedId:(NSInteger) tabId;
-(NSInteger)getSelectedId;
@end

