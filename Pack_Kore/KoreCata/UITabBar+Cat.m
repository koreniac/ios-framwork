//
//  UITabBar+Cat.m
//  mobileTV
//
//  Created by Naruphon Sirimasrungsee on 6/4/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//
#import "UITabBar+Cat.h"



#ifdef USE_UI_CAT
@interface UITabBarItem (Private)
@property(retain, nonatomic) UIImage *selectedImage;
- (void)_updateView;
@end
#endif

@implementation UITabBar (Cat)

-(void)awakeFromNib{
	[super awakeFromNib];
	_tabItemList = nil;
	_tabRes = nil;
	_tabId = 0;
	v = nil;
}
-(void)setTabItemList:(NSArray*) list{
	_tabRes = [[NSArray alloc] initWithArray:list];
}
- (void)recolorItemsWithColor:(UIColor *)color shadowColor:(UIColor *)shadowColor shadowOffset:(CGSize)shadowOffset shadowBlur:(CGFloat)shadowBlur
{
#ifdef USE_PRIVATE_LIB
#ifdef USE_UI_CAT 

    CGColorRef cgColor = [color CGColor];
    CGColorRef cgShadowColor = [shadowColor CGColor];
    for (UITabBarItem *item in [self items])
        if ([item respondsToSelector:@selector(selectedImage)] &&
            [item respondsToSelector:@selector(setSelectedImage:)] &&
            [item respondsToSelector:@selector(_updateView)])
        {
            CGRect contextRect;
            contextRect.origin.x = 0.0f;
            contextRect.origin.y = 0.0f;
            contextRect.size = [[item selectedImage] size];
            // Retrieve source image and begin image context
            UIImage *itemImage = [item image];
            CGSize itemImageSize = [itemImage size];
            CGPoint itemImagePosition; 
            itemImagePosition.x = ceilf((contextRect.size.width - itemImageSize.width) / 2);
            itemImagePosition.y = ceilf((contextRect.size.height - itemImageSize.height) / 2);
            UIGraphicsBeginImageContext(contextRect.size);
            CGContextRef c = UIGraphicsGetCurrentContext();
            // Setup shadow
            CGContextSetShadowWithColor(c, shadowOffset, shadowBlur, cgShadowColor);
            // Setup transparency layer and clip to mask
            CGContextBeginTransparencyLayer(c, NULL);
            CGContextScaleCTM(c, 1.0, -1.0);
            CGContextClipToMask(c, CGRectMake(itemImagePosition.x, -itemImagePosition.y, itemImageSize.width, -itemImageSize.height), [itemImage CGImage]);
            // Fill and end the transparency layer
            CGContextSetFillColorWithColor(c, cgColor);
            contextRect.size.height = -contextRect.size.height;
            CGContextFillRect(c, contextRect);
            CGContextEndTransparencyLayer(c);
            // Set selected image and end context
            [item setSelectedImage:UIGraphicsGetImageFromCurrentImageContext()];
            UIGraphicsEndImageContext();
            // Update the view
            [item _updateView];
        }
	#endif
#endif
}
- (void)setBgColor:(UIColor*) color{
#ifdef USE_UI_CAT
	CGRect frame = CGRectMake(0.0, 0.0, self.bounds.size.width, self.bounds.size.height);
	UIView *v = [[UIView alloc] initWithFrame:frame];

	[v setBackgroundColor:[UIColor redColor]];
	[v setAlpha:0.5];


	[self insertSubview:v atIndex:0];
	//[v release];
#endif
}
- (void)setBgImage:(UIImage*) img{////TLog(@"setBgImage");
#ifdef USE_UI_CAT
    if (!img) {
        return;
    }
	CGRect _frame =  CGRectMake(0, 0, 320, 49);//CGRectMake(0.0, 0.0, self.bounds.size.width, self.bounds.size.height);//
	////TLog(@"frame = %f",_frame.size.width);
	////TLog(@"frame = %f",_frame.size.height);
	if (!v) {
			v = [[UIView alloc] initWithFrame:_frame];////TLog(@"______ 3");
		[self insertSubview:v atIndex:0];////TLog(@"______ 1");
	}
    if (img) {
        UIImageView *imgv = [[UIImageView alloc] initWithFrame:_frame];
        [imgv setImage:img];
        
        [v addSubview:imgv];        
    }
	

	if (!_tabRes) {
        return;
    }
		
		CGFloat offX = 0;
		CGFloat offY = 0;
	NSInteger numTab = [_tabRes count]/2;
	CGFloat tabW  = 320/numTab;
	////TLog(@"______ 2");
	_tabItemList = [[NSMutableArray alloc] init];
		
		for (int i=0; i<numTab; i++) {
			UIImage *img = [UIImage imageNamed:[_tabRes objectAtIndex:(i*2)]];
			UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
			[_tabItemList  addObject:imgView];
			[v addSubview:imgView];
			offY = (49-img.size.height)/2;
			offX = (tabW-img.size.width)/2;
			CGFloat x = (i*tabW)+offX;
			CGFloat y = offY;
			[imgView setFrame:CGRectMake(x, y, img.size.width, img.size.height)];
		}
	
	//[self addSubview:v];

	//[self insertSubview:v atIndex:0];////TLog(@"______ 1");
	
#endif
}
-(void)setSelectedId:(NSInteger) tabId{
	_tabId = tabId;
	for (int i=0; i<[_tabItemList count]; i++) {
		UIImageView *ivTmp = [_tabItemList objectAtIndex:i];
		UIImage *img = nil;
		if (i==tabId) {
			img = [UIImage imageNamed:[_tabRes objectAtIndex:((i*2)+1)]];
			
		}else {
			img = [UIImage imageNamed:[_tabRes objectAtIndex:(i*2)]];
		}
		[ivTmp setImage:img];
	}
}
-(NSInteger)getSelectedId{
	return _tabId;
}
@end

