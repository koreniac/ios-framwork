//
//  UITableViewCell+Nib.h
//  WorldCup
//
//  Created by Naruphon Sirimasrungsee on 3/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//




@interface UITableViewCell (Nib) <NSCopying>
-(id)_initCellWithNib;
@end
