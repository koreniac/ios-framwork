//
//  UITableViewCell+Nib.m
//  WorldCup
//
//  Created by Naruphon Sirimasrungsee on 3/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "UITableViewCell+Nib.h"

@implementation UITableViewCell (Nib)
-(id)_initCellWithNib{
	
	//if(self= [super init])
	{

		NSString*			className	= NSStringFromClass([self class]);
		TLog(@"class Name = %@",className);
		if (self = [[[NSBundle mainBundle] loadNibNamed:className
												  owner:self
												options:nil] objectAtIndex:0]) {
			[self setHidesAccessoryWhenEditing:NO];
		}
//        [className release];
//		[self setBackgroundColor:UIColorFromRGB(APP_THEME_COLOR)];//
//        [[self backgroundView] setBackgroundColor:UIColorFromRGB(APP_THEME_COLOR)];
//        [[self contentView] setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
//        [[self accessoryView] setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
//        UIView *bgView = [[UIView alloc] initWithFrame:CGRectZero];
//        [bgView setBackgroundColor:UIColorFromRGB(APP_BG_COLOR)];
////        bgView.borderColor = [UIColor yellowColor];
//        [self setBackgroundView:bgView];
	}
		
	return self;
}
- (id)copyWithZone:(NSZone *)zone{
	return self;
}
@end
