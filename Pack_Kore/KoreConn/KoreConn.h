//------------------------------------------------------------------------------------------------------------------
//																	version 1.2.0
//	Class KoreConn
//	Create by Naruphon Sirimasrungsee
//  
//	UpDate   07/01/2010												version 1.2.0
//		More Support
//			1. Prepare  Date Decision for Cancel connection
//			2. add @_progress  for get dowload progress  
//
//
//	Description     made everything about connection
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//	HowTo:
//
//
//			_mainCon = [[KoreConn alloc]initCon:self iden:[NSString stringWithFormat:@"page"]];
//
////////////																											## Get Xml
//
//			[_mainCon setUrl:[NSString stringWithFormat:@"http://widget.truelife.com/agg/getxml.jsp?guid=20090710150029773216&pageno=%d",_pageCount]];
//			[_mainCon setMode:GET];
//			[_mainCon startCon];																									V. 1.1.0
//
//			@KoreConnDelegate
//			-(void)connectionDidFinish:(KoreConn *) con{
//				if([[con _identifier] isEqualToString:@"page"]){
//					_xmlParser = [[NSXMLParser alloc] initWithData:[con _receivedData]];
//					[_xmlParser setDelegate:self];
//					[_xmlParser parse];
//				}
//			}
//
//			-(void)connectionDidFailed:(KoreConn *) con{
//
//			}
////////////																											## Get Image
//
//			_mainCon = [[KoreConn alloc]initCon:self iden:[NSString stringWithFormat:@"%d",itemId]];//@"item"
//			[_mainCon setUrl:[getImg _thumbnail]];
//			[_mainCon set_pathIden:[getImg _path]];
//			[_mainCon setMode:GET];
//			[_mainCon startCon];
//
//			@KoreConnDelegate
//			-(void)connectionDidFinish:(KoreConn *) con{
//				if(![[con _identifier] isEqualToString:@"page"]){
//					UIImage *img = [[UIImage alloc]initWithData:[con _receivedData]];
//					ClipClass *setImg = [_arrOfItem objectAtIndex:[[con _identifier] intValue]];
//					[setImg set_uiImage:img];
//					//------------------------------------------------------------------ Display Newly Image
//					ClipCell *cell = (ClipCell*)[self._tableView cellForRowAtIndexPath:[setImg _path]];
//					cell._img.image = img;//iconDownloader.appRecord.appIcon;
//				}
//			}
//
//			-(void)connectionDidFailed:(KoreConn *) con{
//
//			}
//
////////////																											## Post Xml
//
////////////																											## Post Image
//
////////////																											## Check Connection Type
//
//			BOOL GPRS = [KoreConn checkNetType:WWAN];
//
////------------------------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"

#define ShowNetworkActivityIndicator() [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityIndicator() [UIApplication sharedApplication].networkActivityIndicatorVisible = NO
#define FAILED_COUNT 3
#define DEFAULT_TIMEOUT 45.0
typedef enum {
	OFF = 0,
	WWAN = 1,
	WIFI = 2,
	
}NetType;
typedef enum {
	UNKNOWN = 0,
	GET = 1,
	POST = 2,
	POST_IMG = 3,
	FB_GET = 4,
    FB_POST = 5,
	
}ConMode;
@protocol KoreConnDelegate;

@interface KoreConn : NSObject<NSCopying,NSCoding> {
	BOOL _sourceAvailable;
	
	id <KoreConnDelegate> _delegate;
	NSMutableData *_receivedData;
	
	NSMutableString *_strRequest;
//	KoreCircuit *_cir;
	ConMode _mode;
	NSTimeInterval _timeInterval;
	NSString *_param;
	UIImage *_img;
	
	NSString *_identifier;
	NSIndexPath *_pathIden;
	SCNetworkReachabilityRef reachabilityRef;
	
	NSError *_err;
	
	BOOL _statusConn;
	int _failedCount;
	NSDate *_lastUpdate;
	NSTimeInterval _lastUpdateSinceRefDate;	

	
	NSURLConnection *_connection;
	
	NSURL *_url;
	//----------------------------------------------------------------------- HeaderRespond
	NSDate *_lastModified;
	NSTimeInterval _lastModifiedSinceRefDate;
	
	float _progress;
	float _maxLen;
	NSInteger _sumLen;

	
	NSString *_respContent;
	NSString *_respType;
	
	NSDateFormatter *_dateFormatter;
	
	NSString *_txtPost;
	UIImage *_imgPost;
	NSData *_dataPost;
	
	NSMutableDictionary *_multiPathVal;
		NSMutableArray *_multiPathKey;
	
	NSTimer *_timer;
//	BOOL _stFailed;
    //____________________________________________________________________FB
	NSString *_fbAccToken;
 	//----------------------------------------------------------------------- MoreFeature
    NSInteger _httpRespCode;
}
@property NSInteger _httpRespCode;
@property float _progress;
@property BOOL _statusConn;
@property (nonatomic,retain,readonly) NSMutableString *_strRequest;
@property (nonatomic,retain) NSError *_err;
@property BOOL _sourceAvailable;
@property (nonatomic, assign) id _delegate;
@property (assign) NSMutableData *_receivedData;
@property (nonatomic, retain) NSDate *_lastModified;
@property (nonatomic,retain) NSString *_identifier;
@property (nonatomic,retain) NSIndexPath *_pathIden;
@property (nonatomic,retain) 	NSURL *_url;
@property (nonatomic,retain) 	NSString *_respContent;
@property (nonatomic,retain) NSString *_respType;
@property (nonatomic,retain) 	NSDateFormatter *_dateFormatter;
@property (nonatomic,readonly) 	NSTimeInterval _lastModifiedSinceRefDate;
@property (nonatomic,retain) 	NSString *_fbAccToken;

-(NetType)getNetType;
+(BOOL) checkNetType:(NetType) type;
+(BOOL) checkNetAvailable;
+(void)setFailed:(BOOL)fail;
-(BOOL) checkSourceAvailable:(NSString *) sorce;
//- (BOOL) connectedToNetwork;

-(id)initCon:(id<KoreConnDelegate>)theDelegate iden:(NSString *)identifier;
-(void)setTimeOut:(NSInteger) timeOut;
-(void)setUrl:(NSString *)url;
-(void)setMode:(ConMode) mode;
-(void)setParam:(NSString *)param;
-(void)setImage:(UIImage *)img;
-(void)setLastUpadate:(NSDate*) date;
-(void)setLastUpdateSinceRefDate:(NSTimeInterval) refDate;
-(BOOL)startCon;
-(BOOL)startConInQueue;

//for FB
-(NSString*)_serializeUrl:(NSString*)url params:(NSMutableDictionary*)param httpMethod:(NSString*)httpMet;

//for Post
-(void)setValue:(id) txt forKey:(NSString*) key;
-(void)setValueImage:(UIImage*) img forKey:(NSString*) key;
-(void)setValueData:(NSData*) data forKey:(NSString*) key;


-(void)cancleConn;
-(void)cancleConnNotCallDelegate;
-(void)notAvailableConn;
@end
@protocol KoreConnDelegate<NSObject>

//-(void)initCir:(KoreCircuit *)kc;
//
//-(void)loadPersist:(KoreCircuit *)kc;
//-(void)connection:(KoreCircuit *)kc;
-(void)connectionProgress:(CGFloat) percentage from:(KoreConn*) con;
-(void)connectionDidFinish:(KoreConn *) con;
-(void)connectionDidFailed:(KoreConn *) con;
-(void)connectionDidCancel:(KoreConn *) con;
-(void)connectionNotAvailable:(KoreConn *) con;
//-(void)savePersist:(KoreCircuit *)kc;
//-(void)pause:(KoreCircuit *)kc;
//-(void)error:(KoreCircuit *)kc;
//-(BOOL)releaseConnection;
//-(BOOL)checkConnection;
//-(void)finish;
//____________________________________________________other Code
- (NSString *)userVisibleDateTimeStringForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString;
@end
