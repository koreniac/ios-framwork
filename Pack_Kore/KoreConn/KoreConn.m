//
//  KoreConn.m
//  _iLiveMania
//
//  Created by MOB_Kaeng on 6/16/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "KoreConn.h"
//#import <sys/socket.h>
//#import <netinet/in.h>
//#import <netinet6/in6.h>
//#import <arpa/inet.h>
//#import <ifaddrs.h>
//#import <netdb.h>

    
#import "CENTRAL_DB.h"



static NSString* fbUserAgent = @"FacebookConnect";
static NSString* fbStringBoundary = @"3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f";
static Reachability *_netCheck = nil;
static BOOL _stFailed = NO;
//static Reachability *_netAvail = nil;
#import <CoreFoundation/CoreFoundation.h>

@implementation KoreConn
@synthesize _sourceAvailable;
@synthesize _delegate;
@synthesize _receivedData;
@synthesize _lastModified;
@synthesize _identifier,_pathIden;
@synthesize _err;
@synthesize _strRequest;
@synthesize _statusConn;
@synthesize _progress;
@synthesize _url;
@synthesize _respContent;
@synthesize _respType;
@synthesize _dateFormatter;
@synthesize _lastModifiedSinceRefDate;
@synthesize _fbAccToken;
@synthesize _httpRespCode;
- (id)copyWithZone:(NSZone *)zone{
//	BOOL _sourceAvailable;
//	
//	id <KoreConnDelegate> _delegate;
//	NSMutableData *_receivedData;
//	
//	NSMutableString *_strRequest;
//	//	KoreCircuit *_cir;
//	ConMode _mode;
//	int _timeInterval;
//	NSString *_param;
//	UIImage *_img;
//	
//	NSString *_identifier;
//	NSIndexPath *_pathIden;
//	SCNetworkReachabilityRef reachabilityRef;
//	
//	NSError *_err;
//	
//	BOOL _statusConn;
//	int _failedCount;
//	NSDate *_lastUpdate;
//	
//	
//	
//	NSURLConnection *_connection;
//	
//	NSURL *_url;
//	//----------------------------------------------------------------------- HeaderRespond
//	NSDate *_lastModified;
//	
//	float _progress;
//	float _maxLen;
//	NSInteger _sumLen;
//	
//	
//	NSString *_respContent;
//	NSString *_respType;
	
//	KoreConn *newKoreConn = [[[self class] allocWithZone:zone] init];//[[appIcon allocWithZone:zone] init];
//	[newKoreConn set_delegate:_delegate];
//	[newKoreConn set_identifier:_identifier];
//	[newKoreConn set_progress:_progress];
//	[newKoreConn set_statusConn:_statusConn];
////	[newKoreConn set_strRequest:_strRequest];
//	[newKoreConn set_err:_err];
//	[newKoreConn set_sourceAvailable:_sourceAvailable];
//	[newKoreConn set_receivedData:_receivedData];
//	[newKoreConn set_lastModified:_lastModified];
//	[newKoreConn set_pathIden:_pathIden];
//	[newKoreConn set_url:_url];
//	[newKoreConn set_respContent:_respContent];
//	[newKoreConn set_respType:_respType];

    return(self);
}
#pragma mark NSCoding
- (id)initWithCoder:(NSCoder *)decoder{
//	if (self = [super init])
//	{
//		
//		self._info = [decoder decodeObjectForKey:@"INFO"];
//		
//	} 
	return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder{//NSLog(@"encodeWithCoder__1");
	
	
//	[encoder encodeObject:_info forKey:@"INFO"];//NSLog(@"encodeWithCoder__1.5");
	
	//NSLog(@"encodeWithCoder__2");
}
- (void)dealloc
{








//    @property (nonatomic,retain) 	NSString *_fbAccToken;
    if (_fbAccToken) {
        [_fbAccToken release];
        _fbAccToken = nil;
    }
    if (_respType) {
            [_respType release];
        _respType = nil;
    }
    if (_url) {
            [_url release];
        _url = nil;
        
    }
    if(_err){
        [_err release];
        _err = nil;
    }
    if(_identifier){
        [_identifier release];
        _identifier = nil;
    }
    if (_pathIden) {
	[_pathIden release];
        _pathIden  = nil;
    }
    if (_receivedData) {
        [_receivedData release];
        _receivedData = nil;
    }
    if (_strRequest) {
        [_strRequest release];
        _strRequest = nil;
    }
    if (_lastModified) {
	[_lastModified release];
        _lastModified = nil;
    }
    if (_strRequest) {
        [_strRequest release];
        _strRequest = nil;
    }
    if (_dateFormatter) {
        [_dateFormatter release];
        _dateFormatter = nil;
    }

	if(_netCheck!=nil){
//		[_netCheck release];
		
	}
//    if (_connection) {
//        [_connection release];
//        _connection = nil;
//    }
	[super dealloc];
}
-(id)initCon:(id<KoreConnDelegate>)theDelegate iden:(NSString *)identifier{
	if(self = [super init]){
		_progress = 0;
		_sumLen = 0;
		_failedCount = 0;
		_delegate = theDelegate;
		_strRequest = [[NSMutableString alloc]init];
		_identifier = [identifier copy];
		_mode = UNKNOWN;
		_timeInterval = DEFAULT_TIMEOUT;
		_param = nil;
		_lastUpdate = nil;
		_lastModified = nil;
		_statusConn=  NO;
		
		_connection = nil;
		_dateFormatter = [[NSDateFormatter alloc] init];
		_lastUpdateSinceRefDate =0.0f;
		_lastModifiedSinceRefDate = 0.0f;
		
		_txtPost = nil;
		_imgPost = nil;
		_dataPost = nil;
		
		_multiPathVal = nil;
		_multiPathKey = nil;
		_stFailed = NO;
        _receivedData = nil;
	}
	return self;
}
-(void)setTimeOut:(NSInteger) timeOut{
	if (timeOut != DEFAULT_TIMEOUT) {
		_timeInterval = timeOut;
	}
	
}
-(void)setUrl:(NSString *)url{
	[_strRequest appendString:url];
	
}
-(void)setMode:(ConMode) mode{
	_mode = mode;
	
}
-(void)setParam:(NSString *)param{
	_param = param;
	//[_strRequest appendString:param];
}
-(void)setImage:(UIImage *)img{
	_img = img;
}
-(void)setLastUpadate:(NSDate*) date{
	_lastUpdate = [date copy];;
}
-(void)setLastUpdateSinceRefDate:(NSTimeInterval) refDate{
	_lastUpdateSinceRefDate = refDate;
}
-(BOOL)startCon{//NSLog(@"~~~~~~~~~~~~~~~~~~~ startCon");
	if(![KoreConn checkNetAvailable]){
//		NSLog(@"~~~~~~~~~~~~~~~~~~~ cannnot");
//		MyAlertWithMessage(kNetworkNotFound);
//		return NO;
		if(_failedCount>FAILED_COUNT){_failedCount = 0;
			//MyAlertWithMessage(kNetworkNotFound);
	//	NSLog(@"startCon____stFailed = %@",(_stFailed?@"yes":@"no"));
			if (!_stFailed) {
				NSLog(@"FAILED_COUNT _delegate = %@",_delegate);
					 
					 
//				[_delegate connectionDidFailed:nil];
                if([_delegate respondsToSelector:@selector(connectionNotAvailable:)]){
                    [_delegate connectionNotAvailable:self];                    
                }

				_stFailed = YES;
//				[_timer invalidate];
				[self cancleConnNotCallDelegate];
			}else {

				[self notAvailableConn];
			}

			
			return NO;
		}
		else{
			if(_failedCount==0){
				//MyAlertWithMessage(@"กำลังพยายามต่อเข้า WiFi หรือ EDGE/3G\nเพื่อเข้าใช้งาน");
//				_timer = [NSTimer scheduledTimerWithTimeInterval:1.000 target:self selector:@selector(startCon) userInfo:nil repeats:YES];
		
//			NSDate *d = [NSDate dateWithTimeIntervalSinceNow: 1.1];
//			_timer = [[NSTimer alloc] initWithFireDate: d
//											  interval: 1.0
//												target: self
//											  selector:@selector(startCon)
//											  userInfo:nil repeats:NO];
//			[_timer fire];
				
			}
			++_failedCount;NSLog(@"----- _failedCount = %d",_failedCount);
			_timer = [NSTimer scheduledTimerWithTimeInterval:1.500 target:self selector:@selector(startCon) userInfo:nil repeats:NO];
//			[self performSelectorOnMainThread:@selector(startCon) withObject:nil waitUntilDone:NO];

			

		}
	}
	else{//NSLog(@"~~~~~~~~~~~~~~~~~~~ startCon checkNetAvailable");
		ShowNetworkActivityIndicator();
		if (_connection) {
			[_connection cancel];
		}
		_failedCount = 0;
		NSURL *url = nil;
		
		
		NSMutableURLRequest *theRequest = [[[NSMutableURLRequest alloc] init] autorelease];
		/* create the NSMutableData instance that will hold the received data */
		_receivedData = [[NSMutableData alloc] initWithLength:0];
		TLog(@".......................................... _timeInterval = %f",_timeInterval);
		if(_mode == GET){
			if(_param!=nil){
				[_strRequest appendString:_param];
			}
//#ifdef ZDO_APPID
//            [_strRequest appendFormat:@"&%@=%@",ZDO_APPID_KEY,ZDO_APPID];
//#endif
            
#ifdef KORECONN_PRINT
			NSLog(@"_strRequest = %@",_strRequest);
#endif

           
			url = [NSURL URLWithString:[[_strRequest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
			_url = [url copy];
			//theRequest = [NSMutableURLRequest requestWithURL: url
	//									  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData 
	//								  timeoutInterval:_timeInterval];
			
			[theRequest setURL:url]; 
			[theRequest setTimeoutInterval:_timeInterval];
			[theRequest setHTTPMethod:@"GET"];
#ifdef KORECONN_GZIP
            [theRequest setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
#endif
		}
		else if(_mode == POST){
//			NSData *postData = [_param dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
//			
//			url = [NSURL URLWithString:_strRequest];
//			[theRequest setURL:url];
//			[theRequest setTimeoutInterval:_timeInterval];
//			[theRequest setHTTPMethod:@"POST"];
//			[theRequest setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"content-type"];
//			[theRequest setHTTPBody:postData];
			url = [NSURL URLWithString:[[_strRequest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
			[theRequest setURL:url]; 
			[theRequest setTimeoutInterval:_timeInterval];
			
			[theRequest setHTTPMethod:@"POST"];
			NSMutableData *postData = [[NSMutableData alloc] init];
			//if(_txtPost)
			if (_multiPathKey&&[_multiPathKey count]>0) 
			{																									TLog(@"_________MULTI PATH");
				NSString *boundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
				NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
				[theRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
				
				for (NSInteger i=0; i<[_multiPathKey count]; i++) {
					NSString *key = [_multiPathKey objectAtIndex:i];
					id *val = [_multiPathVal valueForKey:key];
					
					TLog(@"val = %@",val);
					if ([val isKindOfClass:[NSString class]]) {//NSUTF16StringEncoding NSUTF8StringEncoding
						[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key] dataUsingEncoding:NSUTF8StringEncoding]];
                        //-_-! may be remove
						NSString *newVal = val;//leVie[val stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
						[postData appendData:[newVal dataUsingEncoding:NSUTF8StringEncoding]];						
//						[postData appendData:[val dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

					}else if ([val isKindOfClass:[UIImage class]]) {TLog(@"val = %@",val);

						
						[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.png\"\r\n",key ,key ] dataUsingEncoding:NSUTF8StringEncoding]];//[NSString stringWithFormat:@"%d",(NSInteger)[NSDate timeIntervalSinceReferenceDate]]] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[@"Content-Type: image/png\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                        NSData *imgData = UIImageJPEGRepresentation(val, 0.95);//UIImagePNGRepresentation(val);
                        TLog(@"imgData.length = %d",[imgData length]);
						[postData appendData: imgData];
						[postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						
//						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"photo\"; filename=\"%@.jpg\"\r\n", thePicture.title] dataUsingEncoding:NSUTF8StringEncoding]];
//						[postData appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
						
					}else if ([val isKindOfClass:[NSData class]]) {//TLog(@"val = %@",val);
						
						
						[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.mov\"\r\n",key ,key ] dataUsingEncoding:NSUTF8StringEncoding]];//[NSString stringWithFormat:@"%d",(NSInteger)[NSDate timeIntervalSinceReferenceDate]]] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[@"Content-Type: video/quicktime\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData: val];
						[postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						
						//						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"photo\"; filename=\"%@.jpg\"\r\n", thePicture.title] dataUsingEncoding:NSUTF8StringEncoding]];
						//						[postData appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
						
					}else if ([val isKindOfClass:[NSDictionary class]]) {
						TLog(@"NSDictionary = %@",val);
						NSString *fileName = [(NSDictionary*)val valueForKey:@"fileName"];
						NSString *fileType = [(NSDictionary*)val valueForKey:@"fileType"];
						NSString *filePath = [(NSDictionary*)val	valueForKey:@"filePath"];
						TLog(@"filePath = %@",filePath);
						NSData *pathToFile = [[NSData alloc] initWithContentsOfFile:filePath];
						
						[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.%@\"\r\n",key ,fileName,fileType ] dataUsingEncoding:NSUTF8StringEncoding]];//[NSString stringWithFormat:@"%d",(NSInteger)[NSDate timeIntervalSinceReferenceDate]]] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[[NSString stringWithFormat:@"Content-Type: application/%@\r\n", fileType] dataUsingEncoding:NSUTF8StringEncoding]];
//						[postData appendData:[@"Content-Type: application/zip\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData: pathToFile];
						[postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						TLog(@">>>>>>> SEND FILE <<<<<<<");
						
						//						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"photo\"; filename=\"%@.jpg\"\r\n", thePicture.title] dataUsingEncoding:NSUTF8StringEncoding]];
						//						[postData appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
						
					}
					
				}
				
#ifdef ZDO_APPID
                //    [_sumUrl appendFormat:@"&%@=%@",ZDO_UID_KEY,[__CTDB__ getMACmdfive]];
                
                [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",ZDO_APPID_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
                //-_-! may be remove
                NSString *newVal = ZDO_APPID;//leVie[val stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [postData appendData:[newVal dataUsingEncoding:NSUTF8StringEncoding]];
                [postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                CENTRAL_DB *cen = [CENTRAL_DB  getInstanceWithDelegate:self];  
                [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",ZDO_UID_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
                //-_-! may be remove
                NSString *newUID = [cen getMACmdfive];//ZDO_UID_KEY;//leVie[val stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [postData appendData:[newUID dataUsingEncoding:NSUTF8StringEncoding]];	
                //						[postData appendData:[val dataUsingEncoding:NSUTF8StringEncoding]];
                [postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//                TLog(@"ZDO_APPID = %@",newVal);
//                TLog(@"newUID = %@",newUID);
#endif
				[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
								
			}
            else if(_imgPost){																					TLog(@"_________IMAGE");
//				NSString *urlString = @"http://xxx.xxx.xx.xx/xxxxxxxxxxx/uploadFile";
//				NSString *filename = g_userEmailId;
//				NSURLRequest* request= [[[NSMutableURLRequest alloc] init] autorelease];
//				[request setURL:[NSURL URLWithString:urlString]];
//				[request setHTTPMethod:@"POST"];
				NSString *boundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
				NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
				[theRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
//param format				
				[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
				[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"type"] dataUsingEncoding:NSUTF8StringEncoding]];
				[postData appendData:[@"photo" dataUsingEncoding:NSUTF8StringEncoding]];
				[postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
				
// media part
				[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
				//				[postData appendData:[@"Content-Disposition: form-data; name=\"media\"; filename=\"dummy.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
				[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.png\"\r\n",@"file",[NSString stringWithFormat:@"%d",(NSInteger)[NSDate timeIntervalSinceReferenceDate]]] dataUsingEncoding:NSUTF8StringEncoding]];
				[postData appendData:[@"Content-Type: image/png\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
				[postData appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
				

				
				// add it to body
				[postData appendData: UIImagePNGRepresentation(_imgPost)];
				[postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
				
				[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
				

			}
            else if(_dataPost){
				postData = _dataPost;
				[theRequest setValue:[NSString stringWithFormat:@"%d",[postData length]] forHTTPHeaderField:@"Content-Length"];
				[theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
			}

//			TLog(@"/////////////////// postData = %@",postData);
			
			[theRequest setHTTPBody:postData];
			NSLog(@"theRequest = %@",[theRequest description]);
			[postData release];//-_-?
		}
        else if(_mode ==FB_GET){TLog(@"_____________ FB_GET");
            //			"access_token" = "158270817602|be44d64f0d1b367baf26a527.3-715059419|8wmmEF5a_ApSnuFR9riDCvKxYhk";
            //			format = json;
            //			sdk = ios;
            //			"sdk_version" = 2;
			
			NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
			[param setValue:_fbAccToken forKey:@"access_token"];
			[param setValue:@"json" forKey:@"format"];
			[param setValue:@"ios" forKey:@"sdk"];
			[param setValue:@"2" forKey:@"sdk_version"];
			[param setValue:@"large" forKey:@"type"];
			NSString *strUrl = [self _serializeUrl:_strRequest params:param httpMethod:@"GET"];
            //			url = [NSURL URLWithString:[[_strRequest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
			url = [NSURL URLWithString:strUrl];
			_url = [url copy];
			TLog(@"_url = %@",_url);
			[theRequest setURL:url]; 
			[theRequest setTimeoutInterval:_timeInterval];
			[theRequest setValue:fbUserAgent forHTTPHeaderField:@"User-Agent"];
			[theRequest setHTTPMethod:@"GET"];
			NSMutableData *postData = [[NSMutableData alloc] init];
			//if(_txtPost)
			
		}
		else if(_mode ==FB_POST){TLog(@"_____________ FB_POST");
			NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
			[param setValue:_fbAccToken forKey:@"access_token"];
			[param setValue:@"json" forKey:@"format"];
			[param setValue:@"ios" forKey:@"sdk"];
			[param setValue:@"2" forKey:@"sdk_version"];
			
			NSString *strUrl = [self _serializeUrl:_strRequest params:param httpMethod:@"POST"];
			TLog(@"FB_POST = %@",strUrl);
            //			url = [NSURL URLWithString:[[_strRequest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
			url = [NSURL URLWithString:strUrl];
			[theRequest setURL:url]; 
			[theRequest setTimeoutInterval:_timeInterval];
			[theRequest setValue:fbUserAgent forHTTPHeaderField:@"User-Agent"];
			[theRequest setHTTPMethod:@"POST"];
			NSMutableData *postData = [[NSMutableData alloc] init];
			//if(_txtPost)
			if (_multiPathKey&&[_multiPathKey count]>0) 
			{																									TLog(@"_FB_POST________MULTI PATH");
				NSString *boundary = fbStringBoundary;
				NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
				[theRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
				
				for (NSInteger i=0; i<[_multiPathKey count]; i++) {
					NSString *key = [_multiPathKey objectAtIndex:i];
					id *val = [_multiPathVal valueForKey:key];
					
					
					if ([val isKindOfClass:[NSString class]]) {
						[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key] dataUsingEncoding:NSUTF8StringEncoding]];
						NSString *newVal = val;//[val stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
						[postData appendData:[newVal dataUsingEncoding:NSUTF8StringEncoding]];						
						//						[postData appendData:[val dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						
					}else if ([val isKindOfClass:[UIImage class]]) {TLog(@"val = %@",val);
						
						
						[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.png\"\r\n",key ,key ] dataUsingEncoding:NSUTF8StringEncoding]];//[NSString stringWithFormat:@"%d",(NSInteger)[NSDate timeIntervalSinceReferenceDate]]] dataUsingEncoding:NSUTF8StringEncoding]];
                        //						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; filename=\"%@.png\"\r\n" ,key ] dataUsingEncoding:NSUTF8StringEncoding]];
						//"Content-Disposition: form-data; name=\"source\"; filename=\"sketch.jpg\"\r"
                        //						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"source\"; filename=\"sketch.jpg\"\r\n" ,key ] dataUsingEncoding:NSUTF8StringEncoding]];
                        //						@"Content-Disposition: form-data; filename=\"%@\"\r\n", key]];
						[postData appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                        //						[postData appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData: UIImagePNGRepresentation(val)];
                        
						[postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                        
					}else if ([val isKindOfClass:[NSData class]]) {//TLog(@"val = %@",val);
						
						
						[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.mov\"\r\n",key ,key ] dataUsingEncoding:NSUTF8StringEncoding]];//[NSString stringWithFormat:@"%d",(NSInteger)[NSDate timeIntervalSinceReferenceDate]]] dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[@"Content-Type: video/quicktime\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						[postData appendData: val];
						[postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
						
						//						[postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"photo\"; filename=\"%@.jpg\"\r\n", thePicture.title] dataUsingEncoding:NSUTF8StringEncoding]];
						//						[postData appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
						
					}
				}
				
				
				[postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
				[theRequest setHTTPBody:postData];
			}
			
		}
//		else{// should be post image
//			if(_param!=nil){
//				[_strRequest appendString:_param];
//			}
//			url = [NSURL URLWithString:_strRequest];
//			//theRequest = [NSMutableURLRequest requestWithURL: url
//			//									  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData 
//			//								  timeoutInterval:_timeInterval];
//			
//			
//			[theRequest setTimeoutInterval:_timeInterval];
//			[theRequest setHTTPMethod:@"GET"]; 
//			
//			
//			
//		}
		/* Create the connection with the request and start loading the
		 data. The connection object is owned both by the creator and the
		 loading system. */
		
		_connection = [[NSURLConnection alloc] initWithRequest:theRequest 
																	  delegate:self 
//					   ];
															  startImmediately:YES];
		TLog(@"~~~~~~~~~~~~~~~~~~~ _connection start");
		if (_connection == nil) {//-_-! have to thrown error out
			///* inform the user that the connection failed */
	//		NSString *message = NSLocalizedString (@"Unable to initiate request.", 
	//											   @"NSURLConnection initialization method failed.");
	//		URLCacheAlertWithMessage(message);
			NSLog(@"~~~~~~~~");
		}
		
		_statusConn = YES;
		return _statusConn;
	}////////TLog(@"end ~~~~~~~~~~~~~~~~~~~ startCon");
//	_statusConn = YES;
//	return _statusConn;
}
-(BOOL)startConInQueue{//NSLog(@"_________________startConInQueue");
	dispatch_queue_t		qConn = dispatch_queue_create("Kore.Conn.queue", NULL);
	dispatch_sync(qConn,		//dispatch_async		//dispatch_sync
				  ^{
					  [self startCon];
				  }
				);
	dispatch_release(qConn);
}

-(NetType)getNetType{
	return OFF;
}
-(NSString*)_serializeUrl:(NSString*)url params:(NSMutableDictionary*)param httpMethod:(NSString*)httpMet{
	NSURL* parsedURL = [NSURL URLWithString:url];
	NSString* queryPrefix = parsedURL.query ? @"&" : @"?";
	
	NSMutableArray* pairs = [NSMutableArray array];
	for (NSString* key in [param keyEnumerator]) {
		if (([[param valueForKey:key] isKindOfClass:[UIImage class]])
			||([[param valueForKey:key] isKindOfClass:[NSData class]])) {
			if ([httpMet isEqualToString:@"GET"]) {
				NSLog(@"can not use GET to upload a file");
			}
			continue;
		}
		
		NSString* escaped_value = (NSString *)CFURLCreateStringByAddingPercentEscapes(
																					  NULL, /* allocator */
																					  (CFStringRef)[param objectForKey:key],
																					  NULL, /* charactersToLeaveUnescaped */
																					  (CFStringRef)@"!*'();:@&=+$,/?%#[]",
																					  kCFStringEncodingUTF8);
		
		[pairs addObject:[NSString stringWithFormat:@"%@=%@", key, escaped_value]];
		[escaped_value release];
	}
	NSString* query = [pairs componentsJoinedByString:@"&"];
//	TLog(@"showSerialize  = %@",[NSString stringWithFormat:@"%@%@%@", url, queryPrefix, query]);
	return [NSString stringWithFormat:@"%@%@%@", url, queryPrefix, query];
}
#pragma mark -
-(void)setValue:(id) txt forKey:(NSString*) key{
	if (!_multiPathKey){
		_multiPathKey = [[NSMutableArray alloc] init];
		_multiPathVal = [[NSMutableDictionary alloc] init];
	}
	NSString *_key = [key copy];
	id _txt = nil;
	if ([txt isKindOfClass:[NSString class]]) {
		_txt = [txt copy];
	}else if ([txt isKindOfClass:[UIImage class]]) {
		_txt = txt;
	
	}else if ([txt isKindOfClass:[NSData class]]) {
		_txt = txt;
		
	}else if ([txt isKindOfClass:[NSDictionary class]]) {
		_txt = txt;
		
	}

	[_multiPathKey addObject:_key];
	[_multiPathVal setValue:_txt forKey:_key];
	
}
-(void)setValueImage:(UIImage*) img forKey:(NSString*) key{
	
}
-(void)setValueData:(NSData*) data forKey:(NSString*) key{
	
}
-(void)setValueText:(NSString*) txt{
	_txtPost = [txt copy];
	
	
}
-(void)setValueImage:(UIImage*) img{
	_imgPost = [img copy];
}
-(void)setValueData:(NSData*) data{
	_dataPost = [data copy];
}
#pragma mark -
-(void)notAvailableConn{
	[_connection cancel];
	[_delegate connectionDidCancel:self];
	//	[_connection release];
	//	_connection = nil;
}
-(void)cancleConn{
	[_connection cancel];
	[_delegate connectionDidCancel:self];
//	[_connection release];
//	_connection = nil;
}
-(void)cancleConnNotCallDelegate{
		[_connection cancel];
	[_connection release];
}
#pragma mark -
+(void)setFailed:(BOOL)fail{
	_stFailed = fail;
	NSLog(@"_stFailed = %@",(_stFailed?@"yes":@"no"));
}
+(BOOL) checkNetType:(NetType) type{
	BOOL result = NO;
	if (_netCheck==nil) {
		_netCheck = [[Reachability reachabilityForInternetConnection] retain];
	}
	if([_netCheck currentReachabilityStatus]==ReachableViaWWAN){////////TLog(@"~~~~~~~~~~ GPRS");
		if (type==WWAN) {
			result = YES;
		}else {
			result = NO;
		}
	}else{////////TLog(@"~~~~~~~~~~ WIFI");
		if (type==WWAN) {
			result = NO;
		}else {
			result = YES;
		}
		
	}

	return result;
}
+(BOOL) checkNetAvailable{
	Reachability *_netAvail = [Reachability reachabilityWithHostName:@"www.google.com"];
	if([_netAvail currentReachabilityStatus]==NotReachable){
		return NO;
	}
	return YES;
	
}

-(BOOL) checkSourceAvailable:(NSString *) sorce{
	//static BOOL checkNetwork = YES;
    if (_sourceAvailable==NO) { // Since checking the reachability of a host can be expensive, cache the result and perform the reachability check once.
       // checkNetwork = NO;
        
        Boolean success;    
        const char *host_name = [sorce cStringUsingEncoding:NSUTF8StringEncoding];
		
        SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, host_name);
        SCNetworkReachabilityFlags flags;
        success = SCNetworkReachabilityGetFlags(reachability, &flags);
        _sourceAvailable = success && (flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired);
        CFRelease(reachability);
    }
    return _sourceAvailable;
	
}
//- (BOOL) connectedToNetwork 
//{ 
//	// Create zero addy 
//	struct sockaddr_in zeroAddress; 
//	bzero(&zeroAddress, sizeof(zeroAddress)); 
//	zeroAddress.sin_len = sizeof(zeroAddress); 
//	zeroAddress.sin_family = AF_INET; 
//	// Recover reachability flags 
//	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress); 
//	SCNetworkReachabilityFlags flags; 
//	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags); 
//	CFRelease(defaultRouteReachability); 
//	if (!didRetrieveFlags) 
//	{ 
//		printf("Error. Could not recover network reachability flags\n"); 
//		return 0; 
//	} 
//	BOOL isReachable = flags & kSCNetworkFlagsReachable; 
//	BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired; 
//	return (isReachable && !needsConnection) ? YES : NO; 
//} 


/*
 ------------------------------------------------------------------------
 NSURLConnection delegate method
 ------------------------------------------------------------------------
 */
static NSDateFormatter *    sUserVisibleDateFormatter;
- (NSString *)userVisibleDateTimeStringForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString
// Returns a user-visible date time string that corresponds to the 
// specified RFC 3339 date time string. Note that this does not handle 
// all possible RFC 3339 date time strings, just one of the most common 
// styles.
{
	static NSDateFormatter *    sRFC3339DateFormatter;
    NSString *                  userVisibleDateTimeString;
    NSDate *                    date;
	
    // If the date formatters aren't already set up, do that now and cache them 
    // for subsequence reuse.
	
    if (sRFC3339DateFormatter == nil) {
        NSLocale *                  enUSPOSIXLocale;
		
        sRFC3339DateFormatter = [[NSDateFormatter alloc] init];
        assert(sRFC3339DateFormatter != nil);
		
        enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];
        assert(enUSPOSIXLocale != nil);
		
        [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
        [sRFC3339DateFormatter setDateFormat:@"eee, dd MMM yyyy HH:mm:ss ZZ"];
        [sRFC3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
	
    if (sUserVisibleDateFormatter == nil) {
        sUserVisibleDateFormatter = [[NSDateFormatter alloc] init];
        assert(sUserVisibleDateFormatter != nil);
		
        [sUserVisibleDateFormatter setDateStyle:NSDateFormatterFullStyle];
        [sUserVisibleDateFormatter setTimeStyle:NSDateFormatterFullStyle];
    }
	
    // Convert the RFC 3339 date time string to an NSDate.
    // Then convert the NSDate to a user-visible date string.
	
    userVisibleDateTimeString = nil;
	
    date = [sRFC3339DateFormatter dateFromString:rfc3339DateTimeString];
    if (date != nil) {
        userVisibleDateTimeString = [sUserVisibleDateFormatter stringFromDate:date];
    }
    return userVisibleDateTimeString;
}
#pragma mark -
#pragma mark NSURLConnection delegate methods
- (void)connection:(NSURLConnection *)connection didCancelAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
	TLog(@"didCancelAuthenticationChallenge");
}
- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{TLog(@"~~~~~~~~~~~~~~ didReceiveResponse");
    /* This method is called when the server has determined that it has
	 enough information to create the NSURLResponse. It can be called
	 multiple times, for example in the case of a redirect, so each time
	 we reset the data. */
	
    [_receivedData setLength:0];
	
	/* Try to retrieve last modified date from HTTP header. If found, format  
	 date so it matches format of cached image file modification date. */
	
	if ([response isKindOfClass:[NSHTTPURLResponse self]]) {
        _httpRespCode = [(NSHTTPURLResponse*)response statusCode];
        TLog(@"_httpRespCode = %d",_httpRespCode);
		NSDictionary *headers = [(NSHTTPURLResponse *)response allHeaderFields];
		TLog(@"headers = %@",headers);
		_maxLen = [[headers valueForKey:@"Content-Length"] floatValue];
		NSString *contentType = [headers valueForKey:@"Content-Type"];
		NSArray *content = [contentType componentsSeparatedByString:@";"];
		NSArray	*type = [[content objectAtIndex:0] componentsSeparatedByString:@"/"];
		_respContent = [[type objectAtIndex:0] copy];
		_respType = [[type objectAtIndex:1] copy];
		
//		NSLog(@"_maxLen =  %f",_maxLen);
		//////TLog(@"Kore Conn----> headers = %@",headers);
		
		NSString *modified = [headers objectForKey:@"Last-Modified"];//Last-Modified
        NSArray *checkList = [modified componentsSeparatedByString:@" "];
		TLog(@"modified = %@",modified);
//		NSString *RFC = [self userVisibleDateTimeStringForRFC3339DateTimeString:modified];
//		TLog(@"RFC = %@",RFC);
		if (modified) {
//            if (_mode==FB_GET||_mode==FB_POST) {TLog(@"___________FB");
            if([checkList count]==1){TLog(@"___________UTC");
                //2011-09-25T06:29:55+0000
                NSArray *timeList = [modified componentsSeparatedByString:@"T"];
                NSArray *dateList = [[timeList objectAtIndex:0] componentsSeparatedByString:@"-"];
                
                NSDateFormatter* df = [[NSDateFormatter alloc]init];
                NSLocale *locale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
                
                [df setLocale:locale];
                [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
//                NSString* str = @"2009-08-11T06:00:00.000-0700";   // NOTE -0700 is the only change
                NSDate* dateFromString = [df dateFromString:modified];
                TLog(@"date = %@",dateFromString);
                _lastModifiedSinceRefDate = [dateFromString timeIntervalSinceReferenceDate];
                //------------------------------------------------------------------------------------------------------------------------ UTC->GMT			
                
//                NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];
//                NSTimeZone* utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
//                
//                NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:dateFromString];
//                NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:dateFromString];
//                NSTimeInterval gmtInterval = gmtOffset - currentGMTOffset;
//                
//                NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:dateFromString] autorelease]; 
//                
//                
//                
//                NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSBuddhistCalendar];
//                NSDateComponents *comps = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:destinationDate];
//                
//                NSDate *date = [gregorian dateFromComponents:comps];
//                //			TLog(@"date = %@",[date description]);
//                
//                NSDate *datetimeInterval = [NSDate dateWithTimeInterval:0 sinceDate:date];
//                //			TLog(@"datetimeInterval = %@",[datetimeInterval description]);
//                
//
//                NSCalendar *gregorian1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSBuddhistCalendar];
//                NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//                [offsetComponents setHour:[comps hour]];
//                [offsetComponents setMinute: [comps minute]];
//                [offsetComponents setSecond: [comps second]];
//                [offsetComponents setYear:[[dateList objectAtIndex:0] intValue]];
//                [offsetComponents setMonth:[comps month]];
//                [offsetComponents setDay:[[dateList objectAtIndex:2] intValue]];
//                // Calculate when, according to Tom Lehrer, World War III will end
//                
//                _lastModified = [[gregorian1 dateFromComponents:offsetComponents] copy];
//                TLog(@"_lastModified = %@",_lastModified);
                //---------------------------------------------------------------------------- gen datecomponent from server
            }else{TLog(@"___________GMT");
                [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];


                NSLocale *locale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
                        
                [dateFormatter setLocale:locale];
                [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss ZZZ"];

                NSDate *dateFromString = [[[NSDate alloc] init] retain];
                dateFromString = [dateFormatter dateFromString:modified];//modified
    //			TLog(@"dateFromString = %@",[dateFromString description]);
    //			TLog(@"intervalSinceNow = %f",[dateFromString timeIntervalSinceReferenceDate]);
                _lastModifiedSinceRefDate = [dateFromString timeIntervalSinceReferenceDate];
        
    //------------------------------------------------------------------------------------------------------------------------ UTC->GMT			

//                NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];
//                NSTimeZone* utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
//                
//                NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:dateFromString];
//                NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:dateFromString];
//                NSTimeInterval gmtInterval = gmtOffset - currentGMTOffset;
//                
//                NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:dateFromString] autorelease]; 
//
//                
//
//                NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSBuddhistCalendar];
//                NSDateComponents *comps = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:destinationDate];
//                NSDate *date = [gregorian dateFromComponents:comps];
//    //			TLog(@"date = %@",[date description]);
//                
//                NSDate *datetimeInterval = [NSDate dateWithTimeInterval:0 sinceDate:date];
//    //			TLog(@"datetimeInterval = %@",[datetimeInterval description]);
//                
//                NSArray *dateInfo = [modified componentsSeparatedByString:@" "];
//                NSCalendar *gregorian1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSBuddhistCalendar];
//                NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//                [offsetComponents setHour:[comps hour]];
//                [offsetComponents setMinute: [comps minute]];
//                [offsetComponents setSecond: [comps second]];
//                [offsetComponents setYear:[[dateInfo objectAtIndex:3] intValue]];
//                [offsetComponents setMonth:[comps month]];
//                [offsetComponents setDay:[[dateInfo objectAtIndex:1] intValue]];
//                // Calculate when, according to Tom Lehrer, World War III will end
//
//                _lastModified = [[gregorian1 dateFromComponents:offsetComponents] copy];
//    //			TLog(@"_lastModified = %@",_lastModified);
    //---------------------------------------------------------------------------- gen datecomponent from server
                
    //---------------------------------------------------------------------------- gen new datecomponent		
    //			NSCalendar *buddhist = [[NSCalendar alloc] initWithCalendarIdentifier:NSBuddhistCalendar];
            }
		}
		else {
			/* default if last modified date doesn't exist (not an error) */
//			_lastModified = [NSDate dateWithTimeIntervalSinceReferenceDate:0];
		}
//			TLog(@"_lastModified 2.= %@",[_lastModified description]);
		
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ if Equal Date please Cancel Connection
		if (_lastUpdateSinceRefDate>0.0&&_lastModifiedSinceRefDate>0.0) {
			TLog(@"sevDate = %f",_lastModifiedSinceRefDate);
			TLog(@"deviceDate = %f",_lastUpdateSinceRefDate);
			if (_lastUpdateSinceRefDate==_lastModifiedSinceRefDate) {
				[connection cancel];
				[_delegate connectionDidCancel:self];
			}
		}
//		if(_lastModified&&_lastUpdate){
//			if ([_lastUpdate compare:_lastModified]==NSOrderedSame) {
//				[connection cancel];
//				[_delegate connectionDidCancel:self];
//			}
//		}
		
	}
}
#pragma mark -
#pragma mark - retrieveData
- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{//////TLog(@"~~~~~~~~~~~~~~ didReceiveData = %@",data);
    /* Append the new data to the received data. */
	
//	NSLog(@"NSURLConnection _maxLen = %f",_maxLen);
//	NSLog(@"NSURLConnection [data length] = %d",[data length]);
//	NSLog(@"NSURLConnection _progress = %f",_progress);
	if (_maxLen==0) {
		_maxLen = INFINITY;
		[_delegate connectionProgress:-1.0 from:self];	
	}else if (_maxLen<INFINITY) {
		_sumLen += [data length];
		_progress = _sumLen/_maxLen;
		[_delegate connectionProgress:(_maxLen==0?_maxLen:_progress) from:self];	
	}

//	[_delegate connectionProgress:(_maxLen==0?_maxLen:_progress) from:self];
    [_receivedData appendData:data];
}
#pragma mark - sendData
- (void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
//	NSLog(@"didSendBodyData = %d",bytesWritten);
//	NSLog(@"totalBytesWritten = %d",totalBytesWritten);
//	NSLog(@"totalBytesExpectedToWrite = %d",totalBytesExpectedToWrite);
//	_sumLen += bytesWritten;
	_progress = totalBytesWritten/(CGFloat)totalBytesExpectedToWrite;
//	NSLog(@"_progress = %f",_progress);

    if ([_delegate respondsToSelector:@selector(connectionProgress:from:)]) {
        [_delegate connectionProgress:_progress from:self];        
    }
}
#pragma mark -



- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    HideNetworkActivityIndicator();
	TLog(@"~~~~~~~~~~~~~~ didFailWithError = %@",error);
	TLog(@"~~~~~~~~~~~~~~ url = %@",_url);
	_statusConn = NO;
	[self set_err:[error copy]];
	
	[_delegate connectionDidFailed:self];
//	if(![_delegate releaseConnection])
//		//////TLog(@"close connection error");
	[connection release];
//	[self release];
}


- (NSCachedURLResponse *) connection:(NSURLConnection *)connection 
				   willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
	/* this Documents does not use a NSURLCache disk or memory cache */
    return nil;
}


- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    HideNetworkActivityIndicator();
//    TLog(@"~~~~~~~~~~~~~~ connectionDidFinishLoading %@",_receivedData);
	//////TLog(@"~~~~~~~~~~~~~~ connectionDidFinishLoading");
//	if(![_delegate releaseConnection])
//		//////TLog(@"close connection error");
    TLog(@"_httpRespCode = %d",_httpRespCode);
    if (_httpRespCode>=400) {
        [_delegate connectionDidFailed:self];
    }else{
        [_delegate connectionDidFinish:self];
    }
	
	[connection release];
	_stFailed = NO;
//		[self release];
}

@end
