//
//  KoreFE_Paging.h
//  TrueMovie
//
//  Created by Naruphon Sirimasrungsee on 8/5/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KoreFeedEngine.h"



#define DEFAULT_RESULT_response @"response" //@"a_channel"
#define DEFAULT_RESULT_header @"header"
#define DEFAULT_RESULT_body @"body"
#define DEFAULT_RESULT_data @"items"
#define DEFAULT_RESULT_item @"item"
#define DEFAULT_RESULT_paging @"paging"

#define DEFAULT_RESULT_response_d @"d_response"
#define DEFAULT_RESULT_header_d @"d_header"
#define DEFAULT_RESULT_body_d @"d_body"
#define DEFAULT_RESULT_data_a @"a_items"
#define DEFAULT_RESULT_item_d @"d_item"
#define DEFAULT_RESULT_paging_d @"d_paging"

#define DEFAULT_K_IMGPATH @"thumbnail"
#define DEFAULT_K_IMGSOURCE @"_image"
#define DEFAULT_K_INDEXPATH @"_path_iden"

#define DEFAULT_K_PAGE_NO @"page_no"
#define DEFAULT_K_PAGE_SIZE @"page_size"
#define DEFAULT_K_PAGE_TOTAL @"total_page"	//@"page_total"

@protocol KoreFE_PagingDelegate;

@interface KoreFE_Paging : KoreFeedEngine {
	id <KoreFE_PagingDelegate> _pagingDelegate;
	NSInteger _pageId;
	NSInteger _pageTotal;
	NSInteger _pageSize;
	
	BOOL _collectData;
	BOOL _reFeed;
	BOOL _chkNoMorePage;
	BOOL _stPage;
	BOOL _param;
	
	NSMutableArray *_itemList;
	NSMutableArray *_btImage;
	
	BOOL _autoLoadImage;
	NSInteger _itemAmount;
	UITableView *_forTBV;
	
	NSString *_imageKey;
	NSString *_imageRes;
}
@property (nonatomic,retain)	NSString *_imageRes;
@property(nonatomic,retain)	NSString *_imageKey;
@property (assign) BOOL _autoLoadImage;
@property (nonatomic,retain)NSMutableArray *_itemList;
@property (nonatomic,retain)id _pagingDelegate;

-(void)_getNewPage;
-(void)_getNewItem:(NSInteger) itemId From:(NSDictionary*) dic forKey:(NSString*) keyStr;
-(void)_refresh;
-(BOOL)_remainPage;
-(void)_dependTBV:(UITableView*) tbv;
//OverRiding
-(NSArray *)_getKey;
-(NSArray *)_getValue;

@end

@protocol KoreFE_PagingDelegate<NSObject>

-(void)imageDidFinish:(NSData *) imgData wihtPath:(NSIndexPath*) idPath dependOnTable:(UITableView*) tbv;
-(void)imageDidFailed:(NSString *) msg andError:(NSError*) error dependOnTable:(UITableView*) tbv;

@end

