//
//  KoreFE_Paging.m
//  TrueMovie
//
//  Created by Naruphon Sirimasrungsee on 8/5/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "KoreFE_Paging.h"


@implementation KoreFE_Paging
@synthesize _pagingDelegate;
@synthesize _itemList;
@synthesize _autoLoadImage;
@synthesize _imageKey,_imageRes;

#pragma mark - OVERIDING
-(id)_initFeed:(id<KoreFeedEngineDelegate>)delegate withUrl:(NSString *)url{

	if(self = [super _initFeed:delegate withUrl:url]){
//
		NSArray *keys = [self _getKey];//[NSArray arrayWithObjects:DEFAULT_RESULT_response, DEFAULT_RESULT_header,DEFAULT_RESULT_body,DEFAULT_RESULT_data,DEFAULT_RESULT_item,DEFAULT_RESULT_paging,nil];//,TEST_RESULT_rows
		NSArray *objects = [self _getValue];//[NSArray arrayWithObjects:DEFAULT_RESULT_response_d, DEFAULT_RESULT_header_d,DEFAULT_RESULT_body_d,DEFAULT_RESULT_data_a,DEFAULT_RESULT_item_d,DEFAULT_RESULT_paging_d,nil];

		//NSArray *exKey = [NSArray	arrayWithObjects:TEST_RESULT_tw_enum,TEST_RESULT_tw_schema,TEST_RESULT_tw_item,nil];
		NSDictionary *dicStruct = [[NSDictionary alloc] initWithObjects: [objects copy] forKeys:[keys copy]];
		//_KFE = [[KoreFeedEngine alloc] _initFeed:self withUrl:K_FEED_HOME];
		[self _initDataSturcture:dicStruct];
		
		_pageId = 1;
		_pageTotal = 1;//1;
		_pageSize = 4;
		_collectData = NO;
		_reFeed = NO;
		_chkNoMorePage = NO;
		_stPage = NO;
		_param = NO;
		_itemList = nil;
		_btImage = nil;
		_autoLoadImage = NO;
		_itemAmount = 0;
//
		_imageKey = DEFAULT_K_IMGPATH;
		_imageRes = DEFAULT_K_IMGSOURCE;
	}


	return self;
}
-(void)_initFeedConn{
	//if((_pageId==1&&_stPage)||_reFeed)
	{
		_stPage = NO;
		
		//////TLog(@"_initFeedConn Paging 1");
		_mainConn = [[KoreConn alloc]initCon:self iden:[NSString stringWithFormat:@"KoreFeedEngine"]];

//		[_mainConn setUrl:[[NSString alloc] initWithFormat:@"%@&%@=%d&%@=%d",_strUrl,DEFAULT_K_PAGE_NO,_pageId,DEFAULT_K_PAGE_SIZE,_pageSize]];
		[_mainConn setUrl:[[NSString alloc] initWithFormat:@"%@",_strUrl]];
		[_mainConn setMode:GET];
		_stPage = ![_mainConn startCon];
		//////TLog(@"_initFeedConn Paging 2");
	}	
}
//- (void)parserDidEndDocument:(NSXMLParser *)parser{//////TLog(@"parserDidEndDocument_Paging");
	-(void)_parserDidFinish{////TLog(@"_parserDidFinish");
	//-_-?[parser release];
	//[_tbvResult reloadData];
	////////TLog(@"_arrOfItem = %@",_arrOfItem);
		if([[_resultDic valueForKey:DEFAULT_RESULT_header]  valueForKey:DEFAULT_K_PAGE_TOTAL] ){
				_pageTotal = [[[_resultDic valueForKey:DEFAULT_RESULT_header]  valueForKey:DEFAULT_K_PAGE_TOTAL] intValue];
		}
		
////TLog(@"_pageId = %d",_pageId);
////TLog(@"_pageTotal = %d",_pageTotal);
//////TLog(@"_itemAmount = %d",_itemAmount);
	
	if(_pageId>_pageTotal){
		_chkNoMorePage  = YES;
	}else{++_pageId;
		_chkNoMorePage = NO;
		if (!_itemList) {
			_itemList = [[NSMutableArray alloc] init];
		}
			
		NSMutableArray *arr = 	[[_resultDic valueForKey:DEFAULT_RESULT_body] valueForKey:DEFAULT_RESULT_data];
		//////TLog(@"arr = %@",arr);
		 for(int i=0;i<[arr count];i++){
			 [_itemList addObject:[[arr objectAtIndex:i] retain]];
		 }
	}
//	[super parserDidEndDocument:parser];
	
	
	//[_delegate feedDidFinish:self];
	if (_autoLoadImage) {
		int j = _itemAmount;
		for (; j<[_itemList count]; j++) {////TLog(@"j = %d",j);
			NSMutableDictionary	 *item = [_itemList objectAtIndex:j];
			[item setValue:[NSIndexPath indexPathForRow:j inSection:0] forKey:DEFAULT_K_INDEXPATH];
			[self _getNewItem:j From:item forKey:_imageKey];
			
		}
		_itemAmount = [_itemList count];
	}
		if(_pageId>_pageTotal){
			_chkNoMorePage  = YES;
		}
}


#pragma mark - FUNCTION
//OverRiding
-(NSArray *)_getKey{
	return [NSArray arrayWithObjects:DEFAULT_RESULT_response, DEFAULT_RESULT_header,DEFAULT_RESULT_body,DEFAULT_RESULT_data,DEFAULT_RESULT_item,DEFAULT_RESULT_paging,nil];//,TEST_RESULT_rows
}
-(NSArray *)_getValue{
	return [NSArray arrayWithObjects:DEFAULT_RESULT_response_d, DEFAULT_RESULT_header_d,DEFAULT_RESULT_body_d,DEFAULT_RESULT_data_a,DEFAULT_RESULT_item_d,DEFAULT_RESULT_paging_d,nil];
}
-(void)_dependTBV:(UITableView*) tbv{
	_forTBV = tbv;
}
-(void)_getNewPage{
	if (_feedComplete) {
		if(_pageId<=_pageTotal||_reFeed){//-_-! Dont get page anymore
			_reFeed = NO;
			[self _startFeed];////TLog(@"------- ");
		}
		
	}
}
-(void)_getNewItem:(NSInteger) itemId From:(NSDictionary*) dic forKey:(NSString*) keyStr{
	if (_feedComplete) {
		NSDictionary *getImg = dic;//[_itemList objectAtIndex:itemId];

		_mainConn = [[KoreConn alloc]initCon:self iden:[NSString stringWithFormat:@"%d",itemId]];//@"item"
		[_mainConn set_pathIden:[NSIndexPath indexPathForRow:itemId inSection:0]];
		[_mainConn setUrl:[getImg valueForKey:keyStr]];////TLog(@"keyStr = %@",keyStr);//////TLog(@"[getImg valueForKey:keyStr] = %@",[getImg valueForKey:keyStr]);
		[_mainConn set_pathIden:[getImg valueForKey:DEFAULT_K_INDEXPATH]];//-_-! Use this cos refresh image in table fast more
		[_mainConn setMode:GET];
		[_mainConn startCon];
	}
}
-(void)_refresh{
	
}
-(BOOL)_remainPage{
	return !_chkNoMorePage;
}
# pragma mark -
#pragma mark KoreConn delegate methods
-(void)connectionDidFinish:(KoreConn *) con{//////TLog(@"KoreFeedEngine___connectionDidFinish_Paging");
	if([[con _identifier] isEqualToString:@"KoreFeedEngine"]){
		[super connectionDidFinish:con];
	}else {
		NSData	*connData = [con _receivedData];//////TLog(@"~~~~~0 connData = %@",connData);
		if (_autoLoadImage) {
			////TLog(@"_itemList count = %d",[_itemList count]);
			NSInteger itemId = [[con	_identifier] intValue];//////TLog(@"~~~~~~~~ itemId = %d",itemId);
			UIImage *img = [[UIImage alloc]initWithData:connData];////TLog(@"~~~~~1");//////TLog(@"_itemList = %@",_itemList);
			NSMutableDictionary *setImg = [_itemList objectAtIndex:itemId];////TLog(@"~~~~~2 img = %@",img);
			[setImg setValue:img forKey:_imageRes];////TLog(@"~~~~~3  = %@",_imageRes);
		}//////TLog(@"~~~~~~ ..");
		[_pagingDelegate imageDidFinish:connData wihtPath:[con _pathIden] dependOnTable:_forTBV];
	}	
}
-(void)connectionDidFailed:(KoreConn *) con{
	if([[con _identifier] isEqualToString:@"KoreFeedEngine"]){
		[super connectionDidFailed:con];
	}else{
		//MyAlertWithMessage(kNetworkNotFound);
		[_pagingDelegate imageDidFailed:[con _strRequest] andError:[con _err] dependOnTable:_forTBV];
	}
}
@end
