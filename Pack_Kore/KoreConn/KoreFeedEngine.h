//------------------------------------------------------------------------------------------------------------------
//                                                                                                 version 1.0.0
//	Class KoreConn
//  Create by Naruphon Sirimasrungsee
//  
//  UpDate   06/22/2010
//  
//
//  Description      parse xml to object
//	____________________________________________________Feature Support
//		1. Transform xml to object by define Structure of xml
//		2. In case xml Structure is not normal form, Can set key exception for ignore some key
//
// ____________________________________________________How To
//
//		NSArray *keys = [NSArray arrayWithObjects:TEST_RESULT_channel, TEST_RESULT_item,nil];//,TEST_RESULT_rows
//		NSArray *objects = [NSArray arrayWithObjects:TEST_RESULT_channel_a, TEST_RESULT_item_d,nil];
//		NSArray *exKey = [NSArray	arrayWithObjects:TEST_RESULT_tw_enum,TEST_RESULT_tw_schema,TEST_RESULT_tw_item,nil];
//		NSDictionary *dicStruct = [[NSDictionary alloc] initWithObjects: [objects copy] forKeys:[keys copy]];
//		_KFE = [[KoreFeedEngine alloc] _initFeed:self withUrl:K_FEED_RESULT];
//		[_KFE _initDataSturcture:dicStruct];
//		[_KFE _setKeyException:exKey];
//		[_KFE _setMode:FEM_URL];
//		[_KFE _startFeed];
//------------------------------------------------------------------------------------------------------------------
//											Support Format
//<response guid="20100310131757514130">
//	−<item>
//			<team1>ฝรั่งเศส </team1>
//			<team2>แอฟริกาใต้ </team2>
//			<stadium>-</stadium>
//			<date>22/06/2010</date>
//	</item>
//	−<item>
//		<team1>ฝรั่งเศส </team1>
//		<team2>แอฟริกาใต้ </team2>
//		<stadium>-</stadium>
//		<date>22/06/2010</date>
//	</item>
//	−<item>
//		<team1>ฝรั่งเศส </team1>
//		<team2>แอฟริกาใต้ </team2>
//		<stadium>-</stadium>
//		<date>22/06/2010</date>
//	</item>
//	−<rows>
//		<date day="22" month="06" year="2010"/>
//		<date day="17" month="06" year="2010"/>
//		<date day="16" month="06" year="2010"/>
//		<date day="11" month="06" year="2010"/>
//	</rows>
//</response>
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//											Support Format
//<response>
//−<header>
//	<code>200</code>
//	<description>Success</description>
//</header>
//−<body>
//	−<data>
//		−<item>
//			<teamA>อาร์เซนอล </teamA>
//			<teamB>ฟูแล่ม </teamB>
//			<score>4-0 </score>
//			−
//			<logoB>
//			http://services.truelife.com/contentEngine/contents/mobile/100504115826645.gif
//			</logoB>
//			−
//			<logoA>
//			http://services.truelife.com/contentEngine/contents/mobile/100504115821317.gif
//			</logoA>	
//		</item>
//		−<item>
//			<teamA>อาร์เซนอล </teamA>
//			<teamB>ฟูแล่ม </teamB>
//			<score>4-0 </score>
//			−
//			<logoB>
//			http://services.truelife.com/contentEngine/contents/mobile/100504115826645.gif
//			</logoB>
//			−
//			<logoA>
//			http://services.truelife.com/contentEngine/contents/mobile/100504115821317.gif
//			</logoA>	
//		</item>
//	</data>
//	-<paging>
//		<pageno>1</pageno>
//		<pagesize>20</pagesize>
//		<totalpage>1</totalpage>
//	</paging>
//</body>
//</response>
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//										 Support Format with set some exception key
//−
//<rss version="2.0">
//−<channel>
//		<title>WORLD CUP</title>
//		<description>วันพุธที่ 23  มิ.ย. 53 21.00 น. | 13.00 น. </description>
//		<lastBuildDate>Wed, 23 Jun 2010  03:22:38 +0700</lastBuildDate>
//		<generator>TrueMove Feed Builder v.1.39</generator>
//		<copyright>TrueMove</copyright>
//		+<tw:enum>
//			<tw:item name="status" value=",-,Live,HT,FT"/>
//			<tw:item name="section" value=",-,1st,2nd,extra1st,extra2nd"/>
//			<tw:item name="template" value="1.SubMenu,2.FootballContent,3.SportContent,4.News"/>
//		</tw:enum>
//		+<tw:schema>
//			<tw:item name="matchDate" title="วันที่แข่ง" type="string"/>
//			<tw:item name="tvChannel" title="ช่องถ่ายทอด" type="string"/>
//			<tw:item name="teamA" title="ทีมเหย้า" type="string"/>
//			<tw:item name="teamA_eng" title="ทีมเหย้า (EN)" type="string"/>
//		</tw:schema>
//		−<item id="1">
//			<matchDate>23/6/53; วันพุธที่ 23 เมษายน 2553 </matchDate>
//			<tvChannel>CH3 </tvChannel>
//			<teamA>สโลเวเนีย  </teamA>
//			<teamA_eng/>
//			<teamB>อังกฤษ </teamB>
//			<teamB_eng/>
//		</item>
//		−<item id="1">
//			<matchDate>23/6/53; วันพุธที่ 23 เมษายน 2553 </matchDate>
//			<tvChannel>CH3 </tvChannel>
//			<teamA>สโลเวเนีย  </teamA>
//			<teamA_eng/>
//			<teamB>อังกฤษ </teamB>
//			<teamB_eng/>
//		</item>
//</channel>
//</rss>
//------------------------------------------------------------------------------------------------------------------

//Prob
//http://widget3.truelife.com/truelifes/tmm/service.php?method=get_highlight
//http://widget3.truelife.com/truelifes/tmm/service.php?method=get_movie_hit
//http://widget3.truelife.com/truelifes/tmm/service.php?method=get_movie_recommend

#import <Foundation/Foundation.h>
#import "KoreConn.h"
//#import "NSMutableArray+Cat.h"
#import "KoreXtoO.h"

typedef enum {
	FEM_NONE = 0,
	FEM_URL = 1,
	FEM_FILEPATH = 2,
	
}FeedEngineMode;


@protocol KoreFeedEngineDelegate;


@interface KoreFeedEngine : NSObject <KoreConnDelegate,NSCopying,KoreXtoODelegate>{//,NSCopying
	

	

	FeedEngineMode _mode;
	ConMode _conMode;
	
	NSMutableArray *_stack;
	NSMutableArray *_stackKey;
	NSMutableArray *_stackKeyCollection;
	id <KoreFeedEngineDelegate> _delegate;
	NSMutableDictionary *_resultDic;
	KoreConn *_mainConn;
	
	NSMutableString*_currentString;
	
	NSString *_strUrl;
	
	NSMutableArray *_identifyNode;
	NSMutableDictionary*_mtdDataStructure;
	NSMutableDictionary*_dicFeedStructure;
	


	NSData *_bbBody;
	NSXMLParser *_xmlParse;
	
	NSString *_postStr;
	UIImage *_postImg;
	
	NSMutableDictionary *_exceptKey;
	
	BOOL _feedComplete;
//	Osp::Base::Runtime::Thread *_th;
//	Osp::Base::Runtime::Thread *_thConn;
//	 More feature Failed checking
//		1. NSError
//		2. failed from request which url
	
	NSString *_identifier;
	
	NSDictionary *_prop;
	NSDate *_lastUpdate;
	NSTimeInterval _lastUpdateSinceRefDate;
	
	KoreXtoO *_xoParser;
	XOMODE _xoMode;
	
	NSTimeInterval _timeOut;
	NSString *_fbAccToken;
//	//________________________________________________new ParseEngine
//	NSMutableArray *_stackNode;
//	NSMutableArray *_stackNodeCount;
//	
//	NSMutableArray *_arrVal;
//	NSMutableArray *_arrKey;
//	NSMutableString *_startElement;
//	EXCEP_CASE _case;							//CASE_NOTVALUE_INDIC = 0X01
}
//@property (nonatomic, assign) id _delegate;
@property (nonatomic,retain)NSString *_identifier;
@property (nonatomic,retain) NSMutableDictionary *_resultDic;
@property (nonatomic,retain) 	KoreConn *_mainConn;

@property (assign) FeedEngineMode _mode;
@property (assign) ConMode _conMode;
@property (nonatomic,retain)NSMutableArray *_stack;
@property (nonatomic,retain)NSMutableArray *_stackKey;
@property (nonatomic,retain)NSMutableArray *_stackKeyCollection;
@property (nonatomic,retain)id <KoreFeedEngineDelegate> _delegate;
@property (nonatomic,retain)NSMutableString*_currentString;
@property (nonatomic,retain)NSString *_strUrl;
@property (nonatomic,retain)NSMutableArray *_identifyNode;
@property (nonatomic,retain)NSMutableDictionary*_mtdDataStructure;
@property (nonatomic,retain)NSMutableDictionary*_dicFeedStructure;
@property (nonatomic,retain)NSData *_bbBody;
@property (nonatomic,retain)NSXMLParser *_xmlParse;
@property (nonatomic,retain)NSString *_postStr;
@property (nonatomic,retain)UIImage *_postImg;
@property (nonatomic,retain)NSMutableDictionary *_exceptKey;
@property (assign)BOOL _feedComplete;
@property (nonatomic,retain)NSDictionary *_prop;
@property (nonatomic,retain)NSDate *_lastUpdate;


-(id)_initFeed:(id<KoreFeedEngineDelegate>)delegate withUrl:(NSString *)url;
-(id)_initFeed:(id<KoreFeedEngineDelegate>)delegate withUrl:(NSString *)url andMode:(XOMODE) mode;
-(id)_initFeedFB:(id<KoreFeedEngineDelegate>)delegate withUrl:(NSString *)url andAccessToken:(NSString*)accToken;
-(void)_initFeedConn;
-(void)_initDataSturcture:(NSDictionary*)dataStruct;
-(void)_startFeed;
-(void)_startParse;
-(void)_reFresh;

-(void)_setMode:(FeedEngineMode) mode;
-(void)_setConnMode:(ConMode) conMode withParam:(NSString*) paramStr;
-(void)_setConnMode:(ConMode) conMode withImage:(UIImage*) paramImg;

-(void)_setKeyException:(NSArray*) exceptKey;
-(void)_parserDidFinish;
-(void)_setLastUpdateSinceRefDate:(NSTimeInterval) refDate;

-(void)_setTimeout:(NSTimeInterval) tOut;

@end

@protocol KoreFeedEngineDelegate<NSObject>
-(void)feedProgress:(CGFloat ) percentage from:(KoreFeedEngine*)kFeed;
-(void)feedDidFinish:(KoreFeedEngine *) kFeed;
-(void)feedDidFailed:(KoreFeedEngine *) kFeed;
-(void)feedDidCancel:(KoreFeedEngine *) kFeed;
-(void)feedNotAvailable:(KoreFeedEngine *) kFeed;

@end