//
//  KoreFeedEngine.m
//  WorldCup
//
//  Created by Naruphon Sirimasrungsee on 6/22/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "KoreFeedEngine.h"
#import "KoreFileManage.h"

static NSInteger pageNotFound = 0;

@interface KoreFeedEngine (private)

//private:
-(void)_init;


@end
@implementation KoreFeedEngine (private)

-(void)_init{
	_exceptKey = nil;
	_resultDic = nil;//[[NSMutableDictionary alloc] init];
	_mainConn = [[KoreConn alloc]initCon:self iden:[NSString stringWithFormat:@"KoreFeedEngine"]];
	_currentString = [[NSMutableString alloc] init];
	_strUrl =nil;

	_xmlParse = nil;
	_mode=FEM_URL;
	_feedComplete = YES;
	_lastUpdate = nil;

	
}

@end


@implementation KoreFeedEngine
@synthesize _identifier;
@synthesize _resultDic;
@synthesize _mainConn;
@synthesize _mode;
@synthesize _conMode;
@synthesize _stack;
@synthesize _stackKey;
@synthesize _stackKeyCollection;
@synthesize _delegate;
@synthesize _currentString;
@synthesize _strUrl;
@synthesize _identifyNode;
@synthesize _mtdDataStructure;
@synthesize _dicFeedStructure;
@synthesize _bbBody;
@synthesize _xmlParse;
@synthesize _postStr;
@synthesize _postImg;
@synthesize _exceptKey;
@synthesize _feedComplete;
@synthesize _prop;
@synthesize _lastUpdate;

- (id)copyWithZone:(NSZone *)zone{

	
//	KoreFeedEngine *newFeed = [[[self class] allocWithZone:zone] init];//[[appIcon allocWithZone:zone] init];
//	[newFeed set_identifier:_identifier];
//	[newFeed set_resultDic:_resultDic];
//	[newFeed set_mainConn:_mainConn];
//	[newFeed set_mode:_mode];
//	[newFeed set_conMode:_conMode];
//	[newFeed set_stack:_stack];
//	[newFeed set_stackKey:_stackKey];
//	[newFeed set_stackKeyCollection:_stackKeyCollection];
//	[newFeed set_delegate:_delegate];
//	[newFeed set_currentString:_currentString];
//	[newFeed set_strUrl:_strUrl];
//	[newFeed set_identifyNode:_identifyNode];
//	[newFeed set_mtdDataStructure:_mtdDataStructure];
//	[newFeed set_dicFeedStructure:_dicFeedStructure];
//	[newFeed set_bbBody:_bbBody];
//	[newFeed set_xmlParse:_xmlParse];
//	[newFeed set_postStr:_postStr];
//	[newFeed set_postImg:_postImg];
//	[newFeed set_exceptKey:_exceptKey];
//	[newFeed set_feedComplete:_feedComplete];
//	[newFeed set_prop:_prop];
	
    return(self);
}
-(void)dealloc{
    if(_identifier){
        [_identifier release];
        _identifier = nil;
    }
    if (_currentString) {
        [_currentString release];
        _currentString = nil;
    }
    if (_strUrl) {
        [_strUrl release];
        _strUrl = nil;
    }
    if (_postStr) {
        [_postStr release];
        _postStr = nil;
    }
    if (_postImg) {
        [_postImg release];
        _postImg = nil;
    }
    if (_lastUpdate) {
        [_lastUpdate release];
        _lastUpdate = nil;
    }
    if (_resultDic) {
        [_resultDic release];
        _resultDic = nil;        
    }
    if (_xoParser) {
        [_xoParser release];
        _xoParser = nil;
    }
    if (_mainConn) {
        [_mainConn release];
        _mainConn = nil;
    }

    [super dealloc];
}
-(void)_initFeedConn{TLog(@"_initFeedConn		_mode = %d",_mode);
	
	if (_mode==FEM_FILEPATH) {
		KoreFileManage *_fm = [[KoreFileManage alloc] init];
		NSData *data = [_fm readDataFrom:_strUrl];
//		_xmlParse = [[NSXMLParser alloc] initWithData:data];
//		[_xmlParse setDelegate:self];
//		[_xmlParse parse];
		_xoParser = [[KoreXtoO alloc] initParseEngine:self withMode:_xoMode andIden:@"KoreFeedEngine"];		TLog(@"----------- 1");
		[_xoParser parseData:data];																		TLog(@"----------- 2");
	}else if(_mode==FEM_URL){
		
		

		[_mainConn setUrl:[[NSString alloc] initWithFormat:@"%@",_strUrl]];
        if (_fbAccToken) {
			[_mainConn set_fbAccToken:_fbAccToken];
			[_mainConn setMode:FB_GET];
		}else {
			[_mainConn setMode:GET];
		}

		[_mainConn startCon];
//		[_mainConn setLastUpadate:_lastUpdate];
				[_mainConn	setLastUpdateSinceRefDate:_lastUpdateSinceRefDate];
			[_mainConn setTimeOut:_timeOut];

	}

}
-(id)_initFeed:(id<KoreFeedEngineDelegate>)delegate withUrl:(NSString *)url andMode:(XOMODE) mode{
	_xoMode = mode;
    _fbAccToken = nil;
	return [self _initFeed:delegate withUrl:url];
}
-(id)_initFeedFB:(id<KoreFeedEngineDelegate>)delegate withUrl:(NSString *)url andAccessToken:(NSString*)accToken{
	_xoMode = XO_MODE_JSON;
	_fbAccToken = [accToken copy];
	return [self _initFeed:delegate withUrl:url];
}
-(id)_initFeed:(id<KoreFeedEngineDelegate>)delegate withUrl:(NSString *)url{
	
	
	if(self = [super init]){
		_stack = nil;
		_stackKey = nil;
		
		[self _init];
		_delegate = delegate;
		_strUrl = [url copy];
		TLog(@"_strUrl >> %@",_strUrl);
		
		_stack = [[NSMutableArray alloc] init];
		
		_stackKey = [[NSMutableArray alloc] init];
		_timeOut = 0.0;
		
	}
	return self;
	
}
-(void)_initDataSturcture:(NSDictionary*)dataStruct{
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	_dicFeedStructure = [[NSMutableDictionary alloc]initWithDictionary:dataStruct];////////TLog(@" _dicFeedStructure = %@",_dicFeedStructure);
	//new HashMap();_dicFeedStructure->Construct(*dataStruct,0.75);
	//dataStruct;
	_mtdDataStructure = [[NSMutableDictionary alloc]init];
	
	NSArray *forKey = (NSArray*)[dataStruct allKeys];
	
	//
	for(int i=0;i<[forKey count];i++){////AppLog("keyCount = %d",i);

		NSString *strKey = [forKey objectAtIndex:i];
		NSString *strVal =  [_dicFeedStructure valueForKey:strKey];
		NSArray *strValSub = [strVal componentsSeparatedByString:@"_"];
		if([[strValSub objectAtIndex:0] isEqualToString:@"a"]){
			NSMutableArray *protArr = [[NSMutableArray alloc]init];
			[_mtdDataStructure setValue:protArr forKey:[strKey copy]];
			
		}else if([[strValSub objectAtIndex:0] isEqualToString:@"d"]){
			NSMutableDictionary *protDic = [[NSMutableDictionary alloc]init];
			[_mtdDataStructure setValue:protDic forKey:[strKey copy]];
		}
		
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}
-(void)_startFeed{//NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~_startFeed");
	_feedComplete = NO;
//	[NSTimer scheduledTimerWithTimeInterval:0.050 target:self selector:@selector(_initFeedConn) userInfo:nil repeats:NO];
	dispatch_queue_t		qConn = dispatch_queue_create("Kore.FeedEngine.queue", NULL);
	dispatch_sync(qConn, 
				  ^{
					  [self _initFeedConn];
				  }
				  );
	dispatch_release(qConn);
}
-(void)_startParse{
	
}
-(void)_reFresh{
	
}

-(void)_setMode:(FeedEngineMode) mode{
	_mode = mode;
	TLog(@"_mode = %d",_mode);
}
-(void)_setConnMode:(ConMode) conMode withParam:(NSString*) paramStr{
	_conMode = conMode;
	_postStr = [paramStr copy];
}
-(void)_setConnMode:(ConMode) conMode withImage:(UIImage*) paramImg{
	_conMode = conMode;	
	_postImg = [paramImg	retain];
}

-(void)_setKeyException:(NSArray*) exceptKey{
	_exceptKey = [[NSMutableDictionary alloc]init];
	for(int i=0;i<[exceptKey count];i++){
		NSString *key = [exceptKey objectAtIndex:i];
		NSString *value = @"exKey";
		[_exceptKey setValue:value forKey:[key copy]];
		
	}
}
-(void)_parserDidFinish{
	
}
-(void)_setLastUpdateSinceRefDate:(NSTimeInterval) refDate{
	_lastUpdateSinceRefDate = refDate;
}
-(void)_setTimeout:(NSTimeInterval) tOut{
	_timeOut = tOut;
}
// Determine V1.0.0
//-(XOTYPE)_determineType:(NSInteger**)startIndex{
//	if ([_arrKey count]==0) {
//		return XO_CANNT_DET;
//	}else if([_arrKey count]==1){
//		NSMutableString	*nodeCount = [_stackNodeCount _pop];
//		[nodeCount release];
//		return XO_ARR;
//	}else if([_arrKey count]>1){
////		TLog(@"_stackNode  = %@",_stackNode);
////		TLog(@"_stackNodeCount  = %@",_stackNodeCount);
////		TLog(@"_arrKey = %@",_arrKey);
////		TLog(@"_arrVal = %@",_arrVal);
//		NSMutableString	*nodeCount = [_stackNodeCount _pop];		
//		NSInteger nCount = [nodeCount intValue];
//		[nodeCount release];
//		NSInteger compare = 0;
////		compare = ([_arrKey count]-nCount);	
//		if (nCount>1) {
//			 compare = ([_arrKey count]-nCount);					
//		}else {
//			 
//		}
//
//		
//		*startIndex = compare;										TLog(@"startIndex = %d",*startIndex);
//		NSString *stStr = [_arrKey objectAtIndex:compare];				TLog(@"stStr = %@",stStr);
//		NSString *ndStr = [_arrKey objectAtIndex:++compare];			TLog(@"ndStr = %@",ndStr);
//		if ([stStr isEqualToString:ndStr]) {
//			return XO_ARR;
//		}else {
//			return XO_DIC;
//		}
//
//
//		
//	}
//
//}




# pragma mark -
#pragma mark KoreConn delegate methods
-(void)connectionProgress:(CGFloat) percentage from:(KoreConn*) con{
//	TLog(@"KoreFeed_percentage = %f",percentage);
	if ([_delegate respondsToSelector:@selector(feedProgress:from:)]) {
		[_delegate feedProgress:percentage from:self];		
	}


}
-(void)connectionNotAvailable:(KoreConn *) con{
    	HideNetworkActivityIndicator();
	[_delegate feedNotAvailable:self];
    [_xoParser release];
    [_mainConn release];
	
}
-(void)connectionDidFinish:(KoreConn *) con{////TLog(@"KoreFeedEngine___connectionDidFinish ++");
	HideNetworkActivityIndicator();
	if([[con _identifier] isEqualToString:@"KoreFeedEngine"]){
#ifdef ENABLE_LOGIN
		NSString *result = [[NSString alloc] initWithData:[con _receivedData] encoding:NSUTF8StringEncoding];
//		NSLog(@"connectionDidFinish >> result = %@",result);
#endif
//        TLog(@"[con _receivedData] = %@",[[con _receivedData] description]);
//        NSString *strData = [[NSString alloc] initWithData:[con _receivedData]  encoding:NSUTF8StringEncoding];
//		[self _initParseEngine];
		//_xmlParse = [[NSXMLParser alloc] initWithData:[con _receivedData]];
//		[_xmlParse setDelegate:self];
//		[_xmlParse parse];
		//////TLog(@"_xmlParser start");
		TLog(@"connectionDidFinish _xoMode = %d",_xoMode);
		_xoParser = [[KoreXtoO alloc] initParseEngine:self withMode:_xoMode andIden:@"KoreFeedEngine"];
		[_xoParser parseData:[con _receivedData]];
	}//else{
	//		
	//		[_delegate imageItemDidFinish:[con _receivedData] wihtPath:[con _pathIden]];
	//
	//		
	//	}
	
	
	
	
	
}
-(void)connectionDidFailed:(KoreConn *) con{
    	HideNetworkActivityIndicator();
	//MyAlertWithMessage(kNetworkNotFound);////TLog(@"connectionDidFailedconnectionDidFailed");
	if (!con) {
//		K_MyAlertWithMessage(@"Network Not Available 1");//TLog(@"connectionDidFailedconnectionDidFailed");
//		[_delegate feedDidFailed:self];
	}else
        if([[con _identifier] isEqualToString:@"KoreFeedEngine"]){
//            TLog(@"[con ] = %@",con);
//                        TLog(@"[con _receivedData] = %@",[con _receivedData]);
            if ([con _receivedData]) {TLog(@" > 400 case data");
                _xoParser = [[KoreXtoO alloc] initParseEngine:self withMode:_xoMode andIden:@"KoreFeedEngine"];
                [_xoParser parseData:[con _receivedData]];            
            }else{TLog(@" > 400 case error");
                [_delegate feedDidFailed:self];
            }
		

        }	
}
-(void)connectionDidCancel:(KoreConn *) con{
    	HideNetworkActivityIndicator();
	//if (!con) 
	{TLog(@">>>>>>>>>> connectionDidCancel");
		[_delegate feedDidCancel:self];
        [_xoParser release];
        [_mainConn release];
	}
	
}
# pragma mark -
#pragma mark KoreXtoODelegate
-(void)parseDidFinish:(NSMutableDictionary*) dic withIden:(NSString*)iden{//TLog(@"dic = %@",dic);
	TLog(@"iden = %@",iden);
	if ([iden isEqualToString:@"KoreFeedEngine"]) {
		_resultDic = dic;
		_feedComplete = YES;
        if ([_mainConn _httpRespCode]>=400) {
            [self _parserDidFinish];            
            [_delegate feedDidFailed:self];
        }else{
            [self _parserDidFinish];            
            [_delegate feedDidFinish:self];
        }


		[_xoParser release];
        [_mainConn release];
        _xoParser = nil;
        _mainConn = nil;
		//TLog(@"KoreFeedEngine = %@",_resultDic);
		
	}
}
-(void)parseDidFailed:(NSError *)parseError  withIden:(NSString*)iden{
    [_xoParser release];
    [_mainConn release];
    _xoParser = nil;
    _mainConn = nil;
}
//#pragma mark NSXMLParser Parsing Callbacks
//
//// Constants for the XML element names that will be considered during the parse. 
//// Declaring these as static constants reduces the number of objects created during the run
//// and is less prone to programmer error.
//
//
//
//- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *) qualifiedName attributes:(NSDictionary *)attributeDict {
//	TLog(@"******************** didStartElement + =%@",elementName);
//	[_currentString setString:@""];
//	[_startElement setString:@""];
//	_case = EXCASE_NONE;
//	TLog(@"[_stackNode count] = %d < %d [_stackNodeCount count]",[_stackNode count],[_stackNodeCount count]);
//	if ([_stackNode count] < [_stackNodeCount count]) {
//		NSMutableString *strCount = [_stackNodeCount _peek];
//		NSInteger count = [strCount intValue];
//		++count;
//		[strCount setString:@""];
//		[strCount appendFormat:@"%d",count];
//		TLog(@"..<%@>.........++....strCount = %@",[_stackNode _peek],strCount);
////		[_stackNodeCount _push:[[NSString alloc] initWithFormat:@"%d",count]];
////		_case = EXCASE_NOTVALUE_INDIC;
//	}else {
//		[_stackNodeCount _push:[[NSMutableString alloc] initWithFormat:@"%d",1]];
//	}
//
//	
//	[_stackNode _push:[[NSString alloc] initWithString:elementName]];
//	[_startElement appendString:elementName];
//}
//
//- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
//	TLog(@"******************** didEndElement - =%@",elementName);
////	if ([_currentString length]>0) {
//	if([_startElement isEqualToString:elementName]){
//		NSString *popKey = [_stackNode _pop];
//		if([_currentString length]>0){
//			[_arrKey addObject:popKey];
//			[_arrVal addObject:[[NSString alloc] initWithString:_currentString]];
//		}
//		//else if([_stackNode count] == [_stackNodeCount count]){
//		else if(_case == EXCASE_NOTVALUE_INDIC){
//			
//		}
//		else{
//			NSMutableString *strCount = [_stackNodeCount _peek];
//			NSInteger count = [strCount intValue];
//			--count;
//			[strCount setString:@""];
//			[strCount appendFormat:@"%d",count];
//			TLog(@"..<%@>......--........strCount = %@",[_stackNode _peek],strCount);
//
//			TLog(@">>>>>>>>>>>>>>>  Node Not Value <<<<<<<<<<<<<<");
//		}
//
//	}else if(![_startElement isEqualToString:elementName]) {
//		if([_arrKey count]!=[_arrVal count]){
//			TLog(@">>>>>>>>>>>>>>>  Engine Was Wrong II <<<<<<<<<<<<<<");
//			return;
//		}
//		
//		NSInteger *sIndex = 0;
//		TLog(@"_determineType ");
//		XOTYPE type = [self _determineType:&sIndex];
//		TLog(@"__________type = %d",type);
//		if (type==XO_CANNT_DET) {
//			TLog(@">>>>>>>>>>>>>>>  Engine Was Wrong III <<<<<<<<<<<<<<");
//			return;
//		}else if(type==XO_DIC){
//			NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//			NSInteger arrKeyCount = [_arrKey count];
//			for (NSInteger i = sIndex; i<arrKeyCount; i++) {
//				[dic setValue:[_arrVal objectAtIndex:i] forKey:[_arrKey objectAtIndex:i]];
//			}
//			TLog(@"__ dic = %@",dic);
//			TLog(@"__ [_arrKey count] = %d",[_arrKey count]);
//			
//			for (NSInteger j = sIndex; j<arrKeyCount; j++) {
//				TLog(@" XO_DIC rem = %d",j);
////				[_arrVal removeObjectAtIndex:j];
////				[_arrKey removeObjectAtIndex:j];
//				[_arrVal removeLastObject];
//				[_arrKey removeLastObject];
//			}
//			TLog(@"__ _arrVal = %@",_arrVal);
//			TLog(@"__ _arrKey = %@",_arrKey);
//			NSString *popKey = [_stackNode _pop];
//			if([popKey isEqualToString:elementName]){
//				[_arrKey addObject:popKey];
//				[_arrVal addObject:dic];
//			}else {
//				TLog(@">>>>>>>>>>>>>>>  Engine Was Wrong IV <<<<<<<<<<<<<<");
//			}
//			
//		}else if(type==XO_ARR){
//			NSMutableArray *arr = [[NSMutableArray alloc] init];
//			NSInteger arrKeyCount = [_arrKey count];
//			for (NSInteger i = sIndex; i<arrKeyCount; i++) {
//
//				[arr addObject:[_arrVal objectAtIndex:i]];
//			}
//			for (NSInteger i = sIndex; i<arrKeyCount; i++) {
//				TLog(@" XO_ARR rem = %d",i);
////				[_arrVal removeObjectAtIndex:i];
////				[_arrKey removeObjectAtIndex:i];
//				[_arrVal removeLastObject];
//				[_arrKey removeLastObject];
//			}
//			NSString *popKey = [_stackNode _pop];
//			if([popKey isEqualToString:elementName]){
//				[_arrKey addObject:popKey];
//				[_arrVal addObject:arr];
//			}else {
//				TLog(@">>>>>>>>>>>>>>>  Engine Was Wrong V <<<<<<<<<<<<<<");
//			}
//		}
//		
//	}
//
//	[_currentString setString:@""];
//	TLog(@"_stackNode  = %@",_stackNode);
//	TLog(@"_stackNodeCount  = %@",_stackNodeCount);
//	TLog(@"_arrKey = %@",_arrKey);
//	TLog(@"_arrVal = %@",_arrVal);
//}
//
//- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
//	[_currentString appendString:string];
//	//////////TLog(@"_currentString = %@",_currentString);
//	TLog(@"foundCharacters = %@",string);
//}
//
///*
// A production application should include robust error handling as part of its parsing implementation.
// The specifics of how errors are handled depends on the application.
// */
//- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
//    // Handle errors as appropriate for your application.
//	
//}
//- (void)parserDidEndDocument:(NSXMLParser *)parser{TLog(@"KoreFeedEngine___parserDidEndDocument");
//	
//	_resultDic = [_arrVal objectAtIndex:0];
//	//////////TLog(@"~~~~~~~~~~ 1 _arrOfItem = %d",[_arrOfItem count]);
//	_feedComplete = YES;
//	[self _parserDidFinish];
//	[_delegate feedDidFinish:self];
//
//	
//	
//	//////////TLog(@"~~~~~~~~~~ 2 ");	
//	
//}

@end
