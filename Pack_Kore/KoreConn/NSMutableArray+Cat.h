//
//  NSMutableArray+Cat.h
//  mobileTV
//
//  Created by Naruphon Sirimasrungsee on 6/2/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSMutableArray (Cat)

-(void)_push:(id) obj;
-(id) _pop;
-(id)_peek;

@end
