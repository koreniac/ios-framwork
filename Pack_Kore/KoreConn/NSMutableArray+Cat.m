//
//  NSMutableArray+Cat.m
//  mobileTV
//
//  Created by Naruphon Sirimasrungsee on 6/2/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "NSMutableArray+Cat.h"


@implementation NSMutableArray (Cat)

-(void)_push:(id) obj{
    TLog(@"************** PUSH ***************");
	[self addObject:obj];
}
-(id) _pop{
	id obj = [self lastObject];
	[self removeLastObject];
	
	return obj;
}
-(id)_peek{
	return [self lastObject];
}
@end
