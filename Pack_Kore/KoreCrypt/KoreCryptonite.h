//
//  KoreCryptonite.h
//  TrueMovie
//
//  Created by Naruphon Sirimasrungsee on 8/14/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//
//________________________________________________________________ HowTo
//Initailize
//		KoreCryptonite *crypt = [KoreCryptonite _getInstance];
//		[crypt _setInitValue:@"fedcba9876543210"];
//		NSString *pass = @"3840384A326DEF29";
//		NSArray *charList = [[NSArray alloc] initWithObjects:@"+",@"-",@"/",@"_",@"=",@",",nil];		//ë+í,  ë/í , ë=í   to   ë-í, ë_í, ë,í  
//		[crypt _replacementWith:charList];
//
//Encrypt
//		NSString *token = [[UIDevice currentDevice] uniqueIdentifier];
//		NSString *value = [crypt _enCtypted:token withPass:pass];
//
//Decrypt
//		NSString *value = @"C_tQbrj5Couk1r8EhBC5I1zYEFrWUyJX-2QLu5fyW7YKNrgXl0QqsE2yUYO7zsLv"
//		NSString *token = [crypt _deCtypted:value withPass:pass];
//
//_______________________________________________________________________

#import <Foundation/Foundation.h>
#import "NSData-AES.h"
#import "Base64.h"

@interface KoreCryptonite : NSObject {
	NSString *_iv;
	NSArray *_charList;
}
+(KoreCryptonite*)_getInstance;
-(void)_replacementWith:(NSArray*) charList;
-(NSString*)_replaceStringForward:(NSString*) str;
-(NSString*)_replaceStringBackward:(NSString*) str;
-(void)_setInitValue:(NSString*) val;
-(NSString*)_enCtypted:(NSString*) val withPass:(NSString*) pass;
-(NSString*)_deCtypted:(NSString*) val withPass:(NSString*) pass;

- (NSString *) md5:(NSString *)str;
-(NSString*) encryptSHA1:(NSString *) str;
@end
