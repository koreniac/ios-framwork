//
//  KoreCryptonite.m
//  TrueMovie
//
//  Created by Naruphon Sirimasrungsee on 8/14/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "KoreCryptonite.h"

static KoreCryptonite *_crypto = nil;
@implementation KoreCryptonite


+(KoreCryptonite*)_getInstance{
	if (!_crypto) {
		_crypto = [[KoreCryptonite alloc] init];
	}
	return _crypto;
}
-(void)_setInitValue:(NSString*) val{
	_iv = [val copy];//@"fedcba9876543210";//[val copy];
}
-(NSString*)_replaceStringForward:(NSString*) str{
	NSString *result = str;
	
	for (int i=0; i<[_charList count]; i +=2) {
		NSString *want= [_charList objectAtIndex:i];
		NSString *replace= [_charList objectAtIndex:(i+1)];
		result = [result stringByReplacingOccurrencesOfString:want withString:replace];
	}
	return result;
}
-(NSString*)_replaceStringBackward:(NSString*) str{
	NSString *result = str;
	for (int i=0; i<[_charList count]; i +=2) {
		NSString *want= [_charList objectAtIndex:i+1];
		NSString *replace= [_charList objectAtIndex:i];
		result = [result stringByReplacingOccurrencesOfString:want withString:replace];
	}
	return result;
}
-(void)_replacementWith:(NSArray*) charList;{
	_charList = [charList retain];
}
-(NSString*)_enCtypted:(NSString*) val withPass:(NSString*) pass{
	//@"fedcba9876543210"
	NSString *password = [pass copy];;//@"3840384A326DEF29";				//@"3840384A326DEF29";								//@"mypassword";
	//	NSString *str = @"KAENG|2222|201007021317|0|1|007|af7|live|haha|wifi";  //@"111111|2222|201007021317|0|1|007|af7|live|haha|wifi";//@"20080116165060659647|2008-02-18 13:13:53";						//@"20080116165060659647|2008-02-18 13:13:53";	//@"hello world 123"; 
	//	NSString *str = @"KAENG|2222|201007021318|0|1|007|af7|live|haha|wifi";
	NSString *str = [val copy];//@"KAENG|2222|201007021319|0|1|007|af7|live|haha|wifi";
	
	// It does not work with UTF8 like @"こんにちは世界"
	
	// 1) Encrypt
//	////TLog(@"encrypting string = %@",str);
	
	NSData *data = [str dataUsingEncoding: NSUTF8StringEncoding];//NSASCIIStringEncoding
	[data _setInitialValue:_iv];
	//NSData *encryptedData = [data AESEncryptWithPassphrase:password];
	NSData *encryptedData = [data AES128EncryptWithKey:password];
	
	// 2) Encode Base 64
	// If you need to send over internet, encode NSData -> Base64 encoded string
	[Base64 initialize];
	NSString *b64EncStr = [Base64 encode:encryptedData];
	
//	////TLog(@"Base 64 encoded = %@",b64EncStr);
	NSString *b64Replace = [self _replaceStringForward:b64EncStr];
//	////TLog(@"Base 64 encoded Replaced= %@",b64Replace);
	return b64Replace;
}
-(NSString*)_deCtypted:(NSString*) val withPass:(NSString*) pass{
	NSString *b64Replace = [self _replaceStringBackward:val];
	NSData	*b64DecData = [Base64 decode:b64Replace];
	[b64DecData _setInitialValue:_iv];
	NSString *password =[pass copy];// @"3840384A326DEF29";	
	
	// 4) Decrypt
	// This should be same before encode -> decode base 64
	//NSData *decryptedData = [encryptedData AESDecryptWithPassphrase:password];
	//NSData *decryptedData = [b64DecData AESDecryptWithPassphrase:password];
	NSData *decryptedData = [b64DecData AES128DecryptWithKey:password];
	
	NSString* decryptedStr = [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];//NSUTF8StringEncoding NSASCIIStringEncoding
	
	//////TLog(@"decrypted string = %@",decryptedStr);
	
}

- (NSString *) md5:(NSString *)str {
//	const char *cStr = [str UTF8String];
//	unsigned char result[16];
//	CC_MD5( cStr, strlen(cStr), result );
//	return [NSString stringWithFormat:
//			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
//			result[0], result[1], result[2], result[3], 
//			result[4], result[5], result[6], result[7],
//			result[8], result[9], result[10], result[11],
//			result[12], result[13], result[14], result[15]
//			]; 
	const char *cStr = [str UTF8String];//[str cStringUsingEncoding:NSASCIIStringEncoding];//
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5( cStr, strlen(cStr), result );
	NSString *upCase = [NSString  stringWithFormat:
						@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
						result[0], result[1], result[2], result[3], result[4],
						result[5], result[6], result[7],
						result[8], result[9], result[10], result[11], result[12],
						result[13], result[14], result[15]
						];
	return [upCase lowercaseString];
}


-(NSString*) encryptSHA1:(NSString *) str {
	NSString *hashkey = str;
	// PHP uses ASCII encoding, not UTF
	const char *s = [hashkey cStringUsingEncoding:NSASCIIStringEncoding];
	NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
	
	// This is the destination
	uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
	// This one function does an unkeyed SHA1 hash of your hash data
	CC_SHA1(keyData.bytes, keyData.length, digest);
	
	// Now convert to NSData structure to make it usable again
	NSData *out = [NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH];
	// description converts to hex but puts <> around it and spaces every 4 bytes
	NSString *hash = [out description];
	hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
	hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
	hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
	// hash is now a string with just the 40char hash value in it
	
//	////TLog(@"SHA1 = %@",hash);
	return hash;
}
@end
