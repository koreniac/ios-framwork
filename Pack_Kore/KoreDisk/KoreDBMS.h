//------------------------------------------------------------------------------------------------------------------
//                                                                                                 version 0.0.1
//	Class KoreFileManage
//  Create by Naruphon Sirimasrungsee
//  
//  StartDate   01/17/2010
//  EndDate		02/12/2010
//
//  Description     Made manage sqlite easy
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//  HowTo:
//
//
//			KoreFileManage *_fm = [[KoreFileManage alloc]init];
////////////																											## Browse All
//
//			NSArray *folderSet = [_fm _browsDirectory:FP_APP withKey:nil];
//	
//			for(int i = 0;i<[folderSet count];i++){
//				NSString *fileName = [folderSet objectAtIndex:i];
//			}
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				PATH IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//				FILE IN PATH
//					DataBase.sqlite
//					IMG_0388.jpg
//					Info.plist
//					MainWindow.nib
//					PkgInfo
//					reMake_Disk
//					reMake_DiskViewController.nib
//
////////////
////////////																											## Browse from file type
//
//			NSArray *folderSet = [_fm _browsDirectory:FP_APP withKey:@"sqlite"];
//	
//			for(int i = 0;i<[folderSet count];i++){
//				NSString *fileName = [folderSet objectAtIndex:i];
//			}
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				PATH IS [..r/Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//				FILE IN PATH
//					DataBase.sqlite
//
//
////////////
////////////																											## Check File 's Permission
//
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_EXIST];								Exist
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_READ];								Read
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_WRITEL];								write
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_EXE];								exe
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_DEL];								Delete
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_EXIST|AT_FILE_READ|AT_FILE_WRITE|AT_FILE_EXE|AT_FILE_DEL];	All
//
//
////////////
////////////																											## Get Full Path
//
//			NSString *path = [_fm _getPathOfDirectory:FP_APP ofFile:nil];													Full Application Path
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				PATH IS [..r/Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//
//			NSString *path = [_fm _getPathOfDirectory:FP_APP ofFile:@"Info.plist"];											Full Info.plist Path
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				PATH IS [..r/Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/Info.plist]
//			
//
////////////
////////////																											## Create File & Folder
//
//			[_fm _createFileAt:[[_fm _getPathOfDirectory:FP_APP ofFile:nil] stringByAppendingString:@"New Folder"] withContent:nil byAttribute:nil];
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				ORIGINAL IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//				UPDATE IS	[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/New Folder/]
//
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
//			[_fm _createFileAt:[[_fm _getPathOfDirectory:FP_APP ofFile:nil] stringByAppendingString:@"abc.abc"] withContent:[[NSData alloc] init]  byAttribute:nil];
//			OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				ORIGINAL IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//				UPDATE IS	[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/abc.abc]
//
//
////////////
////////////																											## Copy
//
//			NSString *src = [_fm _getPathOfDirectory:FP_APP ofFile:@"DBSqlite.sqlite"];
//			NSString *des = [_fm _getPathOfDirectory:FP_DOC ofFile:@"DBSqlite.sqlite"];
//			[_fm _copyFileFrom:src to:des];
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				ORIGINAL IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/DBSqlite.sqlite]
//							[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/Documents/]
//				UPDATE IS	[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/DBSqlite.sqlite]
//							[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/Documents/DBSqlite.sqlite]
//
//
////////////
////////////																											## Move
//
//			NSString *src = [_fm _getPathOfDirectory:FP_APP ofFile:@"DBSqlite.sqlite"];
//			NSString *des = [_fm _getPathOfDirectory:FP_DOC ofFile:@"DBSqlite.sqlite"];
//			[_fm _moveFileFrom:src to:des];
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				ORIGINAL IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/DBSqlite.sqlite]
//							[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/Documents/]
//				UPDATE IS	[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//							[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/Documents/DBSqlite.sqlite]
//
////////////
////////////																											## Delete
//
//			NSString *src = [_fm _getPathOfDirectory:FP_APP ofFile:@"DBSqlite.sqlite"];
//			[_fm _deleteFileAt:src];
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				ORIGINAL IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/DBSqlite.sqlite]
//
//				UPDATE IS	[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//
//

//------------------------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "KoreFileManage.h"
#import <sqlite3.h>

@interface KoreDBMS : NSObject {
	KoreDBMS *_self;
	KoreFileManage *_fm;
	sqlite3	*database;
	NSMutableDictionary *_dicOfDB;
	NSMutableDictionary *_dicOfTBschema;
	NSString *_tbName;
}
-(NSInteger)amountRowOfTable:(NSString*)tbName andCondition:(NSString*)condition;
-(NSInteger)getMaxOf:(NSString*)field onTBname:(NSString*)tbName;

-(BOOL)openDB:(NSString *)filePath  forceOpen:(BOOL) force;
-(BOOL)openDB:(NSString *) fileName atPath:(FolderPath) path forceOpen:(BOOL) force;
-(BOOL)closeDB;
-(BOOL)openTBname:(NSString *) tbName forceOpenCmd:(NSString *) force;
-(BOOL)openTBfromObj:(NSString *) tbName forceOpenCmd:(NSString *) force;
-(BOOL)checkTBexist:(NSString *) tbName;
-(BOOL)queryByCmd:(NSString*) cmd forGetType:(NSData*) data toDic:(NSMutableArray**) dataSet;
-(BOOL)queryByCmd:(NSString*) cmd forGetType:(NSData*) data toObj:(NSMutableArray**) dataSet;
-(BOOL)insertByCmd:(NSString *) cmd fromDataSet:(NSArray *) aObj;
-(BOOL)updateByCmd:(NSString *) cmd fromDataSet:(NSArray *) aObj;
-(BOOL)insertOrReplaceByCmd:(NSString *) cmd fromDataSet:(NSArray *) aObj;	// Support Insert or replace if insert only don't need primary key field
-(BOOL)deleteByCmd:(NSString*)cmd;

-(void)getRowToDic:(NSMutableDictionary**) dic referList:(NSArray*) propList byStatement:(sqlite3_stmt*) stmt;

-(void)getSchema:(NSMutableDictionary **) dic fromTBname:(NSString *) fileName;

-(NSArray *)analyseQueryStatement:(NSString *) cmd;

-(NSString *)genRealInsertCmd:(NSString*) cmd fromDic:(NSDictionary*) dic bySchema:(NSArray *) schema;

-(NSString *)genRealUpdateCmd:(NSString*) cmd fromDic:(NSDictionary*) dic bySchema:(NSArray *) schema andKey:(NSString*) keyCondition;

-(NSString *)analyseInsertStatement:(NSString *)cmd returnFeildSet:(NSMutableArray**) fieldSet;

-(NSString *)analyseUpdateStatement:(NSString *)cmd returnFeildSet:(NSMutableArray**) fieldSet andKeyCondition:(NSString**) keyCondition;
@end
