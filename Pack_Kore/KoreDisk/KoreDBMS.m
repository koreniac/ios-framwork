//
//  KoreDBMS.m
//  reMake_Disk
//
//  Created by Naruphon Sirimasrungsee on 1/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "KoreDBMS.h"


@implementation KoreDBMS
-(id)init{
	if(_self==nil){
		if(self = [super init]){
			_fm	 = [[KoreFileManage alloc]init];
			_self = self;
			
		}
	}
	return _self;
}
-(NSInteger)amountRowOfTable:(NSString*)tbName andCondition:(NSString*)condition{
	NSInteger amount = 0;
	NSString *cmd = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@",tbName,condition];
	const char *sqlStatement = [cmd cStringUsingEncoding:NSUTF8StringEncoding]; //"select * from animals";
	sqlite3_stmt *compiledStatement;TLog(@"---------- 2");
	
	if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {TLog(@"---------- 3");
		sqlite3_step(compiledStatement);
		amount = sqlite3_column_int(compiledStatement, 0);
		
	}
	return amount;
}
-(NSInteger)getMaxOf:(NSString*)field onTBname:(NSString*)tbName{
	NSInteger max = 0;
	NSString *cmd = [NSString stringWithFormat:@"select max(%@) from %@",field,tbName];
	const char *sqlStatement = [cmd cStringUsingEncoding:NSUTF8StringEncoding]; //"select * from animals";
	sqlite3_stmt *compiledStatement;TLog(@"---------- 2");

	if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {TLog(@"---------- 3");
		sqlite3_step(compiledStatement);
		max = sqlite3_column_int(compiledStatement, 0);
	
	}
	return max;
}
-(BOOL)openDB:(NSString *)filePath  forceOpen:(BOOL) force{
	BOOL result = NO;
	NSString * dbPath = [filePath copy];
	if([_fm _checkFileAccessPath:dbPath withAuthorize:AT_FILE_EXIST]){
		if (sqlite3_open([dbPath UTF8String], &database) != SQLITE_OK) {TLog(@"--------------sqlite3_open_nonForce");
			sqlite3_close(database);
			NSAssert(0, @"Failed to open database");
		}result = YES;
	}else if(force){
		if (sqlite3_open([dbPath UTF8String], &database) != SQLITE_OK) {TLog(@"--------------sqlite3_open_Force");
			sqlite3_close(database);
			NSAssert(0, @"Failed to open database");
		}result = YES;
	}
	TLog(@"--------------dbPath = %@",dbPath);
	
	return result;
}
-(BOOL)openDB:(NSString *) fileName atPath:(FolderPath) path forceOpen:(BOOL) force{
	BOOL result = NO;
	NSString * dbPath = [_fm _getPathOfDirectory:path ofFile:fileName];	
	if([_fm _checkFileAccessPath:dbPath withAuthorize:AT_FILE_EXIST]){
		if (sqlite3_open([dbPath UTF8String], &database) != SQLITE_OK) {//TLog(@"--------------sqlite3_open");
			sqlite3_close(database);
			NSAssert(0, @"Failed to open database");
		}result = YES;
	}else if(force){
		if (sqlite3_open([dbPath UTF8String], &database) != SQLITE_OK) {//TLog(@"--------------sqlite3_open");
			sqlite3_close(database);
			NSAssert(0, @"Failed to open database");
		}result = YES;
	}
	TLog(@"--------------dbPath = %@",dbPath);
		
	return result;
}
-(BOOL)closeDB{
	BOOL result = NO;
	sqlite3_close(database);
	result = YES;
	return result;	
}
-(BOOL)openTBname:(NSString *)tbName forceOpenCmd:(NSString *) force{
    TLog(@"openTBname  = %@",tbName);
	BOOL result = NO;
	if([_self checkTBexist:tbName]){//** Table Exist
		if(_dicOfDB==nil){
			_dicOfDB = [[NSMutableDictionary alloc]init];
		}
		_tbName = tbName;
		_dicOfTBschema = nil;
		_dicOfTBschema = [_dicOfDB valueForKey:tbName];
		if(_dicOfTBschema ==nil){//TLog(@"  tbName = %@",tbName);
			_dicOfTBschema = [[NSMutableDictionary alloc] init];
			[self getSchema:&_dicOfTBschema fromTBname:tbName];//TLog(@"--- _dicOfTBschema count = %d",[_dicOfTBschema count]);
			[_dicOfDB setValue:_dicOfTBschema  forKey:tbName];
			//TLog(@"--- _dicOfDB count = %d",[_dicOfDB count]);
//			NSDictionary *tbDic = [_dicOfDB valueForKey:tbName];
//			NSArray *fName = [tbDic valueForKey:@"F_NAME"];
//			NSArray *fType = [tbDic valueForKey:@"F_TYPE"];
//			TLog(@"--- fName count = %d",[fName count]);
//			TLog(@"--- fType count = %d",[fType count]);
		}
		result = YES;
	}else if(force!=nil){
		char *errorMsg;
		if (sqlite3_exec (database, [force  UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {TLog(@"--------------sqlite3_exec");
			sqlite3_close(database);
			NSAssert1(0, @"Error creating table: %s", errorMsg);
			return NO;
		}
		
		result = YES;
	}
	return result;
}
-(BOOL)openTBfromObj:(NSString *)tbName forceOpenCmd:(NSString *) force{
	BOOL result = NO;
	if([_self checkTBexist:tbName]){
		
		
		
	}
	
	return result;
}
-(BOOL)checkTBexist:(NSString *) tbName{
	BOOL result = NO;
	
	NSString * resultStr =nil;
	//	NSMutableString *qCmd = [[NSMutableString alloc]initWithString:@"SELECT name FROM sqlite_master WHERE type='table' AND name='Player'"];
//	NSMutableString *qCmd = [[NSMutableString alloc]initWithString:@"SELECT name FROM sqlite_master WHERE type='table' AND name='"];
//	[qCmd appendString:tbName];
//	[qCmd appendString:@"'"];//	//TLog(@"========tbName = %@",qCmd);
	NSString *qCmd = [[NSString alloc]initWithFormat:@"SELECT name FROM sqlite_master WHERE type='table' AND name='%@'",tbName];
	const char *sqlStatement = [qCmd cStringUsingEncoding:NSUTF8StringEncoding]; //"select * from animals";
	sqlite3_stmt *compiledStatement;////TLog(@"____________________________________ 1");
	if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
		// Loop through the results and add them to the feeds array
		
		while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
			resultStr = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
			////TLog(@"______result = %@",result);
		}
		
	}
	sqlite3_finalize(compiledStatement);
	
	if(resultStr&&[resultStr isEqualToString:tbName]){
		result = YES;
	}else{
		
	}
	
	return result;
}
-(BOOL)queryByCmd:(NSString*) cmd forGetType:(NSData*) data toDic:(NSMutableArray**) dataSet{
	BOOL result = NO;
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Analyse Statement
	
	NSArray *propList = [self analyseQueryStatement:cmd];
	for(int i=0;i<[propList count];i++){
		NSString *fName = [propList objectAtIndex:i];
		TLog(@"------ fname = %@",fName);
		
		
		
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Analyse Statement
	
	*dataSet = [[NSMutableArray alloc]init];TLog(@"---------- 1");
	const char *sqlStatement = [cmd cStringUsingEncoding:NSUTF8StringEncoding]; //"select * from animals";
	sqlite3_stmt *compiledStatement;TLog(@"---------- 2");
	if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {TLog(@"---------- 3");
		while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
			// Read the data from the result row
//			KoreData *prototype = [obj allocNewOne];
//			[self getDataToObj:&prototype byStmt:compiledStatement];
			NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
			[self getRowToDic:&dataDic referList:propList byStatement:compiledStatement];
			[*dataSet addObject:dataDic];
			[dataDic release];
		}
	}
	// Release the compiled statement from memory
	sqlite3_finalize(compiledStatement);
	TLog(@"---------- 4");
	

	return result;
}
-(BOOL)queryByCmd:(NSString*) cmd forGetType:(NSData*) data toObj:(NSMutableArray**) dataSet{
	BOOL result = NO;
	
	
	return result;
	
}
-(BOOL)insertByCmd:(NSString *) cmd fromDataSet:(NSArray *) aObj{
	BOOL result = NO;
	
	
	return result;
}
-(BOOL)updateByCmd:(NSString *) cmd fromDataSet:(NSArray *) aObj{
	BOOL result = NO;
	
	char * errorMsg;
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Analyse
	NSMutableArray *fieldListStr = nil;
	NSString *keyCondition = nil;
	NSString *command = [self analyseUpdateStatement:cmd returnFeildSet:&fieldListStr andKeyCondition:&keyCondition];
	TLog(@" command = %@",command);
	for(int i=0;i<[aObj count];i++)	{
		NSDictionary *tmp = [aObj objectAtIndex:i];
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Synthesize
		NSString *update = [self genRealUpdateCmd:command fromDic:tmp bySchema:fieldListStr andKey:keyCondition];
		TLog(@"update = %@ ",update);
		if (sqlite3_exec (database, [update UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK)
		{// return error may be good
			NSAssert1(0, @"Error Insert or updating tables: %s", errorMsg);	
			return result;
		}

		
	}result = YES;
	
	return result;
}
-(BOOL)insertOrReplaceByCmd:(NSString *) cmd fromDataSet:(NSArray *) aObj{
	BOOL result = NO;
	char * errorMsg;
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Analyse
	NSMutableArray *fieldListStr = nil;
	NSString *command = [self analyseInsertStatement:cmd returnFeildSet:&fieldListStr];
	TLog(@" command = %@",command);
//	sqlite3_config(SQLITE_CONFIG_MULTITHREAD);
    NSTimeInterval timeStart = [NSDate timeIntervalSinceReferenceDate];
    TLog(@"timeStart = %f",timeStart);
    
	for(int i=0;i<[aObj count];i++)	{
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		NSDictionary *tmp = [aObj objectAtIndex:i];
//        TLog(@"tmp = %@",tmp);
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Synthesize
		NSString *insert = [self genRealInsertCmd:command fromDic:tmp bySchema:fieldListStr];
//		TLog(@"insert = %@ ",insert);//
		if (sqlite3_exec (database, [insert UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK)
		{// return error may be good
			NSAssert1(0, @"Error Insert or updating tables: %s", errorMsg);	
			return result;
		}
        sqlite3_exec(database, "PRAGMA synchronous = OFF", NULL, NULL, &errorMsg);
        sqlite3_exec(database, "PRAGMA journal_mode = MEMORY", NULL, NULL, &errorMsg);
        sqlite3_exec(database, "PRAGMA page_size = 1024", NULL, NULL, &errorMsg);
        [pool drain];
	}result = YES;
    
    NSTimeInterval timeStop = [NSDate timeIntervalSinceReferenceDate];
    TLog(@"timeStop = %f",timeStop);
        TLog(@"timeDif = %f",(timeStop-timeStart));
	return result;
	
}
-(BOOL)deleteByCmd:(NSString*)cmd{
    BOOL result = NO;
    
    const char *sqlStatement = [cmd cStringUsingEncoding:NSUTF8StringEncoding]; //"select * from animals";
	sqlite3_stmt *compiledStatement;TLog(@"---------- 2");
    char * errorMsg;
	if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement,NULL) != SQLITE_OK) {TLog(@"---------- 3");
		NSAssert1(0, @"Error Delete tables: %s", errorMsg);	
	}
    //When binding parameters, index starts from 1 and not zero.
//    sqlite3_bind_int(compiledStatement, 1, 1);
    
    if (SQLITE_DONE != sqlite3_step(compiledStatement))
        NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));
    
    sqlite3_reset(compiledStatement);
	// Release the compiled statement from memory
	sqlite3_finalize(compiledStatement);
	TLog(@"---------- 4");

    result = YES;

    return result;
}
-(void)getRowToDic:(NSMutableDictionary**) dic referList:(NSArray*) propList byStatement:(sqlite3_stmt*) stmt{
	NSDictionary *tbSchema = [_dicOfDB objectForKey:_tbName];//TLog(@"[tbSchema count] = %d",[tbSchema count] );
    //	TLog(@"tbSchema = %@",tbSchema);
	NSArray *fName = [tbSchema objectForKey:@"F_NAME"];//TLog(@"[fName count] = %d",[fName count] );
	NSArray *fType = [tbSchema objectForKey:@"F_TYPE"];//TLog(@"[fType count] = %d",[fType count] );
    //    TLog(@"propList = %@",propList);
    if ([[propList objectAtIndex:0] isEqualToString:@"*"]) {
        for(int i=0;i<[fName count];i++){
            
            int schemaIndex = i;//[fName objectAtIndex:i]; //TLog(@" schemaIndex = %d",schemaIndex);
            //NSString *whatType = [fType objectAtIndex:schemaIndex];//TLog(@" whatType = %@",whatType);
            
            char *pCh = (char *)sqlite3_column_text(stmt, i);
            if(pCh!=nil){
                @try {
                    NSString *keyOfValue = [fName objectAtIndex:schemaIndex];//TLog(@" keyOfValue =  %@",keyOfValue);
                    [*dic setValue:[NSString stringWithUTF8String:pCh] forKey:keyOfValue];
                    [*dic setValue:[fType objectAtIndex:schemaIndex] forKey:[NSString stringWithFormat:@"T_%@",keyOfValue]];						
                }
                @catch (NSException * e) {
                    
                }
                @finally {
                    
                }
            }
            
        }
    }else{
        for(int i=0;i<[propList count];i++){
            //            TLog(@"------- i[%d] = %@",i,[propList objectAtIndex:i]);
            int schemaIndex = [fName indexOfObject:[propList objectAtIndex:i]]; //TLog(@" schemaIndex = %d",schemaIndex);
            //NSString *whatType = [fType objectAtIndex:schemaIndex];//TLog(@" whatType = %@",whatType);
            
            char *pCh = (char *)sqlite3_column_text(stmt, i);
            if(pCh!=nil){
                @try {
                    NSString *keyOfValue = [fName objectAtIndex:schemaIndex];//TLog(@" keyOfValue =  %@",keyOfValue);
                    [*dic setValue:[NSString stringWithUTF8String:pCh] forKey:keyOfValue];
                    [*dic setValue:[fType objectAtIndex:schemaIndex] forKey:[NSString stringWithFormat:@"T_%@",keyOfValue]];						
                }
                @catch (NSException * e) {
                    
                }
                @finally {
                    
                }
                //			NSString *keyOfValue = [fName objectAtIndex:schemaIndex];TLog(@" keyOfValue =  %@",keyOfValue);
                //			[*dic setValue:[NSString stringWithUTF8String:pCh] forKey:keyOfValue];
                //			[*dic setValue:[fType objectAtIndex:schemaIndex] forKey:[NSString stringWithFormat:@"T_%@",keyOfValue]];
            }
            //if([whatType isEqualToString:@"INTEGER"]){TLog(@"- - - - - INTEGER");
            //			[*dic setValue:sqlite3_column_int(stmt,i) forKey:[fName objectAtIndex:schemaIndex]];
            //		}else if([whatType isEqualToString:@"TEXT"]||[whatType isEqualToString:@"VARCHAR(255)"]){TLog(@"- - - - - TEXT");
            //			char *pCh = (char *)sqlite3_column_text(stmt, i);
            //			if(pCh!=nil){
            //				[*dic setValue:[NSString stringWithUTF8String:pCh] forKey:[fName objectAtIndex:schemaIndex]];
            //			}
            //				
            //		}else if([whatType isEqualToString:@"NONE"]){TLog(@"- - - - - NONE");
            //			
            //		}else if([whatType isEqualToString:@"REAL"]){TLog(@"- - - - - REAL");
            //			
            //		}else if([whatType isEqualToString:@"NUMERIC"]){TLog(@"- - - - - NUMERIC");
            //			
            //		}
        }
    }
}
//-(void)getRowToDic:(NSMutableDictionary**) dic referList:(NSArray*) propList byStatement:(sqlite3_stmt*) stmt{
//	NSDictionary *tbSchema = [_dicOfDB objectForKey:_tbName];//TLog(@"[tbSchema count] = %d",[tbSchema count] );
//	TLog(@"tbSchema = %@",tbSchema);
//	NSArray *fName = [tbSchema objectForKey:@"F_NAME"];//TLog(@"[fName count] = %d",[fName count] );
//	NSArray *fType = [tbSchema objectForKey:@"F_TYPE"];//TLog(@"[fType count] = %d",[fType count] );
//	for(int i=0;i<[propList count];i++){
//		TLog(@"------- i[%d] = %@",i,[propList objectAtIndex:i]);
//		int schemaIndex = [fName indexOfObject:[propList objectAtIndex:i]]; //TLog(@" schemaIndex = %d",schemaIndex);
//		//NSString *whatType = [fType objectAtIndex:schemaIndex];//TLog(@" whatType = %@",whatType);
//		
//		char *pCh = (char *)sqlite3_column_text(stmt, i);
//		if(pCh!=nil){
//			@try {
//				NSString *keyOfValue = [fName objectAtIndex:schemaIndex];TLog(@" keyOfValue =  %@",keyOfValue);
//				[*dic setValue:[NSString stringWithUTF8String:pCh] forKey:keyOfValue];
//				[*dic setValue:[fType objectAtIndex:schemaIndex] forKey:[NSString stringWithFormat:@"T_%@",keyOfValue]];						
//			}
//			@catch (NSException * e) {
//						
//			}
//			@finally {
//
//			}
////			NSString *keyOfValue = [fName objectAtIndex:schemaIndex];TLog(@" keyOfValue =  %@",keyOfValue);
////			[*dic setValue:[NSString stringWithUTF8String:pCh] forKey:keyOfValue];
////			[*dic setValue:[fType objectAtIndex:schemaIndex] forKey:[NSString stringWithFormat:@"T_%@",keyOfValue]];
//		}
//		//if([whatType isEqualToString:@"INTEGER"]){TLog(@"- - - - - INTEGER");
////			[*dic setValue:sqlite3_column_int(stmt,i) forKey:[fName objectAtIndex:schemaIndex]];
////		}else if([whatType isEqualToString:@"TEXT"]||[whatType isEqualToString:@"VARCHAR(255)"]){TLog(@"- - - - - TEXT");
////			char *pCh = (char *)sqlite3_column_text(stmt, i);
////			if(pCh!=nil){
////				[*dic setValue:[NSString stringWithUTF8String:pCh] forKey:[fName objectAtIndex:schemaIndex]];
////			}
////				
////		}else if([whatType isEqualToString:@"NONE"]){TLog(@"- - - - - NONE");
////			
////		}else if([whatType isEqualToString:@"REAL"]){TLog(@"- - - - - REAL");
////			
////		}else if([whatType isEqualToString:@"NUMERIC"]){TLog(@"- - - - - NUMERIC");
////			
////		}
//	}
//}

-(void)getSchema:(NSMutableDictionary **) dic fromTBname:(NSString *) fileName{
	//*dic = [[NSMutableDictionary alloc] init];
//	NSMutableDictionary *dicInstance = *dic;
	NSString *str=[NSString stringWithFormat:@"PRAGMA table_info('%@')",fileName];	
	NSMutableArray *fName=[[NSMutableArray alloc] init];
	NSMutableArray *fType=[[NSMutableArray alloc] init];
	sqlite3_stmt *compiledStatement;
	
	const char *sqlgetschema =[str cStringUsingEncoding:NSUTF8StringEncoding];
	if(sqlite3_prepare_v2(database, sqlgetschema, -1, &compiledStatement, NULL) == SQLITE_OK) {
		while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
			[fName addObject:[[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)]];
			[fType addObject:[[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)]];
		}
	}
	[*dic setValue:fName forKey:@"F_NAME"];
	[*dic setValue:fType forKey:@"F_TYPE"];
	TLog(@"     *dic count = %d",[*dic count]);
}

-(NSArray *)analyseQueryStatement:(NSString *) cmd{
	NSString *upperCase = [cmd uppercaseString];
	NSRange distinctRange = [upperCase rangeOfString:@"DISTINCT"];
	NSRange selRange;// = [upperCase rangeOfString:@"SELECT"];
	if(distinctRange.length>0){
		NSRange range = [upperCase rangeOfString:@"SELECT"];
		
		selRange.location = range.location;
		selRange.length = (distinctRange.location-range.location)+distinctRange.length;
	}else {
		selRange = [upperCase rangeOfString:@"SELECT"];
	}

	
	
	NSRange fromRange = [upperCase rangeOfString:@"FROM"];

	NSRange splitRange;
	splitRange.location = selRange.location+selRange.length;
	splitRange.length = fromRange.location-splitRange.location;
	NSString *splitStr = [cmd substringWithRange:splitRange];
	TLog(@"  splitStr = %@",splitStr);
	NSArray *listStr = [splitStr componentsSeparatedByString:@","];
	NSMutableArray *muListStr = [[NSMutableArray alloc]init];

	for(int i=0;i<[listStr count];i++){
		NSString *trimStr = [(NSString*)[listStr objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		[muListStr addObject:trimStr];
	}
		
	
	
	return muListStr;
}
-(NSString *)genRealInsertCmd:(NSString*) cmd fromDic:(NSDictionary*) dic bySchema:(NSArray *) schema{
	NSMutableString *result = nil;
	
	result = [[NSMutableString alloc]initWithString:cmd];
	[result appendString:@"("];
	for(int i=0;i<[schema count];i++){
		NSString *key = [schema objectAtIndex:i];
		NSString *tmpVal = [dic valueForKey:key];
		NSString *value = nil;
		if ([tmpVal isKindOfClass:[NSString class]]) {
            NSString *tmp  = [tmpVal stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            
            value = tmp;//[tmp stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		}else {
			value = tmpVal;
		}
//		TLog(@"genRealInsertCmd_value = %@",value);
		NSString *valueType = [dic valueForKey:[NSString stringWithFormat:@"T_%@",value]];
//		TLog(@" [%d] key is = %@",i,key);
//		TLog(@" [%d] value is = %@",i,value);
//		TLog(@" [%d] valueType is = %@",i,valueType);
		if([valueType isEqualToString:@"INTEGER"]||[valueType isEqualToString:@"REAL"]||[valueType isEqualToString:@"BLOB"]){
			[result appendFormat:@"%@",value];
		}else if([valueType isEqualToString:@"NULL"]||value==nil){//||[valueType isEqualToString:@"(null)"]
			[result appendString:@"NULL"];
		}else{
			[result appendFormat:@"'%@'",value];
		}
		if(i<[schema count]-1){
			[result appendString:@","];
		}else {
			[result appendString:@")"];
		}

		
	}
	
	return result;
}

-(NSString *)genRealUpdateCmd:(NSString*) cmd fromDic:(NSDictionary*) dic bySchema:(NSArray *) schema andKey:(NSString*) keyCondition{
		//@"UPDATE Sheet2 SET objPosX = ?, objPosY = ?, objTypeId = ?, objTypeName = ? WHERE objId = ?"
	NSMutableString *result = nil;
	
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Synthesize
	result = [[NSMutableString alloc]initWithString:cmd];
	//[result appendString:@" "];
	for(int i=0;i<[schema count];i++){
		NSString *key = [schema objectAtIndex:i];
		
		NSString *value = [dic valueForKey:key];
		NSString *valueType = [dic valueForKey:[NSString stringWithFormat:@"T_%@",value]];
//		TLog(@" [%d] key is = %@",i,key);
//		TLog(@" [%d] value is = %@",i,value);
//		TLog(@" [%d] valueType is = %@",i,valueType);
		if([valueType isEqualToString:@"INTEGER"]||[valueType isEqualToString:@"REAL"]||[valueType isEqualToString:@"BLOB"]){
			[result appendFormat:@" %@ = %@",key,value];
		}else if([valueType isEqualToString:@"NULL"]||value==nil){//||[valueType isEqualToString:@"(null)"]
			[result appendFormat:@" %@ = NULL",key];
		}else{
			[result appendFormat:@" %@ = '%@'",key,value];
		}
		
		if(i<[schema count]-1){
			[result appendString:@","];
		}else {
			[result appendString:@" WHERE "];
		}
		
		
	}
	NSString *valueKey = [dic valueForKey:keyCondition];
	NSString *valueKeyType = [dic valueForKey:[NSString stringWithFormat:@"T_%@",keyCondition]];
//	TLog(@" valueKey = %@",valueKey);
//	TLog(@" valueKeyType = %@",valueKeyType);
	if([valueKeyType isEqualToString:@"INTEGER"]||[valueKeyType isEqualToString:@"FLOAT"]||[valueKeyType isEqualToString:@"REAL"]||[valueKeyType isEqualToString:@"BLOB"]){
		[result appendFormat:@"%@ = %@",keyCondition,valueKey];
	}else{
		[result appendFormat:@"%@ = '%@'",keyCondition,valueKey];
	}
	
	
	
	return result;
	
}
-(NSString *)analyseInsertStatement:(NSString *)cmd returnFeildSet:(NSMutableArray**) fieldSet{
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Analyse
	NSString *upperCase = [cmd uppercaseString];
	NSRange intoR = [upperCase rangeOfString:@"INTO"];
	NSRange valR = [upperCase rangeOfString:@"VALUES"];
	NSRange splitRange;
	splitRange.location = intoR.location+intoR.length;
	splitRange.length = valR.location-splitRange.location;
	NSString *tableAndField = [[cmd substringWithRange:splitRange] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//	TLog(@"tableAndField = %@",tableAndField);
	NSRange braceLR = [tableAndField rangeOfString:@"("];
	NSRange braceRR = [tableAndField rangeOfString:@")"];
	NSRange tableR;
	tableR.location = 0;
	tableR.length = braceLR.location;
	NSString *tableName = [[tableAndField substringWithRange:tableR] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//	TLog(@"tableName = %@",tableName);
	NSRange fieldR;
	fieldR.location =  braceLR.location+ braceLR.length;
	fieldR.length = braceRR.location-fieldR.location;
	NSString *fieldName = [[tableAndField substringWithRange:fieldR] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	NSArray *listStr = [fieldName componentsSeparatedByString:@","];
	*fieldSet = [[NSMutableArray alloc]init];
	
	for(int i=0;i<[listStr count];i++){
		NSString *trimStr = [(NSString*)[listStr objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		[*fieldSet addObject:trimStr];
//		TLog(@"trimStr =%@;",trimStr);
	}
	
	NSRange commR;
	commR.location = 0;
	commR.length = valR.location+valR.length;
	NSString *command = [[cmd substringWithRange:commR] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	
	return command;
}

-(NSString *)analyseUpdateStatement:(NSString *)cmd returnFeildSet:(NSMutableArray**) fieldSet andKeyCondition:(NSString**) keyCondition{
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Analyse
	NSString *upperCase = [cmd uppercaseString];
	NSRange setR = [upperCase rangeOfString:@"SET"];
	NSRange whereR = [upperCase rangeOfString:@"WHERE"];
	NSRange splitRange;
	splitRange.location = setR.location+setR.length;
	splitRange.length = whereR.location-splitRange.location;
	NSString *setField = [[cmd substringWithRange:splitRange] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//	TLog(@"setField = %@",setField);
	NSArray *listStr = [setField componentsSeparatedByString:@","];
	*fieldSet = [[NSMutableArray alloc]init];
	for(int i=0;i<[listStr count];i++){
		NSString *trimStr = [(NSString*)[listStr objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		NSRange equalR = [trimStr rangeOfString:@"="];
		NSString *chopStr = [trimStr substringToIndex:equalR.location];
//		TLog(@" chopStr = %@",chopStr);
		[*fieldSet addObject:[chopStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
		
		
	}
	NSString *whereStr = [cmd substringFromIndex:(whereR.location+whereR.length)];
	NSRange equalwR = [whereStr rangeOfString:@"="];
	*keyCondition = [[whereStr substringToIndex:equalwR.location] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//	TLog(@" keyStr = %@",*keyCondition);
	
	
	NSRange commR;
	commR.location = 0;
	commR.length = setR.location+setR.length;
	NSString *command = [[cmd substringWithRange:commR] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

	return command;
}
@end
