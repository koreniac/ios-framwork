//------------------------------------------------------------------------------------------------------------------
//                                                                                                 version 0.0.1
//	Class KoreFileManage
//  Create by Naruphon Sirimasrungsee
//  
//  StartDate   01/17/2010
//  EndDate		02/12/2010
//
//  Description     Manage all about File & Folder system
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//  HowTo:
//
//
//			KoreFileManage *_fm = [[KoreFileManage alloc]init];
////////////																											## Browse All
//
//			NSArray *folderSet = [_fm _browsDirectory:FP_APP withKey:nil];
//	
//			for(int i = 0;i<[folderSet count];i++){
//				NSString *fileName = [folderSet objectAtIndex:i];
//			}
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				PATH IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//				FILE IN PATH
//					DataBase.sqlite
//					IMG_0388.jpg
//					Info.plist
//					MainWindow.nib
//					PkgInfo
//					reMake_Disk
//					reMake_DiskViewController.nib
//
////////////
////////////																											## Browse from file type
//
//			NSArray *folderSet = [_fm _browsDirectory:FP_APP withKey:@"sqlite"];
//	
//			for(int i = 0;i<[folderSet count];i++){
//				NSString *fileName = [folderSet objectAtIndex:i];
//			}
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				PATH IS [..r/Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//				FILE IN PATH
//					DataBase.sqlite
//
//
////////////
////////////																											## Check File 's Permission
//
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_EXIST];								Exist
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_READ];								Read
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_WRITEL];								write
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_EXE];								exe
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_DEL];								Delete
//			BOOL permission = [_fm _checkFileAccessPath:fullPath withAuthorize:AT_FILE_EXIST|AT_FILE_READ|AT_FILE_WRITE|AT_FILE_EXE|AT_FILE_DEL];	All
//
//
////////////
////////////																											## Get Full Path
//
//			NSString *path = [_fm _getPathOfDirectory:FP_APP ofFile:nil];													Full Application Path
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				PATH IS [..r/Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//
//			NSString *path = [_fm _getPathOfDirectory:FP_APP ofFile:@"Info.plist"];											Full Info.plist Path
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				PATH IS [..r/Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/Info.plist]
//			
//
////////////
////////////																											## Create File & Folder
//
//			[_fm _createFileAt:[[_fm _getPathOfDirectory:FP_APP ofFile:nil] stringByAppendingString:@"New Folder"] withContent:nil byAttribute:nil];
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				ORIGINAL IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//				UPDATE IS	[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/New Folder/]
//
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
//			[_fm _createFileAt:[[_fm _getPathOfDirectory:FP_APP ofFile:nil] stringByAppendingString:@"abc.abc"] withContent:[[NSData alloc] init]  byAttribute:nil];
//			OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				ORIGINAL IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//				UPDATE IS	[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/abc.abc]
//
//
////////////
////////////																											## Copy
//
//			NSString *src = [_fm _getPathOfDirectory:FP_APP ofFile:@"DBSqlite.sqlite"];
//			NSString *des = [_fm _getPathOfDirectory:FP_DOC ofFile:@"DBSqlite.sqlite"];
//			[_fm _copyFileFrom:src to:des];
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				ORIGINAL IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/DBSqlite.sqlite]
//							[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/Documents/]
//				UPDATE IS	[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/DBSqlite.sqlite]
//							[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/Documents/DBSqlite.sqlite]
//
//
////////////
////////////																											## Move
//
//			NSString *src = [_fm _getPathOfDirectory:FP_APP ofFile:@"DBSqlite.sqlite"];
//			NSString *des = [_fm _getPathOfDirectory:FP_DOC ofFile:@"DBSqlite.sqlite"];
//			[_fm _moveFileFrom:src to:des];
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				ORIGINAL IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/DBSqlite.sqlite]
//							[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/Documents/]
//				UPDATE IS	[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//							[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/Documents/DBSqlite.sqlite]
//
////////////
////////////																											## Delete
//
//			NSString *src = [_fm _getPathOfDirectory:FP_APP ofFile:@"DBSqlite.sqlite"];
//			[_fm _deleteFileAt:src];
//			::OUTPUT::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//				ORIGINAL IS [../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/DBSqlite.sqlite]
//
//				UPDATE IS	[../Applications/CDAAAF2C-011D-4142-83F8-F2F368A65130/reMake_Disk.app/]
//
//

//------------------------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
typedef enum {
	FP_APP = -1,
	FP_DOC = 0,
	FP_LIB = 1,
	FP_TMP = 2,
	FP_HOME = 0XFFFFFF,
}FolderPath;
typedef enum {
	AT_FILE_EXIST = 0X01,
	AT_FILE_READ = 0X02,
	AT_FILE_WRITE = 0X04,
	AT_FILE_EXE = 0X08,
	AT_FILE_DEL = 0X10,
}Authorize;

typedef enum {
	FE_NONE = 0,
	FE_CREATE_FILE = 0X01,
	FE_CREAET_FOLDER = 0X02,
	FE_MOVE = 0X04,
	FE_COPY = 0X08,
	FE_DELETE = 0X10,
}FunctionError;

@interface KoreFileManage : NSObject {
	NSMutableArray *_errorTrace;
	NSFileManager *_fileManager;
	
}
@property (nonatomic,retain)NSMutableArray *_errorTrace;
//																														**BASIC**
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Getting File Manager

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Create Item
-(BOOL)_createFileAt:(NSString*) des withContent:(NSData*) data byAttribute:(NSDictionary*) attb;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Move Item
-(BOOL)_moveFileFrom:(NSString *) src to:(NSString*) des;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Copy Item
-(BOOL)_copyFileFrom:(NSString *) src to:(NSString*) des;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Remove Item
-(BOOL)_deleteFileAt:(NSString *) src;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Discovering Files
// return Array of file name
-(NSMutableArray *)_browsDirectory:(FolderPath) path withKey:(NSString *) keySearch;// have to improve for search anykey
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	File access Authorize
-(NSMutableString *)_getPathOfDirectory:(FolderPath) path ofFile:(NSString *) fileName;
-(BOOL)_checkFileAccessPath:(NSString*) filePath withAuthorize:(Authorize) author;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Managing the Current Directory




//																														**MIX**
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-(BOOL)readFile:(NSString *) name From:(FolderPath) path byData:(NSData**) rec;
-(BOOL)readFile:(NSString *) name From:(FolderPath) path byString:(NSString**) rec;
-(BOOL)readFile:(NSString *) name From:(FolderPath) path byImage:(UIImage**) rec;

-(NSData*)readDataFrom:(NSString *) name;
-(NSString*)readStringFrom:(NSString *) name;
-(UIImage*)readImageFrom:(NSString *) name;

-(BOOL)moveFile:(NSString *) name From:(FolderPath) src To:(FolderPath) des;
-(BOOL)copyFile:(NSString *) name From:(FolderPath) src To:(FolderPath) des;


@end
