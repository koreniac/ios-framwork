//------------------------------------------------------------------------------------------------------------------
//                                                                                                 version 0.0.1
//  Create by Naruphon Sirimasrungsee
//  
//  StartDate   06/30/2009
//  EndDate    06/30/2009
//
//  Description     Manage all about presistance data on iphone
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//                                                                                                 version 0.0.2
//  Modify by Naruphon Sirimasrungsee
//
//  LastUpdate   07/01/2009 
//
//  List of  function ‘s updated 
//
//  List of  function ‘s added
//			1. browsDirectory
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
// Task :
//
//           - initialize step
//				KoreStore *_recStore = [[KoreStore alloc]init];
//				
//
//           - get string of path
//				filePath = [self pathOfDirectory:Application ofFile:xxx.x];
//
//			 - check file exist
//				[self checkFileExist:filePath];
//
//			 - read file to String
//				[_recStore readFileFrom:Application filePath:@"LOCAppDelegate.txt" with:&_str1];
//
//			 - sub String to array
//				NSMutableArray *_arr = [_recStore subString:_str1 by:@"\n"];
//
//			 - sum array string to string
//				NSString *_str = [_recStore sumString:_arr by:@"\n"];
//			
//
//  HowTo:
//
//
//			KoreStore *_recStore = [[KoreStore alloc]init];
//			NSString *_str1 = [[NSString alloc]initWithString:@""];
//
//			[_recStore readFileFrom:Application filePath:@"LOCAppDelegate.txt" with:&_str1];
//
//			NSMutableArray *_arr = [_recStore subString:_str1 by:@"\n"];
//			// get array of string
//------------------------------------------------------------------------------------------------------------------
#import "KoreFileManage.h"

#define ENTER @"\n"
#define DOM_KOREFILEMANAGER @"KoreFileManage"

@implementation KoreFileManage
@synthesize _errorTrace;
//																														**BASIC**
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Getting File Manager
-(id)init{
	if(self=[super init]){
		_fileManager = [NSFileManager defaultManager];
		_errorTrace = [[NSMutableArray alloc]init];
	}
	return self;
}
- (void)dealloc {
	[_fileManager release];
	[_errorTrace release];
    [super dealloc];
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Create Item
-(BOOL)_createFileAt:(NSString*) des withContent:(NSData*) data byAttribute:(NSDictionary*) attb{
	BOOL result = NO;
	if(data){//----------- file
		result = [_fileManager createFileAtPath:des contents:data attributes:attb];
	}else{//-------------- folder
		result = [_fileManager createDirectoryAtPath:des attributes:attb];
	}
	return result;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Move Item
-(BOOL)_moveFileFrom:(NSString *) src to:(NSString*) des{
	NSError *error = [[NSError alloc]initWithDomain:DOM_KOREFILEMANAGER code:FE_MOVE userInfo:nil];
	BOOL result = NO;
	result =  [_fileManager moveItemAtPath:src toPath:des error:&error];
	[_errorTrace addObject:error];
	return result;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Copy Item
-(BOOL)_copyFileFrom:(NSString *) src to:(NSString*) des{
	NSError *error = [[NSError alloc]initWithDomain:DOM_KOREFILEMANAGER code:FE_COPY userInfo:nil];
	BOOL result = NO;
	result =  [_fileManager copyItemAtPath:src toPath:des error:&error];
	[_errorTrace addObject:error];
	return result;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Remove Item
-(BOOL)_deleteFileAt:(NSString *)src{
	NSError *error = [[NSError alloc]initWithDomain:DOM_KOREFILEMANAGER code:FE_DELETE userInfo:nil];
	BOOL result = NO;
	result =  [_fileManager removeItemAtPath:src error:&error];
	[_errorTrace addObject:error];
	return result;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Discovering Files
-(NSMutableArray *)_browsDirectory:(FolderPath) path withKey:(NSString *) keySearch{
	NSString *folPath = [self _getPathOfDirectory:path ofFile:nil];//////TLog(@"------------ folPath = %@",folPath);
    NSError *error = nil;
	NSArray *arrOfFile = [_fileManager contentsOfDirectoryAtPath:folPath error:&error];
	NSMutableArray *muArr = nil;
	
	if(keySearch){//////TLog(@"--------- keySearch != nil");
		muArr = [[NSMutableArray alloc]init];
		for(int i=0;i< [arrOfFile count];i++){
			
			NSString *tmp = [arrOfFile objectAtIndex:i];
			//////TLog(@" ----------------- [%d] = %@",i,tmp);
			NSRange rTmp = [tmp rangeOfString:keySearch];
			if(rTmp.location==([tmp length]-rTmp.length)){
				[muArr addObject:tmp];
				////TLog(@" ----muArr ------------ [%d] = %@",i,tmp);
			}
			
		}
	}else{//////TLog(@"--------- keySearch = nil");
		muArr = [[NSMutableArray alloc]initWithArray:arrOfFile];
	}
	
	return muArr;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	File access Authorize
-(NSMutableString *)_getPathOfDirectory:(FolderPath) path ofFile:(NSString *) fileName{
	NSMutableString *_path = [[[NSMutableString alloc]init] autorelease];
	NSArray *dir = [NSArray arrayWithObjects:@"/Documents/",@"/Library/",@"/tmp/",nil];
	if (path==FP_HOME) {
		[_path appendString:NSHomeDirectory()];
		[_path appendString:@"/"];
		
	}else
	if(path==FP_APP){
		//_path =[[NSBundle mainBundle] bundlePath];
		[_path appendString:[[NSBundle mainBundle] bundlePath]];
		[_path appendString:@"/"];
//		if(fileName != nil){
//			[_path appendString:fileName];
//		}
	}else{//////TLog(@"-------------dir = %@",[dir objectAtIndex:pathType]);
		//_path = [NSHomeDirectory() stringByAppendingString:[dir objectAtIndex:pathType]];
		[_path appendString:NSHomeDirectory()];
		[_path appendString:[dir objectAtIndex:path]];
//		if(fileName != nil){
//			[_path appendString:fileName];
//		}
	}	
	if(fileName != nil){
		[_path appendString:fileName];
	}
	////TLog(@" PATH IS [%@]",_path);
	return _path;
	
}
-(BOOL)_checkFileAccessPath:(NSString*) filePath withAuthorize:(Authorize) author{
	BOOL success = NO;

	if((author&AT_FILE_EXIST)!=0){//////TLog(@"----- AT_FILE_EXIST");
		success = [_fileManager fileExistsAtPath:filePath];
		if(!success){
            TLog(@"FAILED AT_FILE_EXIST [%@]",filePath); 
		}
	}
	if((author&AT_FILE_READ)!=0){//////TLog(@"----- AT_FILE_READ");
		success = [_fileManager isReadableFileAtPath:filePath];
		if(!success){
			////TLog(@"FAILED AT_FILE_READ [%@]",filePath); 
		}
	}
	if((author&AT_FILE_WRITE)!=0){//////TLog(@"----- AT_FILE_WRITE");
		success = [_fileManager isWritableFileAtPath:filePath];
		if(!success){
			////TLog(@"FAILED AT_FILE_WRITE [%@]",filePath); 
		}
	}
	if((author&AT_FILE_EXE)!=0){//////TLog(@"----- AT_FILE_EXE");
		success = [_fileManager isExecutableFileAtPath:filePath];
		if(!success){
			////TLog(@"FAILED AT_FILE_EXE [%@]",filePath); 
		}
	}
	if((author&AT_FILE_DEL)!=0){//////TLog(@"----- AT_FILE_DEL");
		success = [_fileManager isDeletableFileAtPath:filePath];
		if(!success){
			////TLog(@"FAILED AT_FILE_DEL [%@]",filePath); 
		}
	}////TLog(@" OK %d",author);
	return	success;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	Managing the Current Directory



//																														**MIX**
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-(BOOL)readFile:(NSString *) name From:(FolderPath) path byData:(NSData**) rec{
	BOOL result = NO;
	NSString *filePath = [self _getPathOfDirectory:path ofFile:name];

	if([self _checkFileAccessPath:filePath withAuthorize:AT_FILE_EXIST]){
		*rec = [[NSData alloc]initWithContentsOfFile:filePath];
		result = YES;
	}else{//////TLog(@"-------------result = %@",result);
		
	}
	return result;
}
-(BOOL)readFile:(NSString *) name From:(FolderPath) path byString:(NSString**) rec{
	BOOL result = NO;
	NSData *data = nil;
	if([self readFile:name From:path byData:&data]){
		*rec = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		result = YES;
	}else{//////TLog(@"-------------result = %@",result);
		
	}
	return result;
}

-(BOOL)readFile:(NSString *) name From:(FolderPath) path byImage:(UIImage**) rec{
	BOOL result = NO;
		NSData *data = nil;
	if([self readFile:name From:path byData:&data]){
		*rec = [[UIImage alloc]initWithData:data];
		result = YES;
	}else{//////TLog(@"-------------result = %@",result);
		
	}
	return result;
}
-(NSData*)readDataFrom:(NSString *) name{
	NSData *rec = [[NSData alloc]initWithContentsOfFile:name];//[_fileManager contentsAtPath:name];//
	return rec;
}
-(NSString*)readStringFrom:(NSString *) name{
	NSData *data = [self readDataFrom:name];
	NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	return result;
}
-(UIImage*)readImageFrom:(NSString *) name{
	NSData *data = [self readDataFrom:name];
	UIImage *result = [[UIImage alloc]initWithData:data];
	return result;
}
-(BOOL)moveFile:(NSString *) name From:(FolderPath) src To:(FolderPath) des{
	BOOL result = NO;
	NSString *sorce = [self _getPathOfDirectory:src ofFile:name];
	NSString *destination = [self _getPathOfDirectory:des ofFile:name];
	if([self _checkFileAccessPath:sorce withAuthorize:AT_FILE_EXIST]&&[self _checkFileAccessPath:sorce withAuthorize:AT_FILE_EXIST]){
		[self _moveFileFrom:sorce to:destination];
		result = YES;
	}else{
		
	}
	return result;
}
-(BOOL)copyFile:(NSString *) name From:(FolderPath) src To:(FolderPath) des{
	BOOL result = NO;
	NSString *sorce = [self _getPathOfDirectory:src ofFile:name];
	NSString *destination = [self _getPathOfDirectory:des ofFile:name];
	if([self _checkFileAccessPath:sorce withAuthorize:AT_FILE_EXIST]&&[self _checkFileAccessPath:sorce withAuthorize:AT_FILE_EXIST]){
		[self _copyFileFrom:sorce to:destination];
		result = YES;
	}else{
		
	}
	return result;
}

@end

