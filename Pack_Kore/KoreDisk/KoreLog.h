//
//  KoreLog.h
//  reMake_Disk
//
//  Created by Naruphon Sirimasrungsee on 1/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KoreFileManage.h"
#define KFILE @"Klog.txt"
typedef enum {
	LM_NONE = 0,
	LM_CONSOLE = 0x01,
	LM_LOG = 0x02,
	LM_VIEW = 0x04,
	LM_UNVIEW = 0x08,
}LogMode;
@interface KoreLog : NSObject {
	

	UIView *_superView;
	KoreLog *_self;
//	LogMode _mode;
//	FILE *_fileLog;
}
-(id)initWithMode:(LogMode)mode;
-(void)setMode:(LogMode) mode;
-(void)setSuperView:(UIView*) superView;
-(void)resetLog;



void  KLog(char *str);
//void  KLog(char *str, int _int);
@end
