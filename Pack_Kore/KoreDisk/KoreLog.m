//
//  KoreLog.m
//  reMake_Disk
//
//  Created by Naruphon Sirimasrungsee on 1/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "KoreLog.h"

LogMode _mode = LM_CONSOLE;
FILE *_fileLog = NULL;
NSMutableArray *_forView = nil;
NSMutableString *_log = nil;
UITextView *_txtView = nil;
char _sentense[100];
int _count = 0;



@implementation KoreLog
-(id)initWithMode:(LogMode)mode{
	if(_self==nil){
		if(self = [super init]){
			_count = 0;
			_fileLog = NULL;
			_forView = [[NSMutableArray alloc]init];
			_log = [[NSMutableString alloc]initWithString:@"::::::: KLog :::::::\n"];
			_txtView  = [[UITextView alloc]init];
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			[self setMode:mode];
			_self = self;
		}
	}
	return _self;
}
-(void)dealloc{
	//[_self release];
	if(_fileLog!=NULL){
		fclose(_fileLog);
	}
	_self = nil;
	[super dealloc];
}
-(void)setMode:(LogMode) mode{
	_mode = mode;
	if((_mode&LM_CONSOLE)!=0){
		
	}
	if((_mode&LM_LOG)!=0){
		if(_fileLog==NULL){TLog(@"----------- _fileLog==NULL");
			KoreFileManage *_fm = [[KoreFileManage alloc]init];
			NSString *path = [_fm _getPathOfDirectory:FP_TMP ofFile:KFILE];
			_fileLog=fopen([path cStringUsingEncoding:NSUTF8StringEncoding],"w");
			const char* enter = "\n";
			strcat(_sentense, [_log cStringUsingEncoding:NSUTF8StringEncoding]);
			strcat(_sentense, enter);
			char *newSen = _sentense;
			fprintf(_fileLog,newSen);
		}
	}
	if((_mode&LM_VIEW)!=0){
		[_txtView setText:_log];
		if((_mode&LM_UNVIEW)==0){
			[_superView addSubview:_txtView];
		}else{
			[_txtView removeFromSuperview];
		}
	}
}
-(void)setSuperView:(UIView*) superView{
	_superView = superView;
	[_txtView setFrame:[_superView frame]];
	
	_mode |= LM_VIEW;
	[self setMode:_mode];
}
-(void)resetLog{
	
}
void  KLog(char * str){++_count;

	if((_mode&LM_CONSOLE)!=0){
		
	}
	if((_mode&LM_LOG)!=0&&_fileLog!=NULL){//TLog(@"----------- KLog LM_LOG");
		
//		_sentense[0] ='\0';
//		const char* enter = "\n";
//		const char* blank = " ";
//		const char* colon = ":";
		char buffer[200];
		char *ptBuff = buffer;

		sprintf(buffer, " %d : %s \n",_count,str);
//		strcat(_sentense, blank);
//		strcat(_sentense,ptBuff);
//		strcat(_sentense, colon);
//		strcat(_sentense, blank);
//		strcat(_sentense, str);
//		strcat(_sentense, enter);//TLog(@"- - - - %s",_sentense);
//		char *newSen = _sentense;
		fprintf(_fileLog,ptBuff);
	}
	if((_mode&LM_VIEW)!=0){
		[_log appendFormat:@" %d: %s\n",_count,str];//[_txtView setNeedsDisplay];[_superView setNeedsDisplay];
		[_txtView setText:_log];
			
			//NSString *log = [[NSString alloc]initWithCString:str];//NSUTF8StringEncoding
//			[_forView addObject:log];
			//TLog(@"    _log = %@",_log);
		
		
	}
	
}
//void overloaded KLog(char *str, int _int){
//	if((_mode&LM_CONSOLE)!=0){
//		
//	}
//	if((_mode&LM_LOG)!=0&&_fileLog!=NULL){//TLog(@"----------- KLog LM_LOG");
//		fprintf(_fileLog,str,_int);
//	}
//	if((_mode&LM_VIEW)!=0){
//		
//	}
//}
@end
