//
//  KoreMoviePlayer.h
//  TrueLifeService_400
//
//  Created by Naruphon Sirimasrungsee on 7/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
//	Support iOS 4.0

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
@protocol KoreMoviePlayerDelegate;

@interface KoreMoviePlayer : UIViewController {
	id _appDelegate;
	UIImage *_imageSplash;
	
	UIView *_mvpLoading;
	UIActivityIndicatorView *_indicatorView;
	UILabel *_lbServiceLoading;
	//UIViewController *_delegate;
//	NSTimeInterval _timeOut;
	BOOL _streamDone;
	BOOL _timeOuuuut;
	id <KoreMoviePlayerDelegate> _delegate;
	
	MPMoviePlayerController *_player;
	MPMoviePlayerViewController *_playerViewController;
	
	UIWebView *_webView;
	UIButton *_btOverlay;
	UIView	*_vOverlay;
	BOOL _fullScreen;
		NSInteger _fullST;
			NSInteger _fullEnd;
	NSInteger _playCount;
	NSInteger _KMP_ENABLE;
	NSString *_url;
}
@property (nonatomic,retain)id <KoreMoviePlayerDelegate> _delegate;
@property (nonatomic,retain)	MPMoviePlayerController *_player;
@property (nonatomic,retain)	MPMoviePlayerViewController *_playerViewController;
@property (nonatomic,retain)IBOutlet 	UIView	*_vOverlay;
@property (nonatomic,retain)IBOutlet 	UIButton *_btOverlay;

-(void)_init;
-(void)_setDelegate:(UIViewController*) delegate;
-(void)_setSplashPreload:(UIImage*) img;
-(void)_playStreamWithURL:(NSString*) url andSuperView:(UIView*) superView;
-(void)_timeOut;
-(void)_reset;
-(void)_acOverLay;
-(void)_offPlayer;
@end

@protocol KoreMoviePlayerDelegate<NSObject>

-(void)_playerTimeOut;
-(void)_touchOverLay;
-(void)_movieEnd;
-(void)_moviePreloadFin;
-(void)_movieEnterFullScreen;
-(void)_movieExitFullScreen;


@end

