//
//  KoreMoviePlayer.m
//  TrueLifeService_400
//
//  Created by Naruphon Sirimasrungsee on 7/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "KoreMoviePlayer.h"


@implementation KoreMoviePlayer
@synthesize _player;
@synthesize _playerViewController;
@synthesize _vOverlay;
@synthesize _btOverlay;
-(void)awakeFromNib{
	 [self _init];
}
-(id)init{
	if (self=[super init]) {
		[self _init];
	}
	return self;
}
-(void)_reset{
	_streamDone = NO;
	_timeOuuuut = NO;
}
-(void)_init{
	_url = nil;
	_KMP_ENABLE = -1;
	_fullScreen = NO;
	_fullST = 0;
	_fullEnd = 0;
	_playCount = 0;
	_appDelegate = nil;
	_imageSplash = nil;
	
	_mvpLoading = nil;
	_indicatorView  = nil;
	_lbServiceLoading = nil;
	_delegate = nil;
	_player = nil;
	//_playerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:url]];
	_webView = nil;
	
//	_btOverlay = [UIButton buttonWithType:UIButtonTypeCustom];
//	[_btOverlay setFrame:CGRectMake(0, 0, 320, 50)];
//	[_btOverlay addTarget:self action:@selector(_acOverLay) forControlEvents:UIControlEventTouchUpInside];
	[self _reset];
}
- (void)dealloc {
	
	[_imageSplash release];
	
	[_mvpLoading release];
	[_indicatorView release];
	[_lbServiceLoading release];

    [super dealloc];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {TLog(@"interfaceOrientation = %d",interfaceOrientation);
    // Overriden to allow any orientation.
//	if (interfaceOrientation==3) {
//	[_player view].transform = CGAffineTransformMakeRotation((90*M_PI)/180);
//	[_player view].transform = CGAffineTransformTranslate(	[_player view].transform,80,80);
//
//	//[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationPortrait];
//	[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
//	//[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationLandscapeRight];
//	}
    return NO;
}
#pragma mark -
#pragma mark Function

-(void)_setDelegate:(UIViewController*) delegate;{
	_delegate = delegate;
}
-(void)_setSplashPreload:(UIImage*) img{
	_imageSplash = img;
}
-(void)_timeOut{
	if (!_streamDone) {_timeOuuuut = YES;
		////TLog(@"____________________ _streamFailed");
		[(UIViewController*)_delegate dismissMoviePlayerViewControllerAnimated];
		[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
		//[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationPortrait];
//		[[_appDelegate window] sendSubviewToBack:_mvpLoading];
//		[[_appDelegate window] sendSubviewToBack:_indicatorView];
		[_mvpLoading removeFromSuperview];
		[_indicatorView removeFromSuperview];
//		MyAlertWithMessage(@"kTimeOutMsg");
		[_delegate _playerTimeOut];
	}
	
}

-(void)_acOverLay{
	[_delegate _touchOverLay];
}
//-(void)_playStreamWithURL:(NSString*) url andSuperView:(UIView*) superView{
//	if (!_webView) {
//		_webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,69, 320, 200)];
//	}
//	_appDelegate = _UIApplicationDelegate;
////NSString *html = @"http://widget3.truelife.com/truelifes/streaming/testq.html";
//	NSString *html = @"http://widget.truelife.com/test/testWebM.html";
//	[_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:html]]];
////	[[_appDelegate window] addSubview:_webView];
//	[superView addSubview:_webView];
//}
-(void)_playStreamWithURL:(NSString*) url andSuperView:(UIView*) superView{////TLog(@"_playStreamWithURL = %@",url);
	_KMP_ENABLE=0;
	++_KMP_ENABLE;
		[self _reset];
	
	if (!_player) 
	{
		_player = [[MPMoviePlayerController alloc] init];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_movieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification object:_player];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_movieNowPlayingCallback:) name:MPMoviePlayerNowPlayingMovieDidChangeNotification object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_movieDuration:) name:MPMovieDurationAvailableNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_movieTypeAvai:) name:MPMovieMediaTypesAvailableNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_movieNatureSize:) name:MPMovieNaturalSizeAvailableNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_moviePreDidFin:) name:MPMoviePlayerContentPreloadDidFinishNotification object:_player];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_movieEnterFull:) name:MPMoviePlayerDidEnterFullscreenNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_movieExitFull:) name:MPMoviePlayerDidExitFullscreenNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_movieLoadDidChange:) name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_moviePlaybackDidChange:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
		
		//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_touchCheck) name:@"touchEvent" object:nil];
	}
//	else {
//		[_player stop];
//	}
	
	_url = [url copy];
//_player = [[MPMoviePlayerController alloc] init];
	[_player setContentURL:[NSURL URLWithString:_url]];
	//_player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:url]];
//	[_player setFullscreen:YES animated:YES];
	_player.controlStyle = MPMovieControlStyleFullscreen;//MPMovieControlStyleEmbedded;//MPMovieControlStyleEmbedded;//MPMovieControlStyleFullscreen;
//	_player.scalingMode = MPMovieScalingModeFill;//MPMovieScalingModeFill //MPMovieScalingModeAspectFit //MPMovieScalingModeAspectFill
//	_appDelegate = _UIApplicationDelegate;
#ifdef iOS4
//#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 30200	
//	 _playerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:url]];
//	[_playerViewController shouldAutorotateToInterfaceOrientation:YES];
	//MPMoviePlayerPlaybackDidFinishReasonUserInfoKey MPMoviePlayerPlaybackDidFinishNotification
	
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification object:_player];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieNowPlayingCallback:) name:MPMoviePlayerNowPlayingMovieDidChangeNotification object:nil];
//	
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieDuration:) name:MPMovieDurationAvailableNotification object:nil];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieTypeAvai:) name:MPMovieMediaTypesAvailableNotification object:nil];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieNatureSize:) name:MPMovieNaturalSizeAvailableNotification object:nil];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePreDidFin:) name:MPMoviePlayerContentPreloadDidFinishNotification object:_player];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieEnterFull:) name:MPMoviePlayerDidEnterFullscreenNotification object:nil];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieExitFull:) name:MPMoviePlayerDidExitFullscreenNotification object:nil];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieLoadDidChange:) name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlaybackDidChange:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
//	

	
	
//	[_delegate presentMoviePlayerViewControllerAnimated:playerViewController];
	
	//	MyAlertWithActivityIndicatorShow();
	//_______________________________________//
//	_indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//	[_indicatorView setCenter:CGPointMake(320/2, 520/2)];
//	[_indicatorView startAnimating];
//	[_lbServiceLoading setCenter:CGPointMake(202, 480/2)];
//	_mvpLoading = [[UIImageView alloc]initWithImage:_imageSplash];
//	//[self.view addSubview:imgView];
//	[[_appDelegate window] addSubview:_mvpLoading];
//	[[_appDelegate window] addSubview:_indicatorView];
	//[[app window] addSubview:_lbServiceLoading];
	//_______________________________________//
//	MPMoviePlayerController *player = [playerViewController moviePlayer];
//	
//	//1	
//	player.controlStyle = MPMovieControlStyleEmbedded;//MPMovieControlStyleFullscreen;//MPMovieControlStyleEmbedded;
//	
//	player.view.frame = CGRectMake(0, 0, 200	,200);			// Max Embed CGRectMake(0, 0, 300	,290);
//	[player view].center = CGPointMake(160, 169);//self.view.center;
//	
////	[[player view] setHidden:YES];
//	//2	
//	//	player.controlStyle = MPMovieControlStyleFullscreen;
//	//	player.scalingMode = MPMovieScalingModeFill;//MPMovieScalingModeFill //MPMovieScalingModeAspectFit //MPMovieScalingModeAspectFill
//	
//	[player play];
	
//	_player = [_playerViewController moviePlayer];
	
	//1	
//	_player.controlStyle = MPMovieControlStyleEmbedded;//MPMovieControlStyleFullscreen;//MPMovieControlStyleEmbedded;
//	
//	_player.view.frame = CGRectMake(0, 0, 200	,200);			// Max Embed CGRectMake(0, 0, 300	,290);
//	[_player view].center = CGPointMake(160, 169);//self.view.center;
//	
//	[[_player view] setHidden:YES];
	//2	
	//	player.controlStyle = MPMovieControlStyleFullscreen;
	//	player.scalingMode = MPMovieScalingModeFill;//MPMovieScalingModeFill //MPMovieScalingModeAspectFit //MPMovieScalingModeAspectFill
	
	[[_player view] setCenter:CGPointMake(160, 240)];
	_player.view.frame = CGRectMake(0, 89, 320	,200);	
//		_player.view.frame = CGRectMake(0, 0, 320	,480);
	//superView
	NSArray	*subView = [[_appDelegate window] subviews];
	if ([subView indexOfObject:[_player view]]==NSNotFound) {
		[[_appDelegate window] addSubview:[_player view]];		
	}
//	NSArray	*subView = [superView subviews];
//	if ([subView indexOfObject:[_player view]]==NSNotFound) {
//		[superView addSubview:[_player view]];		
//	}	

//		[superView addSubview:[_player view]];

	[_player play];


	
//	[NSTimer  scheduledTimerWithTimeInterval: 0.01 target: self selector: @selector(_touchCheck) userInfo: nil repeats: NO];
//	[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
	
	#endif
}
-(void)_offPlayer{TLog(@"___________________offPlayer");
	_KMP_ENABLE = -101;
	[[_player view]setHidden:YES];	
	[_player stop];
	[[_player view] removeFromSuperview];
	_url = nil;TLog(@"____________________url  = NULLL");
}
#pragma mark -
//-(void)_touchCheck{
//	if ([_player view] hi) {
//		<#statements#>
//	}
//	
//}
#pragma mark moviePlayerNotification
- (void)_movieNowPlayingCallback:(NSError*)notification
{
	if (_KMP_ENABLE<0) {
		return;
	}
	
	TLog(@"movieNowPlayingCallback");
	////TLog(@"NSError = %@",notification);
	//NSDictionary *dic = [notification userInfo];
	//	if (notification) {
	//		MyAlertWithMessage(kStreamNotFound);
	//	}else {
	//		
	//	}
//	#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 30200	
#ifdef iOS4
	//if ([[notification valueForKey:@"name"] isEqualToString:@"MPMoviePlayerNowPlayingMovieDidChangeNotification"]) {
	if(notification){
		//////TLog(@"MPMoviePlayerNowPlayingMovieDidChangeNotification");
		//[_delegate dismissMoviePlayerViewControllerAnimated];
//		[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
	}
	
//For True Movie HD	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarAnimationNone];		

#endif
	//[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarAnimationNone];
}
//- (void) movieNowPlayingCallback:(NSNotification*)notification
//{////TLog(@"================== movieNowPlayingCallback");
//	NSDictionary *dic = [notification userInfo];
//	if (dic) {
//		MyAlertWithMessage(kStreamNotFound);
//	}else {
//		
//	}
//
//	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarAnimationNone];
//}
-(void)_movieExitFullScreen:(NSNotification*)notification{TLog(@"movieExitFullScreen");
	if (_KMP_ENABLE<0) {
		return;
	}
	if (_fullScreen) {
		[_player setFullscreen:NO animated:NO];
	}
}
- (void)_movieFinishedCallback:(NSNotification *)notification{TLog(@"movieFinishedCallback");
	if (_KMP_ENABLE<-100) {TLog(@"_KMP_ENABLE<-100");
		return;
	}
	if (_url) {TLog(@"_url_url_url_url_url_url_url_url_url");
		[self _playStreamWithURL:_url andSuperView:nil];
		
		return;
	}
	
	
	_KMP_ENABLE = -1;
	TLog(@"notification = %@",notification);
	NSDictionary *userInfo = [notification valueForKey:@"userInfo"];
	NSInteger val = [[userInfo valueForKey:@"MPMoviePlayerPlaybackDidFinishReasonUserInfoKey"] intValue];
	_fullST = 0;
	_fullScreen = NO;
	
	
	 if (val==2||val==0)
	{//Done from FullScreen
		_player.controlStyle = MPMovieControlStyleEmbedded;//MPMovieControlStyleEmbedded;//MPMovieControlStyleFullscreen;
		_player.scalingMode = MPMovieScalingModeAspectFit;//MPMovieScalingModeFill //MPMovieScalingModeAspectFit //MPMovieScalingModeAspectFill
		
		
		//For True Movie HD	
		
		
		//[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationPortrait];
//		[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
		//	[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationLandscapeRight];
//		if (_fullEnd==0) {TLog(@"_______________ _fullEnd");
//			[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;	
//			++_fullEnd;
//		}
		[_player view].transform = CGAffineTransformMakeRotation((0*M_PI)/180);
		[_player view].transform = CGAffineTransformTranslate(	[_player view].transform,0,0);
		
		_player.view.frame = CGRectMake(0, 89, 320,200);	
		
		
		if (val==0) {//End Movie
			[[_player view] setHidden:YES];TLog(@"________setDelegate");
			[_delegate _movieEnd];
		}else {
					[_delegate _movieExitFullScreen];
		}

	}
	if (_fullEnd==0) {TLog(@"_______________ _fullEnd");
		[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;	
		++_fullEnd;
	}
	[[UIApplication sharedApplication] setStatusBarHidden:NO animated:NO];
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
	++_playCount;
	
	if (val==0) {//End Movie
//		[[_player view] removeFromSuperview];
	}
			// Max Embed CGRectMake(0, 0, 300	,290);
//	[_player view].center = CGPointMake(480/2, 320/2);

//	_player.view.frame = CGRectMake(0, 69, 320	,200);			// Max Embed CGRectMake(0, 0, 300	,290);
//	//	[_player view].center = CGPointMake(160, 169);	
//	[UIView beginAnimations: nil context: nil]; 
//	[UIView setAnimationDuration:0.2]; 
//	
//	
//	_player.view.frame = CGRectMake(0, 69, 50	,200);			// Max Embed CGRectMake(0, 0, 300	,290);
//	[_player view].center = CGPointMake(160, 169);
//		
//	
//	[UIView commitAnimations]; 
	

	

	
	//	MPMoviePlayerController *moviePlayer = [notification object];//[mvpVC moviePlayer];
	//		[moviePlayer stop];
	//		moviePlayer.initialPlaybackTime = -1.0;
	//	//[[mvpVC view] removeFromSuperview];
	//	[self dismissMoviePlayerViewControllerAnimated];
	//[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
	//[moviePlayer release];	
	
	//	}
	
	
	
	//[self performSelector:@selector(showAfterAction) withObject:nil afterDelay:0.5];
}

-(void)_movieDuration:(NSNotification *)notification{
	if (_KMP_ENABLE<0) {
		return;
	}
	TLog(@"movieDuration");
	
}
-(void)_movieTypeAvai:(NSNotification *)notification{
	if (_KMP_ENABLE<0) {
		return;
	}
	TLog(@"movieTypeAvai");
}
-(void)_movieNatureSize:(NSNotification *)notification{
	if (_KMP_ENABLE<0) {
		return;
	}
	TLog(@"movieNatureSize");
}
-(void)_moviePreDidFin:(NSNotification *)notification{
	if (_KMP_ENABLE<0) {
		return;
	}
	_url = nil;
	TLog(@"------------->moviePreDidFin");
	[_delegate _moviePreloadFin];
	[[_player view] setHidden:NO];
	//_player.view.frame = CGRectMake(0, 89, 50	,200);			// Max Embed CGRectMake(0, 0, 300	,290);
	_player.view.frame = CGRectMake(155, 89, 10	,200);
//	[_player view].center = CGPointMake(160, 169);
	[_player view].alpha = 0;
	[UIView beginAnimations: nil context: nil]; 
	[UIView setAnimationDuration:0.5]; 
	[_player view].alpha = 1;	
//	[_player view].center = CGPointMake(160, 169);
	_player.view.frame = CGRectMake(0, 89, 320	,200);			// Max Embed CGRectMake(0, 0, 300	,290);

	[UIView commitAnimations]; 
//	_player.view.frame = CGRectMake(0, 89, 320	,200);

//	#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 30200
#ifdef iOS4
//	if (_timeOuuuut) {
//		//return;
//	}
	
//For True Movie HD	
//	MPMoviePlayerController *moviePlayer = [notification object];
//	//	[moviePlayer setFullscreen:YES];
//	[moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
//For True Movie HD	
	
//1. Portrait	
//	moviePlayer.view.frame = CGRectMake(0, 0, 320,480);			// Max Embed CGRectMake(0, 0, 300	,290);
//	[moviePlayer view].center = CGPointMake(320/2, 480/2);//self.view.center;
//	[[moviePlayer view] setHidden:NO];
//	[_mvpLoading removeFromSuperview];
//	[_indicatorView removeFromSuperview];
//2. Landscape	
	
//For True Movie HD	
//	moviePlayer.view.frame = CGRectMake(0, 0, 480,320);			// Max Embed CGRectMake(0, 0, 300	,290);
//	[moviePlayer view].center = CGPointMake(480/2, 320/2);//self.view.center;
//For True Movie HD
	
	
//	CGFloat rad = 90;
//	CGFloat rotX =  160;
//	CGFloat rotY = 240;
//	CGFloat oriX =  240;//[_doll center].x;
//	CGFloat oriY = 160;
//	CGFloat delX = oriX-rotX;
//	CGFloat delY = oriY-rotY;
//	
//	CGFloat a = cos(rad); //////TLog(@" a = %f",a);
//	CGFloat b = sin(rad);
//	CGFloat c = -sin(rad);
//	CGFloat d = cos(rad);//////TLog(@" d = %f",d);
//	
//	CGFloat tx = (a*delX)+(c*delY);
//	CGFloat ty = (b*delX)+(d*delY);
//	
//	ty -= delY;//[_doll center].y;
//	tx -= delX;
//	
//	////TLog(@" tx = %f",tx);
//	////TLog(@" ty = %f",ty);
//	
//	
//	[moviePlayer view].transform = CGAffineTransformMake(a,b,c,d,tx,ty);	
//	[moviePlayer view].transform = CGAffineTransformMakeRotation((-90*M_PI)/180);
//	[moviePlayer view].transform = CGAffineTransformTranslate(	[moviePlayer view].transform,-80,-80);
	
//For True Movie HD	
//	[moviePlayer view].transform = CGAffineTransformMakeRotation((90*M_PI)/180);
//	[moviePlayer view].transform = CGAffineTransformTranslate(	[moviePlayer view].transform,80,80);
//	[[moviePlayer view] setHidden:NO];
//	[_mvpLoading removeFromSuperview];
//	[_indicatorView removeFromSuperview];
//	//[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationPortrait];
//	[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
//	//[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationLandscapeRight];
//For True Movie HD	
	_streamDone = YES;
#endif
}
-(void)_videoRotation{TLog(@"_videoRotation");
	[_player setFullscreen:NO animated:NO];
	_player.controlStyle = MPMovieControlStyleFullscreen;//MPMovieControlStyleEmbedded;//MPMovieControlStyleFullscreen;
	_player.scalingMode = MPMovieScalingModeAspectFit;//MPMovieScalingModeFill //MPMovieScalingModeAspectFit //MPMovieScalingModeAspectFill
	
	_player.view.frame = CGRectMake(0, 0, 480,320);			// Max Embed CGRectMake(0, 0, 300	,290);
	[_player view].center = CGPointMake(480/2, 320/2);//self.view.center;
	//For True Movie HD	
	[[_player view] setNeedsDisplay];
	
	//[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationPortrait];
	[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
	//	[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationLandscapeRight];
	
	[_player view].transform = CGAffineTransformMakeRotation((90*M_PI)/180);
	
	[_player view].transform = CGAffineTransformTranslate(	[_player view].transform,80,80);
	[[_player view] setNeedsDisplay];
}
-(void)_movieEnterFull:(NSNotification *)notification{

	if (_KMP_ENABLE<0) {
		
		if (_KMP_ENABLE<-100) {
			return;
		}else {
			_KMP_ENABLE = 0;
		}

		
	}
	TLog(@"movieEnterFull");
	_url = nil;
//	_fullScreen = YES;
//	if (_fullScreen) {
//		[_player setFullscreen:NO animated:NO];
//	}
	_fullEnd=0;
	if (_fullST==0) {
		[_delegate _movieEnterFullScreen];
		[self performSelector:@selector(_videoRotation)];
		++_fullST;
	}
	
	
//	NSOperationQueue *queue = [NSOperationQueue new];
//	
//	NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
//																			selector:@selector(_videoRotation)
//																			  object:nil];
//	
//	[queue addOperation:operation];
//	[operation release];
//	[_player setOrientation:UIDeviceOrientationLandscapeLeft animated:NO]; 
//	_player.controlStyle = MPMovieControlStyleFullscreen;

//	[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
//	[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationLandscapeRight];
	
//	[_player setFullscreen:NO animated:NO];
//	_player.controlStyle = MPMovieControlStyleFullscreen;//MPMovieControlStyleEmbedded;//MPMovieControlStyleFullscreen;
//	_player.scalingMode = MPMovieScalingModeAspectFit;//MPMovieScalingModeFill //MPMovieScalingModeAspectFit //MPMovieScalingModeAspectFill
//	
//	_player.view.frame = CGRectMake(0, 0, 480,320);			// Max Embed CGRectMake(0, 0, 300	,290);
//	[_player view].center = CGPointMake(480/2, 320/2);//self.view.center;
////For True Movie HD	
//	
//
//	//[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationPortrait];
//	[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
////	[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationLandscapeRight];
//	
//	[_player view].transform = CGAffineTransformMakeRotation((90*M_PI)/180);
//
//	[_player view].transform = CGAffineTransformTranslate(	[_player view].transform,80,80);
//	[[_player view] setNeedsDisplay];
//For True Movie HD	
}
-(void)_movieExitFull:(NSNotification *)notification{
	if (_KMP_ENABLE<0) {
		return;
	}
	TLog(@"movieExitFull");

//		[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationPortrait];
//	[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
}
-(void)_movieLoadDidChange:(NSNotification *)notification{
	if (_KMP_ENABLE<0) {
		return;
	}
	TLog(@"movieLoadDidChange");
	TLog(@"notification = %@",notification);
//	if (_fullST==0) {
////		[_player setFullscreen:NO animated:NO];
//		[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
//	}
}
-(void)_moviePlaybackDidChange:(NSNotification *)notification{
	if (_KMP_ENABLE<0) {
		return;
	}
	TLog(@"moviePlaybackDidChange");
//		[UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
//	[self dismissMoviePlayerViewControllerAnimated];
}


@end
