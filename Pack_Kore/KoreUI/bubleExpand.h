//
//  bubleExpand.h
//  TrueLifeChat
//
//  Created by Naruphon Sirimasrungsee on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface bubleExpand : UIView {
	NSMutableArray *imgArray;
}
-(void)_init;
-(void)setImageNameSuite:(NSArray*) array;
-(void)setHeight:(CGFloat) h andWidth:(CGFloat) w;

@end
