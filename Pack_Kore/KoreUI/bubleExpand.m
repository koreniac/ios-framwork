//
//  bubleExpand.m
//  TrueLifeChat
//
//  Created by Naruphon Sirimasrungsee on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "bubleExpand.h"


@implementation bubleExpand

-(void)awakeFromNib{
	[self _init];
}
- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
		[self _init];
    }
    return self;
}
-(void)_init{
	imgArray = nil;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
    [super dealloc];
}


-(void)setImageNameSuite:(NSArray*) array{
	if (!imgArray) {
		imgArray = [[NSMutableArray alloc] init];
	}
	if ([array count]==3) {
		for (NSInteger i=0; i<[array count]; i++) {
			UIImageView *img = [[UIImageView alloc] initWithImage: [UIImage imageNamed:[array objectAtIndex:i]]];
			[imgArray addObject:img];
		}

		
	}
	
}
-(void)setHeight:(CGFloat) h andWidth:(CGFloat) w{
	UIImage *img = [(UIImageView*)[imgArray objectAtIndex:0] image];
	[self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, [img size].width, h)];
//	[self setFrame:CGRectMake(20, 73, 210, 150)];
	CGFloat lastHeight = [(UIImageView*)[imgArray objectAtIndex:2] image].size.height;
	CGFloat _x = 0;
	CGFloat _y = 0;
	CGFloat _w = 0;
	CGFloat _h = 0;
	for (NSInteger i=0; i<[imgArray count]; i++) {
		UIImageView *imgView = (UIImageView*)[imgArray objectAtIndex:i];
		[self addSubview:imgView];
//		_x = 0;
		 _y = (i==0?0:(i==1?_h:(h- imgView.image.size.height)));
		 _w = imgView.image.size.width;
		_h = (i==1?(h-([[imgArray objectAtIndex:0] image].size.height+[[imgArray objectAtIndex:2] image].size.height)):imgView.image.size.height);
		
		
		[imgView setFrame:CGRectMake(_x ,_y, _w, _h)];
	}
	
}

@end
