//
//  KoreXtoO.h
//  NetConn_1021
//
//  Created by Naruphon Sirimasrungsee on 12/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
// Not support this format 
//	http://10.1.10.202/product/sso/rest/?uid=2DB79A92-3288-5DD3-AA75-6CE42703BF96&token=HtyXQ7pVqL1S6h1OaCL4RSzws42YrQaqmNKGs3Kw1yf2yUvrXjUNVT0U5mrTgA9aGRrtxjFHEphW3Qx3k3JimQ,,&method=login&email=snarlman1115QWER@hotmail.com&password=GpPxNoiD8Bdt-L3I-1fwog,,
//<response>
//	<code>200</code>
//	<description>Success</description>
//	<method>login</method>
//	<data>
//		<userid/>
//		<access_token/>
//		<expires/>
//	</data>
//</response>
#import <Foundation/Foundation.h>
//#import "NSMutableArray+Cat.h"
#import "JSON.h"

typedef enum {
	XO_NONE_MODE = 0,	//CANNOT DETERMINE TYPE
	XO_MODE_XML = 1,
	XO_MODE_JSON = 2,
	
}XOMODE;	//xml Object type
typedef enum {
	XO_CANNT_DET = 0,	//CANNOT DETERMINE TYPE
	XO_DIC = 1,
	XO_ARR = 2,
	
}XOTYPE;	//xml Object type
typedef enum{
	EXCASE_NONE = 0x00,
	EXCASE_NOTVALUE_INDIC = 0x01,
}EXCEP_CASE;
@protocol KoreXtoODelegate;
@interface KoreXtoO : NSObject<NSXMLParserDelegate> {
	//________________________________________________new ParseEngine
		NSMutableString*_currentString;
	NSXMLParser *_xmlParse;
	
	NSMutableArray *_stackNode;
	NSMutableArray *_stackNodeCount;
	
	NSMutableArray *_arrVal;
	NSMutableArray *_arrKey;
	NSMutableString *_startElement;
	EXCEP_CASE _case;							//CASE_NOTVALUE_INDIC = 0X01
	
	XOMODE _mode;
	NSString *_identifier;
	id <KoreXtoODelegate> _delegate;
	
	NSXMLParser *_cdataParse;
	NSMutableString* resultString;
	NSMutableData *_cdata;
	NSInteger _countDicSame;
	id parseObj;
	NSDictionary *entityMap;
	NSArray *entityArray;
}
@property (nonatomic,retain)	id parseObj;
+(NSMutableDictionary*)parseJsonFromPath:(NSString*)path;

-(XOTYPE)_determineType:(NSInteger**)startIndex;

-(KoreXtoO*)initParseEngine:(id<KoreXtoODelegate>)theDelegate withMode:(XOMODE) mode andIden:(NSString *)identifier;
-(id)parseData:(NSData*) data;
-(NSString*)cutCDATAoff:(NSString*)string;

-(NSMutableDictionary*)parseJSONdata:(NSData*)data;

@end

@protocol KoreXtoODelegate<NSObject>

-(void)parseDidFinish:(NSMutableDictionary*) dic withIden:(NSString*)iden;
-(void)parseDidFailed:(NSError *)parseError  withIden:(NSString*)iden;

@end
