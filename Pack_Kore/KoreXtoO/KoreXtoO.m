//
//  KoreXtoO.m
//  NetConn_1021
//
//  Created by Naruphon Sirimasrungsee on 12/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "KoreXtoO.h"


@implementation KoreXtoO
@synthesize parseObj;
- (void)dealloc
{
    if (_currentString) {
        [_currentString release];
        _currentString = nil;
    }
    if (_xmlParse) {
        [_xmlParse release];
        _xmlParse = nil;
    }
	if (_stackNode) {
        [_stackNode release];
        _stackNode = nil;
    }

    if (_stackNodeCount) {
        [_stackNodeCount release];
        _stackNodeCount = nil;
    }
	if (_arrVal) {
        [_arrVal release];
        _arrVal = nil;
    }

    if (_arrKey) {
        [_arrKey release];
        _arrKey = nil;
    }
    if (_startElement) {
        [_startElement release];
        _startElement = nil;
    }
	if (_identifier) {
        [_identifier release];
        _identifier = nil;
    }
	if (_cdataParse) {
        [_cdataParse release];
        _cdataParse = nil;
    }
    if (resultString) {
        [resultString release];
        resultString = nil;
    }

    if (_cdata) {
        [_cdata release];
        _cdata = nil;
    }
    if (parseObj) {
        [parseObj release];
        parseObj = nil;
    }
//    if (entityMap) {
//        [entityMap release];
//        entityMap = nil;
//    }
    if (entityArray) {
        [entityArray release];
        entityArray = nil;
    }

//	[_currentString release];
//	[_identifier release];
//	[_stackNode release];
//	[_stackNodeCount release];
//	
//	[_arrVal release];
//	[_arrKey release];
//	[_xmlParse release];
//    [parseObj release];
//    parseObj = nil;
//    _currentString = nil;
	[super dealloc];
}
// Determine V1.0.1
-(XOTYPE)_determineType:(NSInteger**)startIndex{
	NSMutableString	*nodeCount = [_stackNodeCount _pop];
	NSInteger nCount = [nodeCount intValue];
	[nodeCount release];
	//TLog(@">> nCount = %d",nCount);
	if(nCount==1){
		NSInteger compare = ([_arrKey count]-nCount);	
		*startIndex = compare;
		//*startIndex  = 1;
		return XO_ARR;//XO_DIC;
	}else if(nCount>1){
		NSInteger compare = ([_arrKey count]-nCount);	
		*startIndex = compare;
		NSString *stStr = nil;				//TLog(@"stStr = %@",stStr);
		NSString *ndStr = nil;
//		@try {
			stStr = [_arrKey objectAtIndex:compare];				//TLog(@"stStr = %@",stStr);
			ndStr = [_arrKey objectAtIndex:++compare];			//TLog(@"ndStr = %@",ndStr);
//		}
//		@catch (NSException * e) {
//			return XO_DIC;
//		}
//		@finally {
//			return XO_DIC;
//		}
	
		
//		NSString *stStr = [_arrKey objectAtIndex:compare];				//TLog(@"stStr = %@",stStr);
//		NSString *ndStr = [_arrKey objectAtIndex:(compare+nCount)-1];			//TLog(@"ndStr = %@",ndStr);
		
		
		
		if ([stStr isEqualToString:ndStr]) {
			return XO_ARR;
		}else {
			return XO_DIC;
		}
		
	}
	
	
	
}
#pragma mark -
+(NSMutableDictionary*)parseJsonFromPath:(NSString*)path{
    
    NSData *data = [NSData dataWithContentsOfFile:path];
    SBJSON *json = [[SBJSON new] autorelease];
    NSError *jsonError;
    
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *_dataDic = [[NSDictionary alloc]initWithDictionary:[json objectWithString:jsonString error:&jsonError]];
//    NSError *objError;
//    NSString *objStr = [json stringWithObject:_dataDic allowScalar:YES error:&objError];
//    NSMutableDictionary *_resultDic = [[NSDictionary alloc]initWithDictionary:[json objectWithString:objStr error:&jsonError]];
//    TLog(@"**********_resultDic= %@",_resultDic);
    //    [_delegate parseDidFinish:[_dataDic retain] withIden:_identifier];
    //    [_dataDic release];
    return  _dataDic;    
}
#pragma mark Function 
-(KoreXtoO*)initParseEngine:(id<KoreXtoODelegate>)theDelegate withMode:(XOMODE) mode andIden:(NSString *)identifier{
	if (self=[super init]) {
		 entityMap = [NSDictionary dictionaryWithObjectsAndKeys: 
								   @"'", @"&lsquo;",
								   @"'", @"&rsquo;", 
								   @"-",@"&ndash;",	
								     @".",@"&bull;",
									@" ",@"&nbsp;",
										nil];

		_delegate = theDelegate;
		_mode = mode;
		_identifier = [identifier copy];
		if (_mode==XO_MODE_XML) {
			
			_stackNode = nil;
			_stackNodeCount = nil;
			
			_arrVal = nil;
			_arrKey = nil;
			
			_stackNode = [[NSMutableArray alloc] init];
			_stackNodeCount = [[NSMutableArray alloc] init];
			
			_arrVal = [[NSMutableArray alloc] init];
			_arrKey = [[NSMutableArray alloc] init];
			_startElement = [[NSMutableString alloc] initWithString:@""];		
			_currentString = [[NSMutableString alloc] init];
			_cdata = [[NSMutableData alloc] init];
			_countDicSame = 0;
		}else if(_mode==XO_MODE_JSON) {
			
		}
		
	}
	return self;
}
-(id)parseData:(NSData*) data{
	if (_mode==XO_MODE_XML) {
		NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		NSArray *mapList = [entityMap allKeys];
		for (NSInteger i=0; i<[mapList count]; i++) {
			NSString *key = [mapList objectAtIndex:i];
			NSString *val = [entityMap valueForKey:key];
			str = [str stringByReplacingOccurrencesOfString:key withString:val];
		}

		NSData *encodeData = [str dataUsingEncoding:NSUTF8StringEncoding];
		_xmlParse = [[NSXMLParser alloc] initWithData:encodeData];
		[_xmlParse setDelegate:self];
		[_xmlParse setShouldResolveExternalEntities:YES];
		[_xmlParse parse];		
	}else if(_mode==XO_MODE_JSON) {
//		SBJSON *json = [[SBJSON new] autorelease];
        SBJSON *json = [SBJSON new];
		NSError *jsonError;

		NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

        NSDictionary *jDic = [json objectWithString:jsonString error:&jsonError];

		NSDictionary *_dataDic = [[NSDictionary alloc] initWithDictionary:jDic];
        parseObj = [_dataDic retain];
//        parseObj = [[NSDictionary alloc] initWithDictionary:jDic];
		if (_delegate) {

            [_delegate parseDidFinish:parseObj withIden:_identifier];			
		}else {
					
		}

        [jsonString release];
//        _dataDic = nil;
        jsonString = nil;
        [json release];
        json = nil;
//        [jsonError release];
//        jsonError = nil;

	}
	

}
-(NSMutableDictionary*)parseJSONdata:(NSData*)data{
	SBJSON *json = [[SBJSON new] autorelease];
	NSError *jsonError;
	
	NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	
	NSMutableDictionary *_dataDic = [[NSDictionary alloc]initWithDictionary:[json objectWithString:jsonString error:&jsonError]];
//    NSError *objError;
//    NSString *objStr = [json stringWithObject:_dataDic allowScalar:YES error:&objError];
//    NSMutableDictionary *_resultDic = [[NSDictionary alloc]initWithDictionary:[json objectWithString:objStr error:&jsonError]];
//    TLog(@"**********_resultDic= %@",_resultDic);
	return _dataDic;
}
-(NSString*)cutCDATAoff:(NSString*)string{
	NSString *result = nil;
	NSRange range = [string rangeOfString:@"<![CDATA["];
	
//	TLog(@"range.length = %d",range.length);
//	TLog(@"string = %@",string);
	if (range.length>0) {
//		NSRange rangeII = [string rangeOfString:@"]]"];
//		NSRange rangeIII = {9, ([string length]-rangeII.location)};
				NSRange rangeIII = {10, ([string length]-13)};
		NSString *pureString = [string substringWithRange:rangeIII];
		TLog(@"pureString = %@",pureString);
		result = [[NSString alloc] initWithString:pureString];
	}else {
		result = [[NSString alloc] initWithString:string];
	}
	NSString	*trim = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [result release];
	return trim;

}
- (NSString*)convertEntiesInString:(NSString*)s {
    if(s == nil) {
		NSLog(@"ERROR : Parameter string is nil");
    }
    NSString* xmlStr = [NSString stringWithFormat:@"<d>%@</d>", s];
    NSData *data = [xmlStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    _cdataParse = [[NSXMLParser alloc] initWithData:data];
    [_cdataParse setDelegate:self];
    [_cdataParse parse];
    NSString* returnStr = [[NSString alloc] initWithFormat:@"%@",resultString];
	return returnStr;
}
# pragma mark -
#pragma mark NSXMLParser Parsing Callbacks
// NSXMLParser delegate method
- (NSData *)parser:(NSXMLParser *)parser resolveExternalEntityName:(NSString *)entityName systemID:(NSString *)systemID {
	TLog(@"entityName = %@",entityName);
//	TLog(@"systemID = %@",systemID);
//	NSData *data = [[entityMap objectForKey:entityName] dataUsingEncoding: NSUTF8StringEncoding];
//	TLog(@"data = %@",data);
//	NSAttributedString *entityString = [[NSAttributedString alloc] initWithString:entityName attributes:entityMap];

//	NSAttributedString *entityString = [[NSAttributedString alloc] initWithString:[entityMap valueForKey:entityName]];
//	NSLog(@"resolved entity name: %@", [entityString string]);
//	
//	return [[entityString string] dataUsingEncoding:NSUTF8StringEncoding];

	NSString *string = [[NSString alloc] initWithString:@" "]; 
	NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding]; 
	return data;
	//    return data;
}
// Constants for the XML element names that will be considered during the parse. 
// Declaring these as static constants reduces the number of objects created during the run
// and is less prone to programmer error.



- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *) qualifiedName attributes:(NSDictionary *)attributeDict {
//	TLog(@"******************** didStartElement + =%@",elementName);
//	if ([_cdataParse isEqual:parser]) {
//		return;
//	}
//	if ([elementName isEqualToString:@"BR"]) {
//		[_currentString appendString:@"\r\n"];
//		return;
//	}
	[_currentString setString:@""];
	[_startElement setString:@""];
	_case = EXCASE_NONE;
//	TLog(@"[_stackNode count] = %d < %d [_stackNodeCount count]",[_stackNode count],[_stackNodeCount count]);
	if ([_stackNode count] < [_stackNodeCount count]) {
		NSMutableString *strCount = [_stackNodeCount _peek];
		NSInteger count = [strCount intValue];
		++count;
		[strCount setString:@""];
		[strCount appendFormat:@"%d",count];
//		TLog(@"..<%@>.........++....strCount = %@",[_stackNode _peek],strCount);
		//		[_stackNodeCount _push:[[NSString alloc] initWithFormat:@"%d",count]];
		//		_case = EXCASE_NOTVALUE_INDIC;
	}else {
		[_stackNodeCount _push:[[NSMutableString alloc] initWithFormat:@"%d",1]];
	}
	
	
	[_stackNode _push:[[NSString alloc] initWithString:elementName]];
	[_startElement appendString:elementName];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
//	TLog(@"******************** didEndElement - =%@",elementName);
	//	if ([_currentString length]>0) {
//	if ([_cdataParse isEqual:parser]) {
//		resultString = [[NSString alloc] initWithData:_cdata encoding:NSUTF8StringEncoding];
//		TLog(@"resultString = %@",resultString);
//		return;
//	}
	
	if([_startElement isEqualToString:elementName]){
		NSString *popKey = [_stackNode _pop];
		if([_currentString length]>0){
			[_arrKey addObject:popKey];
//			[_arrVal addObject:[[NSString alloc] initWithString:_currentString]];
//			[_arrVal addObject:[self convertEntiesInString:_currentString]];
			[_arrVal addObject:[self cutCDATAoff:_currentString]];
		}
		//else if([_stackNode count] == [_stackNodeCount count]){
		else if(_case == EXCASE_NOTVALUE_INDIC){
			
		}
		else{
			NSMutableString *strCount = [_stackNodeCount _peek];
			NSInteger count = [strCount intValue];
			--count;
			[strCount setString:@""];
			[strCount appendFormat:@"%d",count];
			TLog(@"..<%@>......--........strCount = %@",[_stackNode _peek],strCount);
			
			TLog(@">>>>>>>>>>>>>>>  Node Not Value <<<<<<<<<<<<<<");
		}
		
	}else if(![_startElement isEqualToString:elementName]) {
		if([_arrKey count]!=[_arrVal count]){
			TLog(@">>>>>>>>>>>>>>>  Engine Was Wrong II <<<<<<<<<<<<<<");
			return;
		}
		
		NSInteger *sIndex = 0;
		//TLog(@"_determineType ");
		XOTYPE type = [self _determineType:&sIndex];
		//TLog(@"__________type = %d",type);
		if (type==XO_CANNT_DET) {
			TLog(@">>>>>>>>>>>>>>>  Engine Was Wrong III <<<<<<<<<<<<<<");
			return;
		}else if(type==XO_DIC){
			NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
			NSInteger arrKeyCount = [_arrKey count];
			for (NSInteger i = sIndex; i<arrKeyCount; i++) {
				NSString *keyDic = [_arrKey objectAtIndex:i];
				NSString *keyVal = [_arrVal objectAtIndex:i];
				NSString *newKey = nil;
				id check = [dic valueForKey:keyDic];
				if (check||([check isKindOfClass:[NSString class]]&&![check isEqualToString:@"(null)"])) {
					newKey = [[NSString alloc] initWithFormat:@"%@>%d",keyDic,(_countDicSame+1)];
				}else {
					newKey = keyDic;
				}

				[dic setValue:keyVal forKey:newKey];
			}
//			TLog(@"__ dic = %@",dic);
//			TLog(@"__ [_arrKey count] = %d",[_arrKey count]);
			
			for (NSInteger j = sIndex; j<arrKeyCount; j++) {
//				TLog(@" XO_DIC rem = %d",j);
				//				[_arrVal removeObjectAtIndex:j];
				//				[_arrKey removeObjectAtIndex:j];
				[_arrVal removeLastObject];
				[_arrKey removeLastObject];
			}
//			TLog(@"__ _arrVal = %@",_arrVal);
//			TLog(@"__ _arrKey = %@",_arrKey);
			NSString *popKey = [_stackNode _pop];
			if([popKey isEqualToString:elementName]){
				[_arrKey addObject:popKey];
				[_arrVal addObject:dic];
				_countDicSame = 0;
			}else {
				TLog(@">>>>>>>>>>>>>>>  Engine Was Wrong IV <<<<<<<<<<<<<<");
			}
			
		}else if(type==XO_ARR){
			NSMutableArray *arr = [[NSMutableArray alloc] init];
			NSInteger arrKeyCount = [_arrKey count];
			for (NSInteger i = sIndex; i<arrKeyCount; i++) {
				
				[arr addObject:[_arrVal objectAtIndex:i]];
			}
			for (NSInteger i = sIndex; i<arrKeyCount; i++) {
//				TLog(@" XO_ARR rem = %d",i);
				//				[_arrVal removeObjectAtIndex:i];
				//				[_arrKey removeObjectAtIndex:i];
				[_arrVal removeLastObject];
				[_arrKey removeLastObject];
			}
			NSString *popKey = [_stackNode _pop];
			if([popKey isEqualToString:elementName]){
				[_arrKey addObject:popKey];
				[_arrVal addObject:arr];
			}else {
				TLog(@">>>>>>>>>>>>>>>  Engine Was Wrong V <<<<<<<<<<<<<<");
			}
		}
		
	}
	
	[_currentString setString:@""];
//	TLog(@"_stackNode  = %@",_stackNode);
//	TLog(@"_stackNodeCount  = %@",_stackNodeCount);
//	TLog(@"_arrKey = %@",_arrKey);
//	TLog(@"_arrVal = %@",_arrVal);
}

//*************************************************************************************************************
//- (void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock{//TLog(@"CDATABlock = %@",CDATABlock);
////	if ([_cdataParse isEqual:parser]) {
////		resultString = [[NSMutableString alloc] init];
////		[resultString appendString:string];
////		return;
////	}
//	[_cdata appendData:CDATABlock];
////	NSString *string = [[NSString alloc] initWithData:CDATABlock];
////	[_currentString appendString:string];
//}
//*************************************************************************************************************
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {//TLog(@"string = %@",string);
	
//	if ([string isEqualToString:@"<"]||[string isEqualToString:@"![CDATA[ "]||[string isEqualToString:@">"]) {
//		return;
//	}
//	NSRange range = [string rangeOfString:@"]]"];
//	if (range.length>0) {
//		NSString *cutOff = [string substringToIndex:range.location];
//		[_currentString appendString:cutOff];
//	}else {
//		[_currentString appendString:string];
//	}
	[_currentString appendString:string];

	
	//////////TLog(@"_currentString = %@",_currentString);
//	TLog(@"foundCharacters = %@",string);
}

/*
 A production application should include robust error handling as part of its parsing implementation.
 The specifics of how errors are handled depends on the application.
 */
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
	TLog(@"parseError = %@",parseError);
//	if ([_cdataParse isEqual:parser]) {
//		return;
//	}
    // Handle errors as appropriate for your application.
	[_delegate parseDidFailed:parseError withIden:_identifier];
}
- (void)parserDidEndDocument:(NSXMLParser *)parser{TLog(@"KoreFeedEngine___parserDidEndDocument");
//	if ([_cdataParse isEqual:parser]) {
//		return;
//	}
//	_resultDic = [_arrVal objectAtIndex:0];
//	//////////TLog(@"~~~~~~~~~~ 1 _arrOfItem = %d",[_arrOfItem count]);
//	_feedComplete = YES;
//	[self _parserDidFinish];
//	[_delegate feedDidFinish:self];
	TLog(@"_arrVal = %@",_arrVal);
	parseObj = nil;
	if ([_arrVal count]>0) {
		parseObj = [[_arrVal objectAtIndex:0] retain];		
	}

	[_delegate parseDidFinish:parseObj withIden:_identifier];
	
	
	
	//////////TLog(@"~~~~~~~~~~ 2 ");	
	
}

@end
