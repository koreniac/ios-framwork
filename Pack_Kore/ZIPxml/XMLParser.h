//
//  XMLParser.h
//  ChargingFeeds
//
//  Created by tu on 8/18/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface XMLParser : NSObject {
@private
	NSMutableDictionary *root;
	NSMutableData *receiveData;
	NSMutableDictionary *itemsDictionary;
	NSMutableArray *items;
	
	NSString *requestURL;
	NSString *currentElement;
	NSString *itemElement;
	BOOL success;
	BOOL loading;
	BOOL parsed;
	
	BOOL inItem;
	
	NSInteger callID;
	id parentDelegate;
	SEL onCompleteCallback;
}

@property (nonatomic, retain) NSString *itemElement;
- (id) init:(NSInteger) identifier;
- (id) init:(NSInteger) identifier withElementName:(NSString *) element;
- (void) parse:(NSString *) url withDelegate:(id) sender onComplete:(SEL) callback;
- (void) parseWithString:(NSString *)url withDelegate:(id) sender onComplete:(SEL) callback ;
- (void) parseResponse;

- (NSDictionary *) getRoot;
- (NSArray *) getItems;
- (NSInteger)getId;

- (BOOL) isSuccessful;
- (BOOL) isLoading;
- (BOOL) isParsed;


@end
