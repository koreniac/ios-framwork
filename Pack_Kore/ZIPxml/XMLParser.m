//
//  XMLParser.m
//  ChargingFeeds
//
//  Created by tu on 8/18/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "XMLParser.h"


@implementation XMLParser

@synthesize itemElement;

- (id) init:(NSInteger) identifier withElementName:(NSString *) element
{
	self = [super init];
	if (self)
	{
		callID = identifier;
		root = [[NSMutableDictionary alloc] init];
		items = [[NSMutableArray alloc] init];
		success = NO;
		loading = NO;
		parsed = NO;
		currentElement = nil;
		self.itemElement = element;
		//NSLog(@"Start");
	}
	return self;
}

- (id) init:(NSInteger) identifier
{
	self = [super init];
	if (self)
	{
		callID = identifier;
		root = [[NSMutableDictionary alloc] init];
		items = [[NSMutableArray alloc] init];
		success = NO;
		loading = NO;
		parsed = NO;
		currentElement = nil;
		self.itemElement = @"item";
	}
	return self;
}

- (void) parseWithString:(NSString *)url withDelegate:(id) sender onComplete:(SEL) callback 
{
	parentDelegate = sender;
	onCompleteCallback = callback;
	requestURL = [url retain];
	loading = YES;
	
	NSMutableData *data;
	data = [NSMutableData dataWithBytes:[url cStringUsingEncoding:NSUTF8StringEncoding] length:[url length]];
	receiveData = [data retain];
	[self parseResponse];
	
}
- (void) parse:(NSString *)url withDelegate:(id) sender onComplete:(SEL) callback 
{
	parentDelegate = sender;
	onCompleteCallback = callback;
	requestURL = [url retain];
	loading = YES;
	
	NSLog(@"Get");
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
	[request setURL:[[NSURL alloc] initWithString:requestURL]];
	[request setHTTPMethod:@"GET"];
	[request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
	
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
	[request release];
	
	receiveData = [[NSMutableData data] retain];
	
}

- (BOOL) isSuccessful
{
	return success;
}

- (BOOL) isLoading
{
	return loading;
}

- (BOOL) isParsed
{
	return parsed;
}

- (NSInteger) getId
{
	return callID;
}

- (NSDictionary *) getRoot
{
	return root;
}

- (NSArray *) getItems
{
	return items;
}


- (void) connection:(NSURLConnection *) connection didFailWithError:(NSError *) error
{
	//NSLog(@"Data Error");
	success = NO;
	loading = NO;
	if ([parentDelegate respondsToSelector:onCompleteCallback])
	{
		[parentDelegate performSelector:onCompleteCallback withObject:self];
	}
}

- (void) connection:(NSURLConnection *) connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *) challenge
{
	success = NO;
}

- (void) connection:(NSURLConnection *) connection didReceiveData:(NSData *) data 
{
	//NSLog(@"%@", receiveData);
	[receiveData appendData:data];
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
		NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
		if ([httpResponse statusCode] >= 400 & [httpResponse statusCode] <= 599)
		{
			success = NO;
		} else if ([httpResponse statusCode] >= 100 && [httpResponse statusCode] <= 299)
		{
			success = YES;
		} else {
			NSLog(@"Unknown");
		}

	}
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection 
{
	NSString *dataString = [[[NSString alloc] initWithData:receiveData encoding:NSASCIIStringEncoding] autorelease];
	NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]; 
	
	//NSLog(@"%@", receiveData);
	//NSString *dataString = [[[NSString alloc] initWithData:receiveData encoding:NSUTF8StringEncoding] autorelease];
	
	
	//if ([dataString length] > 0)
	if ([data length] > 0)
	{
		[self parseResponse];
	} else {
		root = nil;
		items = nil;
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Can not convert data" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
		
		if ([parentDelegate respondsToSelector:onCompleteCallback])
		{
			[parentDelegate performSelector:onCompleteCallback withObject:self];
		}
		
	}

	loading = NO;
}

- (void) parseResponse
{
	//NSString *dataString = [[[NSString alloc] initWithData:receiveData encoding:NSASCIIStringEncoding] autorelease];
	//NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]; 

	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:receiveData];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse];
}

- (void) parser:(NSXMLParser *) parser parseErrorOccurred:(NSError *) parseError
{
	parsed = NO;
	loading = NO;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *) elementName namespaceURI:(NSString *) namespaceURI
  qualifiedName:(NSString *) qName attributes:(NSDictionary *) attributeDict
{
	NSString *element = [elementName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	//NSLog(@"Start:%@", element);
	currentElement = element;
	if ([[currentElement lowercaseString] isEqual:self.itemElement])
	{
		inItem = YES;
		itemsDictionary = [[NSMutableDictionary alloc] init];
	}  else if ([[currentElement lowercaseString] isEqual:@"channel1"])
	{
		//NSLog(@"start: %@", [attributeDict objectForKey:@"name"]);
	}
}

- (void) parser:(NSXMLParser *) parser didEndElement:(NSString *) elementName namespaceURI:(NSString *) namespaceURI
qualifiedName:(NSString *) qName
{
	NSString *element = [elementName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	//NSLog(@"end: %@", element);
	if ([[element lowercaseString] isEqual:self.itemElement])
	{
		inItem = NO;
		[items addObject:itemsDictionary];
	}  else if ([[element lowercaseString] isEqual:@"channel1"])
	{
		//NSLog(@"Hello");
	}
}

- (void) parser:(NSXMLParser *) parser foundCharacters:(NSString *) string 
{
	NSString *stringValue = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	NSString *element = [currentElement stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	//NSLog(@"ch: %@", stringValue);
	if (stringValue == nil || [stringValue isEqual:@""])
	{
		return;
	}
	
	if (element != nil && [element length] > 0)
	{
		if (inItem)
		{
			if ([itemsDictionary objectForKey:element] != nil)
			{
				if ([element isEqual:@"category"])
				{
					[itemsDictionary setObject:[NSString stringWithFormat:@"%@, %@",[itemsDictionary objectForKey:element], stringValue] forKey:element];
				} else {
					
					[itemsDictionary setObject:[NSString stringWithFormat:@"%@%@",[itemsDictionary objectForKey:element], stringValue] forKey:element];
				} 
				
			} else 
			{
				[itemsDictionary setObject:stringValue forKey:element];
			} //itemsDictionary
		} //inItem
		else
		{
			if ([root objectForKey:element] != nil) 
			{
				if ([element isEqual:@"category"])
				{
					[root setObject:[NSString stringWithFormat:@"%@, %@",[root objectForKey:element], stringValue] forKey:element];
				} else
				{
					[root setObject:[NSString stringWithFormat:@"%@%@",[root objectForKey:element], stringValue] forKey:element];
				}
			} else
			{
				[root setObject:stringValue forKey:element];
			}
		} //inItem
	}
}

- (void) parserDidStartDocument:(NSXMLParser *) parser
{
	
}

- (void) parserDidEndDocument:(NSXMLParser *) parser
{
	parsed = YES;
	loading = NO;
	
	if ([parentDelegate respondsToSelector:onCompleteCallback])
	{
		[parentDelegate performSelector:onCompleteCallback withObject:self];
	}
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void) dealloc
{
	[itemElement release];
	 itemElement = nil;
	 
	[root release];
	root = nil;
	
	[itemsDictionary release];
	itemsDictionary = nil;
	
	[items release];
	items = nil;
	
	
	[requestURL release];
	requestURL = nil;
	
	[receiveData release];
	receiveData = nil;
	
	[super dealloc];
}

@end
